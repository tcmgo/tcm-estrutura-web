package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoRelatorioEnumeracao implements NumberEnumeracaoPersistente {
	BALANCETE_EXECUTIVO(0), BALANCETE_AUTARQUIA(1), BALANCO(2), BALANCETE_LEGISLATIVO(3), PPA_LOA(4), PPA(5), LOA(6);

	int valor;

	TipoRelatorioEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Balancete Executivo";
		case 1:
			return "Balancete Autarquia";
		case 2:
			return "Balan�o";
		case 3:
			return "Balancete Legislativo";
		case 4:
			return "PPA_LOA";
		case 5:
			return "PPA";
		case 6:
			return "LOA";
		default:
			return "";
		}
	}
}
