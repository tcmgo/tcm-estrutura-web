package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoPessoaEnumeracao implements StringEnumeracaoPersistente{
	FISICA("F"),JURIDICA_CGC("J"),JURIDICA_CPF("O");
	String valor;
	
	TipoPessoaEnumeracao(String valor){
		this.valor = valor;
	}
	
	public String getValorOrdinal(){
		return this.valor;
	}

	public EnumeracaoPersistente valor(){
		if(valor.equals("F")){
			return TipoPessoaEnumeracao.FISICA;
		} else if(valor.equals("J")){
			return TipoPessoaEnumeracao.JURIDICA_CGC;
		} else if(valor.equals("O")){
			return TipoPessoaEnumeracao.JURIDICA_CPF;
		} 
		return null;
	}
	
	@Override
	public String toString(){
		if(valor.equals("F")){
			return "F�sica";
		} else if(valor.equals("J")){
			return "Juridica CGC";
		} else if(valor.equals("O")){
			return "Juridica CPF";
		} 
		return "";
	}
	
}

