package br.gov.go.tcm.estrutura.relatorio;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import br.gov.go.tcm.estrutura.relatorio.identificador.IdentificadoresRelatorio;
import br.gov.go.tcm.estrutura.util.diretorio.AjudanteDiretorio;

public class FabricaRelatorio implements IdentificadoresRelatorio {

	private boolean identificadoresCarregados = false;

	private Map<String, String> relatorios;

	private String diretorioArquivosEntradaRelatorio;

	private String diretorioRelativoArquivosSaidaRelatorio;

	public String getDiretorioRelativoArquivosSaidaRelatorio() {
		return diretorioRelativoArquivosSaidaRelatorio;
	}

	public void setDiretorioRelativoArquivosSaidaRelatorio(String diretorioArquivosSaidaRelatorio) {
		this.diretorioRelativoArquivosSaidaRelatorio = diretorioArquivosSaidaRelatorio;
	}

	public void setRelatorios(Map<String, String> relatorios) {
		this.relatorios = relatorios;
	}

	public String getDiretorioArquivosEntradaRelatorio() {
		return diretorioArquivosEntradaRelatorio;
	}

	public void setDiretorioArquivosEntradaRelatorio(String diretorioArquivosRelatorio) {
		this.diretorioArquivosEntradaRelatorio = diretorioArquivosRelatorio;
	}

	public Map<String, String> getRelatorios() {
		if (relatorios == null)
			relatorios = new HashMap<String, String>();
		return relatorios;
	}

	public File obterRelatorio(String identificador) {
		if (!identificadoresCarregados) {

			File diretorio = new File(AjudanteDiretorio.obterCaminhoAbsoluto(diretorioArquivosEntradaRelatorio));
			if (diretorio.isDirectory()) {
				String[] file = diretorio.list();
				for (int i = 0; i < file.length; ++i) {
					getRelatorios().put(file[i].replaceFirst(".jasper", ""), file[i]);
				}
			}

			identificadoresCarregados = true;
		}

		return new File(AjudanteDiretorio.obterCaminhoAbsoluto(diretorioArquivosEntradaRelatorio) + "/"
				+ getRelatorios().get(identificador));
	}
}
