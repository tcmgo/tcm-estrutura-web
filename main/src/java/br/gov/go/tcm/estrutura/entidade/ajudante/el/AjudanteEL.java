package br.gov.go.tcm.estrutura.entidade.ajudante.el;


public class AjudanteEL {

	public static java.lang.Integer tamanhoColecao(java.util.Collection<?> c){
		return (c == null) ? 0 : c.size();
	}
	
	public static java.lang.Boolean contem(java.util.Collection<?> c, java.lang.String conteudo ){
		boolean retorno = false;
		
		if( c != null )
			retorno = c.contains( conteudo );
			
		return retorno;
	}
}