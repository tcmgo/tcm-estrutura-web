package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoInstitucionalEnumeracao implements NumberEnumeracaoPersistente {
	
	HISTORICO(1), ORGANIZACAO(2), COMPOSICAO(3), COMPETENCIA(4);
	
	int valor;
	
	TipoInstitucionalEnumeracao(int valor) {
		this.valor = valor;
	}
	
	public Number getValorOrdinal(){
		return this.valor;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoInstitucionalEnumeracao.HISTORICO;
		case 2:
			return TipoInstitucionalEnumeracao.ORGANIZACAO;
		case 3:
			return TipoInstitucionalEnumeracao.COMPOSICAO;
		case 4:
			return TipoInstitucionalEnumeracao.COMPETENCIA;
		
		}
		return null;
	}
	
	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Histórico";
		case 2:
			return "Organização";
		case 3:
			return "Composição";
		case 4:
			return "Competência";			
		default:
			return "";
		}
		
	}

}
