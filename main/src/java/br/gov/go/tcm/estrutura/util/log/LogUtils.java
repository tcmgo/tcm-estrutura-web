package br.gov.go.tcm.estrutura.util.log;

import br.gov.go.tcm.estrutura.entidade.base.EntidadeComposta;

public class LogUtils {

	/**
	 * M�todo respons�vel por gerar dados para inspe��o de logs.
	 * 
	 * @param objetos
	 *            Array de objetos para descri��o do log
	 * @return String impressa no log
	 */
	public static String gerarDadosLog(Object... objetos) {
		StringBuffer erro = new StringBuffer();
		for (Object objeto : objetos) {
			if (objeto == null) {
				erro.append("Entidade nula").append(System.getProperty("line.separator"));
				continue;
			}
			erro.append(objeto.getClass().getName())
					.append(": ")
					.append(objeto instanceof EntidadeComposta ? ((EntidadeComposta) objeto).toString() : objeto
							.toString()).append(System.getProperty("line.separator"));
		}
		erro.append("Erro: ");

		return erro.toString();
	}
}
