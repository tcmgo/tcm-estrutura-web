package br.gov.go.tcm.estrutura.controlador.base;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller("gerenciadorDePaginas")
@Scope("session")
public class GerenciadorDePaginas extends ControladorPadrao {

	private static final long serialVersionUID = -4014584284284170921L;

	private String pagina;

	public String getPagina() {

		if (pagina == null || pagina.equals("")) {
			pagina = "../principal.xhtml";
		}

		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}
}