package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum LicitacaoStatusEnumeracao implements NumberEnumeracaoPersistente{
	
	ABERTO(1), FINALIZADO(2);

	private final int valor;

	LicitacaoStatusEnumeracao(int valor) {
		this.valor = valor;
	}
	
	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Aberto";
		case 2:
			return "Finalizado";
		default:
			return "";
		}
	}
	
}
