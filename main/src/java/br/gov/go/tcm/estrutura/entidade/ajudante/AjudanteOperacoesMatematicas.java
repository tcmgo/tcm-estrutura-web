package br.gov.go.tcm.estrutura.entidade.ajudante;

import java.util.Iterator;

import br.gov.go.tcm.estrutura.entidade.base.Entidade;

public class AjudanteOperacoesMatematicas {

	@SuppressWarnings("unchecked")
	public static java.lang.Double soma(java.util.Collection<?> c,
			java.lang.String propriedade) {
		AjudanteReflexao ajudanteReflexao = new AjudanteReflexao();
		Double soma = 0.0;

		Iterator<Entidade> i = (Iterator<Entidade>) c.iterator();
		while (i.hasNext()) {
			Object e = i.next();
			Double valor = (Double) ajudanteReflexao.obterValorPropriedade(e,
					propriedade);
			soma = soma + valor;
		}

		return soma;
	}
	
	@SuppressWarnings({ "rawtypes" })
	public static java.lang.Integer somaInteiros(java.util.Collection<?> c,
			java.lang.String propriedade) {
		AjudanteReflexao ajudanteReflexao = new AjudanteReflexao();
		Integer soma = 0;

//		Iterator<Entidade> i = (Iterator<Entidade>) c.iterator();
		Iterator i = c.iterator();
		while (i.hasNext()) {
			Object e = i.next();
			Integer valor = (Integer) ajudanteReflexao.obterValorPropriedade(e,
					propriedade);
			soma = soma + valor;
		}

		return soma;
	}

	public static java.lang.Integer somaTamanhoColecoes(
			java.util.Collection<?> c, java.util.Collection<?> c2) {

		return (c == null || c2 == null) ? 0 : (c.size() + c2.size());

	}
	
	@SuppressWarnings("unchecked")
	public static java.lang.Double soma(java.util.Collection<?> c,
			java.lang.String propriedade, java.util.Collection<?> c2,
			java.lang.String propriedade2) {
		
		AjudanteReflexao ajudanteReflexao = new AjudanteReflexao();
		Double soma1 		= 0.0;

		Iterator<Entidade> i = (Iterator<Entidade>) c.iterator();
		while (i.hasNext()) {
			Entidade e = i.next();
			Double valor = (Double) ajudanteReflexao.obterValorPropriedade(e,
					propriedade);
			soma1 = soma1 + valor;
		}
		
		Double soma2 		= 0.0;
		
		i = (Iterator<Entidade>) c2.iterator();
		while (i.hasNext()) {
			Entidade e = i.next();
			Double valor = (Double) ajudanteReflexao.obterValorPropriedade(e,
					propriedade2);
			soma2 = soma2 + valor;
		}
		
		return soma1 + soma2;
	}
	
	@SuppressWarnings("unchecked")
	public static java.lang.Double subtrair(java.util.Collection<?> c,
			java.lang.String propriedade, java.util.Collection<?> c2,
			java.lang.String propriedade2) {
		
		AjudanteReflexao ajudanteReflexao = new AjudanteReflexao();
		Double soma1 = 0.0;

		Iterator<Entidade> i = (Iterator<Entidade>) c.iterator();
		while (i.hasNext()) {
			Entidade e = i.next();
			Double valor = (Double) ajudanteReflexao.obterValorPropriedade(e,
					propriedade);
			soma1 = soma1 + valor;
		}
		
		Double soma2 = 0.0;

		i = (Iterator<Entidade>) c2.iterator();
		while (i.hasNext()) {
			Entidade e = i.next();
			Double valor = (Double) ajudanteReflexao.obterValorPropriedade(e,
					propriedade2);
			soma2 = soma2 + valor;
		}
		
		return soma1 - soma2;
	}
	
	
	@SuppressWarnings("unchecked")
	public static java.lang.Double subtrair(java.util.Collection<?> c,
			java.lang.String propriedade, java.util.Collection<?> c2,
			java.lang.String propriedade2, java.util.Collection<?> c3,
			java.lang.String propriedade3, java.util.Collection<?> c4,
			java.lang.String propriedade4) {
		
		AjudanteReflexao ajudanteReflexao = new AjudanteReflexao();
		Double soma1 = 0.0;

		Iterator<Entidade> i = (Iterator<Entidade>) c.iterator();
		while (i.hasNext()) {
			Entidade e = i.next();
			Double valor = (Double) ajudanteReflexao.obterValorPropriedade(e,
					propriedade);
			soma1 = soma1 + valor;
		}
		
		Double soma2 = 0.0;

		i = (Iterator<Entidade>) c2.iterator();
		while (i.hasNext()) {
			Entidade e = i.next();
			Double valor = (Double) ajudanteReflexao.obterValorPropriedade(e,
					propriedade2);
			soma2 = soma2 + valor;
		}
		
		
		Double soma3 = 0.0;

		i = (Iterator<Entidade>) c3.iterator();
		while (i.hasNext()) {
			Entidade e = i.next();
			Double valor = (Double) ajudanteReflexao.obterValorPropriedade(e,
					propriedade3);
			soma3 = soma3 + valor;
		}
		
		Double soma4 = 0.0;

		i = (Iterator<Entidade>) c4.iterator();
		while (i.hasNext()) {
			Entidade e = i.next();
			Double valor = (Double) ajudanteReflexao.obterValorPropriedade(e,
					propriedade4);
			soma4 = soma4 + valor;
		}
		
		return (soma1 + soma2) - soma3 - soma4;
	}
	
	public static Double divisao(Double dividendo, Double divisor){
		Double resto = dividendo/divisor;
		
		return resto;
		
	}

}
