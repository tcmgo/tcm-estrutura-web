package br.gov.go.tcm.estrutura.seguranca.filtro;

import java.io.IOException;
import java.util.List;

import javax.faces.FactoryFinder;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.jsf.FacesContextUtils;

import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoSessao;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioPerfilModulo;
import br.gov.go.tcm.estrutura.entidade.enumeracao.UsuarioEnumeracao;
import br.gov.go.tcm.estrutura.entidade.sessao.UsuarioLogadoInterno;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.ModuloDAO;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.UsuarioPerfilModuloDAO;

public class ControleSegurancaFiltroNovo implements Filter {
	
	private static final String INIT_PARAM_EXCLUIR_PAGINAS = "excluirPaginas";
	

	private FilterConfig filterConfig;

	private FacesContext facesContext = null;

	private LifecycleFactory lifecycleFactory;

	private javax.faces.context.FacesContextFactory contextFactory;

	private Lifecycle lifecycle;

	private ModuloDAO moduloDAO;

	private UsuarioPerfilModuloDAO usuarioPerfilModuloDAO;
	
	public void init(FilterConfig config) throws ServletException {
		this.filterConfig = config;
		this.contextFactory = (FacesContextFactory) FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
		this.lifecycleFactory = (LifecycleFactory) FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
		this.lifecycle = lifecycleFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);
	}

	@SuppressWarnings("static-access")
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException,
			ServletException {
		if (filterConfig == null)
			return;
		
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;

//		String recursoSolicitado = request.getRequestURI().replaceAll(request.getContextPath() + "/", "");
		String recursoSolicitado = request.getRequestURI().substring(1);
		
		/**
		 * Verifica se o InitParameter excluirPaginas est� preenchido 
		 * na declara��o do filtro no descritor web.xml do projeto, se
		 * tiver, as p�ginas l� referenciadas n�o ser�o redirecionadas
		 * para a tela de login.
		 * 
		 * @param excluirPaginas - Nome que dever ser colocado no initParameter do web.xml 
		 * @return String - Retorna a String referente a p�gina a ser exclu�da. Quando mais 
		 * de uma deve separar utilizando v�rgula(,).
		 */
		String paginasTemp = filterConfig.getInitParameter(INIT_PARAM_EXCLUIR_PAGINAS);
		String[] paginas = new String[]{};
		if (paginasTemp != null) 
			paginas = paginasTemp.split(",");
		
		boolean excluirPagina = false;
		for(int i=0; i<paginas.length; i++) {
			if (recursoSolicitado.equals(paginas[i].trim())) {
				excluirPagina = true;
			}
		}
		
		String padraoUrl = request.getContextPath();

		padraoUrl = padraoUrl.substring(1) + "/";
		
		AjudanteContextoSessao contextoSessao = AjudanteContextoSessao.obterContextoSessao(request);
		facesContext = contextFactory.getFacesContext(filterConfig.getServletContext(), request, response, lifecycle);

		moduloDAO = (ModuloDAO) FacesContextUtils.getWebApplicationContext(facesContext.getCurrentInstance()).getBean(
				"ModuloDAO");

		//String contexto = request.getContextPath().replaceAll("/", "");
		
		if (recursoSolicitado.indexOf("/a4j/") >= 0
				|| recursoSolicitado.indexOf("javax.faces.resource") >= 0) {
			chain.doFilter(req, resp);
			return;
		}

		Modulo modulo = null;
		modulo = moduloDAO.selecionarModuloDoRecursoSolicitado(padraoUrl, recursoSolicitado);
		
		if (excluirPagina) {			
			chain.doFilter(req, resp);
			
		} else if (modulo != null) {
			if (contextoSessao.getUsuarioLogado() == null) {
				//response.sendRedirect(request.getContextPath() + "/" + modulo.getLoginUrl());
				response.sendRedirect( "/" + modulo.getLoginUrl());
				return;
			}
			if (contextoSessao.getUsuarioLogado().getUsuarioPerfilModulo().getModulo().getId().equals(
					modulo.getId())) {
				chain.doFilter(req, resp);
			} else {
				UsuarioPerfilModulo usuarioPerfilModulo = null;
				if (contextoSessao.getUsuarioLogado() instanceof UsuarioLogadoInterno) {
					UsuarioLogadoInterno usuarioLogadoInterno = (UsuarioLogadoInterno) contextoSessao
							.getUsuarioLogado();

					usuarioPerfilModuloDAO = (UsuarioPerfilModuloDAO) FacesContextUtils.getWebApplicationContext(
							facesContext.getCurrentInstance()).getBean("UsuarioPerfilModuloDAO");

					List<UsuarioPerfilModulo> lstUsuarioPerfilModulo = usuarioPerfilModuloDAO
							.selecionarLstUsuarioPerfilModuloPorModuloUsuarioIdTipoUsuario(modulo,
									usuarioLogadoInterno.getUsuarioMonitor().getId(),
									UsuarioEnumeracao.USUARIO_INTERNO);
					usuarioPerfilModulo = lstUsuarioPerfilModulo != null && lstUsuarioPerfilModulo.size() > 0 ? lstUsuarioPerfilModulo
							.get(0)
							: null;
					if (usuarioPerfilModulo == null) {
						//response.sendRedirect(request.getContextPath() + "/" + modulo.getLoginUrl());
						response.sendRedirect(modulo.getLoginUrl());
						return;
					}
				}
				contextoSessao.getUsuarioLogado().setUsuarioPerfilModulo(usuarioPerfilModulo);
				chain.doFilter(req, resp);
			}
		} else {
			chain.doFilter(req, resp);
		}
	}

	public void destroy() {
	}
}
