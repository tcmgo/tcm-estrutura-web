package br.gov.go.tcm.estrutura.persistencia.base;

public class DAOExcecao extends RuntimeException {

	private static final long serialVersionUID = 478513020552980751L;
	
	public DAOExcecao(String mensagem) {
		super(mensagem);
	}

}