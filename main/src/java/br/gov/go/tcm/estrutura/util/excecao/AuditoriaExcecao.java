package br.gov.go.tcm.estrutura.util.excecao;

public class AuditoriaExcecao extends RuntimeException {

	public AuditoriaExcecao(String message) {
		super(message);
	}

	private static final long serialVersionUID = -8722927589401187377L;

}
