package br.gov.go.tcm.estrutura.controlador.base;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoFaces;
import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoRequisicao;
import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoSessao;
import br.gov.go.tcm.estrutura.container.faces.FacesMensagemNivel;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioMonitor;
import br.gov.go.tcm.estrutura.entidade.enumeracao.AnoEnumeracao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.RegraSegurancaEnumeracao;
import br.gov.go.tcm.estrutura.entidade.transiente.Arquivo;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.UsuarioMonitorDAO;
import br.gov.go.tcm.estrutura.seguranca.ajudante.AjudanteSeguranca;

@Controller("controladorPadrao")
@Scope("session")
public class ControladorPadrao implements Serializable {

	private static final long serialVersionUID = -7179809376164502496L;

	private String maioresInformacoesLog = "";
	
	@Autowired
	private UsuarioMonitorDAO usuarioMonitorDAO;

	public static final Logger log = Logger.getLogger(ControladorPadrao.class);

	protected static final String LOG_ERRO = "Erro";

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected List<SelectItem> criarOpcoesEnumeracao(
			final Class<? extends Enum> enumClass) {
		List<SelectItem> items = new ArrayList<SelectItem>();

		if (enumClass.getSimpleName().equals("AnoEnumeracao")) {
			return criarOpcoesAnoEnumeracao(null, null);
		}

		EnumSet<? extends Enum> enumSet = EnumSet.allOf(enumClass);
		for (Enum enumIn : enumSet) {
			if (enumIn.toString() != null && !enumIn.toString().equals(""))
				items.add(new SelectItem(enumIn, enumIn.toString()));
		}
		return items;
	}

	protected List<SelectItem> criarOpcoesAnoEnumeracao(AnoEnumeracao anoFinal,
			List<SelectItem> itensJaExistentes) {
		if (itensJaExistentes == null)
			itensJaExistentes = new ArrayList<SelectItem>();

		DateFormat dateFormat = new SimpleDateFormat("yyyy");
		Integer anoAtual = Integer.valueOf(dateFormat.format(new Date()));

		anoAtual++; // At� 1 ano ap�s o atual

		for (AnoEnumeracao ano : AnoEnumeracao.values()) {

			if (anoFinal != null) {

				if (ano.getValorOrdinal().intValue() <= anoAtual
						&& ano.getValorOrdinal().intValue() >= anoFinal
								.getValorOrdinal().intValue()) {
					itensJaExistentes.add(new SelectItem(ano, ano.toString()));
				}

			} else if (ano.getValorOrdinal().intValue() <= anoAtual
					&& ano.getValorOrdinal().intValue() > 0) {
				itensJaExistentes.add(new SelectItem(ano, ano.toString()));
			}

		}

		return itensJaExistentes;
	}

	protected List<SelectItem> criarOpcoesAnoEnumeracaoAteAtual(
			List<SelectItem> selectItem) {
		if (selectItem == null)
			selectItem = new ArrayList<SelectItem>();

		DateFormat dateFormat = new SimpleDateFormat("yyyy");
		Integer anoAtual = Integer.valueOf(dateFormat.format(new Date()));

		for (AnoEnumeracao ano : AnoEnumeracao.values()) {

			if (ano.getValorOrdinal().intValue() <= anoAtual
					&& ano.getValorOrdinal().intValue() > 0) {
				selectItem.add(new SelectItem(ano, ano.toString()));
			}
		}

		return selectItem;
	}

	protected List<SelectItem> criarOpcoesAnoEnumeracaoMenosUmAno(
			AnoEnumeracao anoFinal, List<SelectItem> itensJaExistentes) {
		if (itensJaExistentes == null)
			itensJaExistentes = new ArrayList<SelectItem>();

		DateFormat dateFormat = new SimpleDateFormat("yyyy");
		Integer anoAtual = Integer.valueOf(dateFormat.format(new Date()));

		anoAtual--; // 1 ano anterior

		for (AnoEnumeracao ano : AnoEnumeracao.values()) {

			if (anoFinal != null) {

				if (ano.getValorOrdinal().intValue() <= anoAtual
						&& ano.getValorOrdinal().intValue() >= anoFinal
								.getValorOrdinal().intValue()) {
					itensJaExistentes.add(new SelectItem(ano, ano.toString()));
				}

			} else if (ano.getValorOrdinal().intValue() <= anoAtual
					&& ano.getValorOrdinal().intValue() > 0) {
				itensJaExistentes.add(new SelectItem(ano, ano.toString()));
			}

		}

		return itensJaExistentes;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected List<SelectItem> criarOpcoesEnumeracao(
			Class<? extends Enum> enumClass, List<SelectItem> items) {

		if (enumClass.getSimpleName().equals("AnoEnumeracao")) {
			return criarOpcoesAnoEnumeracao(null, items);
		}

		EnumSet<? extends Enum> enumSet = EnumSet.allOf(enumClass);
		for (Enum enumIn : enumSet) {
			items.add(new SelectItem(enumIn, enumIn.toString()));
		}
		return items;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected List<SelectItem> criarOpcoesEnumeracaoAPartirItemEnumeracao(
			Class<? extends Enum> enumClass, Enum enumeracao) {

		if (enumClass.getSimpleName().equals("AnoEnumeracao")) {
			return criarOpcoesAnoEnumeracao((AnoEnumeracao) enumeracao, null);
		}

		List<SelectItem> items = new ArrayList<SelectItem>();
		EnumSet<? extends Enum> enumSet = EnumSet.allOf(enumClass);
		for (Enum enumIn : enumSet) {
			if (enumIn.compareTo(enumeracao) <= 0) {
				if (enumIn.toString() != null && !enumIn.toString().equals(""))
					items.add(new SelectItem(enumIn, enumIn.toString()));
			}
		}
		return items;
	}

	public AjudanteContextoRequisicao getContextoRequisicao() {
		return AjudanteContextoRequisicao
				.obterContextoRequisicao((ServletRequest) FacesContext
						.getCurrentInstance().getExternalContext().getRequest());
	}

	public AjudanteContextoSessao getContextoSessao() {
		return AjudanteContextoSessao
				.obterContextoSessao((ServletRequest) FacesContext
						.getCurrentInstance().getExternalContext().getRequest());
	}

	public String getNomeUsuario() {
		Long usuarioId = this.getContextoSessao().getUsuarioLogado()
				.getUsuarioPerfilModulo().getUsuarioId();
		UsuarioMonitor obterEntidadePorId = (UsuarioMonitor) usuarioMonitorDAO
				.obterEntidadePorId(UsuarioMonitor.class, usuarioId);
		return obterEntidadePorId.getNomeCompleto();
	}

	protected Boolean temPermissao(RegraSegurancaEnumeracao regraAcesso) {
		if (!AjudanteSeguranca.temPermissaoAcesso(regraAcesso)) {
			FacesContext
					.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_INFO,
									"Usu�rio n�o tem permiss�o para essa opera��o.",
									""));
			return false;
		}

		return true;
	}

	public static void addMessage(FacesMensagemNivel mensagemDeNivel,
			String mensagem) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(mensagemDeNivel.getNivel(), mensagem, ""));
	}

	public void addMessageError(String mensagem) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, ""));
	}

	public void limparComponentesFormulario() {
		FacesContext context = FacesContext.getCurrentInstance();

		UIViewRoot viewRoot = context.getViewRoot();

		List<UIComponent> iter = viewRoot.getChildren();
		resetarCampo(iter);

		context.renderResponse();
	}

	private void resetarCampo(List<UIComponent> no1) {

		for (UIComponent no : no1) {
			if (no instanceof UIInput) {
				((UIInput) no).resetValue();
			}

			if (no.getChildren().size() > 0) {
				resetarCampo(no.getChildren());
			}
		}

	}

	public List<SelectItem> criarEInstanciarListSelectItem() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(null, ""));
		return items;
	}
	
	public void downloadPDF(Arquivo arquivo) {
		try {
			ExternalContext ec = FacesContext.getCurrentInstance()
					.getExternalContext();
	
			HttpServletResponse response = (HttpServletResponse) ec
					.getResponse();
	
			response.reset();
			response.setHeader("Content-Type", "application/pdf");
			response.addHeader("Content-Disposition", "attachment; filename="
					+ System.currentTimeMillis() + "." + arquivo.getSufixo());
	
			byte[] bytes = new byte[2048];
			int byteRead = 0;
	
			ServletOutputStream outputStream;
				outputStream = response.getOutputStream();
	
			while ((byteRead = arquivo.getStream().read(bytes)) > 0) {
				outputStream.write(bytes, 0, byteRead);
			}
	
			arquivo.getStream().close();
			outputStream.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		FacesContext.getCurrentInstance().responseComplete();
	}
	
	public boolean isNewRequest() {
        final FacesContext fc = FacesContext.getCurrentInstance();
        final boolean getMethod = ((HttpServletRequest) fc.getExternalContext().getRequest()).getMethod().equals("GET");
        final boolean ajaxRequest = fc.getPartialViewContext().isAjaxRequest();
        final boolean validationFailed = fc.isValidationFailed();
        return getMethod && !ajaxRequest && !validationFailed;
    }
	
	public boolean filterIgnoreCase(Object value, Object filter, Locale locale) {
		String filterText = (filter == null) ? null : filter.toString().trim();
		if (filterText == null || filterText.equals("")) return true;

		if (value == null) return false;

		String objectText = value.toString().toUpperCase();
		filterText = filterText.toUpperCase();

		if (objectText.contains(filterText)) return true;
		else return false;
	}

	public void addMaioresInformacoesLog(String atributo, String valor) {
		StringBuilder sbInfoLog = new StringBuilder();
		
		sbInfoLog.append(atributo + ": " + valor);
		sbInfoLog.append("\n");
		
		maioresInformacoesLog = maioresInformacoesLog + sbInfoLog.toString();
	}
	
	public String getMaioresInformacoesLog() {
		return maioresInformacoesLog;
	}
	
	public String getServerName() {
		HttpServletRequest request = 
				(HttpServletRequest) AjudanteContextoFaces.getFacesContext().getExternalContext().getRequest();
		return request.getServerName();
	}
	
}
