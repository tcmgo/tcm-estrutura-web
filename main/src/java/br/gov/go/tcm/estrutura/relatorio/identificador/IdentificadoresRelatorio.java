package br.gov.go.tcm.estrutura.relatorio.identificador;

public interface IdentificadoresRelatorio {

	public static final String RELATORIO_HISTORICO_ALTERACAO_DADOS_USUARIO_EXTERNO_ANALISADOR = "relatorioHistoricoAlteracaoDadosUsuarioExternoAnalisador";

	public static final String RELATORIO_DADOS_CADASTRO_AUTORIDADE_TELEFONE_SUB_REPORT = "relatorioDadosCadastroAutoridadeTelefoneSubReport";

	public static final String RELATORIO_IMPRESSAO_ERROS_DIVERGENCIAS = "relatorioImpressaoErrosDivergencias";

	public static final String RELATORIO_CONCURSO_PUBLICO_INSCRICAO_IMPRESSAO = "relatorioConcursoPublicoInscricaoImpressao";

	public static final String RELATORIO_CONCURSO_PUBLICO_INSCRICAO_EXPORTACAO = "relatorioConcursoPublicoInscricaoExportacao";

	public static final String RELATORIO_ERROS_CRITICAS = "relatorioErrosCriticas";

	public static final String RELATORIO_REGULARIDADE = "relatorioRegularidade";

	public static final String RELATORIO_REGULARIDADE_STATUS_PROCESSO = "relatorioRegularidadeStatusProcesso";

	public static final String RELATORIO_ADIMPLENCIA = "relatorioAdimplencia";

	public static final String RELATORIO_POSICAO_PROCESSOS = "relatorioPosicaoProcessos";

	public static final String RELATORIO_PROCESSOS_TRAMITACAO = "relatorioProcessosTramitacao";

	public static final String RELATORIO_RECEITA_TOTAL = "relatorioReceitaTotal";
	
	public static final String RELATORIO_RECEITA_TOTAL_A_PARTIR_2012 = "relatorioReceitaTotalAPartirDe2012";

	public static final String RELATORIO_RECEITA_CORRENTE_LIQUIDA = "relatorioReceitaCorrenteLiquida";

	public static final String RELATORIO_RECEITA_CORRENTE_LIQUIDA_COM_RETENCOES = "relatorioReceitaCorrenteLiquidaComRetencoes";

	public static final String RELATORIO_RECEITA_CORRENTE_LIQUIDA_COM_RETENCOES_SUB_REPORT = "relatorioReceitaCorrenteLiquidaComRetencoesSubReport";
	
	public static final String RELATORIO_GASTOS_EDUCACAO = "relatorioGastosEducacao";

	public static final String RELATORIO_GASTOS_EDUCACAO_RECEITA_IMPOSTOS = "relatorioGastosEducacaoReceitaImpostos";

	public static final String RELATORIO_GASTOS_EDUCACAO_RECEITA_CONVENIOS = "relatorioGastosEducacaoReceitaConvenios";

	public static final String RELATORIO_GASTOS_EDUCACAO_RECEITA_FUNDEF = "relatorioGastosEducacaoReceitaFundef";

	public static final String RELATORIO_GASTOS_EDUCACAO_CONTAS_RETIFICADORAS = "relatorioGastosEducacaoContasRetificadoras";

	public static final String RELATORIO_GASTOS_EDUCACAO_GASTOS_CREDORES = "relatorioGastosEducacaoGastosCredor";

	public static final String RELATORIO_GASTOS_EDUCACAO_RESTOS_A_PAGAR = "relatorioGastosEducacaoRestosAPagar";

	public static final String RELATORIO_GASTOS_EDUCACAO_ANULACAO_GASTOS = "relatorioGastosEducacaoAnulacaoGastos";

	public static final String RELATORIO_GASTOS_EDUCACAO_AJUSTE_JUSTIFICATIVA = "relatorioGastosEducacaoAjusteJustificativa";

	public static final String RELATORIO_GASTOS_SAUDE = "relatorioGastosSaude";

	public static final String RELATORIO_GASTOS_SAUDE_RECEITA_IMPOSTOS = "relatorioGastosSaudeReceitaImpostos";

	public static final String RELATORIO_GASTOS_SAUDE_RECEITA_CONVENIOS = "relatorioGastosSaudeReceitaConvenios";

	public static final String RELATORIO_GASTOS_SAUDE_GASTOS_CREDORES = "relatorioGastosSaudeGastosCredor";

	public static final String RELATORIO_GASTOS_SAUDE_RESTOS_A_PAGAR = "relatorioGastosSaudeRestosAPagar";

	public static final String RELATORIO_GASTOS_SAUDE_ANULACAO_GASTOS = "relatorioGastosSaudeAnulacaoGastos";

	public static final String RELATORIO_GASTOS_SAUDE_AJUSTE_JUSTIFICATIVA = "relatorioGastosSaudeAjustaJustificativa";

	public static final String RELATORIO_GASTOS_PESSOAL = "relatorioGastosPessoal";

	public static final String RELATORIO_GASTOS_PESSOAL_ELEMENTO_DESPESA = "relatorioGastosPessoalElementoDespesa";

	public static final String RELATORIO_GASTOS_PESSOAL_GASTOS_ADMINISTRACAO_DIRETA = "relatorioGastosPessoalGastosAdministracaoDireta";

	public static final String RELATORIO_GASTOS_PESSOAL_GASTOS_ADMINISTRACAO_INDIRETA = "relatorioGastosPessoalGastosAdministracaoIndireta";

	public static final String RELATORIO_GASTOS_PESSOAL_ANULACAO_GASTOS_ADMINISTRACAO_DIRETA = "relatorioGastosPessoalAnulacaoGastosAdministracaoDireta";

	public static final String RELATORIO_GASTOS_PESSOAL_ANULACAO_GASTOS_ADMINISTRACAO_INDIRETA = "relatorioGastosPessoalAnulacaoGastosAdministracaoIndireta";

	public static final String RELATORIO_GASTOS_PESSOAL_LEGISLATIVO = "relatorioGastosPessoalLegislativo";

	public static final String RELATORIO_GASTOS_PESSOAL_LEGISLATIVO_EMPENHOS_ANULADOS = "relatorioGastosPessoalLegislativoEmpenhosAnulados";

	public static final String RELATORIO_GASTOS_PESSOAL_AJUSTE_JUSTIFICATIVA = "relatorioGastosPessoalAjustaJustificativa";

	public static final String RELATORIO_DUODECIMO = "relatorioDuodecimo";

	public static final String RELATORIO_DUODECIMO_SUB_REPORT = "relatorioDuodecimoSubReport";

	public static final String RELATORIO_CONTROLE_SUPLEMENTACAO = "relatorioControleSuplementacao";

	public static final String RELATORIO_CONTROLE_SUPLEMENTACAO_SUB_REPORT = "relatorioControleSuplementacaoSubReport";
	
	public static final String RELATORIO_CONTROLE_REALOCACAO_RECURSOS_SUB_REPORT = "relatorioControleRealocacaoRecursosSubReport";
	
	public static final String RELATORIO_CONTROLE_ALTERACAO_PPA_SUB_REPORT = "relatorioControleAlteracaoPPASubReport";
	
	public static final String RELATORIO_CONTROLE_DECRETOS_ABERTURA_SUB_REPORT = "relatorioControleDecretosAberturaSubReport";

	public static final String RELATORIO_PESQUISA_EMPENHOS = "relatorioPesquisaEmpenhos";

	public static final String RELATORIO_PESQUISA_EMPENHOS_CABECALHO_SUB_REPORT = "relatorioPesquisaEmpenhosCabecalhoSubReport";

	public static final String RELATORIO_PESQUISA_EMPENHOS_ORDENS_PAGAMENTO = "relatorioPesquisaEmpenhosOrdensPagamento";

	public static final String RELATORIO_PESQUISA_EMPENHOS_ORDENS_PAGAMENTO_SUB_REPORT = "relatorioPesquisaEmpenhosOrdensPagamentoSubReport";

	public static final String RELATORIO_PESQUISA_EMPENHOS_ORDENS_PAGAMENTO_SUB_REPORT_DADOS_BANCARIOS = "relatorioPesquisaEmpenhosOrdensPagamentoSubReportDadosBancarios";
	
	public static final String RELATORIO_PESQUISA_EMPENHOS_ORDENS_PAGAMENTO_SUB_REPORT_RETENCOES = "relatorioPesquisaEmpenhosOrdensPagamentoSubReportRetencoes";

	public static final String RELATORIO_PESQUISA_EMPENHOS_LIQUIDACAO_DESPESAS = "relatorioPesquisaEmpenhosLiquidacaoDespesa";

	public static final String RELATORIO_PESQUISA_EMPENHOS_LIQUIDACAO_DESPESAS_SUB_REPORT = "relatorioPesquisaEmpenhosLiquidacaoDespesaSubReport";

	public static final String RELATORIO_PESQUISA_EMPENHOS_NOTAS_FISCAIS = "relatorioPesquisaEmpenhosNotasFiscais";

	public static final String RELATORIO_PESQUISA_EMPENHOS_NOTAS_FISCAIS_SUB_REPORT = "relatorioPesquisaEmpenhosNotasFiscaisSubReport";

	public static final String RELATORIO_PESQUISA_EMPENHOS_ANULACOES = "relatorioPesquisaEmpenhosAnulacoes";

	public static final String RELATORIO_PESQUISA_EMPENHOS_ANULACOES_SUB_REPORT = "relatorioPesquisaEmpenhosAnulacoesSubReport";

	public static final String RELATORIO_PESQUISA_EMPENHOS_CONTRATOS = "relatorioPesquisaEmpenhosContratos";

	public static final String RELATORIO_PESQUISA_EMPENHOS_CONTRATOS_SUB_REPORT = "relatorioPesquisaEmpenhosContratosSubReport";

	public static final String RELATORIO_CALCULO_APOSENTADORIA = "relatorioCalculoAposentadoria";

	public static final String RELATORIO_CALCULO_APOSENTADORIA_SEM_LISTA_CONTRIBUICOES = "relatorioCalculoAposentadoriaSemListaContribuicoes";

	public static final String RELATORIO_ENTREGAS_ANALISADOR_PODER_EXECUTIVO = "relatorioEntregasAnalisadorPoderExecutivo";

	public static final String RECIBO_ENTREGA_ANALISADOR_BALANCO = "reciboEntregaAnalisadorBalanco";

	public static final String RECIBO_ENTREGA_ANALISADOR_REENVIO_AUTORIZADO = "reciboEntregaAnalisadorReenvioAutorizado";

	public static final String RECIBO_ENTREGA_ANALISADOR_BALANCO_REENVIO_AUTORIZADO = "reciboEntregaAnalisadorBalancoReenvioAutorizado";

	public static final String RELATORIO_ARQUIVO_BALANCO = "relatorioArquivoBalanco";

	public static final String RELATORIO_ARQUIVO_BALANCO_SUB_REPORT = "relatorioArquivoBalancoSubReport";

	public static final String RELATORIO_BALANCO_PATRIMONIAL = "relatorioBalancoPatrimonial";

	public static final String RELATORIO_BALANCO_PATRIMONIAL_SUB_REPORT = "relatorioBalancoPatrimonialSubReport";

	public static final String RELATORIO_ANALISADOR_BALANCO = "relatorioAnalisadorBalanco";

	public static final String RELATORIO_BALANCO_FINANCEIRO_ANEXO_15 = "relatorioBalancoFinanceiroAnexo15";

	public static final String RELATORIO_MUTACAO_PATRIMONIAL = "relatorioMutacaoPatrimonial";

	public static final String RELATORIO_EXECUCAO_ORCAMENTARIA = "relatorioExecucaoOrcamentaria";

	public static final String RECIBO_ENTREGA_ENVIO_DADOS_BALANCETE = "reciboEntregaEnvioDadosBalancete";

	public static final String RELATORIO_ENTREGAS_ANALISADOR_PODER_LEGISLATIVO = "relatorioEntregasAnalisadorPoderLegislativo";

	public static final String RELATORIO_ENTREGAS_ANALISADOR_AUTARQUIA = "relatorioEntregasAnalisadorAutarquia";

	public static final String RELATORIO_ENTREGAS_ANALISADOR_BALANCO = "relatorioEntregasAnalisadorBalanco";

	public static final String RELATORIO_ENTREGAS_ANALISADOR_PPA_LOA = "relatorioEntregasAnalisadorPPA_LOA";
	
	public static final String RELATORIO_ENTREGAS_ANALISADOR_PPA = "relatorioEntregasAnalisadorPPA";
	
	public static final String RELATORIO_ENTREGAS_ANALISADOR_LOA = "relatorioEntregasAnalisadorLOA";

	public static final String RECIBO_ENTREGA_ANALISADOR_PPA_LOA = "reciboEntregaAnalisadorPPA_LOA";

	public static final String RECIBO_ENTREGA_ANALISADOR_PPA_LOA_REENVIO_AUTORIZADO = "reciboEntregaAnalisadorPPA_LOA_ReenvioAutorizado";

	public static final String RELATORIO_IMPRESSAO_ERROS = "relatorioImpressaoErros";

	public static final String RELATORIO_DADOS_CADASTRO_AUTORIDADE_SIMPLIFICADO = "relatorioDadosCadastroAutoridadeSimplificado";

	public static final String RELATORIO_DADOS_CADASTRO_AUTORIDADE_SIMPLIFICADO_ATIVO_SUB_REPORT = "relatorioDadosCadastroAutoridadeSimplificadoAtivoSubReport";

	public static final String RELATORIO_DADOS_CADASTRO_AUTORIDADE_SIMPLIFICADO_HISTORICO_SUB_REPORT = "relatorioDadosCadastroAutoridadeSimplificadoHistoricoSubReport";

	public static final String RELATORIO_DADOS_CADASTRO_AUTORIDADE_DETALHADO = "relatorioDadosCadastroAutoridadeDetalhado";

	public static final String RELATORIO_DADOS_CADASTRO_AUTORIDADE_DETALHADO_ATIVO_SUB_REPORT = "relatorioDadosCadastroAutoridadeDetalhadoAtivoSubReport";

	public static final String RELATORIO_DADOS_CADASTRO_AUTORIDADE_DETALHADO_HISTORICO_SUB_REPORT = "relatorioDadosCadastroAutoridadeDetalhadoHistoricoSubReport";

	public static final String RECIBO_DESLIGAMENTO_AUTORIDADE_CARGO = "reciboDesligamentoAutoridadeCargo";

	public static final String RECIBO_DESLIGAMENTO_AUTORIDADE_CARGO_CONTAS_ENVIADAS_SUB_REPORT = "reciboDesligamentoAutoridadeCargoContasEnviadasSubReport";

	public static final String RECIBO_DESLIGAMENTO_AUTORIDADE_CARGO_CONTAS_ISENTAS_SUB_REPORT = "reciboDesligamentoAutoridadeCargoContasIsentasSubReport";

	public static final String RECIBO_DESLIGAMENTO_AUTORIDADE_CARGO_MENSAGEM_SUB_REPORT = "reciboDesligamentoAutoridadeCargoMensagemSubReport";

	public static final String RELATORIO_PESQUISA_PROCESSOS = "relatorioPesquisaProcessos";

	public static final String RELATORIO_PESQUISA_PROCESSOS_JULGAMENTO_SUB_REPORT = "relatorioPesquisaProcessosJulgamentoSubReport";

	public static final String RELATORIO_PESQUISA_PROCESSOS_APENSADO_SUB_REPORT = "relatorioPesquisaProcessosApensadoSubReport";

	public static final String COMPROVANTE_INSCRICAO_SELECAO_ESTAGIARIOS = "comprovanteInscricaoSelecaoEstagiarios";

	public static final String COMPROVANTE_INSCRICAO_EVENTO = "comprovanteInscricaoEvento";

	public static final String COMPROVANTE_INSCRICAO_EVENTO_CRONOGRAMA_SUB_REPORT = "comprovanteInscricaoEventoCronogramaSubReport";

	public static final String RELATORIO_TIPO_RETENCAO = "relatorioTipoRetencao";

	public static final String RELATORIO_TIPO_AFASTAMENTO_PESSOAL = "relatorioTipoAfastamentoPessoal";

	public static final String RELATORIO_TIPO_DE_EXONERACAO = "relatorioTipoDeExoneracao";

	public static final String RELATORIO_AUTORIZACAO_REENVIO_ARQUIVO_ANALISADOR = "RelatorioAutorizacaoReenvioArquivoAnalisador";

	public static final String EXTRATO_GESTOR = "extratoGestor";

	public static final String RELATORIO_TIPO_ADMISSAO = "relatorioTipoAdmissao";

	public static final String RELATORIO_TIPO_DESCONTO = "relatorioTipoDesconto";

	public static final String RELATORIO_TIPO_EXONERACAO = "relatorioTipoExoneracao";
	
	public static final String RELATORIO_TIPO_REMUNERACAO = "relatorioTipoRemuneracao";
	
	public static final String RELATORIO_TIPO_MEIO_PUBLICACAO = "relatorioTipoMeiosPublicacao";
	
	public static final String RELATORIO_TIPO_UNIDADE_MEDIDA = "relatorioTipoUnidadeMedida";
	
	public static final String CERTIFICADO_ENCONTROS_REGIONAIS_2010 = "certificadoEncontrosRegionais2010";
	
	public static final String CERTIFICADO_ENCONTROS_REGIONAIS_2012 = "certificadoEncontrosRegionais2012";
	
	public static final String CERTIFICADO_SEMINARIO_TRANSICAO_2012 = "certificadoSeminarioTransicao2012";
	
	public static final String RELATORIO_TIPO_ALTERACOES_ORCAMENTARIAS = "relatorioTipoAlteracoesOrcamentarias";

	public static final String RELATORIO_TIPO_DOCUMENTOS_FISCAIS = "relatorioTipoDocumentosFiscais";
	
	public static final String RELATORIO_TIPO_SUBTIPO_VEICULO = "relatorioSubTipoVeiculo";
	
	public static final String RELATORIO_BASE_LEGAL_CONCESSAO_APOSENTADORIA = "relatorioBaseLegalConcessaoAposentadoria";
	
	public static final String CONSULTA_PROCESSO = "listagemConsultaDeProcesso";
	
	/** =============================== ESCOLA DE CONTAS ================================= **/

	public static final String RELATORIO_MUNICIPIOS_NAO_INSCRITOS = "relatorioMunicipiosNaoInscritos";
	
	public static final String RELATORIO_INSCRITOS_POR_REGIAO = "relatorioInscritosPorRegiao";
	
	
	/** ========================== SIGECOM - GEST�O DE COMPRAS =========================== **/
	
	public static final String REQUISICAO_DESPESA = "requisicaoDespesa";
	
	public static final String SOLICITACAO_ORCAMENTO = "solicitacaoOrcamento";

	/** ========================== BOLETO CAIXA =========================== **/
	
	public static final String BOLETO_RECOLHIMENTO_MULTA_CAIXA = "boletoRecolhimentoMultaCaixa";
	
	public static final String BOLETO_RECOLHIMENTO_MULTA_CAIXA_INSTRUCOES = "boleto-default_instrucoes";
}