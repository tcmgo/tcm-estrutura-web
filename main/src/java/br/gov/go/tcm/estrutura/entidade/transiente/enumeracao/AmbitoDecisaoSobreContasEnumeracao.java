package br.gov.go.tcm.estrutura.entidade.transiente.enumeracao;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteTexto;
import br.gov.go.tcm.estrutura.entidade.enumeracao.NumberEnumeracaoPersistente;

public enum AmbitoDecisaoSobreContasEnumeracao implements NumberEnumeracaoPersistente{
	
	RESOLUCAO(1),ACORDAO(2);
	
	private int valor;
	
	AmbitoDecisaoSobreContasEnumeracao(int valor){
		this.valor = valor;
	}
	
	public Number getValorOrdinal() {
		return this.valor;
	}
	
	@Override
	public String toString(){
		switch(this.valor){
			case 1:
				return "Resolu��o";
			case 2:
				return "Acord�o";
			case 3:
				return "Julgado";
			default:
				return AjudanteTexto.TEXTO_VAZIO;
		}		
	}
	
}
