package br.gov.go.tcm.estrutura.entidade.transiente;

import br.gov.go.tcm.estrutura.entidade.base.EntidadeProcedimento;

public class SequencialGED extends EntidadeProcedimento {
	private static final long serialVersionUID = 2094650515010784487L;

	String sequencial;

	public String getSequencial() {
		return sequencial;
	}

	public void setSequencial(String sequencial) {
		this.sequencial = sequencial;
	}
}