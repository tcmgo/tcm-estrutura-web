package br.gov.go.tcm.estrutura.container.faces;

import javax.servlet.ServletRequest;

public class AjudanteContextoRequisicao {

	public final static String CONTEXTO_REQUISICAO = "contextoRequisicao";

	public static AjudanteContextoRequisicao obterContextoRequisicao(ServletRequest request) {
		AjudanteContextoRequisicao contextoRequisicao = (AjudanteContextoRequisicao) request.getAttribute(CONTEXTO_REQUISICAO);
		if (contextoRequisicao == null) {
			contextoRequisicao = new AjudanteContextoRequisicao();
			request.setAttribute(CONTEXTO_REQUISICAO, contextoRequisicao);
		}
		return contextoRequisicao;
	}

//	private SessaoHibernateRequisicao sessaoHibernateRequisicao;

	private String caminhoContexto;

	private String ipOrigem;

	public String getIpOrigem() {
		return ipOrigem;
	}

	public void setIpOrigem(String ipOrigem) {
		this.ipOrigem = ipOrigem;
	}

	public String getCaminhoContexto() {
		return caminhoContexto;
	}

	public void setCaminhoContexto(String descricaoContextoRequisicao) {
		this.caminhoContexto = descricaoContextoRequisicao;
	}

//	public SessaoHibernateRequisicao getSessaoHibernateRequisicao() {
//		return sessaoHibernateRequisicao;
//	}
//
//	public void setSessaoHibernateRequisicao(SessaoHibernateRequisicao sessaoHibernateRequisicao) {
//		this.sessaoHibernateRequisicao = sessaoHibernateRequisicao;
//	}

}
