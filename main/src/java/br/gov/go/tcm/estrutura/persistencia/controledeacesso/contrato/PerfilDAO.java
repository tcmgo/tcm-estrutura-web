package br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato;

import java.util.List;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.Perfil;
import br.gov.go.tcm.estrutura.entidade.web.Regra;
import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;

public interface PerfilDAO extends GenericoDAO {

	public List<Perfil> selecionarPerfilPorModuloEDescricaoExata(Long moduloId, String descricao);
	
	public List<Perfil> selecionarListaPerfilPorDescricaoExata(String descricao);

	public List<Perfil> obterLstPerfilPorModuloEDescricaoQualquerLugar(Modulo modulo, String descricao);

	public List<Perfil> obterLstPerfilOrdenadoPorDescricao();

	public List<Perfil> obterLstPerfilPorModulo(Modulo modulo);

	public Perfil obterPerfilPorId(Long perfilId);

	public List<Perfil> obterLstPerfilPorRegra(Regra regra);
	
}
