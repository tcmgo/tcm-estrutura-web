package br.gov.go.tcm.estrutura.entidade.ajudante;

public class AjudanteExcecao extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AjudanteExcecao(String message) {
		super(message);
	}
}
