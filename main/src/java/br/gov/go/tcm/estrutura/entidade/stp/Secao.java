package br.gov.go.tcm.estrutura.entidade.stp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.gov.go.tcm.estrutura.entidade.base.EntidadeComposta;
import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoTipo;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoSecaoEnumeracao;
import br.gov.go.tcm.estrutura.entidade.pk.stp.SecaoPK;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;

@Entity
@Table(name = "Secao")
@Proxy(lazy = true, proxyClass = Secao.class)
@Banco(nome = Banco.STP)
@TypeDefs( { @TypeDef(name = "tipoSecao", typeClass = EnumeracaoTipo.class, parameters = { 
	@Parameter(name = "enumClassName", value = "br.gov.go.tcm.estrutura.entidade.enumeracao.TipoSecaoEnumeracao") }) })
public class Secao extends EntidadeComposta {
	
	private static final long serialVersionUID = -8924896879097798489L;
	
	public final static int CODIGO_SECAO_SETOR_DADOS_ARQUIVO = 36;
	public final static int CODIGO_SECAO_EXPEDICAO = 69;
	public final static int CODIGO_SECAO_PROTOCOLO_APENSADO = 99;
	public final static int CODIGO_SECAO_SETOR_DILIGENCIA = 27;

	/*
	 * 1 CodSecao char 2 0 0 DescrSecao varchar 50 0 0 AbrevSecao varchar 17 1 0
	 * SenhaSecao varchar 10 1 0 Distribuir char 1 0 0 PrazoAnalise int 4 0 0
	 * TpSecao char 1 0 0 FazTramitacao char 1 1 0 OrdemHierarquica char 10 1 0
	 * Situacao char 1 1 0 Arquivo char 1 1 0 GeraGrafico bit 1 0
	 */

	@EmbeddedId
	private SecaoPK PK;

	@Column(name = "DescrSecao")
	private String descricao;

	@Column(name = "AbrevSecao")
	private String abreviacaoDescricao;

	@Column(name = "SenhaSecao")
	private String senhaSecao;

	@Column(name = "Distribuir")
	private Boolean fazDistribuicao;

	@Column(name = "PrazoAnalise")
	private Short qtdDias;

	@Type(type = "tipoSecao")
	@Column(name = "TpSecao")
	private TipoSecaoEnumeracao tipoSecao;

	@Column(name = "FazTramitacao")
	private Boolean fazTramitacao;

	@Column(name = "Situacao")
	private String ativa;

	@Override
	public SecaoPK getPK() {
		return PK;
	}

	public String getAbreviacaoDescricao() {
		return abreviacaoDescricao;
	}

	public String getAtiva() {
		return ativa;
	}

	public String getDescricao() {
		return descricao;
	}

	public Boolean getFazDistribuicao() {
		return fazDistribuicao;
	}

	public Boolean getFazTramitacao() {
		return fazTramitacao;
	}

	public Short getQtdDias() {
		return qtdDias;
	}

	public String getSenhaSecao() {
		return senhaSecao;
	}

	public TipoSecaoEnumeracao getTipoSecao() {
		return tipoSecao;
	}

	public void setPK(SecaoPK id) {
		this.PK = id;
	}

	public void setAbreviacaoDescricao(String abreviacaoDescricao) {
		this.abreviacaoDescricao = abreviacaoDescricao;
	}

	public void setAtiva(String ativa) {
		this.ativa = ativa;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setFazDistribuicao(Boolean fazDistribuicao) {
		this.fazDistribuicao = fazDistribuicao;
	}

	public void setFazTramitacao(Boolean fazTramitacao) {
		this.fazTramitacao = fazTramitacao;
	}

	public void setQtdDias(Short qtdDias) {
		this.qtdDias = qtdDias;
	}

	public void setSenhaSecao(String senhaSecao) {
		this.senhaSecao = senhaSecao;
	}

	public void setTipoSecao(TipoSecaoEnumeracao tipoSecao) {
		this.tipoSecao = tipoSecao;
	}

	@Override
	public String toString() {
		return descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((PK == null) ? 0 : PK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Secao other = (Secao) obj;
		if (PK == null) {
			if (other.PK != null)
				return false;
		} else if (!PK.equals(other.PK))
			return false;
		return true;
	}
	
	
}
