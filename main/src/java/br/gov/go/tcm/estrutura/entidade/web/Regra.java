package br.gov.go.tcm.estrutura.entidade.web;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;

import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "Regra")
@Proxy(lazy = true, proxyClass = Regra.class)
@Banco(nome = Banco.TCMWEB)
@NamedQueries( {
		@NamedQuery(name = "selecionarRegraPorRotulo", query = "SELECT r FROM Regra r WHERE lower(r.rotulo) like lower('%' || :rotulo || '%') "),
		@NamedQuery(name = "selecionarLstRegraOrdenadaPorRotulo", query = "SELECT r FROM Regra r ORDER BY r.rotulo ASC"),
		@NamedQuery(name = "selecionarLstRegraPorModulo", query = "SELECT r FROM Regra r WHERE r.modulo = :modulo ORDER BY r.modulo.descricao ASC, r.rotulo ASC") })
public class Regra extends EntidadePadrao {

	private static final long serialVersionUID = 671718410876874438L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	@NotNull
	private String rotulo;

	@Column
	@NotNull
	private String descricao;

	@ManyToOne(fetch = FetchType.EAGER)
	private Modulo modulo;

	@Override
	public Long getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getRotulo() {
		return rotulo;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setRotulo(String rotulo) {
		this.rotulo = rotulo;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	@Override
	public int hashCode() {
		return ((id == null) ? 0 : id.hashCode());
	}
	
	@Override
	public boolean equals(Object obj){
		try{
			if (obj == null)
				return false;
			Regra regra = (Regra) obj;
			return regra.getId().equals(this.getId());
		}
		catch (ClassCastException e){
			return false;
		}
	}
}
