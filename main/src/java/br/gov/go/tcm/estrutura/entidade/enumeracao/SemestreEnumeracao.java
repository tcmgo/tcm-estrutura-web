package br.gov.go.tcm.estrutura.entidade.enumeracao;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteEnumeracao;


public enum SemestreEnumeracao implements NumberEnumeracaoPersistente {

	SEMESTRE_01(1), SEMESTRE_02(2);

	private final int valor;

	SemestreEnumeracao(int valor) {
		this.valor = valor;
	}

	public String getNomeConstante() {
		switch (valor) {
		case 1:
			return "SEMESTRE_01";
		case 2:
			return "SEMESTRE_02";		
		default:
			return "";
		}
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return SemestreEnumeracao.SEMESTRE_01;
		case 2:
			return SemestreEnumeracao.SEMESTRE_02;		
		}
		return null;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "1� Semestre";
		case 2:
			return "2� Semestre";		
		default:
			return "";
		}
	}

	public static String toString(int semestreTemp) {
		switch (semestreTemp) {
		case 1:
			return "1� Semestre";
		case 2:
			return "2� Semestre";		
		default:
			return "";
		}
	}

	public static SemestreEnumeracao obterQuadrimestreAnterior(
			SemestreEnumeracao semestreEnum) {
		int quadrimestreAnterior = semestreEnum.getValorOrdinal()
				.intValue() - 1;
		return (quadrimestreAnterior <= 0) ? SemestreEnumeracao.SEMESTRE_02
						: (SemestreEnumeracao) AjudanteEnumeracao
								.obterEnumeracaoPorValorOrdinal(
										SemestreEnumeracao.class,
										quadrimestreAnterior);
	}
}