package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum TipoStatusAreaInteresse implements BooleanEnumeracaoPersistente {

	NAO(0), SIM(1);

	private final int valor;

	TipoStatusAreaInteresse(int valor) {
		this.valor = valor;
	}

	public Integer getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "NAO";
		case 1:
			return "SIM";
		default:
			return "";
		}
	}
}
