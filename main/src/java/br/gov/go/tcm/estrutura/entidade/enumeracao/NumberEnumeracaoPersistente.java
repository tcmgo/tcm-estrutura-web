package br.gov.go.tcm.estrutura.entidade.enumeracao;


public interface NumberEnumeracaoPersistente extends EnumeracaoPersistente{
	
	Number getValorOrdinal();
}
