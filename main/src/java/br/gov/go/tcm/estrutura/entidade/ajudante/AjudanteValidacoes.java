package br.gov.go.tcm.estrutura.entidade.ajudante;

public class AjudanteValidacoes {

	public static boolean isCpfValido(String cpfTemp) {

		String cpf = cpfTemp.replaceAll("[.]", "").replaceAll("-", "");

		if (cpf == null || cpf.length() != 11) {
			return Boolean.FALSE;
		}

		if (existeTodosDigitosIdenticosCPF(cpf)) {
			return Boolean.FALSE;
		}

		String numDig = cpf.substring(0, 9);

		if (!calcDigVerif(numDig).equals(cpf.substring(9, 11))) {
			return Boolean.FALSE;
		}

		return Boolean.TRUE;
	}

	private static String calcDigVerif(String num) {
		Integer primDig, segDig;
		int soma = 0, peso = 10;
		for (int i = 0; i < num.length(); i++)
			soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;

		if (soma % 11 == 0 | soma % 11 == 1)
			primDig = new Integer(0);
		else
			primDig = new Integer(11 - (soma % 11));

		soma = 0;
		peso = 11;
		for (int i = 0; i < num.length(); i++)
			soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;

		soma += primDig.intValue() * 2;
		if (soma % 11 == 0 | soma % 11 == 1)
			segDig = new Integer(0);
		else
			segDig = new Integer(11 - (soma % 11));

		return primDig.toString() + segDig.toString();
	}

	private static boolean existeTodosDigitosIdenticosCPF(String cpf) {
		String primeiroDigito = cpf.substring(0, 1);
		int contadorNumerosIguais = 0;
		for (int i = 0; i < cpf.length(); i++) {
			if (cpf.substring(i, i + 1).equals(primeiroDigito)) {
				contadorNumerosIguais++;
			}
		}
		if (contadorNumerosIguais == 11) {
			return true;
		}
		return false;
	}

	public static boolean isCnpjValido(String cnpjTemp) {

		String cnpjInfo = cnpjTemp.replaceAll("\\D", "");

		if ((cnpjInfo == null) || (cnpjInfo.length() == 0)) {

			return Boolean.FALSE;

		} else if (existeTodosDigitosIdenticosCNPJ(cnpjInfo)) {

			return Boolean.FALSE;

		} else if (!isValidCNPJ(cnpjInfo)) {

			return Boolean.FALSE;

		}
		return Boolean.TRUE;
	}

	private static boolean existeTodosDigitosIdenticosCNPJ(String cnpj) {
		
		String primeiroDigito = cnpj.substring(0, 1);
		
		int contadorDigitosIguais = 0;
		
		for (int i = 0; i < cnpj.length(); i++) {
			
			if (cnpj.substring(i, i + 1).equals(primeiroDigito)) {
				
				contadorDigitosIguais++;
			}
		}
		
		if (contadorDigitosIguais == 14) {
			
			return true;
		}
		
		return false;
	}

	private static final int[] pesoCNPJ = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

	private static int calcularDigito(String str, int[] peso) {
		
		int soma = 0;
		
		for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
			
			digito = Integer.parseInt(str.substring(indice, indice + 1));
			
			soma += digito * peso[peso.length - str.length() + indice];
		}
		
		soma = 11 - soma % 11;
		
		return soma > 9 ? 0 : soma;
	}

	public static boolean isValidCNPJ(String cnpj) {
		
		if ((cnpj == null) || (cnpj.length() != 14)) {
			
			return false;
		}
		
		if (!cnpj.substring(8, 12).equals("0001")) {
			
			return false;
		}

		Integer digito1 = calcularDigito(cnpj.substring(0, 12), pesoCNPJ);
		
		Integer digito2 = calcularDigito(cnpj.substring(0, 12) + digito1, pesoCNPJ);
		
		return cnpj.equals(cnpj.substring(0, 12) + digito1.toString() + digito2.toString());
	}
}
