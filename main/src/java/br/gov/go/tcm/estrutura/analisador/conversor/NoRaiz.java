package br.gov.go.tcm.estrutura.analisador.conversor;

import java.util.ArrayList;
import java.util.List;

public class NoRaiz {

	private List<NoConversor> filhos;
	
	public List<NoConversor> obterFilhos(){
		if(filhos == null)
			filhos = new ArrayList<NoConversor>();
		return filhos;
	}
	
	public void adicionarFilho(NoConversor noConversor){
		noConversor.setNoPai(this);
		obterFilhos().add(noConversor);
	}
	
}
