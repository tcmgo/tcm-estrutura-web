package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum AssuntosEnumeracao {
	
	COD_ASSUNTO("Agrupados"), 
	CAT_ASSUNTO("Categorias"), 
	ESP_ASSUNTO("Específicos");

	private final String tipoFiltroAssunto;

	AssuntosEnumeracao(String tipoFiltroAssunto) {
		this.tipoFiltroAssunto = tipoFiltroAssunto;
	}

	public String getTipoFiltroAssunto() {
		return tipoFiltroAssunto;
	}
	
	@Override
	public String toString() {
		return this.getTipoFiltroAssunto();
	}

}
