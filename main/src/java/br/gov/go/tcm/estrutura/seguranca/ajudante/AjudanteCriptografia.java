package br.gov.go.tcm.estrutura.seguranca.ajudante;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteTexto;

public class AjudanteCriptografia implements Serializable {

	private static final long serialVersionUID = 7543013012738638232L;

	private static Logger log = Logger.getLogger(AjudanteCriptografia.class);

	public static String gerarMD5(final String senha) {

		MessageDigest md5original;

		try {

			md5original = MessageDigest.getInstance("MD5");

			md5original.update(senha.toUpperCase().getBytes());

			final BigInteger hash = new BigInteger(1, md5original.digest());

			String retornaSenha = hash.toString(16);

			retornaSenha = AjudanteTexto.preencherTextoComZeros(retornaSenha, 32);

			return retornaSenha.toUpperCase();

		} catch (final NoSuchAlgorithmException e) {

			AjudanteCriptografia.log.error("Erro na geracao do MD5.", e);

		} catch (final Exception e) {

			AjudanteCriptografia.log.error("Erro na geracao do MD5.", e);
		}

		return null;
	}

}
