package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoSituacaoInscricaoEnumeracao implements NumberEnumeracaoPersistente {
	TODOS(0), PRE_INSCRITO(1), SELECIONADO(2), CONFIRMADO(3), MATRICULADO(4), CANCELADO(5);
	
	private int valor;

	TipoSituacaoInscricaoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}
	
	public Boolean foiConfirmado(){
		return getValorOrdinal().equals(TipoSituacaoInscricaoEnumeracao.CONFIRMADO.getValorOrdinal()); 
	}
	
	public Boolean foiSelecionado(){
		return getValorOrdinal().equals(TipoSituacaoInscricaoEnumeracao.SELECIONADO.getValorOrdinal());
	}
	
	public Boolean foiCancelado() {
		return getValorOrdinal().equals(TipoSituacaoInscricaoEnumeracao.CANCELADO.getValorOrdinal());
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Todos";
		case 1:
			return "Pr�-inscrito";
		case 2:
			return "Selecionado";
		case 3:
			return "Confirmado";
		case 4:
			return "Matriculado";
		case 5:
			return "Cancelado";
		default:
			return "";
		}
	}
	
}
