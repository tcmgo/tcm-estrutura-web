package br.gov.go.tcm.estrutura.modelo.identificador;

public interface IdentificadoresModelo {

	public final static String EMAIL_DOWNLOAD_CHAVE_NOVO_USUARIO_ANALISADOR = "emailDownloadChaveNovoUsuarioAnalisador";

	public final static String EMAIL_CONFIRMACAO_NOVO_USUARIO_ANALISADOR = "emailConfirmacaoNovoUsuarioAnalisador";
	
	public final static String EMAIL_CONFIRMACAO_CADASTRO_DIARIO_OFICIAL = "emailConfirmacaoCadastroDiarioOficial";

	public final static String FICHA_CADASTRO_USUARIO_ANALISADOR_GERADO_PELA_SUP_INFORMATICA = "fichaCadastroUsuarioAnalisadorGeradoPelaSupInformatica";

	public final static String EMAIL_CONFIRMACAO_INSCRICAO_CURSO = "emailConfirmacaoInscricaoCurso";

	public final static String BALANCETE_FINANCEIRO = "balanceteFinanceiro";

	public final static String FICHA_CADASTRO_NOVO_USUARIO_ANALISADOR = "fichaCadastroNovoUsuarioAnalisador";

	public final static String ANALISE_JULGADO = "analiseJulgado";

	public static final String INADIMPLENTE_AUTUACAO_FISICA = "inadimplenteAutuacaoFisica";

	public final static String ADIMPLENTE = "adimplente";

	public final static String INADIMPLENTE_ENTREGA_ELETRONICA = "inadimplenteEntregaEletronica";

	public final static String IMPEDIMENTO_PESSOA_DETALHE = "impedimentoPessoaDetalhe";
	
	public final static String CERTIDAO_DESPESA_PESSOAL_QUADRIMESTRAL = "certidaoDespesaPessoalQuadrimestral";
	
	public final static String CERTIDAO_DESPESA_PESSOAL_SEMESTRAL = "certidaoDespesaPessoalSemestral";
	
	public final static String CERTIDAO_PESSOA_FISICA = "pessoaFisica";
	
	public final static String CERTIDAO_EDUCACAO_ANUAL = "certidaoEducacaoAnual";
	
	public final static String CERTIDAO_SAUDE_ANUAL = "certidaoSaudeAnual";
	
	public final static String CERTIDAO_DUODECIMO_ANUAL = "certidaoDuodecimoAnual";
	
	public final static String AUTORIZACAO_CERTIDAO    = "autorizacaoCertidao";
	
	public final static String AUTORIZACAO_CERTIDAO_PF = "autorizacaoCertidaoPF";
	
	public final static String CERTIDAO_POSITIVA = "certidaoPositiva";
	
	public final static String CERTIDAO_FUNDEB_ANUAL = "certidaoFundebAnual";
	
	public final static String EMAIL_SELECAO_ESTAGIARIOS_SELECIONADO = "emailSelecaoEstagiariosSelecionado";
	
	public final static String EMAIL_SELECAO_ESTAGIARIOS_REJEITADO = "emailSelecaoEstagiariosRejeitado";
	
	public final static String EMAIL_ESQUECI_MINHA_SENHA = "emailEsqueciMinhaSenha";
	
	public final static String EMAIL_SOLICITACAO_ORCAMENTO = "emailSolicitacaoOrcamento";
	
	public final static String EMAIL_ASSINATURA_DOE = "emailAssinaturaDOE";
	
	public final static String EMAIL_CONFIRMACAO_CADASTRO_USUARIO_RESPONSAVEL_MUNICIPAL = "emailConfirmacaoCadastroUsuarioResponsavelMunicipal";
	
	public final static String EMAIL_AVISO_VALIDACAO_USUARIO_RESPONSAVEL_MUNICIPAL = "emailAvisoValidacaoUsuarioResponsavelMunicipal";
	
	public final static String CERTIFICADO_EVENTO = "certificadoEvento";
	
	public final static String LISTAGEM_INSCRITOS_EVENTO = "listagemInscritosEvento";

	public final static String LISTAGEM_INSCRITOS_OFICINA = "listagemInscritosOficina";
}
