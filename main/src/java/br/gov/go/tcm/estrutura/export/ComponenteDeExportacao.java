package br.gov.go.tcm.estrutura.export;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIComponentBase;

public class ComponenteDeExportacao extends UIComponentBase {

	@SuppressWarnings("unchecked")
	public static <T> List<T> getFilhosPorTipo(List<UIComponent> filhos, Class<T> tipoFilho ){
		List<T> elementos = new ArrayList<T>();
		for( UIComponent filho : filhos ){
			if( tipoFilho.isAssignableFrom( filho.getClass() )){
				elementos.add((T) filho );
			}
		}
		
		return elementos;
	}
	
	
	@Override
	public String getFamily() {
		// TODO Auto-generated method stub
		return null;
	}

}
