package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoParecerEnumeracao implements NumberEnumeracaoPersistente {

	RESOLUCAO(1), ACORDAO(2);

	int valor;

	TipoParecerEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Resolu��o";
		case 2:
			return "Acord�o";
		default:
			return "";

		}
	}

}
