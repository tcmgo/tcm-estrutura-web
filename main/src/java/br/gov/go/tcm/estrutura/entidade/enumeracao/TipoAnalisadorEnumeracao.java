package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoAnalisadorEnumeracao implements StringEnumeracaoPersistente {

	BALANCETES("BL"), FOLHA_PAGAMENTO("FP"), PPA_LOA("PL"), BALANCO("BA"), PPA("PP"), LOA("LO"), HOMOLOGACAO_CONCURSO_PUBLICO("CP");
	String valor;

	TipoAnalisadorEnumeracao(String valor) {
		this.valor = valor;
	}

	public String getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		if (valor.equals("BL")) {
			return TipoAnalisadorEnumeracao.BALANCETES;
		} else if (valor.equals("FP")) {
			return TipoAnalisadorEnumeracao.FOLHA_PAGAMENTO;
		} else if (valor.equals("PL")) {
			return TipoAnalisadorEnumeracao.PPA_LOA;
		} else if (valor.equals("BA")) {
			return TipoAnalisadorEnumeracao.BALANCO;
		} else if (valor.equals("PP")) {
			return TipoAnalisadorEnumeracao.PPA;
		} else if (valor.equals("LO")) {
			return TipoAnalisadorEnumeracao.LOA;
		} else if (valor.equals("CP")) {
			return TipoAnalisadorEnumeracao.HOMOLOGACAO_CONCURSO_PUBLICO;
		}
		return null;
	}

	@Override
	public String toString() {
		if (valor.equals("BL")) {
			return "Cont�bil";
		} else if (valor.equals("FP")) {
			return "Pessoal";
		} else if (valor.equals("PL")) {
			return "PPA_LOA";
		} else if (valor.equals("BA")) {
			return "Balan�o";
		} else if (valor.equals("PP")) {
			return "PPA";
		} else if (valor.equals("LO")) {
			return "LOA";
		} else if (valor.equals("CP")) {
			return "Homologa��o em Concursos P�blicos";
		}
		return "";
	}
}
