package br.gov.go.tcm.estrutura.persistencia.controledeacesso.implementacao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioMonitor;
import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.UsuarioMonitorDAO;

@Repository("UsuarioMonitorDAO")
public class UsuarioMonitorDAOImplementacao extends GenericoDAOImplementacao implements UsuarioMonitorDAO {

	private static final long serialVersionUID = 1692380139007613989L;

	@Autowired
	public UsuarioMonitorDAOImplementacao(@Qualifier("sessionFactoryUsuario") SessionFactory factory,
			@Qualifier("hibernateTemplateUsuario") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}

	@SuppressWarnings("unchecked")
	public List<UsuarioMonitor> obterLstUsuarioMonitorPorNome(String nome) {
		String parametro = "nome";
		Object objeto = nome;

		return getHibernateTemplate(getSessionFactoryUsuario()).findByNamedQueryAndNamedParam("selecionarUsuarioMonitorPorNome", parametro,
				objeto);
	}

	@SuppressWarnings("unchecked")
	public UsuarioMonitor obterUsuarioMonitorPorNomeSenha(UsuarioMonitor usuarioMonitor) {
		String[] parametro = { "login", "senha" };
		Object[] objeto = { usuarioMonitor.getNomeUsuario(), usuarioMonitor.getSenha() };

		List<UsuarioMonitor> lstUsuarioMonitor = getHibernateTemplate(getSessionFactoryUsuario()).findByNamedQueryAndNamedParam(
				"selecionarUsuarioMonitorPorLoginSenha", parametro, objeto);
		return lstUsuarioMonitor != null && lstUsuarioMonitor.size() > 0 ? lstUsuarioMonitor.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	public UsuarioMonitor obterUsuarioMonitorPorNome(String usuario) {
		String[] parametro = { "login" };
		Object[] objeto = { usuario };

		List<UsuarioMonitor> lstUsuarioMonitor = getHibernateTemplate(getSessionFactoryUsuario())
				.findByNamedQueryAndNamedParam(
						"selecionarUsuarioMonitorPorLogin", parametro, objeto);
		return lstUsuarioMonitor != null && lstUsuarioMonitor.size() > 0 ? lstUsuarioMonitor
				.get(0)
				: null;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public UsuarioMonitor obterUsuarioMonitorAtivoPorNomeSenha(final UsuarioMonitor usuarioMonitor) {
		
		List<UsuarioMonitor> lstUsuarioMonitor = (List<UsuarioMonitor>) getHibernateTemplate(getSessionFactoryUsuario())
				.execute(new HibernateCallback() {

					@Override
					public Object doInHibernate(Session sessao)
							throws HibernateException, SQLException {
						
						Criteria c = sessao.createCriteria(UsuarioMonitor.class);
						c.add(Restrictions.eq("nomeUsuario", usuarioMonitor.getNomeUsuario()));
						c.add(Restrictions.eq("senha", usuarioMonitor.getSenha()));
						c.add(Restrictions.eq("ativo", Boolean.TRUE));
						
						return c.list();
					}
				});
		
		return lstUsuarioMonitor != null && lstUsuarioMonitor.size() > 0 ? lstUsuarioMonitor.get(0) : null;
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<UsuarioMonitor> obterTodosUsuariosAtivos() {
		
		return (List<UsuarioMonitor>) getHibernateTemplate(getSessionFactoryUsuario()).execute(new HibernateCallback() {
			
			@Override
			public Object doInHibernate(Session sessao) throws HibernateException,
					SQLException {
				
				Criteria c = sessao.createCriteria(UsuarioMonitor.class);
				c.add(Restrictions.eq("ativo", Boolean.TRUE));
				
				c.addOrder(Order.asc("nomeCompleto"));
				
				return c.list();
			}
		});
		
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<UsuarioMonitor> obterTodosUsuariosAtivosPorSecao(final String codigoSecao) {
		
		return (List<UsuarioMonitor>) getHibernateTemplate(getSessionFactoryUsuario()).execute(new HibernateCallback() {
			
			@Override
			public Object doInHibernate(Session sessao) throws HibernateException,
					SQLException {
				
				Criteria c = sessao.createCriteria(UsuarioMonitor.class);				
				c.add(Restrictions.eq("ativo", Boolean.TRUE));
				c.add(Restrictions.eq("codigoSecao", codigoSecao));
				
				c.addOrder(Order.asc("nomeCompleto"));
				
				return c.list();
			}
		});
		
	}
}
