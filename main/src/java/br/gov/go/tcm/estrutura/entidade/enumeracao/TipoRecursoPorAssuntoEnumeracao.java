package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoRecursoPorAssuntoEnumeracao implements NumberEnumeracaoPersistente {

	EMBARGO_DECLARACAO(1), EMBARGO_DIVERGENCIA(2), RECURSO_ORDINARIO(3), RECURSO_REVISAO(4), RECLAMACAO(5);

	private final int valor;

	TipoRecursoPorAssuntoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Embargo Declara��o";
		case 2:
			return "Embargo Diverg�ncia";
		case 3:
			return "Recurso Ordin�rio";
		case 4:
			return "Recurso Revis�o";
		case 5:
			return "Reclama��o";
		default:
			return "";
		}
	}

}
