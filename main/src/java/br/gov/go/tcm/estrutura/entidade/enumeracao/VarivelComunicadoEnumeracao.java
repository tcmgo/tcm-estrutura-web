package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum VarivelComunicadoEnumeracao implements NumberEnumeracaoPersistente {

	MUNICIPIO(0), ORGAO(1), ENDERECO_ORGAO(2), NOME(3), CPF(4), IDENTIDADE(5), ENDERECO(6), EMAIL(7), CARGO(8), ENDERECO_CORRESPONDENCIA(
			9), DATA_HORA_ENVIO(10);

	private final int valor;

	VarivelComunicadoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public String getNomeReferenciaVariavel() {
		switch (valor) {
		case 0:
			return "municipio";
		case 1:
			return "orgao";
		case 2:
			return "enderecoOrgao";
		case 3:
			return "nome";
		case 4:
			return "cpf";
		case 5:
			return "identidade";
		case 6:
			return "endereco";
		case 7:
			return "email";
		case 8:
			return "cargo";
		case 9:
			return "enderecoCorrespondencia";
		case 10:
			return "dataHoraEnvio";
		default:
			return "";
		}
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Munic�pio";
		case 1:
			return "Org�o";
		case 2:
			return "Endere�o do Org�o";
		case 3:
			return "Nome";
		case 4:
			return "Cpf";
		case 5:
			return "Identidade";
		case 6:
			return "Endere�o";
		case 7:
			return "E-mail";
		case 8:
			return "Cargo";
		case 9:
			return "Endereco Correspond�ncia";
		case 10:
			return "Data Hora Envio";
		default:
			return "";
		}
	}

}
