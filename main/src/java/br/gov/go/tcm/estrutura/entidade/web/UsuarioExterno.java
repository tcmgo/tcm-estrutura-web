package br.gov.go.tcm.estrutura.entidade.web;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;

@Entity
@Table(name = "UsuarioExterno")
@Inheritance(strategy = InheritanceType.JOINED)
@Proxy(lazy = true, proxyClass = UsuarioExterno.class)
@Banco(nome = Banco.TCMWEB)
@NamedQueries( {
		@NamedQuery(name = "selecionarUsuarioExternoPorNome", query = "SELECT u FROM UsuarioExterno u WHERE lower(u.pessoa.nome) like lower('%' || :nome || '%') "),
		@NamedQuery(name = "selecionarUsuarioExternoOrdenadoPorNome", query = "SELECT u FROM UsuarioExterno u ORDER BY u.pessoa.nome"),
		@NamedQuery(name = "selecionarUsuarioExternoAtivoPorLoginSenha", query = "SELECT u FROM UsuarioExterno u WHERE u.login = :login AND u.senha = :senha AND u.ativoWeb = :ativoWeb"),
		@NamedQuery(name = "selecionarUsuarioExternoPorId", query = "SELECT u FROM UsuarioExterno u LEFT JOIN fetch u.pessoa WHERE u.id = :id") })
public class UsuarioExterno extends EntidadePadrao {

	private static final long serialVersionUID = -6535361095177290286L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String login;

	@Column
	private String senha;

	@ManyToOne(fetch = FetchType.LAZY)
	//@Cascade( { CascadeType.ALL, CascadeType.DELETE_ORPHAN })
	private Pessoa pessoa;

	@Column
	private Timestamp dataCriacao;

	@Column
	private String observacao;

	@Column
	private Boolean ativoWeb;

	@Column
	private String cargo;

	@Override
	public Long getId() {
		return id;
	}

	public Boolean getAtivoWeb() {
		return ativoWeb;
	}

	public Timestamp getDataCriacao() {
		return dataCriacao;
	}

	public String getLogin() {
		return login;
	}

	public String getObservacao() {
		return observacao;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public String getSenha() {
		return senha;
	}

	public String getCargo() {
		return cargo;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setDataCriacao(Timestamp dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public void setAtivoWeb(Boolean ativoWeb) {
		this.ativoWeb = ativoWeb;
	}

}
