package br.gov.go.tcm.estrutura.persistencia.orcafi.contrato;

import java.util.List;

import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoPesquisaOrgaoEnumeracao;
import br.gov.go.tcm.estrutura.entidade.orcafi.Municipio;
import br.gov.go.tcm.estrutura.entidade.orcafi.Orgao;
import br.gov.go.tcm.estrutura.entidade.pk.orcafi.OrgaoPK;
import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;

public interface OrgaoDAO extends GenericoDAO {

	List<Orgao> obterLstOrgaoPorDescricaoEMunicipio(String descricao, Municipio municipio);
	
	List<Orgao> obterLstOrgaoPorMunicipio(Municipio municipio);
	
	List<Orgao> obterLstOrgaosAtivos();
	
	Integer selecionarTotalDePossiveisEntregasBalanceteCompleto(
			TipoPesquisaOrgaoEnumeracao tipoPesquisaOrgao);
	
	public Orgao obterOrgaoPorId(OrgaoPK pk);
}
