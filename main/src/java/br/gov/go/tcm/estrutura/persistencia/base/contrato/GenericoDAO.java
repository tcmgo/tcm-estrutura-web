package br.gov.go.tcm.estrutura.persistencia.base.contrato;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.transform.ResultTransformer;
import org.springframework.orm.hibernate3.HibernateTemplate;

import br.gov.go.tcm.estrutura.entidade.base.Entidade;
import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;


public interface GenericoDAO{
	void setHibernateTemplate( HibernateTemplate template );
	HibernateTemplate getHibernateTemplate();
	void salvar(Object objeto);
	void salvarOuAtualizar(Object objeto);
	void atualizar(Object objeto);
	void excluir(Object objeto);
//	Object obterPorId(Long id, Class<?> classe);
	List<?> obterTodos(Class<?> classe);
	List<?> obterPorExemplo(Object exemplo);
	List<?> obterPorExemplo(Object exemplo,int exemploBusca);
	List<?> obterPorCriterio(final Class<? extends EntidadePadrao> classe, final ResultTransformer transformador, final Projection projecoes, final Order order, final Criterion ... criterio);
	void atachar(Object objeto);
	void desatachar(Object entity);
	Entidade obterEntidadePorId(Class<? extends Entidade> classe, Serializable id);
	boolean contemObjetoNaSessao(Object objeto);
	Entidade carregarObjetoNaSessao(Class<? extends Entidade> classe, Serializable id);
}