package br.gov.go.tcm.estrutura.entidade.ajudante;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import br.gov.go.tcm.estrutura.entidade.enumeracao.AnoEnumeracao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.MesEnumeracao;

public class AjudanteData {

	public final static String DATA_INICIO = "01/01/1900";

	public static class MesAnoAjudante {

		private MesEnumeracao mesEnumeracao;
		private AnoEnumeracao anoEnumeracao;

		public MesEnumeracao getMesEnumeracao() {
			return mesEnumeracao;
		}

		public AnoEnumeracao getAnoEnumeracao() {
			return anoEnumeracao;
		}

		public void setMesEnumeracao(MesEnumeracao mesEnumeracao) {
			this.mesEnumeracao = mesEnumeracao;
		}

		public void setAnoEnumeracao(AnoEnumeracao anoEnumeracao) {
			this.anoEnumeracao = anoEnumeracao;
		}

	}

	public static int calcularIdade(Date nascimento) {

		Calendar dataNascimento = Calendar.getInstance();

		dataNascimento.setTime(nascimento);

		Calendar dataAtual = Calendar.getInstance();

		dataAtual.setTime(new Date(System.currentTimeMillis()));

		int idade = dataAtual.get(Calendar.YEAR) - dataNascimento.get(Calendar.YEAR);

		if (dataAtual.get(Calendar.MONTH) < dataNascimento.get(Calendar.MONTH)) {

			idade--;

		} else if (dataAtual.get(Calendar.MONTH) == dataNascimento.get(Calendar.MONTH)) {

			if (dataAtual.get(Calendar.DAY_OF_MONTH) < dataNascimento.get(Calendar.DAY_OF_MONTH)) {

				idade--;
			}
		}

		return idade;
	}

	public static boolean eMaior(AnoEnumeracao anoEnum1, AnoEnumeracao anoEnum2) {

		return anoEnum1.getValorOrdinal().intValue() > anoEnum2.getValorOrdinal().intValue();

	}

	public static boolean eMaiorIgual(AnoEnumeracao anoEnum1, AnoEnumeracao anoEnum2) {

		return anoEnum1.getValorOrdinal().intValue() >= anoEnum2.getValorOrdinal().intValue();

	}

	/**
	 * Retorna a diferen�a em dias entre duas datas
	 * 
	 * @param c1
	 *            Menor data.
	 * @param c2
	 *            Maior data.
	 * @return Diferen�a em dias entre duas datas
	 */
	public static int diferencaEmDias(Calendar c1, Calendar c2) {
		long m1 = c1.getTimeInMillis();
		long m2 = c2.getTimeInMillis();
		return (int) ((m2 - m1) / (24 * 60 * 60 * 1000));
	}

	@SuppressWarnings("static-access")
	public static int diferencaEmAno(GregorianCalendar dataInicial) {
		GregorianCalendar hj = new GregorianCalendar();
		hj.getInstance();
		int anohj = hj.get(Calendar.YEAR);
		int anoInicial = dataInicial.get(Calendar.YEAR);
		return new Integer(anohj - anoInicial);
	}

	public static String converterDataParaModeloPadrao(Date data) {
		try {
			if (data == null)
				return new String("NADA CONSTA");

			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			return format.format(data);
		} catch (Exception e) {
			return null;
		}
	}

	public static String converterDataParaString(Date data, String pattern) {
		try {
			if (data == null)
				return new String("NADA CONSTA");

			SimpleDateFormat format = new SimpleDateFormat(pattern);
			return format.format(data);
		} catch (Exception e) {
			return null;
		}
	}

	public static String converterDataParaModeloReduzido(Date data) {
		try {
			if (data == null)
				return new String("NADA CONSTA");

			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			return format.format(data);
		} catch (Exception e) {
			return null;
		}
	}

	public static String obterHorarioDaData(Date data) {
		try {
			if (data == null)
				return null;

			SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
			return format.format(data);
		} catch (Exception e) {
			return null;
		}
	}

	public static String obterDiaSemanaParaData(Date data) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		switch (calendar.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.SUNDAY:
			return "Domingo";
		case Calendar.MONDAY:
			return "Segunda-Feira";
		case Calendar.TUESDAY:
			return "Ter�a-Feira";
		case Calendar.WEDNESDAY:
			return "Quarta-Feira";
		case Calendar.THURSDAY:
			return "Quinta-Feira";
		case Calendar.FRIDAY:
			return "Sexta-Feira";
		case Calendar.SATURDAY:
			return "S�bado";
		default:
			return "";
		}
	}

	public MesAnoAjudante obterMesAnoAnterior(MesAnoAjudante mesAnoAjudante) {

		AnoEnumeracao anoEnumeracao = mesAnoAjudante.getAnoEnumeracao();
		MesEnumeracao mesEnumeracao = mesAnoAjudante.getMesEnumeracao();

		mesEnumeracao = MesEnumeracao.obterMesAnterior(mesEnumeracao);

		if (mesEnumeracao.equals(MesEnumeracao.DEZEMBRO)) {
			anoEnumeracao = AnoEnumeracao.obterAnoAnterior(anoEnumeracao);
		}

		mesAnoAjudante.setMesEnumeracao(mesEnumeracao);
		mesAnoAjudante.setAnoEnumeracao(anoEnumeracao);

		return mesAnoAjudante;
	}

	public static int obterDiferencaEntreDatasEmAnos(Date dataFim, Date dataInicio) {

		Calendar calendarDataInicio = Calendar.getInstance();
		Calendar calendarDataFim = Calendar.getInstance();

		calendarDataInicio.setTime(dataInicio);
		calendarDataFim.setTime(dataFim);

		int anoDataInicio = calendarDataInicio.get(Calendar.YEAR);
		int anoDataFim = calendarDataFim.get(Calendar.YEAR);

		int diferencaAnos = anoDataFim - anoDataInicio;

		if (calendarDataInicio.get(Calendar.MONTH) > calendarDataFim.get(Calendar.MONTH)) {
			diferencaAnos--;
		} else if (calendarDataFim.get(Calendar.MONTH) == calendarDataInicio.get(Calendar.MONTH)) {

			if (calendarDataInicio.get(Calendar.DAY_OF_MONTH) > calendarDataFim.get(Calendar.DAY_OF_MONTH)) {
				diferencaAnos--;
			}
		}

		return diferencaAnos;
	}

	public static Date converterModeloPadraoParaData(String modeloData) {
		try {
			if (modeloData == null)
				return null;

			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			return format.parse(modeloData);
		} catch (Exception e) {
			return null;
		}
	}

	public static Date converterParaData(String modeloData) {
		try {
			if (modeloData == null)
				return null;

			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			return format.parse(modeloData);
		} catch (Exception e) {
			return null;
		}
	}

	public static AnoEnumeracao obterAnoParaData(Date dataEmpenho) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataEmpenho);
		return (AnoEnumeracao) AjudanteEnumeracao.obterEnumeracaoPorValorOrdinal(AnoEnumeracao.class,
				calendar.get(Calendar.YEAR));
	}

	public static Boolean dataEstaEntreOPeriodo(Date dataReferencia, Date dataInicio, Date dataFim) {

		Calendar calendarDataReferencia = Calendar.getInstance();
		Calendar calendarDataInicio = Calendar.getInstance();
		Calendar calendarDataFim = Calendar.getInstance();

		calendarDataReferencia.setTime(zerarHoraData(dataReferencia));
		calendarDataInicio.setTime(zerarHoraData(dataInicio));
		calendarDataFim.setTime(zerarHoraData(dataFim));

		return (calendarDataReferencia.compareTo(calendarDataInicio) >= 0)
				&& (calendarDataReferencia.compareTo(calendarDataFim) <= 0);

	}

	public static void zerarHoraData(Calendar data) {

		data.set(Calendar.HOUR, 0);
		data.set(Calendar.MINUTE, 0);
		data.set(Calendar.SECOND, 0);
		data.set(Calendar.MILLISECOND, 0);

	}
	
	public static Date zerarHoraData(Date data) {

		Calendar calendar = Calendar.getInstance();

		calendar.setTime(data);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	public static Boolean dataMenorQueReferencia(Date data, Date dataReferencia) {

		Calendar calendarDataReferencia = Calendar.getInstance();
		Calendar calendarData = Calendar.getInstance();

		calendarDataReferencia.setTime(zerarHoraData(dataReferencia));
		calendarData.setTime(zerarHoraData(data));

		return (data.compareTo(dataReferencia) < 0);
	}
	
	public static Boolean dataMenorIgualQueReferencia(Date data, Date dataReferencia) {

		Calendar calendarDataReferencia = Calendar.getInstance();
		Calendar calendarData = Calendar.getInstance();

		calendarDataReferencia.setTime(zerarHoraData(dataReferencia));
		calendarData.setTime(zerarHoraData(data));

		return (data.compareTo(dataReferencia) <= 0);
	}

	public Boolean dataEhAnteriorAReferencia(Date data, Date dataReferencia) {
		Calendar calendarDataReferencia = Calendar.getInstance();
		Calendar calendarData = Calendar.getInstance();

		calendarDataReferencia.setTime(dataReferencia);
		calendarData.setTime(data);

		return (data.compareTo(dataReferencia) < 0);
	}

	public static Date obterUltimaHora(Date dataAtual) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(dataAtual);
		gc.set(Calendar.HOUR_OF_DAY, 23);
		gc.set(Calendar.MINUTE, 59);
		gc.set(Calendar.SECOND, 59);
		gc.set(Calendar.MILLISECOND, 999);
		return gc.getTime();
	}

	public static Date obterPrimeiraHora(Date dataAtual) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(dataAtual);
		gc.set(Calendar.HOUR_OF_DAY, 00);
		gc.set(Calendar.MINUTE, 00);
		gc.set(Calendar.SECOND, 00);
		gc.set(Calendar.MILLISECOND, 000);
		return gc.getTime();
	}

	public static Date obterDecimaSegundaHora(Date dataAtual) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(dataAtual);
		gc.set(Calendar.HOUR_OF_DAY, 12);
		gc.set(Calendar.MINUTE, 00);
		gc.set(Calendar.SECOND, 00);
		gc.set(Calendar.MILLISECOND, 000);
		return gc.getTime();
	}

	public static Date obterDecimaOitavaHora(Date dataAtual) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(dataAtual);
		gc.set(Calendar.HOUR_OF_DAY, 18);
		gc.set(Calendar.MINUTE, 00);
		gc.set(Calendar.SECOND, 00);
		gc.set(Calendar.MILLISECOND, 000);
		return gc.getTime();
	}

	public static boolean horaEstaNoPeriodoMatutino(Date dataReferencia) {
		return dataEstaEntreOPeriodo(dataReferencia, obterPrimeiraHora(dataReferencia),
				obterDecimaSegundaHora(dataReferencia));
	}

	public static boolean horaEstaNoPeriodoVespertino(Date dataReferencia) {
		return dataEstaEntreOPeriodo(dataReferencia, obterDecimaSegundaHora(dataReferencia),
				obterDecimaOitavaHora(dataReferencia));
	}

	public static boolean horaEstaNoPeriodoNoturno(Date dataReferencia) {
		return dataEstaEntreOPeriodo(dataReferencia, obterDecimaOitavaHora(dataReferencia),
				obterUltimaHora(dataReferencia));
	}

	public static boolean anoAnterior2009(Date dataReferencia) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(dataReferencia);

		try {
			if (dataReferencia.after(df.parse("31/12/2008"))) {
				return false;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return true;
	}

	public static Calendar converteDateEmCalendar(Date data) {

		Calendar calendar = Calendar.getInstance();

		calendar.setTime(data);

		return calendar;

	}

	public static Boolean dataMaiorDataAtual(Date data) {

		Calendar dt = Calendar.getInstance();
		dt.setTime(data);

		Calendar dtHoje = Calendar.getInstance();
		dtHoje.setTime(DDMMYYYY());

		return dt.compareTo(dtHoje) > 0;
	}

	public static Boolean dataMaiorIgualDataAtual(Date data) {

		Calendar dt = Calendar.getInstance();
		dt.setTime(data);

		Calendar dtHoje = Calendar.getInstance();
		dtHoje.setTime(DDMMYYYY());

		return dt.compareTo(dtHoje) >= 0;
	}

	public static Calendar adicionarDias(Calendar data, int quantidade) {

		data.add(Calendar.DATE, quantidade);

		return data;
	}

	public static Boolean isDiaUtil(Calendar data) {

		return !(data.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
				&& !(data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY);
	}

	public static Date DDMMYYYY() {

		Calendar calendar = Calendar.getInstance();

		calendar.setTime(new Date());

		calendar.set(Calendar.HOUR, 0);

		calendar.set(Calendar.HOUR_OF_DAY, 0);

		calendar.set(Calendar.MINUTE, 0);

		calendar.set(Calendar.SECOND, 0);

		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	public static String obterDataAtualFormatadaModeloPadrao() {

		return AjudanteData.converterDataParaModeloPadrao(new Date());
	}

	public static Calendar calendar(Date data) {

		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(data);
		
		return calendar;
	}

	public static Date hoje() {

		return new Date();
	}
}
