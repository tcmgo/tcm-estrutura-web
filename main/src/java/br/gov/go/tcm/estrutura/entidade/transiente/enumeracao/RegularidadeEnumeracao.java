package br.gov.go.tcm.estrutura.entidade.transiente.enumeracao;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteTexto;
import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.NumberEnumeracaoPersistente;

public enum RegularidadeEnumeracao implements NumberEnumeracaoPersistente{
	
	NAO_AUTUADO(1),EM_ANALISE(2),JULGADO(3);
	
	private final int valor;
	
	RegularidadeEnumeracao(int valor){
		this.valor = valor;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return RegularidadeEnumeracao.NAO_AUTUADO;
		case 2:
			return RegularidadeEnumeracao.EM_ANALISE;
		case 3:
			return RegularidadeEnumeracao.JULGADO;
		}
		return null;
	}
	
	
	public Number getValorOrdinal() {
		return this.valor;
	}
	
	@Override
	public String toString(){
		switch(this.valor){
			case 1:
				return "N�o Autuado";
			case 2:
				return "Em An�lise";
			case 3:
				return "Julgado";
			default:
				return AjudanteTexto.TEXTO_VAZIO;
		}		
	}
	
}
