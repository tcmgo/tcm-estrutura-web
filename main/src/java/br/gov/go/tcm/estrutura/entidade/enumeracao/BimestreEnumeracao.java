package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum BimestreEnumeracao implements NumberEnumeracaoPersistente {

	BIMESTRE_01(1), BIMESTRE_02(2), BIMESTRE_03(3), BIMESTRE_04(4), BIMESTRE_05(5), BIMESTRE_06(6);

	private final int valor;

	BimestreEnumeracao(int valor) {
		this.valor = valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return BimestreEnumeracao.BIMESTRE_01;
		case 2:
			return BimestreEnumeracao.BIMESTRE_02;
		case 3:
			return BimestreEnumeracao.BIMESTRE_03;
		case 4:
			return BimestreEnumeracao.BIMESTRE_04;
		case 5:
			return BimestreEnumeracao.BIMESTRE_05;
		case 6:
			return BimestreEnumeracao.BIMESTRE_06;
		}
		return null;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}
	

	public String getNomeConstante() {
		switch (valor) {
		case 1:
			return "BIMESTRE_01";
		case 2:
			return "BIMESTRE_02";
		case 3:
			return "BIMESTRE_03";
		case 4:
			return "BIMESTRE_04";
		case 5:
			return "BIMESTRE_05";
		case 6:
			return "BIMESTRE_06";
		default:
			return "";
		}
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "1� Bimestre";
		case 2:
			return "2� Bimestre";
		case 3:
			return "3� Bimestre";
		case 4:
			return "4� Bimestre";
		case 5:
			return "5� Bimestre";
		case 6:
			return "6� Bimestre";
		default:
			return "";
		}
	}

}