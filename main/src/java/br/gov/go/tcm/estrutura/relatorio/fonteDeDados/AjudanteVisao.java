package br.gov.go.tcm.estrutura.relatorio.fonteDeDados;

import java.io.Serializable;

public class AjudanteVisao implements Serializable{
	
	private static final long serialVersionUID = -1193180834837090571L;
	private String nomeArquivo;
	private Boolean erroRelatorio;

	public Boolean getErroRelatorio() {
		if(erroRelatorio == null)
			erroRelatorio = Boolean.FALSE;
		return erroRelatorio;
	}

	public void setErroRelatorio(Boolean erroRelatorio) {
		this.erroRelatorio = erroRelatorio;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	
}
