package br.gov.go.tcm.estrutura.entidade.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoTipo;
import br.gov.go.tcm.estrutura.entidade.enumeracao.SexoEnumeracao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.UnidadeFederacaoEnumeracao;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;

@Entity
@Table(name = "Pessoa")
@Proxy(lazy = true, proxyClass = Pessoa.class)
@Banco(nome = Banco.TCMWEB)
@TypeDefs( { @TypeDef(name = "sexo", typeClass = EnumeracaoTipo.class, parameters = { @Parameter(name = "enumClassName", value = "br.gov.go.tcm.estrutura.entidade.enumeracao.SexoEnumeracao")}),
			 @TypeDef(name = "uf", typeClass = EnumeracaoTipo.class, parameters = { @Parameter(name = "enumClassName", value = "br.gov.go.tcm.estrutura.entidade.enumeracao.UnidadeFederacaoEnumeracao")})
	})
@NamedQueries( { @NamedQuery(name = "obterPessoaPorNome", query = "SELECT p FROM Pessoa p WHERE lower(p.nome) like lower('%' || :nome || '%') ORDER BY p.nome ASC ") })
public class Pessoa extends EntidadePadrao {

	private static final long serialVersionUID = -8147710034901382332L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String nome;

	@Column
	private String cpf;

	@Column
	private String identidade;

	@Column
	private String orgaoExpedidor;

	@Column
	private String logradouro;

	@Column
	private String bairro;

	@Column
	private String cep;

	@Column
	private String cidade;

	@Column
	private String email;

	@Type(type = "sexo")
	@Column
	private SexoEnumeracao sexo;

	@Column
	private String nomeMae;

	@Column
	private Date dataNascimento;
	
	@Type(type = "uf")
	@Column
	private UnidadeFederacaoEnumeracao uf;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "PessoaTelefone", joinColumns = @JoinColumn(name = "pessoa_id"), inverseJoinColumns = @JoinColumn(name = "telefone_id"))
	@Cascade( { CascadeType.ALL, CascadeType.DELETE_ORPHAN })
	private List<Telefone> lstTelefone;

	@Override
	public Long getId() {
		return id;
	}

	public String getBairro() {
		return bairro;
	}

	public String getCep() {
		return cep;
	}

	public String getCidade() {
		return cidade;
	}

	public String getCpf() {
		return cpf;
	}

	public String getOrgaoExpedidor() {
		return orgaoExpedidor;
	}

	public String getEmail() {
		return email;
	}

	public String getIdentidade() {
		return identidade;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public String getNome() {
		return nome;
	}

	public List<Telefone> getLstTelefone() {
		if (lstTelefone == null)
			lstTelefone = new ArrayList<Telefone>();
		return lstTelefone;
	}

	public SexoEnumeracao getSexo() {
		return sexo;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setIdentidade(String identidade) {
		this.identidade = identidade;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setLstTelefone(List<Telefone> lstTelefone) {
		this.lstTelefone = lstTelefone;
	}

	public void setOrgaoExpedidor(String orgaoExpedidor) {
		this.orgaoExpedidor = orgaoExpedidor;
	}

	public void setSexo(SexoEnumeracao sexo) {
		this.sexo = sexo;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public UnidadeFederacaoEnumeracao getUf() {
		return uf;
	}

	public void setUf(UnidadeFederacaoEnumeracao uf) {
		this.uf = uf;
	}

}
