package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoDeDocumentoEnumeracao implements NumberEnumeracaoPersistente {

	DOCUMENTO_RESOLUCAO(1), DOCUMENTO_DESPACHO(2), TAXAS(3);

	int valor;

	TipoDeDocumentoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoDeDocumentoEnumeracao.DOCUMENTO_RESOLUCAO;
		case 2:
			return TipoDeDocumentoEnumeracao.DOCUMENTO_DESPACHO;
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Resolu��o";
		case 2:
			return "Despacho";
		default:
			return "";
		}
	}
}