package br.gov.go.tcm.estrutura.entidade.transiente;

import java.util.Date;

public class HistoricoProcesso {

	private String historico;
	private Date dataHoraDestino;
	private String analista;

	private Number seqDocExec;
	private Number anoDocExec;
	private String codParecerExec;

	private Number seqDocLeg;
	private Number anoDocLeg;
	private String codParecerLeg;

	private String parecerExec;
	private String parecerLeg;

	private Number docNumeroDocumento;
	private Number docAnoDocumento;
	private String docParecer;
	private String tipoParecer;

	private Number anoProcesso;
	private Number seqProcesso;
	private Number seqTramite;

	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public Date getDataHoraDestino() {
		return dataHoraDestino;
	}

	public void setDataHoraDestino(Date dataHoraDestino) {
		this.dataHoraDestino = dataHoraDestino;
	}

	public String getAnalista() {
		return analista;
	}

	public void setAnalista(String analista) {
		this.analista = analista;
	}

	public Number getSeqDocExec() {
		return seqDocExec;
	}

	public void setSeqDocExec(Number seqDocExec) {
		this.seqDocExec = seqDocExec;
	}

	public Number getAnoDocExec() {
		return anoDocExec;
	}

	public void setAnoDocExec(Number anoDocExec) {
		this.anoDocExec = anoDocExec;
	}

	public String getCodParecerExec() {
		return codParecerExec;
	}

	public void setCodParecerExec(String codParecerExec) {
		this.codParecerExec = codParecerExec;
	}

	public Number getSeqDocLeg() {
		return seqDocLeg;
	}

	public void setSeqDocLeg(Number seqDocLeg) {
		this.seqDocLeg = seqDocLeg;
	}

	public Number getAnoDocLeg() {
		return anoDocLeg;
	}

	public void setAnoDocLeg(Number anoDocLeg) {
		this.anoDocLeg = anoDocLeg;
	}

	public String getCodParecerLeg() {
		return codParecerLeg;
	}

	public void setCodParecerLeg(String codParecerLeg) {
		this.codParecerLeg = codParecerLeg;
	}

	public Number getDocNumeroDocumento() {
		return docNumeroDocumento;
	}

	public void setDocNumeroDocumento(Number docNumeroDocumento) {
		this.docNumeroDocumento = docNumeroDocumento;
	}

	public Number getDocAnoDocumento() {
		return docAnoDocumento;
	}

	public void setDocAnoDocumento(Number docAnoDocumento) {
		this.docAnoDocumento = docAnoDocumento;
	}

	public String getDocParecer() {
		return docParecer;
	}

	public void setDocParecer(String docParecer) {
		this.docParecer = docParecer;
	}

	public String getTipoParecer() {
		return tipoParecer;
	}

	public void setTipoParecer(String tipoParecer) {
		this.tipoParecer = tipoParecer;
	}

	public Number getAnoProcesso() {
		return anoProcesso;
	}

	public void setAnoProcesso(Number anoProcesso) {
		this.anoProcesso = anoProcesso;
	}

	public Number getSeqProcesso() {
		return seqProcesso;
	}

	public void setSeqProcesso(Number seqProcesso) {
		this.seqProcesso = seqProcesso;
	}

	public Number getSeqTramite() {
		return seqTramite;
	}

	public void setSeqTramite(Number seqTramite) {
		this.seqTramite = seqTramite;
	}

	public String getParecerExec() {

		if ((seqDocExec != null && seqDocExec.intValue() > 0)
				&& (anoDocExec != null && anoDocExec.intValue() > 0)) {

			parecerExec = seqDocExec + "/" + anoDocExec + ""
					+ (codParecerExec == null ? "" : "-" + codParecerExec);

		} else if ((docNumeroDocumento != null && docNumeroDocumento.intValue() > 0)
				&& (docAnoDocumento != null && docAnoDocumento.intValue() > 0)
				&& tipoParecer.equalsIgnoreCase("E")) {

			parecerExec = docNumeroDocumento + "/" + docAnoDocumento + ""
					+ (docParecer == null ? "" : "-" + docParecer);
		}

		return parecerExec;
	}

	public String getParecerLeg() {

		if ((seqDocLeg != null && seqDocLeg.intValue() > 0)
				&& (anoDocLeg != null && anoDocLeg.intValue() > 0)) {
			parecerLeg = seqDocLeg + "/" + anoDocLeg + ""
					+ (codParecerLeg == null ? "" : "-" + codParecerLeg);
		} else if ((docNumeroDocumento != null && docNumeroDocumento.intValue() > 0)
				&& (docAnoDocumento != null && docAnoDocumento.intValue() > 0)
				&& tipoParecer.equalsIgnoreCase("L")) {

			parecerLeg = docNumeroDocumento + "/" + docAnoDocumento + ""
					+ (docParecer == null ? "" : "-" + docParecer);
		}

		return parecerLeg;
	}
}