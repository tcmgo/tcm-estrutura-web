package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoArquivoProcessoEnumeracao implements StringEnumeracaoPersistente{
	
	CAIXA("C"),PRATELEIRA("P"), PACOTE("T"), INCINERAR("I");
	String valor;
	
	TipoArquivoProcessoEnumeracao(String valor){
		this.valor = valor;
	}
	
	public String getValorOrdinal(){
		return this.valor;
	}
	
	public EnumeracaoPersistente valor(){
		if(valor.equals("C")){
			return TipoArquivoProcessoEnumeracao.CAIXA;
		} else if(valor.equals("P")){
			return TipoArquivoProcessoEnumeracao.PRATELEIRA;
		} else if(valor.equals("T")){
			return TipoArquivoProcessoEnumeracao.PACOTE;
		} else if(valor.equals("I")){
			return TipoArquivoProcessoEnumeracao.INCINERAR;
		}  
		return null;
	}
	
	@Override
	public String toString(){
		if(valor.equals("C")){
			return "Caixa";
		} else if(valor.equals("P")){
			return "Prateleira";
		} else if(valor.equals("T")){
			return "Pacote";
		} else if(valor.equals("I")){
			return "Incinerar";
		}
		return "";
	}
	
}
