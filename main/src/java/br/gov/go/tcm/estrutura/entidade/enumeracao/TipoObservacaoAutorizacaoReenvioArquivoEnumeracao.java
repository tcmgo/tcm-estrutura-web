package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoObservacaoAutorizacaoReenvioArquivoEnumeracao implements NumberEnumeracaoPersistente {

	REIMPORTACAO(1), REIMPORTACAO_WEB(2);

	private final int valor;

	TipoObservacaoAutorizacaoReenvioArquivoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Reimportação";
		case 2:
			return "Reimportação WEB";
		default:
			return "";
		}
	}
}
