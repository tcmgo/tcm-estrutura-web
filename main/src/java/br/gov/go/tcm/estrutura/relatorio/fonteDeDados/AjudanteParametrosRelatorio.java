package br.gov.go.tcm.estrutura.relatorio.fonteDeDados;

import java.util.ArrayList;
import java.util.List;

import br.gov.go.tcm.estrutura.util.diretorio.AjudanteDiretorio;

public class AjudanteParametrosRelatorio {
	
	private final String LOGO_ID = "logotipoTCM";
	private final String SELO_TCM = "seloTCM";
	private final String LOGO_ESCOLA_CONTAS = "logotipoEC";
	
	
	private List<ParametroVisao> parametrosRelatorio = new ArrayList<ParametroVisao>();

	public List<ParametroVisao> getParametrosRelatorio() {
		return parametrosRelatorio;
	}

	public void adicionarParametroRelatorio(String id,Object valor){
		parametrosRelatorio.add(new ParametroVisao(id,valor));
	}
	
	public List<ParametroVisao> obterListaParametrosRelatorio(){
		AjudanteDiretorio assistenteDiretorio = new AjudanteDiretorio();
		adicionarParametroRelatorio(LOGO_ID,assistenteDiretorio.obterCaminhoContexto() + "imagens/tcm_logomarca.jpg");
		adicionarParametroRelatorio(SELO_TCM,assistenteDiretorio.obterCaminhoContexto() + "imagens/selo_tcm.jpg" );
		adicionarParametroRelatorio(LOGO_ESCOLA_CONTAS, assistenteDiretorio.obterCaminhoContexto() + "imagens/ec_logomarca.jpg");
		return parametrosRelatorio;
	}
}
