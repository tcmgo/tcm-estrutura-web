package br.gov.go.tcm.estrutura.entidade.enumeracao;

public interface TextoEnumeracao extends EnumeracaoPersistente {
	public String getValorOrdinal();
}
