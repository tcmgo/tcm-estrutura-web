package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum AreaDoConhecimentoEnumeracao implements NumberEnumeracaoPersistente {

	CIENCIAS_EXATAS_TERRA(1), CIENCIAS_SAUDE(2), CIENCIAS_AGRARIAS(3), 
	CIENCIAS_SOCIAIS_APLICADAS(4), CIENCIAS_HUMANAS(5), LINGUISTICA_LETRAS_ARTES(6), 
	MULTIDISCIPLINAR(7);

	int valor;

	private AreaDoConhecimentoEnumeracao(int valor) {
		this.valor = valor;
	}
	
	@Override
	public String toString(){
		switch (valor) {
		case 1:
			return "Ci�ncias Exatas e da Terra"; 
		case 2:
			return "Ci�ncias da Sa�de";
		case 3:
			return "Ci�ncias Agr�rias";
		case 4:
			return "Ci�ncias Sociais Aplicadas";
		case 5:
			return "Ci�ncias Humanas";
		case 6:
			return "Lingu�stica, Letras e Artes";
		case 7:
			return "Multidisciplinar";

		default:
			return "";
		}
	}
	
	@Override
	public Number getValorOrdinal() {
		return this.valor;
	}
	
	public AreaDoConhecimentoEnumeracao getPorValorOrdinal(int numero) {
		for (AreaDoConhecimentoEnumeracao area : AreaDoConhecimentoEnumeracao
				.values()) {
			if (area.valor == numero) {
				return area;
			}
		}
		return null;
	}

}
