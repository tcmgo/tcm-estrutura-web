package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum EnderecoCorrespondenciaEnumeracao implements NumberEnumeracaoPersistente{
	
	AUTORIDADE(1), REPRESENTANTE(2), ORGAO(3);

	int valor;

	EnderecoCorrespondenciaEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Autoridade";
		case 2:
			return "Representante";
		case 3:
			return "�rg�o";
		default:
			return "";
		}
	}
	
}
