package br.gov.go.tcm.estrutura.util.excecao;

public class RegistroDeveSerUnicoExcecao extends RuntimeException{

	private static final long serialVersionUID = 2666004593939897672L;

	public RegistroDeveSerUnicoExcecao(String message) {
		super(message);
	}

}
