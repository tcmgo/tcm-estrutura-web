package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum StatusEnumeracao implements NumberEnumeracaoPersistente {
	
	STATUS_INATIVO(0), STATUS_ATIVO(1);
	
	private int valor;
	
	private StatusEnumeracao(int valor){
		this.valor = valor;
	}
	
	@Override
	public Number getValorOrdinal() {
		return this.valor;
	}
	
	@Override
	public String toString(){
		switch (valor) {
		case 0:
			return "Inativo";
		case 1:
			return "Ativo";
		default:
			return "";
		}
	}
}