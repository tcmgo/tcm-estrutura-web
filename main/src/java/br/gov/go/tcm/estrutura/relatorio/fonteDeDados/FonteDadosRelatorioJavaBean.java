package br.gov.go.tcm.estrutura.relatorio.fonteDeDados;

import java.util.List;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class FonteDadosRelatorioJavaBean implements FonteDadosRelatorio{
	
	private JRBeanCollectionDataSource fonteDados;
	
	public FonteDadosRelatorioJavaBean(List<Object> fonteDados) {
		this.fonteDados = new JRBeanCollectionDataSource(fonteDados);
	}	
	
	public JRBeanCollectionDataSource getFonteDados() {
		return fonteDados;
	}

	public void setFonteDados(JRBeanCollectionDataSource listaDados) {
		this.fonteDados = listaDados;
	}
}
