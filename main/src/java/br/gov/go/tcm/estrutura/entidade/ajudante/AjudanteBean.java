package br.gov.go.tcm.estrutura.entidade.ajudante;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import br.gov.go.tcm.estrutura.entidade.ajudante.excecao.ReflexaoExcecao;

public class AjudanteBean {

	public static Boolean instanciaDe(Object objeto, String classe) {
		try {
			Class<?> classeDinamica = Class.forName(classe);
			return classeDinamica.isAssignableFrom(objeto.getClass());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static void copiarPropriedadesDoBean(Object objetoOrigem,
			Object objetoDestino) throws AjudanteExcecao {

		AjudanteReflexao assistenteReflexao = new AjudanteReflexao();

		List<String> lstPropriedades = assistenteReflexao
				.obterPropriedades(objetoOrigem.getClass());

		for (String propriedade : lstPropriedades) {

			try {
				if (assistenteReflexao.temPropriedade(objetoDestino,
						propriedade)) {

					assistenteReflexao.aplicarValorPropriedade(objetoDestino,
							propriedade, assistenteReflexao
									.obterValorPropriedade(objetoOrigem,
											propriedade));

				}
			} catch (ReflexaoExcecao e) {
				throw new AjudanteExcecao(e.getMessage());
			}

		}
	}

	public static void copiarPropriedadesMapParaBean(
			Map<?, ?> parametrosControlador, Object destino)
			throws ReflexaoExcecao {

		AjudanteReflexao assistenteReflexao = new AjudanteReflexao();

		Iterator<?> iter = parametrosControlador.entrySet().iterator();

		while (iter.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry entrada = (Entry) iter.next();

			if (assistenteReflexao.temPropriedade(destino,
					(String) entrada.getKey())) {

				assistenteReflexao.aplicarValorPropriedade(destino,
						(String) entrada.getKey(), (String) entrada.getValue());

			} else {
				throw new ReflexaoExcecao("Propriedade inv�lida: "
						+ entrada.getKey());
			}
		}
	}
}