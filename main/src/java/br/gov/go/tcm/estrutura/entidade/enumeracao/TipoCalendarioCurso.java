package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoCalendarioCurso implements NumberEnumeracaoPersistente {

	CURSO_INICIO(1), CURSO_INSCRICAO_INICIO(2), CURSO_INSCRICAO_FIM(3);

	int valor;

	private TipoCalendarioCurso(int valor) {
		this.valor = valor;
	}
	
	

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "In�cio de curso";
		case 2:
			return "In�cio de inscri��o para curso";
		case 3:
			return "Fim de inscri��o para curso";
		default:
			return "";
		}
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

}
