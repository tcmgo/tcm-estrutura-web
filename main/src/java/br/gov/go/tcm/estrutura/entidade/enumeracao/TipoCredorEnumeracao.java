package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoCredorEnumeracao implements NumberEnumeracaoPersistente{

	PESSOA_FISICA(1), PESSOA_JURIDICA(2), EMPRESA_ESTRANGEIRA(3);
	int valor;

	TipoCredorEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoCredorEnumeracao.PESSOA_FISICA;
		case 2:
			return TipoCredorEnumeracao.PESSOA_JURIDICA;		
		case 3:
			return TipoCredorEnumeracao.EMPRESA_ESTRANGEIRA;
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Pessoa F�sica";
		case 2:
			return "Pessoa Jur�dica";
		case 3:
			return "Empresa Estrangeira";	
		default:
			return "";
		}
	}


}
