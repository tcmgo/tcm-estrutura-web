package br.gov.go.tcm.estrutura.analisador.balanco.arquivo;

import java.util.Date;

import br.gov.go.tcm.estrutura.analisador.arquivo.ArquivoAnalisador;

public class ArquivoIDE extends ArquivoAnalisador {

	public final static String MARCADOR_REGISTRO_IDE_BALANCO = "30";
	private Long codigoMunicipio;

	private Integer ano;

	private Date dataGeracao;

	private String nomeChefe;

	private String cpfChefe;

	private String logradouro;

	private String setorLogradouro;

	private String cidadeLogradouro;

	private String ufLogradouro;

	private String cepLogradouro;

	private String logradouroGestor;

	private String setorLogradouroGestor;

	private String cidadeLogradouroGestor;

	private String ufLogradouroGestor;

	private String cepLogradouroGestor;

	private String nomeContador;

	private String cpfContador;

	private String crcContador;

	private String ufCrcContador;

	private String nomeControleInterno;

	private String cpfControleInterno;

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getCepLogradouro() {
		return cepLogradouro;
	}

	public void setCepLogradouro(String cepLogradouro) {
		this.cepLogradouro = cepLogradouro;
	}

	public String getCepLogradouroGestor() {
		return cepLogradouroGestor;
	}

	public void setCepLogradouroGestor(String cepLogradouroGestor) {
		this.cepLogradouroGestor = cepLogradouroGestor;
	}

	public String getCidadeLogradouro() {
		return cidadeLogradouro;
	}

	public void setCidadeLogradouro(String cidadeLogradouro) {
		this.cidadeLogradouro = cidadeLogradouro;
	}

	public String getCidadeLogradouroGestor() {
		return cidadeLogradouroGestor;
	}

	public void setCidadeLogradouroGestor(String cidadeLogradouroGestor) {
		this.cidadeLogradouroGestor = cidadeLogradouroGestor;
	}

	public Long getCodigoMunicipio() {
		return codigoMunicipio;
	}

	public void setCodigoMunicipio(Long codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}

	public String getCpfChefe() {
		return cpfChefe;
	}

	public void setCpfChefe(String cpfChefe) {
		this.cpfChefe = cpfChefe;
	}

	public String getCpfContador() {
		return cpfContador;
	}

	public void setCpfContador(String cpfContador) {
		this.cpfContador = cpfContador;
	}

	public String getCpfControleInterno() {
		return cpfControleInterno;
	}

	public void setCpfControleInterno(String cpfControleInterno) {
		this.cpfControleInterno = cpfControleInterno;
	}

	public String getCrcContador() {
		return crcContador;
	}

	public void setCrcContador(String crcContador) {
		this.crcContador = crcContador;
	}

	public Date getDataGeracao() {
		return dataGeracao;
	}

	public void setDataGeracao(Date dataGeracao) {
		this.dataGeracao = dataGeracao;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getLogradouroGestor() {
		return logradouroGestor;
	}

	public void setLogradouroGestor(String logradouroGestor) {
		this.logradouroGestor = logradouroGestor;
	}

	public String getNomeChefe() {
		return nomeChefe;
	}

	public void setNomeChefe(String nomeChefe) {
		this.nomeChefe = nomeChefe;
	}

	public String getNomeContador() {
		return nomeContador;
	}

	public void setNomeContador(String nomeContador) {
		this.nomeContador = nomeContador;
	}

	public String getNomeControleInterno() {
		return nomeControleInterno;
	}

	public void setNomeControleInterno(String nomeControleInterno) {
		this.nomeControleInterno = nomeControleInterno;
	}

	public String getSetorLogradouro() {
		return setorLogradouro;
	}

	public void setSetorLogradouro(String setorLogradouro) {
		this.setorLogradouro = setorLogradouro;
	}

	public String getSetorLogradouroGestor() {
		return setorLogradouroGestor;
	}

	public void setSetorLogradouroGestor(String setorLogradouroGestor) {
		this.setorLogradouroGestor = setorLogradouroGestor;
	}

	public String getUfCrcContador() {
		return ufCrcContador;
	}

	public void setUfCrcContador(String ufCrcContador) {
		this.ufCrcContador = ufCrcContador;
	}

	public String getUfLogradouro() {
		return ufLogradouro;
	}

	public void setUfLogradouro(String ufLogradouro) {
		this.ufLogradouro = ufLogradouro;
	}

	public String getUfLogradouroGestor() {
		return ufLogradouroGestor;
	}

	public void setUfLogradouroGestor(String ufLogradouroGestor) {
		this.ufLogradouroGestor = ufLogradouroGestor;
	}
}