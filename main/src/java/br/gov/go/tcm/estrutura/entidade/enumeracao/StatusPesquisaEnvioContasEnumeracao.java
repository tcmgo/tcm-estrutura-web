package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum StatusPesquisaEnvioContasEnumeracao implements NumberEnumeracaoPersistente {
	TODOS(0), ENVIADO(1), NAO_ENVIADO(2), ENVIADO_FORA_PRAZO(3);

	int valor;

	StatusPesquisaEnvioContasEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Todos";
		case 1:
			return "Enviado";
		case 2:
			return "N�o Enviado";
		case 3:
			return "Enviado Fora Prazo";
		default:
			return "";
		}
	}
}
