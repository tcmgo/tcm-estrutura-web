package br.gov.go.tcm.estrutura.container.faces;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ControleFluxoExecucao {

	public static void encaminharFluxoServlet(FacesContext facesContext,
			String servlet) throws Exception {
		HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
		HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher(servlet);
		
		try {
			requestDispatcher.forward(request,response);
		} catch (ServletException e) {
			throw new Exception(e.getMessage());
		} catch (IOException e) {
			throw new Exception(e.getMessage());
		}
	}

}
