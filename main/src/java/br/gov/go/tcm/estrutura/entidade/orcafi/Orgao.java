package br.gov.go.tcm.estrutura.entidade.orcafi;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.gov.go.tcm.estrutura.entidade.base.EntidadeComposta;
import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoTipo;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoOrgaoEnumeracao;
import br.gov.go.tcm.estrutura.entidade.pk.PK;
import br.gov.go.tcm.estrutura.entidade.pk.orcafi.OrgaoPK;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;

@Entity
@Table(name = "Orgaos")
@Banco(nome = Banco.ORCAFI)
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.ALL, selectBeforeUpdate = true, dynamicUpdate = true)
@TypeDefs( { @TypeDef(name = "tipoOrgao", typeClass = EnumeracaoTipo.class, parameters = { @Parameter(name = "enumClassName", value = "br.gov.go.tcm.estrutura.entidade.enumeracao.TipoOrgaoEnumeracao") }) })
@NamedQueries( {
		@NamedQuery(name = "selecionarOrgaosPorDescricaoEMunicipio", 
				query = "SELECT o FROM Orgao o WHERE lower(o.descricao) like lower('%' || :descricao || '%') and  o.municipio= :municipio order by o.descricao asc"),
		@NamedQuery(name = "selecionarOrgaosPorMunicipio", 
				query = "SELECT o FROM Orgao o WHERE o.municipio= :municipio order by o.descricao asc"),		
		@NamedQuery(name = "selecionarOrgaosAtivos", query = "SELECT o FROM Orgao o WHERE o.ativo= :ativo") })
public class Orgao extends EntidadeComposta {

	private static final long serialVersionUID = -3084338973652554828L;

	@EmbeddedId
	@NotFound(action = NotFoundAction.IGNORE)
	private OrgaoPK orgaoPk;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "codMunicipio", insertable = false, updatable = false)
	@NotFound(action = NotFoundAction.IGNORE)
	private Municipio municipio;

	@Column(name = "descOrgao")
	private String descricao;

	@Column(name = "lograOrgao")
	private String logradouro;

	@Column(name = "setorLograOrgao")
	private String setor;

	@Column(name = "cepLograOrgao")
	private String cep;

	@Type(type = "tipoOrgao")
	@Column(name = "tipoOrgao")
	private TipoOrgaoEnumeracao tipoOrgao;

	@Column
	private Boolean ativo;

	public OrgaoPK getOrgaoPk() {
		if (orgaoPk == null)
			orgaoPk = new OrgaoPK();
		return orgaoPk;
	}

	public String getDescricao() {
		return descricao;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public TipoOrgaoEnumeracao getTipoOrgao() {
		return tipoOrgao;
	}

	@Override
	public PK getPK() {
		return getOrgaoPk();
	}

	public String getCep() {
		return cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public String getSetor() {
		return setor;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public void setOrgaoPk(OrgaoPK orgaoPk) {
		this.orgaoPk = orgaoPk;
	}

	public void setTipoOrgao(TipoOrgaoEnumeracao tipoOrgao) {
		this.tipoOrgao = tipoOrgao;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		if (this != null && this.getOrgaoPk() != null && this.getOrgaoPk().getCodigoMunicipio() != null
				&& this.getOrgaoPk().getCodigoOrgao() != null) {
			return (int) (this.getOrgaoPk().getCodigoMunicipio() + this.getOrgaoPk().getCodigoOrgao());
		}
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		try{
			Orgao orgao = (Orgao) obj;
			if (orgao != null && this != null && orgao.getOrgaoPk() != null && this.getOrgaoPk() != null
				&& orgao.getOrgaoPk().getCodigoMunicipio() != null && this.getOrgaoPk().getCodigoMunicipio() != null
				&& orgao.getOrgaoPk().getCodigoOrgao() != null && this.getOrgaoPk().getCodigoOrgao() != null) {
				return orgao.getOrgaoPk().getCodigoMunicipio().equals(this.getOrgaoPk().getCodigoMunicipio())
					&& orgao.getOrgaoPk().getCodigoOrgao().equals(this.getOrgaoPk().getCodigoOrgao());
			}
		}catch (ClassCastException e) {
			return false;
		}
		return false;
	}

}
