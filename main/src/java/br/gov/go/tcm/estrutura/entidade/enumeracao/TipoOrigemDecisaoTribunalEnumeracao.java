package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoOrigemDecisaoTribunalEnumeracao implements NumberEnumeracaoPersistente {

	PLENARIO(0), PRIMEIRA_CAMARA(1), SEGUNDA_CAMARA(2),PRESIDENTE(3);
	int valor;

	TipoOrigemDecisaoTribunalEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Plen�rio";
		case 1:
			return "1� Camara";
		case 2:
			return "2� C�mara";
		case 3:
			return "Presidente";
		default:
			return "";
		}
	}
}

