package br.gov.go.tcm.estrutura.analisador.conversor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import br.gov.go.tcm.estrutura.analisador.arquivo.ArquivoAnalisador;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoMensagemAnalisadorEnumeracao;

public class ConversorPadrao implements Conversor {

	private final static String TEXT_ENCODING = "ISO-8859-1";

	private ConversorOuvinte conversorOuvinte;

	public ConversorPadrao(ConversorOuvinte conversorOuvinte) {
		this.conversorOuvinte = conversorOuvinte;
	}

	public List<ArquivoAnalisador> converter(File f, NoRaiz noRaiz) {

		if (noRaiz == null)
			throw new RuntimeException("N� raiz deve ser especificado");

		List<String> conteudoArquivo = new ArrayList<String>();
		String line = null;

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(f), TEXT_ENCODING));
			while ((line = reader.readLine()) != null) {
				line = line.replaceAll("\\s+$", "");
				conteudoArquivo.add(line);
			}
			reader.close();
		} catch (IOException e) {
			conversorOuvinte.notificarMensagem(e.getMessage(),
					TipoMensagemAnalisadorEnumeracao.ERRO);
		}

		InterpretadorArvoreNos interpretadorArvoreNos = new InterpretadorArvoreNos(
				conversorOuvinte);
		return interpretadorArvoreNos.processarLinhasEGerarArquivos(
				conteudoArquivo, noRaiz);
	}

	public ConversorOuvinte getConversorOuvinte() {
		return conversorOuvinte;
	}
}
