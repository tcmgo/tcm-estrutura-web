package br.gov.go.tcm.estrutura.util.email;

public class OrigemMail {
	
	private String origem;
	private String destino;
	private String rotuloOrigem;
	private String rotuloDestino;

	public String getRotuloDestino() {
		return rotuloDestino;
	}

	public void setRotuloDestino(String rotuloDestino) {
		this.rotuloDestino = rotuloDestino;
	}

	public String getRotuloOrigem() {
		return rotuloOrigem;
	}

	public void setRotuloOrigem(String rotuloOrigem) {
		this.rotuloOrigem = rotuloOrigem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String sujeito) {
		this.origem = sujeito;
	}
}