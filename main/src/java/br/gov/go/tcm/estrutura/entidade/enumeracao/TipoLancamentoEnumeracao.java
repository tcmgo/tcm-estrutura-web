package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoLancamentoEnumeracao implements NumberEnumeracaoPersistente {

	DESPESAS_A_PAGAR_CONTRAPARTIDA(1), SERVICO_DIVIDA_A_PAGAR(
			2), PADRAO_2004(0);

	int valor;

	TipoLancamentoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {		
		case 0:
			return TipoLancamentoEnumeracao.PADRAO_2004;
		case 1:
			return TipoLancamentoEnumeracao.DESPESAS_A_PAGAR_CONTRAPARTIDA;
		case 2:
			return TipoLancamentoEnumeracao.SERVICO_DIVIDA_A_PAGAR;
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "";
		case 1:
			return "Lanšamento em Despesas a Pagar Contrapartida.";
		case 2:
			return "Lanšamento em Servišo da DÝvida a Pagar";
		}
		return "";
	}

}
