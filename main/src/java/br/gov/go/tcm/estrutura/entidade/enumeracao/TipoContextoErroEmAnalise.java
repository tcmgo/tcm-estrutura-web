package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum TipoContextoErroEmAnalise implements NumberEnumeracaoPersistente {
	
	SINTESE(0),EXAME_DE_CONTAS(1);
	
	int valor;
	
	TipoContextoErroEmAnalise(int valor){
		this.valor = valor;
	}
	
	public void setValorOrdinal(int valorOrdinal) {
		this.valor = valorOrdinal;
	}

	@Override
	public Number getValorOrdinal() {
		return valor;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 0:
			return TipoContextoErroEmAnalise.SINTESE;
		case 1:
			return TipoContextoErroEmAnalise.EXAME_DE_CONTAS;		
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "S�ntese";
		case 1:
			return "Exame de Contas";
		default:
			return "";
		}
	}

}
