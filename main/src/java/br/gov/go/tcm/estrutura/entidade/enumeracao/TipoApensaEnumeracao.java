package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoApensaEnumeracao implements StringEnumeracaoPersistente{

	NAO_APENSADO("0"),APENSADO("1"),JUNTADO("2");
	
	private String valor;
	
	TipoApensaEnumeracao(String valor){
		this.valor = valor;
	}
	
	public String getValorOrdinal(){
		return this.valor;
	}
	
	public EnumeracaoPersistente valor(){
		if(valor.equals("0")){
			return TipoApensaEnumeracao.NAO_APENSADO;
		} else if(valor.equals("1")){
			return TipoApensaEnumeracao.APENSADO;
		} else if(valor.equals("2")){
			return TipoApensaEnumeracao.JUNTADO;
		}		
		return null;
	}
	
	@Override
	public String toString(){
		if(valor.equals("0")){
			return "N�o Apensado";
		} else if(valor.equals("1")){
			return "Apensado";
		} else if(valor.equals("2")){
			return "Juntado";
		} else
			return "";
	}

}
