package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum TipoAnalisadorCadastroEnumeracao implements StringEnumeracaoPersistente{
	
	BALANCETES("BL"),PPA_LOA("PL"),BALANCO("BA"), HOMOLOGACAO_CONCURSO_PUBLICO("CP");
	String valor;
		
	TipoAnalisadorCadastroEnumeracao(String valor){
		this.valor = valor;
	}
	
	public String getValorOrdinal(){
		return this.valor;
	}

	public EnumeracaoPersistente valor(){
		if(valor.equals("BL")){
			return TipoAnalisadorCadastroEnumeracao.BALANCETES;
		} else if(valor.equals("PL")){
			return TipoAnalisadorCadastroEnumeracao.PPA_LOA;
		} else if(valor.equals("BA")){
			return TipoAnalisadorCadastroEnumeracao.BALANCO;
		} else if(valor.equals("CP")){
			return TipoAnalisadorCadastroEnumeracao.HOMOLOGACAO_CONCURSO_PUBLICO;
		}
		return null;
	}
	
	@Override
	public String toString(){
		if(valor.equals("BL")){
			return "Balancete";
		} else if(valor.equals("PL")){
			return "PPA_LOA";
		} else if(valor.equals("BA")){
			return "Balan�o";
		} else if(valor.equals("CP")){
			return "Homologa��o em Concurso P�blicos";
		} 
		return "";
	}
	
}

