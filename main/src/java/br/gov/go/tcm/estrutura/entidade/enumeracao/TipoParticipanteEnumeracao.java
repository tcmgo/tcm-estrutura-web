package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum TipoParticipanteEnumeracao implements NumberEnumeracaoPersistente {
	JURISDICIONADO(1), TCM(2), PARCEIRO(3);
	
	private int valor;

	TipoParticipanteEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Servidor Municipal / Assessor";
		case 2:
			return "Servidor TCM";
		case 3:
			return "Parceiro / Entidade";
		default:
			return "";
		}
	}
	

	public TipoParticipanteEnumeracao obterPorValor(int i) {
		switch (i) {
		case 1:
			 return TipoParticipanteEnumeracao.JURISDICIONADO;

		case 2:
			 return TipoParticipanteEnumeracao.TCM;

		case 3:
			 return TipoParticipanteEnumeracao.PARCEIRO;


		default:
			return null;
		}

	}
	
	public boolean eJurisidicionado(){
		return valor == JURISDICIONADO.getValorOrdinal().intValue();
	}
	
	public boolean eTcm(){
		return valor == TCM.getValorOrdinal().intValue();
	}
	
	public boolean eParceiro(){
		return valor == PARCEIRO.getValorOrdinal().intValue();
	}
	
	

}
