package br.gov.go.tcm.estrutura.entidade.ajudante;

import java.math.BigDecimal;
import java.sql.SQLException;

import org.hibernate.lob.SerializableClob;

public abstract class AjudanteConversao {

	public enum TipoConversao {
		String, Inteiro, Double, Float, Boolean, Short, Long, BigDecimal, Character;
	}

	public static Object conveter(Object objeto, TipoConversao tipoConversao) {
		return converter(objeto, tipoConversao);
	}

	public static Object converter(Object objeto, TipoConversao tipoConversao) {
		if (objeto == null)
			return null;
		if (objeto instanceof Short)
			return convertShortPara(objeto, tipoConversao);
		else if (objeto instanceof Integer)
			return convertInteiroPara(objeto, tipoConversao);
		else if (objeto instanceof Double)
			return convertDoublePara(objeto, tipoConversao);
		else if (objeto instanceof String)
			return convertStringPara(objeto, tipoConversao);
		else if (objeto instanceof Boolean)
			return convertBooleanPara(objeto, tipoConversao);
		else if (objeto instanceof BigDecimal)
			return convertBigDecimalPara(objeto, tipoConversao);
		else if (objeto instanceof SerializableClob) {
			return convertClobPara(objeto, tipoConversao);
		} else if (objeto instanceof Character) {
			return convertCharacterPara(objeto, tipoConversao);
		} else
			throw new AjudanteExcecao("N�o existe conversor de " + objeto.getClass().getSimpleName() + " para "
					+ tipoConversao.name());
	}

	private static Object convertCharacterPara(Object objeto, TipoConversao tipoConversao) {
		if (TipoConversao.Inteiro.equals(tipoConversao)) {
			return Integer.valueOf(objeto.toString());
		}
		return objeto;
	}

	private static Object convertClobPara(Object objeto, TipoConversao tipoConversao) {
		if (TipoConversao.String.equals(tipoConversao)) {
			try {
				SerializableClob clob = (SerializableClob) objeto;
				return clob.getSubString(1l, (int) clob.length());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else
			throw new AjudanteExcecao("N�o existe conversor de " + objeto.getClass().getSimpleName() + " para "
					+ tipoConversao.name());
		return null;
	}

	public static void main(String[] args) {
		AjudanteConversao.converter(new Long(2), TipoConversao.Inteiro);
	}

	private static Object convertBooleanPara(Object objeto, TipoConversao tipoConversao) {

		if (TipoConversao.Boolean.equals(tipoConversao)) {
			return objeto;
		} else if (TipoConversao.Inteiro.equals(tipoConversao)) {
			if ((Boolean) objeto) {
				return new Integer(1);
			}
			return new Integer(0);
		}

		return objeto;
	}

	private static Object convertShortPara(Object objeto, TipoConversao tipoConversao) {

		if (TipoConversao.Inteiro.equals(tipoConversao)) {
			return new Short((Short) objeto).intValue();
		} else if (TipoConversao.Long.equals(tipoConversao)) {
			return new Short((Short) objeto).longValue();
		} else if (TipoConversao.Double.equals(tipoConversao)) {
			return Short.valueOf((Short) objeto).doubleValue();
		}

		return objeto;
	}

	private static Object convertInteiroPara(Object objeto, TipoConversao tipoConversao) {

		if (TipoConversao.Short.equals(tipoConversao)) {
			return new Short(((Integer) objeto).shortValue());
		} else if (TipoConversao.Long.equals(tipoConversao)) {
			return new Long(((Integer) objeto).longValue());
		} else if (TipoConversao.Double.equals(tipoConversao)) {
			return new Double(((Integer) objeto).doubleValue());
		}

		return objeto;
	}

	private static Object convertDoublePara(Object objeto, TipoConversao tipoConversao) {

		if (TipoConversao.Short.equals(tipoConversao)) {
			return new Double((Double) objeto).shortValue();
		} else if (TipoConversao.Long.equals(tipoConversao)) {
			return new Double((Double) objeto).longValue();
		} else if (TipoConversao.Inteiro.equals(tipoConversao)) {
			return new Double((Double) objeto).intValue();
		} else if (TipoConversao.BigDecimal.equals(tipoConversao)) {
			return new BigDecimal((Double) objeto);

		}

		return objeto;
	}

	private static Object convertBigDecimalPara(Object objeto, TipoConversao tipoConversao) {

		if (TipoConversao.Short.equals(tipoConversao)) {
			return ((BigDecimal) objeto).shortValue();
		} else if (TipoConversao.Long.equals(tipoConversao)) {
			return ((BigDecimal) objeto).longValue();
		} else if (TipoConversao.Inteiro.equals(tipoConversao)) {
			return ((BigDecimal) objeto).intValue();
		} else if (TipoConversao.Double.equals(tipoConversao)) {
			return ((BigDecimal) objeto).doubleValue();
		}

		return objeto;
	}

	private static Object convertStringPara(Object objeto, TipoConversao tipoConversao) {

		if (TipoConversao.Short.equals(tipoConversao)) {
			return new Double((String) objeto).shortValue();
		} else if (TipoConversao.Long.equals(tipoConversao)) {
			return new Double((String) objeto).longValue();
		} else if (TipoConversao.Inteiro.equals(tipoConversao)) {
			return new Double((String) objeto).intValue();
		}

		return objeto;
	}

}