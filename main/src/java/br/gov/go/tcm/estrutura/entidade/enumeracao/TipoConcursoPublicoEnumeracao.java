package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum TipoConcursoPublicoEnumeracao implements NumberEnumeracaoPersistente {
	FUNCIONARIOS(1), ESTAGIARIOS(2);

	private int valor;

	TipoConcursoPublicoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Funcionários";
		case 2:
			return "Estagiários";
		default:
			return "";
		}
	}
}
