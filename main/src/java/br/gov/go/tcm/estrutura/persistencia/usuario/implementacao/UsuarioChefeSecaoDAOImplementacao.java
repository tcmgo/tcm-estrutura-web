package br.gov.go.tcm.estrutura.persistencia.usuario.implementacao;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.entidade.usuario.UsuarioChefeSecao;
import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.usuario.contrato.UsuarioChefeSecaoDAO;

@Repository("UsuarioChefeSecaoDAO")
public class UsuarioChefeSecaoDAOImplementacao extends GenericoDAOImplementacao implements UsuarioChefeSecaoDAO {

	private static final long serialVersionUID = 1692380139007613989L;

	@Autowired
	public UsuarioChefeSecaoDAOImplementacao(@Qualifier("sessionFactoryUsuario") SessionFactory factory,
			@Qualifier("hibernateTemplateUsuario") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public UsuarioChefeSecao obterUsuarioChefeSecao(final Long idUsuario, final Long codigoSecao) {

		return (UsuarioChefeSecao) getHibernateTemplate().execute(new HibernateCallback() {

			@Override
			public Object doInHibernate(final Session session) throws HibernateException, SQLException {
				
				final String hql = "select usuarioChefe from UsuarioChefeSecao as usuarioChefe where usuarioChefe.usuarioChefeSecaoPK.usuarioId = ? and usuarioChefe.usuarioChefeSecaoPK.secaoId = ?";
				
				final Query q = session.createQuery(hql);
				
				q.setParameter(0, idUsuario);
				
				q.setParameter(1, codigoSecao);
				
				return q.uniqueResult();
			}
		});
	}

}
