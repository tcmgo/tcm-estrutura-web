package br.gov.go.tcm.estrutura.persistencia.ajudante.implementacao;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.entidade.transiente.SequencialGED;
import br.gov.go.tcm.estrutura.persistencia.ajudante.contrato.SequencialGEDDAO;
import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;

@Repository
public class SequencialGEDDAOImplementacao extends GenericoDAOImplementacao implements SequencialGEDDAO {

	private static final long serialVersionUID = -1694928528719000387L;

	@Autowired
	public SequencialGEDDAOImplementacao(@Qualifier("sessionFactory") SessionFactory factory,
			@Qualifier("hibernateTemplate") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public synchronized SequencialGED obterUltimoSequencialGED() {
		return (SequencialGED) getHibernateTemplate(getSessionFactoryWeb()).execute(new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				Query query = session.createSQLQuery("exec UltimoSequencialGED");

				String sequencial = null;

				sequencial = (String) query.uniqueResult();

				SequencialGED sequencialGED = new SequencialGED();
				sequencialGED.setSequencial(sequencial);
				return sequencialGED;
			}
		});
	}
}
