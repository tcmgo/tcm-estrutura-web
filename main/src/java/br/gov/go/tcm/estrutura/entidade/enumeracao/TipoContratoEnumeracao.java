package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoContratoEnumeracao implements NumberEnumeracaoPersistente {

	OUTROS(0), ADMISSAO(1), PRESTACAO_SERVICO(2), EMPREGO_PUBLICO(3), PRAZO_DETERMINADO(4), CONCURSADO(5), PROCESSO_SELETIVO_SIMPLIFICADO(
			6);
	int valor;

	TipoContratoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 0:
			return TipoContratoEnumeracao.OUTROS;
		case 1:
			return TipoContratoEnumeracao.ADMISSAO;
		case 2:
			return TipoContratoEnumeracao.PRESTACAO_SERVICO;
		case 3:
			return TipoContratoEnumeracao.EMPREGO_PUBLICO;
		case 4:
			return TipoContratoEnumeracao.PRAZO_DETERMINADO;
		case 5:
			return TipoContratoEnumeracao.CONCURSADO;
		case 6:
			return TipoContratoEnumeracao.PROCESSO_SELETIVO_SIMPLIFICADO;

		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Outros";
		case 1:
			return "Admiss�o";
		case 2:
			return "Presta��o de Servi�os";
		case 3:
			return "Emprego P�blico";
		case 4:
			return "Prazo Determinado";
		case 5:
			return "Concursado";
		case 6:
			return "Processo Seletivo Simplificado";
		default:
			return "";
		}
	}
}
