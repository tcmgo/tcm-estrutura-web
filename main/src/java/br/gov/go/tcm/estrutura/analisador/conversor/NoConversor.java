package br.gov.go.tcm.estrutura.analisador.conversor;

import java.util.ArrayList;
import java.util.List;

import br.gov.go.tcm.estrutura.analisador.arquivo.ArquivoAnalisador;
import br.gov.go.tcm.estrutura.analisador.conversor.regra.RegraAnalisador;

public class NoConversor extends NoRaiz{
	
	private Class<? extends ArquivoAnalisador> classeDestino;
	private Short tamanhoLinha;
	private List<RegraAnalisador> regras;
	private String marcadorInicial;
	private Boolean opcional;
	private String[] chaves;
	private NoRaiz noPai;
	
	public String[] getChaves() {
		return chaves;
	}

	public List<RegraAnalisador> obterRegras() {
		return getRegras();
	}
		
	public void adicionarRegra(final RegraAnalisador regraAnalisador) {
		obterRegras().add(regraAnalisador);		
	}
	
	public String getMarcadorInicial() {
		return marcadorInicial;
	}
	
	public void setMarcadorInicial(final String marcadorInicial) { 
		this.marcadorInicial = marcadorInicial;
	}

	public void setRegras(final List<RegraAnalisador> regras) {
		this.regras = regras;
	}

	public Short getTamanhoLinha() {
		return tamanhoLinha;
	}

	public void setTamanhoLinha(final Short tamanhoLinha) {
		this.tamanhoLinha = tamanhoLinha;
	}

	public Class<? extends ArquivoAnalisador> getClasseDestino() {
		return classeDestino;
	}

	public void setClasseDestino(final Class<? extends ArquivoAnalisador> classeDestino) {
		this.classeDestino = classeDestino;
	}
	
	public Boolean getOpcional() {
		if(opcional == null){
			opcional = Boolean.FALSE;
		}
		return opcional;
	}

	public void setOpcional(final Boolean opcional) {
		this.opcional = opcional;
	}

	public void setChaves(final String[] chaves) {
		this.chaves = chaves;		
	}

	public NoRaiz getNoPai() {		
		return noPai;
	}

	public void setNoPai(final NoRaiz noRaiz) {
		this.noPai = noRaiz;
	}

	public List<RegraAnalisador> getRegras() {
		if(regras == null){
			regras = new ArrayList<RegraAnalisador>();
		}
		return regras;
	}
}