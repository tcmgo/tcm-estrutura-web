package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoPessoaNumericoEnumeracao implements NumberEnumeracaoPersistente {

	FISICA(1), JURIDICA(2);
	
	int valor;

	TipoPessoaNumericoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoPessoaNumericoEnumeracao.FISICA;
		case 2:
			return TipoPessoaNumericoEnumeracao.JURIDICA;
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "F�sica";
		case 2:
			return "Jur�dica";
		default:
			return "";
		}
	}
}
