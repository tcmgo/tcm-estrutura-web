package br.gov.go.tcm.estrutura.entidade.transiente;

import java.io.Serializable;
import java.util.Date;

public class InfoFases implements Serializable {

	private static final long serialVersionUID = 5727351971371135823L;

	private Number nrFase;
	private String assunto;
	private Date dataAutuacao;
	private String responsavel;
	private String teor;
	private Date dtJulgamento;
	private Number anoResolucao;
	private Number nrResolucao;
	private String parecerResolucao;
	private Number anoAcordao;
	private Number nrAcordao;
	private String parecerAcordao;
	private Integer principal;
	private boolean ehPrincipal;
	private Number julgadoId;

	public Number getNrFase() {
		return nrFase;
	}

	public void setNrFase(Number nrFase) {
		this.nrFase = nrFase;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public Date getDataAutuacao() {
		return dataAutuacao;
	}

	public void setDataAutuacao(Date dataAutuacao) {
		this.dataAutuacao = dataAutuacao;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public String getTeor() {
		return teor;
	}

	public void setTeor(String teor) {
		this.teor = teor;
	}

	public Date getDtJulgamento() {
		return dtJulgamento;
	}

	public void setDtJulgamento(Date dtJulgamento) {
		this.dtJulgamento = dtJulgamento;
	}

	public Number getAnoResolucao() {
		return anoResolucao;
	}

	public void setAnoResolucao(Number anoResolucao) {
		this.anoResolucao = anoResolucao;
	}

	public Number getNrResolucao() {
		return nrResolucao;
	}

	public void setNrResolucao(Number nrResolucao) {
		this.nrResolucao = nrResolucao;
	}

	public String getParecerResolucao() {
		return parecerResolucao;
	}

	public void setParecerResolucao(String parecerResolucao) {
		this.parecerResolucao = parecerResolucao;
	}

	public Number getAnoAcordao() {
		return anoAcordao;
	}

	public void setAnoAcordao(Number anoAcordao) {
		this.anoAcordao = anoAcordao;
	}

	public Number getNrAcordao() {
		return nrAcordao;
	}

	public void setNrAcordao(Number nrAcordao) {
		this.nrAcordao = nrAcordao;
	}

	public String getParecerAcordao() {
		return parecerAcordao;
	}

	public void setParecerAcordao(String parecerAcordao) {
		this.parecerAcordao = parecerAcordao;
	}

	public Integer getPrincipal() {
		return principal;
	}

	public void setPrincipal(Integer principal) {
		this.principal = principal;
	}
	
	public Number getJulgadoId() {
		return julgadoId;
	}

	public void setJulgadoId(Number julgadoId) {
		this.julgadoId = julgadoId;
	}

	public boolean getEhPrincipal() {
		if( principal == 1 )
			ehPrincipal = true;
		else
			ehPrincipal = false;
		
		return ehPrincipal;
	}
	
	
}