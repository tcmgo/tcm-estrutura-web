package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum ServerNameEnumeracao {
	
	LOCAL_HOST("localhost"),
	HOMOLOGACAO("homologa.tcm.go.gov.br"),
	PRODUCAO("tcm.go.gov.br");
	
	private final String serverName;

	ServerNameEnumeracao(String serverName) {
		this.serverName = serverName;
	}

	public String getServerName() {
		return serverName;
	}
	
	@Override
	public String toString() {
		return this.getServerName();
	}

}
