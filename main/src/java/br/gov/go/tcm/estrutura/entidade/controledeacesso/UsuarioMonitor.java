package br.gov.go.tcm.estrutura.entidade.controledeacesso;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;

@Entity
@Table(name = "Usuario")
@Banco(nome = Banco.USUARIO)
@NamedQueries({ @NamedQuery(name = "selecionarUsuarioMonitorPorNome", query = "SELECT u FROM UsuarioMonitor u WHERE lower(u.nomeCompleto) like lower('%' || :nome || '%') "),
		@NamedQuery(name = "selecionarUsuarioMonitorPorLoginSenha", query = "SELECT u FROM UsuarioMonitor u WHERE u.nomeUsuario =:login and u.senha =:senha"), @NamedQuery(name = "selecionarUsuarioMonitorPorLogin", query = "SELECT u FROM UsuarioMonitor u WHERE u.nomeUsuario =:login "),
		@NamedQuery(name = "selecionarUsuarioPorMatricula", query = "SELECT um FROM UsuarioMonitor um WHERE um.matricula = :matricula ") })
@XmlRootElement(name = "person")
public class UsuarioMonitor extends EntidadePadrao {

	private static final long serialVersionUID = 6326518714536518679L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "UsuarioID")
	private Long id;

	@Column(name = "Matricula")
	private String matricula;

	@Column(name = "NomeCriador")
	private String nomeCriador;

	@Column(name = "NomeCompleto")
	private String nomeCompleto;

	@Column(name = "NomeUsuario")
	private String nomeUsuario;

	@Column(name = "senha")
	private String senha;

	@Column(name = "Master")
	private String master;

	@Column(name = "Observacao")
	private String observacao;

	@Column(name = "SecaoID")
	private String codigoSecao;

	@Column(name = "Situacao")
	private Boolean ativo;

	@Column(name = "ResetSenha")
	private Boolean resetarSenha;

	@Column(name = "tipoUsuarioID")
	private Integer tipoUsuario;

	@Column(name = "EMail")
	private String email;

	public Integer getTipoUsuario() {

		return this.tipoUsuario;
	}

	public void setTipoUsuario(final Integer tipoUsuario) {

		this.tipoUsuario = tipoUsuario;
	}

	@Override
	public Long getId() {

		return this.id;
	}

	public String getMatricula() {

		return this.matricula;
	}

	public Boolean getAtivo() {

		return this.ativo;
	}

	public Boolean getResetarSenha() {

		return this.resetarSenha;
	}

	public String getMaster() {

		return this.master;
	}

	public String getNomeCompleto() {

		return this.nomeCompleto;
	}

	public String getNomeCriador() {

		return this.nomeCriador;
	}

	public String getNomeUsuario() {

		return this.nomeUsuario;
	}

	public String getObservacao() {

		return this.observacao;
	}

	public String getCodigoSecao() {

		return this.codigoSecao;
	}

	public String getSenha() {

		return this.senha;
	}

	@Override
	public void setId(final Long id) {

		this.id = id;
	}

	public void setMatricula(final String matricula) {

		this.matricula = matricula;
	}

	public void setAtivo(final Boolean ativo) {

		this.ativo = ativo;
	}

	public void setMaster(final String master) {

		this.master = master;
	}

	public void setNomeCompleto(final String nomeCompleto) {

		this.nomeCompleto = nomeCompleto;
	}

	public void setNomeCriador(final String nomeCriador) {

		this.nomeCriador = nomeCriador;
	}

	public void setNomeUsuario(final String nomeUsuario) {

		this.nomeUsuario = nomeUsuario;
	}

	public void setObservacao(final String observacao) {

		this.observacao = observacao;
	}

	public void setCodigoSecao(final String codigoSecao) {

		this.codigoSecao = codigoSecao;
	}

	public void setSenha(final String senha) {

		this.senha = senha;
	}

	public void setResetarSenha(final Boolean b) {

		this.resetarSenha = b;
	}

	/**
	 * Retorna o valor do atributo <code>email</code>
	 *
	 * @return <code>String</code>
	 */
	public String getEmail() {

		return this.email;
	}

	/**
	 * Define o valor do atributo <code>email</code>.
	 *
	 * @param email
	 */
	public void setEmail(final String email) {

		this.email = email;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.id == null ) ? 0 : this.id.hashCode() );
		return result;
	}

	@Override
	public boolean equals(final Object obj) {

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final UsuarioMonitor other = (UsuarioMonitor) obj;
		if (this.id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!this.id.equals(other.id)) {
			return false;
		}
		return true;
	}

}
