package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoRegiaoEnumeracao implements NumberEnumeracaoPersistente{
	
	TODAS(0),REGIAO_1(1), REGIAO_2(2), REGIAO_3(3),REGIAO_4(4), REGIAO_5(5), REGIAO_6(6);
	
	int valor;

	TipoRegiaoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Todas";
		case 1:
			return "1� Regi�o";
		case 2:
			return "2� Regi�o";
		case 3:
			return "3� Regi�o";
		case 4:
			return "4� Regi�o";
		case 5:
			return "5� Regi�o";
		case 6:
			return "6� Regi�o";
		default:
			return "";
		}
	}
	
}
