package br.gov.go.tcm.estrutura.entidade.enumeracao;

public interface NumeroEnumeracao extends EnumeracaoPersistente{
	public Number getValorOrdinal();
}
