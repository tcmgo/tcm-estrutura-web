package br.gov.go.tcm.estrutura.persistencia.historico.implementacao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.historico.contrato.HistoricoWebDAO;

/**
 * Classe DAO que define opera��es para a entidade {@link br.gov.go.tcm.estrutura.entidade.web.HistoricoWeb}
 */
@Repository
public class HistoricoWebDAOImplementacao extends GenericoDAOImplementacao implements HistoricoWebDAO {

	private static final long serialVersionUID = -4963973806682129060L;

	@Autowired
	public HistoricoWebDAOImplementacao(@Qualifier("sessionFactory") SessionFactory factory,
			@Qualifier("hibernateTemplate") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}
}
