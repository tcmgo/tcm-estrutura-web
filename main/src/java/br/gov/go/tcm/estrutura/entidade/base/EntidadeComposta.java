package br.gov.go.tcm.estrutura.entidade.base;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import br.gov.go.tcm.estrutura.entidade.pk.PK;

public abstract class EntidadeComposta implements Entidade {

	private static final long serialVersionUID = -2825362496316748315L;

	public final static String PK = "pk";

	public abstract PK getPK();

	public Boolean isTransiente() {
		return this.getPK() == null;
	}

	@Override
	public String toString() {
		StringBuffer sbRetorno = new StringBuffer();
		PK pk = getPK();

		if (pk == null)
			return "null";

		Class<? extends PK> objectClass = pk.getClass();
		Method[] methods = objectClass.getDeclaredMethods();
		for (int i = 0; i < methods.length; i++) {
			Method getter = methods[i];
			if (getter.getName().indexOf("get") == 0) {
				try {
					Object retorno = methods[i].invoke(pk, new Object[0]);
					if (retorno == null) {
						sbRetorno.append("null: null, ");
					} else {
						sbRetorno.append(retorno.getClass().getSimpleName() + ": " + retorno.toString()).append(", ");
					}
				} catch (IllegalArgumentException e) {
					e.getMessage();
				} catch (IllegalAccessException e) {
					e.getMessage();
				} catch (InvocationTargetException e) {
					e.getMessage();
				}
			}
		}

		return sbRetorno.toString();
	}

	public static Boolean algumMembroChaveNulo(EntidadeComposta entidadeComposta) throws EntidadeInvalidaException {
		PK pk = entidadeComposta.getPK();

		if (pk == null)
			return true;

		Class<? extends PK> objectClass = pk.getClass();
		Method[] methods = objectClass.getDeclaredMethods();

		for (int i = 0; i < methods.length; i++) {
			Method getter = methods[i];
			if (getter.getName().indexOf("get") == 0) {
				try {
					Object retorno = methods[i].invoke(pk, new Object[0]);
					if (retorno == null)
						return true;
					continue;
				} catch (IllegalArgumentException e) {
					throw new EntidadeInvalidaException(e.getMessage());
				} catch (IllegalAccessException e) {
					throw new EntidadeInvalidaException(e.getMessage());
				} catch (InvocationTargetException e) {
					throw new EntidadeInvalidaException(e.getMessage());
				}
			}
		}

		return false;
	}

}
