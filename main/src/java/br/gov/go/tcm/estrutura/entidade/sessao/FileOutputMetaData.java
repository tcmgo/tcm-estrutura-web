package br.gov.go.tcm.estrutura.entidade.sessao;

public class FileOutputMetaData {
	
	private String contentType;
	
	private String nome;
	
	private String extensao;

	public String getContentType() {
		return contentType;
	}

	public String getExtensao() {
		return extensao;
	}

	public String getNome() {
		return nome;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
