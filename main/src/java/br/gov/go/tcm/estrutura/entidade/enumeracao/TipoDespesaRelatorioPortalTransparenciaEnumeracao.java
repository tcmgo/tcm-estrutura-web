package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoDespesaRelatorioPortalTransparenciaEnumeracao implements NumberEnumeracaoPersistente {
	
	PESSOAL(1), DIARIAS_PASSAGENS(2), INVESTIMENTOS_BENS_SERVICOS(3);
	
	int valor;
	
	TipoDespesaRelatorioPortalTransparenciaEnumeracao(int valor) {
		this.valor = valor;
	}
	
	public Number getValorOrdinal(){
		return this.valor;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoDespesaRelatorioPortalTransparenciaEnumeracao.PESSOAL;
		case 2:
			return TipoDespesaRelatorioPortalTransparenciaEnumeracao.DIARIAS_PASSAGENS;
		case 3:
			return TipoDespesaRelatorioPortalTransparenciaEnumeracao.INVESTIMENTOS_BENS_SERVICOS;
		
		}
		return null;
	}
	
	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Despesa de Pessoal";
		case 2:
			return "Di�rias e Passagens";
		case 3:
			return "Investimentos, Bens e Servi�os";
		default:
			return "";
		}		
	}

}
