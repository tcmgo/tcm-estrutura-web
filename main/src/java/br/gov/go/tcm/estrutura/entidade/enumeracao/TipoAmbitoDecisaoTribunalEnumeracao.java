package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoAmbitoDecisaoTribunalEnumeracao implements NumberEnumeracaoPersistente {

	INTERNA(0), EXTERNA(1), INTERNA_E_EXTERNA(2);
	int valor;

	TipoAmbitoDecisaoTribunalEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Interna";
		case 1:
			return "Externa";
		case 2:
			return "Interna e Externa";
		default:
			return "";
		}
	}
}
