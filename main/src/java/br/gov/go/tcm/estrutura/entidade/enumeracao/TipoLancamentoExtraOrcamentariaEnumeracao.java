package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum TipoLancamentoExtraOrcamentariaEnumeracao implements
		StringEnumeracaoPersistente {

	DEPOSITOS_CONSIGNACOES("01"), DEBITOS_TESOURARIA("02"), ATIVO_REALIZAVEL(
			"03"), TRANSFERENCIAS_FINANCEIRAS("04"), OUTROS("99");

	String valor;

	TipoLancamentoExtraOrcamentariaEnumeracao(String valor) {
		this.valor = valor;
	}

	public String getValorOrdinal() {
		return this.valor;
	}


	@Override
	public String toString() {
		if (valor.equals("01")) {
			return "Dep�sitos / Consigna��es";
		} else if (valor.equals("02")) {
			return "D�bitos / Tesouraria";
		} else if (valor.equals("03")) {
			return "Ativo / Realiz�vel";
		} else if (valor.equals("04")) {
			return "Transfer�ncias Financeiras";
		} else if (valor.equals("99")) {
			return "Outros";
		}
		return "";
	}
}
