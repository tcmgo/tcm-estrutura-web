package br.gov.go.tcm.estrutura.analisador.balanco.processador;

import java.io.File;
import java.util.List;

import br.gov.go.tcm.estrutura.analisador.arquivo.ArquivoAnalisador;
import br.gov.go.tcm.estrutura.analisador.balanco.arquivo.ArquivoIDE;
import br.gov.go.tcm.estrutura.analisador.conversor.NoConversor;
import br.gov.go.tcm.estrutura.analisador.conversor.NoRaiz;
import br.gov.go.tcm.estrutura.analisador.conversor.ProcessadorConversao;
import br.gov.go.tcm.estrutura.analisador.conversor.regra.RegraAnalisadorPadrao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.NumberEnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoAnalisadorEnumeracao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoArquivoAnalisadorBalancoEnumeracao;

public class ProcessadorConversaoIDE extends ProcessadorConversao {

	@Override
	public TipoAnalisadorEnumeracao getTipoAnalisadorEnum() {
		return TipoAnalisadorEnumeracao.BALANCO;
	}

	@Override
	public NumberEnumeracaoPersistente getTipoArquivoAnalisador() {
		return TipoArquivoAnalisadorBalancoEnumeracao.IDE;
	}

	@Override
	public List<ArquivoAnalisador> converter(final File arquivo) {
		NoConversor noConversor = new NoConversor();
		noConversor.setClasseDestino(ArquivoIDE.class);
		noConversor.setMarcadorInicial(ArquivoIDE.MARCADOR_REGISTRO_IDE_BALANCO);
		noConversor.setOpcional(Boolean.FALSE);
		noConversor.setTamanhoLinha((short) 405);
		noConversor.setChaves(new String[] { "tipoRegistro" });

		noConversor.adicionarRegra(new RegraAnalisadorPadrao(1, 2,
				"tipoRegistro"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(3, 6,
				"codigoMunicipio"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(7, 10,
				"ano"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(11, 18,
				"dataGeracao"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(19, 63,
				"nomeChefe"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(64, 74,
				"cpfChefe"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(75, 124,
				"logradouro"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(125, 144,
				"setorLogradouro"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(145, 164,
				"cidadeLogradouro"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(165, 166,
				"ufLogradouro"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(167, 174,
				"cepLogradouro"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(175, 224,
				"logradouroGestor"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(225, 244,
				"setorLogradouroGestor"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(245, 264,
				"cidadeLogradouroGestor"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(265, 266,
				"ufLogradouroGestor"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(267, 274,
				"cepLogradouroGestor"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(275, 319,
				"nomeContador"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(320, 330,
				"cpfContador"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(331, 341,
				"crcContador"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(342, 343,
				"ufCrcContador"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(344, 388,
				"nomeControleInterno"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(389, 399,
				"cpfControleInterno"));
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(400, 405,
				"numeroSequencial"));
		
		final NoRaiz noRaiz = new NoRaiz();
		noRaiz.adicionarFilho(noConversor);

		return getConversorPadrao().converter(arquivo, noRaiz);

	}

}
