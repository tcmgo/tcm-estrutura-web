package br.gov.go.tcm.estrutura.entidade.transiente;

import java.io.InputStream;

public class Arquivo {
	
	private String nome;
	
	private InputStream stream;
	
	private String sufixo;

	public InputStream getStream() {
		return stream;
	}

	public String getSufixo() {
		return sufixo;
	}

	public void setStream(InputStream stream) {
		this.stream = stream;
	}

	public void setSufixo(String sufixo) {
		this.sufixo = sufixo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
