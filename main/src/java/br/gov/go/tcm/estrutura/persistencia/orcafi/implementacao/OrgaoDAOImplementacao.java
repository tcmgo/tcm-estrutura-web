package br.gov.go.tcm.estrutura.persistencia.orcafi.implementacao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoOrgaoEnumeracao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoPesquisaOrgaoEnumeracao;
import br.gov.go.tcm.estrutura.entidade.orcafi.Municipio;
import br.gov.go.tcm.estrutura.entidade.orcafi.Orgao;
import br.gov.go.tcm.estrutura.entidade.pk.orcafi.OrgaoPK;
import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.orcafi.contrato.OrgaoDAO;

@Repository("OrgaoDAO")
public class OrgaoDAOImplementacao extends GenericoDAOImplementacao implements OrgaoDAO {

	private static final long serialVersionUID = 5386181358064612542L;

	@Autowired
	public OrgaoDAOImplementacao(@Qualifier("sessionFactoryOrcafi") SessionFactory factory,
			@Qualifier("hibernateTemplateOrcafi") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}

	@SuppressWarnings("unchecked")
	public List<Orgao> obterLstOrgaoPorDescricaoEMunicipio(String descricao, Municipio municipio) {
		String[] parametro = { "descricao", "municipio" };
		Object[] objeto = { descricao, municipio };

		return getHibernateTemplate(getSessionFactoryOrcafi()).findByNamedQueryAndNamedParam("selecionarOrgaosPorDescricaoEMunicipio",
				parametro, objeto);
	}

	@SuppressWarnings("unchecked")
	public List<Orgao> obterLstOrgaoPorMunicipio(Municipio municipio) {
		String parametro = "municipio";
		Object objeto = municipio;

		return getHibernateTemplate(getSessionFactoryOrcafi()).findByNamedQueryAndNamedParam("selecionarOrgaosPorMunicipio", parametro, objeto);
	}

	@SuppressWarnings("unchecked")
	public List<Orgao> obterLstOrgaosAtivos() {
		String parametro = "ativo";
		Object objeto = Boolean.TRUE;

		return getHibernateTemplate(getSessionFactoryOrcafi()).findByNamedQueryAndNamedParam("selecionarOrgaosAtivos", parametro, objeto);
	}

	public Integer selecionarTotalDePossiveisEntregasBalanceteCompleto(TipoPesquisaOrgaoEnumeracao tipoPesquisaOrgao) {
		DetachedCriteria c = DetachedCriteria.forClass(Orgao.class);
		c.add(Restrictions.eq("ativo", Boolean.TRUE));

		if (tipoPesquisaOrgao.equals(TipoPesquisaOrgaoEnumeracao.EXECUTIVO)) {
			c.add(Restrictions.eq("tipoOrgao", TipoOrgaoEnumeracao.ADMINISTRACAO_DIRETA_PREFEITURA));
		} else if (tipoPesquisaOrgao.equals(TipoPesquisaOrgaoEnumeracao.LEGISLATIVO_AUTARQUIA)) {
			c.add(Restrictions.ne("tipoOrgao", TipoOrgaoEnumeracao.ADMINISTRACAO_DIRETA_PREFEITURA));
		}

		return getHibernateTemplate(getSessionFactoryOrcafi()).findByCriteria(c).size();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Orgao obterOrgaoPorId(final OrgaoPK pk) {
		return (Orgao) getHibernateTemplate().execute(new HibernateCallback() {

			@Override
			public Object doInHibernate(final Session session) throws HibernateException, SQLException {
				final String hql = "SELECT o " + " FROM Orgao o " + " WHERE o.orgaoPk.codigoOrgao=? "
						+ " AND o.orgaoPk.codigoMunicipio=?";
				final Query q = session.createQuery(hql);
				q.setParameter(0, pk.getCodigoOrgao());
				q.setLong(1, pk.getCodigoMunicipio());
				return q.uniqueResult();
			}
		});
	}

}
