package br.gov.go.tcm.estrutura.seguranca.filtro;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.FactoryFinder;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.jsf.FacesContextUtils;

import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoSessao;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioPerfilModulo;
import br.gov.go.tcm.estrutura.entidade.enumeracao.UsuarioEnumeracao;
import br.gov.go.tcm.estrutura.entidade.sessao.UsuarioLogadoInterno;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.ModuloDAO;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.UsuarioPerfilModuloDAO;

public class ControleSegurancaFiltro implements Filter, Serializable {

	private static final long serialVersionUID = -9147149003925618263L;

	private FilterConfig filterConfig;

	private FacesContext facesContext = null;

	private LifecycleFactory lifecycleFactory;

	private javax.faces.context.FacesContextFactory contextFactory;

	private Lifecycle lifecycle;

	private ModuloDAO moduloDAO;

	private UsuarioPerfilModuloDAO usuarioPerfilModuloDAO;
	
	private static final String PORTALJURISDICIONADOS = "portaljurisdicionados";
	
	public static final Logger log = Logger.getLogger(ControleSegurancaFiltro.class);
	
	protected static final String LOG_ERRO = "Erro";

	public void init(FilterConfig config) throws ServletException {
		this.filterConfig = config;
		this.contextFactory = (FacesContextFactory) FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
		this.lifecycleFactory = (LifecycleFactory) FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
		this.lifecycle = lifecycleFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);
	}

	@SuppressWarnings("static-access")
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException,
			ServletException, ViewExpiredException {
		if (filterConfig == null)
			return;

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;

		String recursoSolicitado = request.getRequestURI().replaceAll(request.getContextPath() + "/", "");

		AjudanteContextoSessao contextoSessao = AjudanteContextoSessao.obterContextoSessao(request);
		facesContext = contextFactory.getFacesContext(filterConfig.getServletContext(), request, response, lifecycle);

		moduloDAO = (ModuloDAO) FacesContextUtils.getWebApplicationContext(facesContext.getCurrentInstance()).getBean(
				"ModuloDAO");

		String contexto = request.getContextPath().replaceAll("/", "");

		Modulo modulo = null;

		if (contexto.equals(PORTALJURISDICIONADOS)) {
			modulo = moduloDAO.selecionarModuloDoRecursoSolicitado(Modulo.MODULO_PORTAL_JURISDICIONADOS, recursoSolicitado);
		} else if (contexto.equals("adm")) {
			modulo = moduloDAO.selecionarModuloDoRecursoSolicitado(Modulo.MODULO_PORTAL_JURISDICIONADOS, recursoSolicitado);
		} else if (contexto.equals("ecadm")) {
			modulo = moduloDAO.selecionarModuloDoRecursoSolicitado(Modulo.MODULO_ADMINSTRACAO_ESCOLA_CONTAS,
					recursoSolicitado);
		}

		if (modulo != null) {
			if (contextoSessao.getUsuarioLogado() == null) {
				response.sendRedirect(request.getContextPath() + "/" + modulo.getLoginUrl());
				return;
			}
			if (contextoSessao.getUsuarioLogado().getUsuarioPerfilModulo().getModulo().getId().equals(
					modulo.getId())) {
				chain.doFilter(req, resp);
			} else {
				UsuarioPerfilModulo usuarioPerfilModulo = null;
				if (contextoSessao.getUsuarioLogado() instanceof UsuarioLogadoInterno) {
					UsuarioLogadoInterno usuarioLogadoInterno = (UsuarioLogadoInterno) contextoSessao
							.getUsuarioLogado();

					usuarioPerfilModuloDAO = (UsuarioPerfilModuloDAO) FacesContextUtils.getWebApplicationContext(
							facesContext.getCurrentInstance()).getBean("UsuarioPerfilModuloDAO");

					List<UsuarioPerfilModulo> lstUsuarioPerfilModulo = usuarioPerfilModuloDAO
							.selecionarLstUsuarioPerfilModuloPorModuloUsuarioIdTipoUsuario(modulo,
									usuarioLogadoInterno.getUsuarioMonitor().getId(),
									UsuarioEnumeracao.USUARIO_INTERNO);
					usuarioPerfilModulo = lstUsuarioPerfilModulo != null && lstUsuarioPerfilModulo.size() > 0 ? lstUsuarioPerfilModulo
							.get(0)
							: null;
					if (usuarioPerfilModulo == null) {
						response.sendRedirect(request.getContextPath() + "/" + modulo.getLoginUrl());
						return;
					}
				}
				contextoSessao.getUsuarioLogado().setUsuarioPerfilModulo(usuarioPerfilModulo);
				chain.doFilter(req, resp);
			}
		} else {
			// TODO 13/08/2010 verificar se esse trecho � para sess�o expirada;
			/*if (recursoSolicitado != null && recursoSolicitado.equals("login.jsf")) {
				chain.doFilter(req, resp);
			} else if (recursoSolicitado != null && recursoSolicitado.equals("index.jsf")) {
				chain.doFilter(req, resp);
			} else if (recursoSolicitado != null && recursoSolicitado.equals("indexAdministracao.jsf")) {
				chain.doFilter(req, resp);
			} else {
				response.sendRedirect(request.getContextPath() + "/login.jsf");
				return;
			}*/
			try {
				chain.doFilter(req, resp);
			} catch (ServletException e) {
				throw new ViewExpiredException();
			}
		}
	}

	public void destroy() {
	}
}
