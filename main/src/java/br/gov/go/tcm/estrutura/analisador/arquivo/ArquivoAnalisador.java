package br.gov.go.tcm.estrutura.analisador.arquivo;

import java.util.List;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteReflexao;
import br.gov.go.tcm.estrutura.entidade.ajudante.excecao.ReflexaoExcecao;

public class ArquivoAnalisador {
	
	public final static String MARCADOR_FIM_ARQUIVO = "99";
	
	public final static String MARCADOR_REGISTRO_PRINCIPAL = "10";
	
	public final static String PROPRIEDADE_NUMERO_SEQUENCIAL = "numeroSequencial";

	public final static String CHAVE_IDENTIFICACAO = "chaveIdentificacao";
	
	
	private String chaveIdentificacao;
	private Long numeroSequencial;
	private String tipoRegistro;

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Long getNumeroSequencial() {
		return numeroSequencial;
	}

	public void setNumeroSequencial(Long numeroSequencial) {
		this.numeroSequencial = numeroSequencial;
	}
	
	@Override
	public String toString(){
		String descObjeto = "[";
		AjudanteReflexao assistenteReflexao = new AjudanteReflexao();
		List<String> propriedades = assistenteReflexao.obterPropriedades(this.getClass());
		
		for(String propriedade : propriedades){
			
			if(propriedade.equals(ArquivoAnalisador.CHAVE_IDENTIFICACAO))
				continue;
			
			Object valor;
			try {
				valor = assistenteReflexao.obterValorPropriedade(this,propriedade);
			} catch (ReflexaoExcecao e) {
				e.printStackTrace();
				return "";				
			}
			
			if(valor != null && List.class.isAssignableFrom(valor.getClass())){
				continue;
			}
			
			descObjeto += propriedade + "=" + ((valor != null) ? valor.toString().trim() : "") + " ,"; 
		}
		if(descObjeto.length() > 1)
			descObjeto = descObjeto.substring(0,descObjeto.length() - 1);
		descObjeto += "]";
		return descObjeto;
	}

	public String getChaveIdentificacao() {
		return chaveIdentificacao;
	}

	public void setChaveIdentificacao(String chaveIdentificacao) {
		this.chaveIdentificacao = chaveIdentificacao;
	}
}
