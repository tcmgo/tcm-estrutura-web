package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoContratoBalanceteEnumeracao implements NumberEnumeracaoPersistente{

	ORIGINAL(1), ADITIVO(2);
	int valor;

	TipoContratoBalanceteEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoContratoBalanceteEnumeracao.ORIGINAL;
		case 2:
			return TipoContratoBalanceteEnumeracao.ADITIVO;		
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Original";
		case 2:
			return "Aditivo";
		default:
			return "";
		}
	}


}
