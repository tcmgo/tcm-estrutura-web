package br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato;

import java.util.List;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;

public interface ModuloDAO extends GenericoDAO {

	List<Modulo> selecionarTodosModulosOrdenadosPorDescricao();
	
	List<Modulo> obterModuloPorDescricao(String descricao);
	
	Modulo selecionarModuloDoRecursoSolicitado(Long id, String recursoSolicitado);
	
	Modulo selecionarModuloDoRecursoSolicitado(String padraoUrl, String recursoSolicitado);

	Modulo obterModuloPorDescricaoExata(String descricao);

}
