package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum ConcursoPublicoEscolaridadeEnumeracao implements NumberEnumeracaoPersistente {
	FUNDAMENTAL(0), MEDIO_INCOMPLETO(1),MEDIO_COMPLETO(2), GRADUACAO_INCOMPLETA(3), GRADUACAO_COMPLETA(4), 
	POS_GRADUACAO(5), MESTRADO(6), DOUTORADO(7);

	private int valor;

	ConcursoPublicoEscolaridadeEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Fundamental";
		case 1:
			return "M�dio Incompleto";
		case 2:
			return "M�dio Completo";
		case 3:
			return "Gradua��o Incompleta";
		case 4:
			return "Gradua��o Completa";
		case 5:
			return "P�s-Gradua��o";
		case 6:
			return "Mestrado";
		case 7:
			return "Doutorado";
		default:
			return "";
		}
	}

}

