package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoEnderecoEnumeracao implements NumberEnumeracaoPersistente {
	RESIDENCIAL(1), PROFISSIONAL(2);
	
	private int valor;

	TipoEnderecoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Residencial";
		case 2:
			return "Profissional";				
		default:
			return "";
		}
	}
}
