package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum UnidadeEnumeracao implements NumberEnumeracaoPersistente {

	GABINETE_PRESIDENCIA(1), FUNDO_ESPECIAL_REAPARELHAMENTO(50);

	private final int valor;

	UnidadeEnumeracao(int valor) {
		this.valor = valor;
	}

	@Override
	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Gabinete de PresidÍncia";
		case 50:
			return "Fundo Especial de Reaparelhamento";
		default:
			return "";
		}
	}
}