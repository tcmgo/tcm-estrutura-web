package br.gov.go.tcm.estrutura.entidade.ajudante;

import java.util.EnumSet;
import java.util.Iterator;

import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;
import br.gov.go.tcm.estrutura.entidade.enumeracao.AnoEnumeracao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.NumberEnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.NumeroEnumeracao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.StringEnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TextoEnumeracao;

public class AjudanteEnumeracao {
	
	public static String converterEnumeracaoString(java.lang.Enum<?> enumeracaoPersistente){
		
		if (enumeracaoPersistente==null)
			return "";
		
		if (enumeracaoPersistente instanceof NumberEnumeracaoPersistente){
			return ((NumberEnumeracaoPersistente) enumeracaoPersistente).toString();
		} else if(enumeracaoPersistente instanceof StringEnumeracaoPersistente){
			return ((StringEnumeracaoPersistente) enumeracaoPersistente).toString();
		} else
			return "Erro: Enum n�o Convertido";
		

	}
	
	@SuppressWarnings("rawtypes")
	public static EnumeracaoPersistente obterEnumeracaoPorValorOrdinal(Class classe,Object valor){
		@SuppressWarnings("unchecked")
		EnumSet<?> set = EnumSet.allOf(classe);
		Iterator<?> iter = set.iterator();
		while (iter.hasNext()) {
			EnumeracaoPersistente enumeration = (EnumeracaoPersistente) iter
					.next();
			
			if(enumeration instanceof NumberEnumeracaoPersistente){
				
				NumberEnumeracaoPersistente numberEnum = (NumberEnumeracaoPersistente) enumeration;
				if(numberEnum.getValorOrdinal().equals(valor)){
					return numberEnum;
				}
				else if(numberEnum.getValorOrdinal().toString().equals(valor)){
					return numberEnum;
				}
				
			} else if(enumeration instanceof StringEnumeracaoPersistente){
				
				StringEnumeracaoPersistente stringEnum = (StringEnumeracaoPersistente) enumeration;				
				if(stringEnum.getValorOrdinal().equals(valor)){
					return stringEnum;
				}
				else if(stringEnum.toString().equals(valor)){
					return stringEnum;
				}
				
			}
		}
		return null;
	}
	
	public static EnumeracaoPersistente obterEnumeracaoPorValorOrdinal(String classe,Object valor){
		
		try {
			@SuppressWarnings("rawtypes")
			Class clazz = Class.forName("br.gov.go.tcm.estrutura.entidade.enumeracao." + classe);
			return AjudanteEnumeracao.obterEnumeracaoPorValorOrdinal(clazz, valor);
		} catch (ClassNotFoundException e) {			
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public static EnumeracaoPersistente obterEnumeracaoPorValorOrdinal(
			String nomeBaseDaClasse, Object valor, Integer ano) {

		try {
			@SuppressWarnings("rawtypes")
			Class classe = Class
					.forName("br.gov.go.tcm.estrutura.entidade.enumeracao."
							+ nomeBaseDaClasse + ano.toString());

			@SuppressWarnings("unchecked")
			EnumSet<?> conjunto = EnumSet.allOf(classe);
			Iterator<?> iterador = conjunto.iterator();
			while (iterador.hasNext()) {
				EnumeracaoPersistente enumeracao = (EnumeracaoPersistente) iterador.next();

				if (enumeracao instanceof NumeroEnumeracao) {

					NumeroEnumeracao numeroEnumeracao = (NumeroEnumeracao) enumeracao;
					if (numeroEnumeracao.getValorOrdinal().equals(valor)) {
						return numeroEnumeracao;
					}

				} else if (enumeracao instanceof TextoEnumeracao) {

					TextoEnumeracao textoEnumeracao = (TextoEnumeracao) enumeracao;
					if (textoEnumeracao.getValorOrdinal().equals(valor)) {
						return textoEnumeracao;
					}
				}
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}

		return null;
	}	
	
	public static EnumeracaoPersistente obterEnumeracaoPorValorOrdinal(
			String nomeBaseDaClasseInicio, String nomeBaseDaClasseFim, Object valor, Integer ano) {

		try {
			@SuppressWarnings("rawtypes")
			Class classe = Class
					.forName("br.gov.go.tcm.estrutura.entidade.enumeracao."
							+ nomeBaseDaClasseInicio + ano.toString() + nomeBaseDaClasseFim);

			@SuppressWarnings("unchecked")
			EnumSet<?> conjunto = EnumSet.allOf(classe);
			Iterator<?> iterador = conjunto.iterator();
			while (iterador.hasNext()) {
				EnumeracaoPersistente enumeracao = (EnumeracaoPersistente) iterador.next();

				if (enumeracao instanceof NumeroEnumeracao) {

					NumeroEnumeracao numeroEnumeracao = (NumeroEnumeracao) enumeracao;
					if (numeroEnumeracao.getValorOrdinal().equals(valor)) {
						return numeroEnumeracao;
					}

				} else if (enumeracao instanceof TextoEnumeracao) {

					TextoEnumeracao textoEnumeracao = (TextoEnumeracao) enumeracao;
					if (textoEnumeracao.getValorOrdinal().equals(valor)) {
						return textoEnumeracao;
					}
				}
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static EnumeracaoPersistente valorEnumeracaoConstante(java.lang.String nomeConstante){
		String[] partesNomeConstante = nomeConstante.split("[.]");
		
		if(partesNomeConstante.length != 2){
			return null;
		}
		
		try {
			@SuppressWarnings("rawtypes")
			Class[] enums = AjudanteReflexao.obterClassesDoPacote(ContextoSpring.getPacoteContemEnumeracaoPersistente());
			
			for(int i = 0 ; i < enums.length; ++i){
				String nomeClass = enums[i].getName().replaceAll(enums[i].getPackage().getName() + ".","");
				if(nomeClass.equals(partesNomeConstante[0])){
					return (EnumeracaoPersistente) Enum.valueOf(enums[i], partesNomeConstante[1]);					
				}
			}
			
		} catch (AjudanteExcecao e) {
			throw new RuntimeException(e.getMessage());
		}
		
		return null;
	}
	
	public static EnumeracaoPersistente obterAnoCorrenteEnumeracao(){
		return AnoEnumeracao.obterAnoCorrente();
	}
	
}