package br.gov.go.tcm.estrutura.negocio.ajudante;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;
import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteExcecao;
import br.gov.go.tcm.estrutura.entidade.transiente.Arquivo;
import br.gov.go.tcm.estrutura.entidade.transiente.SequencialGED;
import br.gov.go.tcm.estrutura.persistencia.ajudante.contrato.SequencialGEDDAO;
import br.gov.go.tcm.estrutura.util.diretorio.AjudanteDiretorio;
import br.gov.go.tcm.estrutura.util.excecao.ArquivoNaoEncontradoExcecao;

@Service("ajudanteDeArquivos")
public class AjudanteDeArquivos implements Serializable {

	private static final long serialVersionUID = 231335919348711356L;

	private static final Integer DIRETORIO_GED_BASE = 1;

	private static final Integer DIRETORIO_GED_DIGITAL = 2;
	
	@SuppressWarnings("unused")
	private static final Integer DIRETORIO_GED_3 = 3;

	private static File diretorioDigitalGed;

	private static File diretorioBase;
	
	private static File diretorioBaseGED3;

	@Autowired
	private SequencialGEDDAO sequencialGEDDAO;

	public SequencialGED obterUltimoSequencialGED() {
		return sequencialGEDDAO.obterUltimoSequencialGED();
	}
	
	public String salvar(Arquivo arquivo) throws AjudanteExcecao {

		try {

			SequencialGED sequencialGED = obterUltimoSequencialGED();
			
			if (sequencialGED == null || sequencialGED.getSequencial() == null
					|| sequencialGED.getSequencial().length() != 13) {
				throw new AjudanteExcecao("Sequencial GED inv�lido. " + sequencialGED);
			}
			
			File diretorioAtual = getDiretorioAtual(sequencialGED.getSequencial());
			String nomeArquivo = sequencialGED.getSequencial().substring(8, 13);
			File file = new File(diretorioAtual.getAbsolutePath()
					+ AjudanteDiretorio.BARRA + nomeArquivo
					+ arquivo.getSufixo().toLowerCase());

			BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));

			byte[] bytes = new byte[arquivo.getStream().available()];
			arquivo.getStream().read(bytes);

			bufferedOutputStream.write(bytes);

			bufferedOutputStream.flush();
			bufferedOutputStream.close();

			return getIdPorArquivo(file);

		} catch (FileNotFoundException e) {
			throw new AjudanteExcecao(e.getMessage());

		} catch (IOException e) {
			throw new AjudanteExcecao(e.getMessage());
		}
	}
	
	public String salvarGED3(Arquivo arquivo) throws AjudanteExcecao {

		try {

			SequencialGED sequencialGED = obterUltimoSequencialGED();
			
			if (sequencialGED == null || sequencialGED.getSequencial() == null
					|| sequencialGED.getSequencial().length() != 13) {
				throw new AjudanteExcecao("Sequencial GED inv�lido. " + sequencialGED);
			}
			
			File diretorioAtual = getDiretorioAtualGED3(sequencialGED.getSequencial());
			String nomeArquivo = sequencialGED.getSequencial().substring(8, 13);
			File file = new File(diretorioAtual.getAbsolutePath()
					+ AjudanteDiretorio.BARRA + nomeArquivo
					+ arquivo.getSufixo().toLowerCase());

			BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));

			byte[] bytes = new byte[arquivo.getStream().available()];
			arquivo.getStream().read(bytes);

			bufferedOutputStream.write(bytes);

			bufferedOutputStream.flush();
			bufferedOutputStream.close();

			return getIdPorArquivo(file);

		} catch (FileNotFoundException e) {
			throw new AjudanteExcecao(e.getMessage());

		} catch (IOException e) {
			throw new AjudanteExcecao(e.getMessage());
		}
	}

	public static Arquivo abrir(String id) throws ArquivoNaoEncontradoExcecao, AjudanteExcecao {
		if (id == null)
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id nulo");

		id = id.trim();
		File file = getArquivoPorId(id);
		if (!file.exists()) {
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		try {
			BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));

			Arquivo arquivo = new Arquivo();
			arquivo.setStream(bufferedInputStream);
			arquivo.setSufixo(file.getName().substring(file.getName().lastIndexOf('.') + 1));

			return arquivo;

		} catch (FileNotFoundException e) {
			throw new ArquivoNaoEncontradoExcecao(e.getMessage());
		}
	}
	
	public static Arquivo abrir(String id, String caminhoGed) throws ArquivoNaoEncontradoExcecao, AjudanteExcecao {
		if (id == null)
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id nulo");

		id = id.trim();
		File file = getArquivoPorId(id, caminhoGed);
		if (!file.exists()) {
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id" + id);
		}

		try {
			BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));

			Arquivo arquivo = new Arquivo();
			arquivo.setStream(bufferedInputStream);
			arquivo.setSufixo(file.getName().substring(file.getName().lastIndexOf('.') + 1));

			return arquivo;

		} catch (FileNotFoundException e) {
			throw new ArquivoNaoEncontradoExcecao(e.getMessage());
		}
	}
	
	public static Arquivo abrirGED3(String id) throws ArquivoNaoEncontradoExcecao, AjudanteExcecao {
		if (id == null)
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id nulo");

		id = id.trim();
		File file = getArquivoPorIdGED3(id);
		if (!file.exists()) {
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		try {
			BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));

			Arquivo arquivo = new Arquivo();
			arquivo.setStream(bufferedInputStream);
			arquivo.setSufixo(file.getName().substring(file.getName().lastIndexOf('.') + 1));

			return arquivo;

		} catch (FileNotFoundException e) {
			throw new ArquivoNaoEncontradoExcecao(e.getMessage());
		}
	}

	public static Arquivo abrirDocumentoDigital(String id) throws ArquivoNaoEncontradoExcecao, AjudanteExcecao {
		return abrir(id, DIRETORIO_GED_DIGITAL);
	}
	
	public static Arquivo abrirDocumentoDigitalProcesso(String id) throws ArquivoNaoEncontradoExcecao, AjudanteExcecao {
		return abrirArquivoProcesso(id, DIRETORIO_GED_DIGITAL);
	}

	private static Arquivo abrir(String id, Integer diretorio) throws ArquivoNaoEncontradoExcecao, AjudanteExcecao {
		if (id == null)
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id nulo");

		id = id.trim();
		File file = getArquivoPorId(id, diretorio);
		if (!file.exists()) {
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		try {
			BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));

			Arquivo arquivo = new Arquivo();
			arquivo.setStream(bufferedInputStream);
			arquivo.setSufixo(file.getName().substring(file.getName().lastIndexOf('.') + 1));

			return arquivo;

		} catch (FileNotFoundException e) {
			throw new ArquivoNaoEncontradoExcecao(e.getMessage());
		}
	}
	
	private static Arquivo abrirArquivoProcesso(String id, Integer diretorio) throws ArquivoNaoEncontradoExcecao, AjudanteExcecao {
		if (id == null)
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id nulo");

		id = id.trim();
		File file = getArquivoProcessoPorId(id, diretorio);
		if (!file.exists()) {
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		try {
			BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));

			Arquivo arquivo = new Arquivo();
			arquivo.setStream(bufferedInputStream);
			arquivo.setSufixo(file.getName().substring(file.getName().lastIndexOf('.') + 1));

			return arquivo;

		} catch (FileNotFoundException e) {
			throw new ArquivoNaoEncontradoExcecao(e.getMessage());
		}
	}

	private static File getArquivoPorId(String id, Integer diretorio) throws AjudanteExcecao {
		
		if (id.length() < 8) {
			throw new AjudanteExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		StringBuffer path = new StringBuffer(id.substring(0, 8));
		path.insert(4, '/');
		path.insert(7, '/');

		File dir = null;

		if (diretorio.equals(DIRETORIO_GED_BASE)) {
			dir = new File(getDiretorioBase() + AjudanteDiretorio.BARRA + path.toString());
		} else if (diretorio.equals(DIRETORIO_GED_DIGITAL)) {
			dir = new File(getDiretorioGedDigital() + AjudanteDiretorio.BARRA + path.toString());
		}

		String[] nomes = dir.list(new FiltroNome(id.substring(8)));
		if ((nomes == null) || (!(nomes.length > 0))) {
			throw new AjudanteExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		if (diretorio.equals(DIRETORIO_GED_BASE)) {
			System.out.println("Abrindo:" + getDiretorioBase() + AjudanteDiretorio.BARRA + path.toString()
					+ AjudanteDiretorio.BARRA + nomes[0]);
			return new File(getDiretorioBase() + AjudanteDiretorio.BARRA + path.toString() + AjudanteDiretorio.BARRA
					+ nomes[0]);
		}
		
		System.out.println("Abrindo:" + getDiretorioGedDigital() + AjudanteDiretorio.BARRA + path.toString()
				+ AjudanteDiretorio.BARRA + nomes[0]);
		return new File(getDiretorioGedDigital() + AjudanteDiretorio.BARRA + path.toString() + AjudanteDiretorio.BARRA
				+ nomes[0]);
	}
	
	private static File getArquivoProcessoPorId(String id, Integer diretorio) throws AjudanteExcecao {
		
		if (id.length() < 8) {
			throw new AjudanteExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		StringBuffer path = new StringBuffer(id.substring(0, 8));
		path.insert(4, '/');
		path.insert(7, '/');

		File dir = null;

		if (diretorio.equals(DIRETORIO_GED_BASE)) {
			dir = new File(getDiretorioBase() + AjudanteDiretorio.BARRA + path.toString());
		} else if (diretorio.equals(DIRETORIO_GED_DIGITAL)) {
			dir = new File(getDiretorioGedDigital() + AjudanteDiretorio.BARRA + path.toString());
		}

		String[] nomes = dir.list(new FiltroNomeProcesso(id.substring(8)));
		if ((nomes == null) || (!(nomes.length > 0))) {
			throw new AjudanteExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		if (diretorio.equals(DIRETORIO_GED_BASE)) {
			System.out.println("Abrindo:" + getDiretorioBase() + AjudanteDiretorio.BARRA + path.toString()
					+ AjudanteDiretorio.BARRA + nomes[0]);
			return new File(getDiretorioBase() + AjudanteDiretorio.BARRA + path.toString() + AjudanteDiretorio.BARRA
					+ nomes[0]);
		}
		
		System.out.println("Abrindo:" + getDiretorioGedDigital() + AjudanteDiretorio.BARRA + path.toString()
				+ AjudanteDiretorio.BARRA + nomes[0]);
		return new File(getDiretorioGedDigital() + AjudanteDiretorio.BARRA + path.toString() + AjudanteDiretorio.BARRA
				+ nomes[0]);
	}

	private static File getDiretorioGedDigital() throws AjudanteExcecao {
		if (diretorioDigitalGed == null) {
			diretorioDigitalGed = new File(ContextoSpring.getDiretorioGedDigital());
			if (!diretorioDigitalGed.exists()) {
				diretorioDigitalGed.mkdirs();
			}
		}
		return diretorioDigitalGed;
	}

	private static synchronized String getIdPorArquivo(File arquivo) {
		StringBuffer id = new StringBuffer();
		id.insert(0, arquivo.getName().substring(0, arquivo.getName().lastIndexOf('.')));

		File parent;

		// dia
		parent = arquivo.getParentFile();
		id.insert(0, parent.getName());

		// mes
		parent = parent.getParentFile();
		id.insert(0, parent.getName());

		// ano
		parent = parent.getParentFile();
		id.insert(0, parent.getName());

		return id.toString();
	}

	public static String obterCaminhoArquivoPorId(String id) throws ArquivoNaoEncontradoExcecao, AjudanteExcecao {
		
		if (id.length() < 8) {
			throw new AjudanteExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		StringBuffer path = new StringBuffer(id.substring(0, 8));
		path.insert(4, '/');
		path.insert(7, '/');

		File dir = new File(getDiretorioBase() + AjudanteDiretorio.BARRA + path.toString());
		
		String[] nomes = dir.list(new FiltroNome(id.substring(8)));

		if ((nomes == null) || (!(nomes.length > 0))) {
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}
		
		String caminho = getDiretorioBase() + AjudanteDiretorio.BARRA + path.toString() + AjudanteDiretorio.BARRA + nomes[0];
		
		return caminho;
	}
	
	private static File getArquivoPorId(String id) throws AjudanteExcecao, ArquivoNaoEncontradoExcecao {

		if (id.length() < 8) {
			throw new AjudanteExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		StringBuffer path = new StringBuffer(id.substring(0, 8));
		path.insert(4, '/');
		path.insert(7, '/');

		File dir = new File(getDiretorioBase() + AjudanteDiretorio.BARRA + path.toString());
		String[] nomes = dir.list(new FiltroNome(id.substring(8)));
		if ((nomes == null) || (!(nomes.length > 0))) {
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		return new File(getDiretorioBase() + AjudanteDiretorio.BARRA + path.toString() + AjudanteDiretorio.BARRA
				+ nomes[0]);
	}
	
	private static File getArquivoPorId(String id, String caminhoGed) throws AjudanteExcecao, ArquivoNaoEncontradoExcecao {

		if (id.length() < 8) {
			throw new AjudanteExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		StringBuffer path = new StringBuffer(id.substring(0, 8));
		path.insert(4, '/');
		path.insert(7, '/');

		String caminho = caminhoGed + AjudanteDiretorio.BARRA + path.toString() + AjudanteDiretorio.BARRA + id.substring(8);
		
		caminho = caminho.replace(AjudanteDiretorio.BARRA+AjudanteDiretorio.BARRA, AjudanteDiretorio.BARRA);
		
		File file = new File(caminho);
		
		if (!file.exists()) {
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		return file;
	}
	
	private static File getArquivoPorIdGED3(String id) throws AjudanteExcecao, ArquivoNaoEncontradoExcecao {

		if (id.length() < 8) {
			throw new AjudanteExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		StringBuffer path = new StringBuffer(id.substring(0, 8));
		path.insert(4, '/');
		path.insert(7, '/');

		File dir = new File(getDiretorioBaseGED3() + AjudanteDiretorio.BARRA + path.toString());
		String[] nomes = dir.list(new FiltroNome(id.substring(8)));
		if ((nomes == null) || (!(nomes.length > 0))) {
			throw new ArquivoNaoEncontradoExcecao("N�o foi possivel encontrar o arquivo com id " + id);
		}

		return new File(getDiretorioBaseGED3() + AjudanteDiretorio.BARRA + path.toString() + AjudanteDiretorio.BARRA
				+ nomes[0]);
	}
	
	private static File getDiretorioAtual(String sequencial) throws AjudanteExcecao {
		
		String path = sequencial.substring(0, 4) + AjudanteDiretorio.BARRA
				+ sequencial.substring(4, 6) + AjudanteDiretorio.BARRA
				+ sequencial.substring(6, 8);

		File diretorioAtual = new File(getDiretorioBase().getAbsolutePath() + AjudanteDiretorio.BARRA + path);

		if (!diretorioAtual.exists()) {
			diretorioAtual.mkdirs();
		}

		return diretorioAtual;
	}
	
	private static File getDiretorioAtualGED3(String sequencial) throws AjudanteExcecao {

		String path = sequencial.substring(0, 4) + AjudanteDiretorio.BARRA
				+ sequencial.substring(4, 6) + AjudanteDiretorio.BARRA
				+ sequencial.substring(6, 8);

		File diretorioAtual = new File(getDiretorioBaseGED3().getAbsolutePath()
				+ AjudanteDiretorio.BARRA + path);

		if (!diretorioAtual.exists()) {
			diretorioAtual.mkdirs();
		}

		return diretorioAtual;
	}

	@SuppressWarnings("unused")
	private static String preencheComZeros(int valor, int tamanho) {
		
		String resultado = String.valueOf(valor);
		while (resultado.length() < tamanho) {
			resultado = "0" + resultado;
		}

		return resultado;
	}

	private static File getDiretorioBase() throws AjudanteExcecao {
		if (diretorioBase == null) {
			diretorioBase = new File(ContextoSpring.getDiretorioGed());
			if (!diretorioBase.exists()) {
				diretorioBase.mkdirs();
			}
		}
		return diretorioBase;
	}
	
	private static File getDiretorioBaseGED3() throws AjudanteExcecao {
		if (diretorioBaseGED3 == null) {
			diretorioBaseGED3 = new File(ContextoSpring.getDiretorioGed3());
			if (!diretorioBaseGED3.exists()) {
				diretorioBaseGED3.mkdirs();
			}
		}
		return diretorioBaseGED3;
	}

	public SequencialGEDDAO getSequencialGEDDAO() {
		return sequencialGEDDAO;
	}

	public void setSequencialGEDDAO(SequencialGEDDAO sequencialGEDDAO) {
		this.sequencialGEDDAO = sequencialGEDDAO;
	}
}

class FiltroNome implements FilenameFilter {

	private String nome;

	public FiltroNome(String nome) {
		this.nome = nome;
	}

	public boolean accept(File dir, String name) {
		int index = name.lastIndexOf('.');
		//int index = name.indexOf('.');
		if (index > 0) {
			return getNome().equals(name.substring(0, index));
		}
		return getNome().equals(name);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}

class FiltroNomeProcesso implements FilenameFilter {

	private String nome;

	public FiltroNomeProcesso(String nome) {
		this.nome = nome;
	}

	public boolean accept(File dir, String name) {
		int index = name.indexOf('.');
		if (index > 0) {
			return getNome().equals(name.substring(0, index));
		}
		return getNome().equals(name);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}