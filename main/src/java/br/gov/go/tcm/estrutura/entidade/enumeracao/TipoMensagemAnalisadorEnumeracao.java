package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoMensagemAnalisadorEnumeracao implements NumberEnumeracaoPersistente {

//	C�digo comentado para corrigir erro de migracao de classe enum.
//	INFO(0), LOG(1), ERRO(2);
	
	LOG(0), ADVERTENCIA(1), ERRO(2);
	int valor;

	TipoMensagemAnalisadorEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 0:
			return TipoMensagemAnalisadorEnumeracao.LOG;
		case 1:
			return TipoMensagemAnalisadorEnumeracao.ADVERTENCIA;
		case 2:
			return TipoMensagemAnalisadorEnumeracao.ERRO;		
		}
		return null;
	}

	@Override
	public String toString(){
		switch (valor) {
			case 0:
				return "Log";
			case 1:
				return "Advert�ncia";
			case 2:
				return "Erro";	
			default:
				return "";
		}
	}
}
