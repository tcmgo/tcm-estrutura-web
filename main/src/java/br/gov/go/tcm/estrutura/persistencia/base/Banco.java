package br.gov.go.tcm.estrutura.persistencia.base;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 
 * @author felipe_a
 * 
 * � impeditivo que alterem as String's com o nome dos bancos.
 * Utilizamos no GenericoDaoImplementacao para saber qual inst�ncia do hibernateTemplate deve ser recuperada em n�vel de execu��o.
 * 
 */
@Target(TYPE) @Retention(RUNTIME) @Inherited
public @interface Banco {

	public final static String ORCAFI 	= "ORCAFI";
	public final static String TCMWEB 	= "TCMWEB";
	public final static String STP 		= "STP";
	public final static String USUARIO  = "USUARIO";
	public final static String ADMINISTRACAO  = "ADMINISTRACAO";
	public final static String WS_TCM  = "WS_TCM";
	public final static String OUVIDORIA  = "OUVIDORIA";
	public final static String SOPHOS  = "SOPHOS";
	
	String nome() default TCMWEB;
}
