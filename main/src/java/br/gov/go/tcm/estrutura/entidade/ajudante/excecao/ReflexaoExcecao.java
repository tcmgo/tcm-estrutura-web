package br.gov.go.tcm.estrutura.entidade.ajudante.excecao;

public class ReflexaoExcecao extends RuntimeException {

	private static final long serialVersionUID = 7942439738025605294L;

	public ReflexaoExcecao(String message) {
		super(message);
	}

}
