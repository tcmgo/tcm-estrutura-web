package br.gov.go.tcm.estrutura.util.excecao;

public class FluxoExcecao extends Exception {

	private static final long serialVersionUID = 995316901900780884L;

	public FluxoExcecao(String message) {
		super(message);
	}

}
