package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoLicitacaoAtaEnumeracao implements NumberEnumeracaoPersistente{
	
	PARCIAL(0), DEFINITIVA(1);

	private final int valor;

	TipoLicitacaoAtaEnumeracao(int valor) {
		this.valor = valor;
	}
	
	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Parcial";
		case 1:
			return "Definitiva";		
		default:
			return "";
		}
	}

	
}
