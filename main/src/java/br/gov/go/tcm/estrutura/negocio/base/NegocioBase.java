package br.gov.go.tcm.estrutura.negocio.base;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Service;

import br.gov.go.tcm.estrutura.entidade.base.Entidade;
import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;
import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;

@Service("negocioBase")
public abstract class NegocioBase {

	private GenericoDAO genericoDAO;

	public void salvar(Class<? extends Entidade> entidade, Object objeto) {
		genericoDAO.salvar(objeto);
	}

	public void salvarOuAtualizar(Class<? extends Entidade> entidade,
			Object objeto) {
		genericoDAO.salvarOuAtualizar(objeto);
	}

	public void excluir(Class<? extends Entidade> entidade, Object objeto) {
		genericoDAO.excluir(objeto);
	}

	public void atualizar(Class<? extends EntidadePadrao> entidade,
			Object objeto) {
		genericoDAO.atualizar(objeto);
	}

	public Entidade obterEntidadePorId(Class<? extends Entidade> entidade,
			Serializable id) {
		return genericoDAO.obterEntidadePorId(entidade, id);
	}

	public List<?> obterTodos(Class<? extends EntidadePadrao> entidade) {
		return genericoDAO.obterTodos(entidade);
	}

	public void desatachar(Object objeto) {
		genericoDAO.desatachar(objeto);
	}
	
	public boolean contemObjetoNaSessao(Object objeto) {
		
		return genericoDAO.contemObjetoNaSessao(objeto);
	}
	
	public Entidade carregarObjetoNaSessao(Class<? extends Entidade> classe,
			Serializable id) {		
		return genericoDAO.carregarObjetoNaSessao(classe, id);
	}
}
