package br.gov.go.tcm.estrutura.analisador.conversor;

import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoMensagemAnalisadorEnumeracao;


public interface ConversorOuvinte {
	
	public void notificarMensagem(String mensagem, TipoMensagemAnalisadorEnumeracao tipo);
	
	public void notificarMensagem(String mensagem, TipoMensagemAnalisadorEnumeracao tipo, Object objeto);
	
}
