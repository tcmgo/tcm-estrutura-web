package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum UsuarioEnumeracao implements NumberEnumeracaoPersistente {

	USUARIO_EXTERNO(1), USUARIO_INTERNO(2);

	int valor;

	UsuarioEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return UsuarioEnumeracao.USUARIO_EXTERNO;
		case 2:
			return UsuarioEnumeracao.USUARIO_INTERNO;		
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Usu�rio Externo";
		case 2:
			return "Usu�rio Interno";
		default:
			return "";
		}
	}

}
