package br.gov.go.tcm.estrutura.analisador.conversor.regra;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumSet;
import java.util.Iterator;

import br.gov.go.tcm.estrutura.analisador.conversor.ConversorExcecao;
import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteData;
import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteTexto;
import br.gov.go.tcm.estrutura.entidade.enumeracao.NumberEnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.StringEnumeracaoPersistente;

public class ParserPadrao {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object converter(Class classe, String value)
			throws ConversorExcecao {
		
		if (Boolean.class.isAssignableFrom(classe)) {
			if(value.trim().equals("1"))
				return Boolean.TRUE;
			else if(value.trim().equals("0"))
				return Boolean.FALSE;
		}
		if (Long.class.isAssignableFrom(classe)) {
			try{
				if(value.toString().indexOf(AjudanteTexto.ESPACO) >= 0)
					throw new ConversorExcecao("Falha ao converter " + value + " para um valor num�rico.");
				return new Long(value.toString());
			} catch(NumberFormatException e){
				throw new ConversorExcecao("Falha ao converter " + value + " para um valor num�rico.");
			}			
		} else if (Integer.class.isAssignableFrom(classe)) {
			try{
				if(value.toString().indexOf(AjudanteTexto.ESPACO) >= 0)
					throw new ConversorExcecao("Falha ao converter " + value + " para um valor num�rico.");
				return new Integer(value.toString());
			} catch(NumberFormatException e){
				throw new ConversorExcecao("Falha ao converter " + value + " para um valor num�rico.");
			}
		} else if (Short.class.isAssignableFrom(classe)) {
			try{
				if(value.toString().indexOf(AjudanteTexto.ESPACO) >= 0)
					throw new ConversorExcecao("Falha ao converter " + value + " para um valor num�rico.");
				return new Short(value.toString());
			} catch(NumberFormatException e){
				throw new ConversorExcecao("Falha ao converter " + value + " para um valor num�rico.");
			}
		} else if (String.class.isAssignableFrom(classe)) {
			if(value.trim().equals(""))
				return null;
			return new String(value.toString());
		} else if (Date.class.isAssignableFrom(classe)) {

			if(value.toString().length() != 8)
				throw new ConversorExcecao("Falha ao converter " + value + " para uma data v�lida.");
			
			try{
				if(new Long(value) == 0)
					return null;
			} catch(NumberFormatException e){
				throw new ConversorExcecao("Falha ao converter " + value + " para uma data v�lida.");
			}
			
			String data = value.toString().substring(0, 2) + "/"
					+ value.toString().substring(2, 4) + "/"
					+ value.toString().substring(4, 8);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
					"dd/MM/yyyy");
			simpleDateFormat.setLenient(false);
						
			try {
				Date dataRaiz = simpleDateFormat.parse(AjudanteData.DATA_INICIO);
				Date dataCorrente = simpleDateFormat.parse(data);
				if(dataCorrente.before(dataRaiz)){
					throw new ParseException("",0);
				}				
				return dataCorrente;
			} catch (ParseException e) {
				throw new ConversorExcecao("Falha ao converter " + value + " para uma data v�lida.");
			} catch (NullPointerException e){
				throw new ConversorExcecao("Falha ao converter " + value + " para uma data v�lida.");
			}

		} else if(Double.class.isAssignableFrom(classe)){
			
			try{
				if(value.toString().indexOf(AjudanteTexto.ESPACO) >= 0)
					throw new ConversorExcecao("Falha ao converter " + value + " para um valor num�rico.");
				
				if(value.toString().indexOf(AjudanteTexto.PONTO) >= 0)
					throw new ConversorExcecao("Falha ao converter " + value + " para um valor num�rico.");
				
				if(value.toString().indexOf(AjudanteTexto.TRACO) >= 1)
					throw new ConversorExcecao("Falha ao converter " + value + " para um valor num�rico.");
				
				String replaced = value.replaceAll(AjudanteTexto.VIRGULA,AjudanteTexto.PONTO);
				Double valor = new Double(replaced);
				return valor;
			} catch(NumberFormatException e){
				throw new ConversorExcecao("Falha ao converter " + value + " para um valor num�rico.");
			}			
			
		} else if (NumberEnumeracaoPersistente.class.isAssignableFrom(classe)) {
			
			EnumSet<?> enumSet = EnumSet.allOf(classe);
			
			for (Iterator<?> iter = enumSet.iterator(); iter.hasNext();) {
				NumberEnumeracaoPersistente enumeration = (NumberEnumeracaoPersistente) iter.next();
				if(enumeration.getValorOrdinal().equals(new Integer(value)))
					return enumeration;
			}
			
			return null;
			
		} else if (StringEnumeracaoPersistente.class.isAssignableFrom(classe)) {

			EnumSet<?> enumSet = EnumSet.allOf(classe);
			
			for (Iterator<?> iter = enumSet.iterator(); iter.hasNext();) {
				StringEnumeracaoPersistente enumeration = (StringEnumeracaoPersistente) iter.next();
				if(enumeration.getValorOrdinal().equals(value))
					return enumeration;
			}
			
			return null;
			
		} else
			throw new ConversorExcecao("Tipo de dado nao pode ser convertido");

	}

}
