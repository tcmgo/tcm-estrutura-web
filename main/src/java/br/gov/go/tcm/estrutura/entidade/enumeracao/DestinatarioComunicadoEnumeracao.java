package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum DestinatarioComunicadoEnumeracao implements NumberEnumeracaoPersistente {

	AUTORIDADES(0), REPRESENTANTES(1), RESPONSAVEIS(2);

	private int valor;

	DestinatarioComunicadoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Autoridades";
		case 1:
			return "Representantes";
		case 2:
			return "Responsáveis";
		default:
			return "";
		}
	}
}
