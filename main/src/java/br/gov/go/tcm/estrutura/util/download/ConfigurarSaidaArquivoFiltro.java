package br.gov.go.tcm.estrutura.util.download;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class ConfigurarSaidaArquivoFiltro implements Filter {

	public static final String FILE_OUTPUT_META_DATA = "fileOutputMetaData";

	public void init(FilterConfig filterConfig) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		try {
			filterChain.doFilter(request, response);

			HttpServletResponse httpResponse = (HttpServletResponse) response;

			if (request.getAttribute(FILE_OUTPUT_META_DATA) != null) {
				
				FileOutputMetaData fileOutputMetaData = (FileOutputMetaData) request
					.getAttribute(FILE_OUTPUT_META_DATA);
				
				String fileName = fileOutputMetaData.getNome() + "." +  fileOutputMetaData.getExtensao();
				
				fileName = fileName.replace(":", "_");
				
				httpResponse
						.setContentType(fileOutputMetaData.getContentType());
				httpResponse
						.addHeader("Content-Disposition",
								"attachment;filename=\""
										+ fileName
										+ "\"");
			}

		} catch (Exception e) {
			e.printStackTrace();
			if(e.getCause() != null)
				e.getCause().printStackTrace();
		}
	}

	public void destroy() {
	}

}
