package br.gov.go.tcm.estrutura.entidade.sessao;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioPerfilModulo;

public abstract class UsuarioLogado {

	private UsuarioPerfilModulo usuarioPerfilModulo;

	public abstract String getNomeUsuario();

	public abstract String getNomeCompletoUsuario();

	public UsuarioPerfilModulo getUsuarioPerfilModulo() {
		if (usuarioPerfilModulo == null)
			usuarioPerfilModulo = new UsuarioPerfilModulo();
		return usuarioPerfilModulo;
	}

	public void setUsuarioPerfilModulo(UsuarioPerfilModulo usuarioPerfilModulo) {
		this.usuarioPerfilModulo = usuarioPerfilModulo;
	}
}
