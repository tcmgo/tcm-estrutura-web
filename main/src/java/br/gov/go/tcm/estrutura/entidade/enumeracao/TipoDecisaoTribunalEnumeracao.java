package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum TipoDecisaoTribunalEnumeracao implements StringEnumeracaoPersistente {

	ADMINISTRATIVA("ADMINISTRATIVA"), CONSULTA("CONSULTA"), NORMATIVA("NORMATIVA"), PLENARIA("PLENARIA");
	String valor;

	TipoDecisaoTribunalEnumeracao(String valor) {
		this.valor = valor;
	}

	public String getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		if (valor.equalsIgnoreCase("ADMINISTRATIVA")) {
			return TipoDecisaoTribunalEnumeracao.ADMINISTRATIVA;
		} else if (valor.equalsIgnoreCase("CONSULTA")) {
			return TipoDecisaoTribunalEnumeracao.CONSULTA;
		} else if (valor.equalsIgnoreCase("NORMATIVA")) {
			return TipoDecisaoTribunalEnumeracao.NORMATIVA;
		} else if (valor.equalsIgnoreCase("PLENARIA")) {
			return TipoDecisaoTribunalEnumeracao.PLENARIA;
		} 
		return null;
	}

	@Override
	public String toString() {
		if (valor.equalsIgnoreCase("PLENARIA")) {
			return "Decis�o Plen�ria";
		} else if (valor.equalsIgnoreCase("NORMATIVA")) {
			return "Resolu��o Normativa";
		} else if (valor.equalsIgnoreCase("CONSULTA")) {
			return "Resolu��o Consulta";
		} else if (valor.equalsIgnoreCase("ADMINISTRATIVA")) {
			return "Resolu��o Administrativa";
		}
		return "";
	}
}
