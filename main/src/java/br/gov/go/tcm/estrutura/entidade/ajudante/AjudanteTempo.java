package br.gov.go.tcm.estrutura.entidade.ajudante;

public class AjudanteTempo {

	long delta;
	
	public void marcarInstanteInicial(){
		delta = System.currentTimeMillis();
	}
	
	public int marcarInstanteFinal(){
		return (int) ((System.currentTimeMillis() - delta));
	}
	
	public static Long tempoCorrenteEmMilisegundos(){
		return System.currentTimeMillis();
	}
}
