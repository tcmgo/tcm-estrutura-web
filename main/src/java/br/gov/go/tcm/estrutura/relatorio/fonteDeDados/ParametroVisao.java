package br.gov.go.tcm.estrutura.relatorio.fonteDeDados;

public class ParametroVisao {
	
	private String id;
	
	private Object valor;

	public ParametroVisao(String descricao, Object valor) {
		this.id = descricao;
		this.valor = valor;
	}

	public String getId() {
		return id;
	}

	public Object getValor() {
		return valor;
	}
	
}
