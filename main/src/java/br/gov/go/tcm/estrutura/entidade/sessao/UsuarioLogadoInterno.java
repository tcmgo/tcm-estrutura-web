package br.gov.go.tcm.estrutura.entidade.sessao;

import java.io.Serializable;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioMonitor;

public class UsuarioLogadoInterno extends UsuarioLogado implements Serializable {

	/** Atributo serialVersionUID. */
	private static final long serialVersionUID = -7870504313620021454L;

	private UsuarioMonitor usuarioMonitor;

	public UsuarioMonitor getUsuarioMonitor() {

		return this.usuarioMonitor;
	}

	public void setUsuarioMonitor(final UsuarioMonitor usuario) {

		this.usuarioMonitor = usuario;
	}

	@Override
	public String getNomeUsuario() {

		return this.getUsuarioMonitor().getNomeUsuario();
	}

	@Override
	public String getNomeCompletoUsuario() {

		return this.getUsuarioMonitor().getNomeCompleto();
	}
}
