package br.gov.go.tcm.estrutura.persistencia.orcafi.implementacao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.orcafi.contrato.MunicipioDAO;

@Repository("MunicipioDAO")
public class MunicipioDAOImplementacao extends GenericoDAOImplementacao
		implements MunicipioDAO {

	@Autowired
	public MunicipioDAOImplementacao(@Qualifier("sessionFactoryOrcafi") SessionFactory factory,
			@Qualifier("hibernateTemplateOrcafi") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}
	
	
}