package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoLiquidacaoEnumeracao implements NumberEnumeracaoPersistente{

	LIQUIDACAO_DESPESA_EXERCICIO(1),LIQUIDACAO_DESPESAS_RESTO_PAGAR(2);

	int valor;
	
	TipoLiquidacaoEnumeracao(int valor){
		this.valor = valor;
	}
	
	public Number getValorOrdinal(){
		return this.valor;
	}

	public EnumeracaoPersistente valor(){		
		switch(valor){
			case 1:
				return TipoLiquidacaoEnumeracao.LIQUIDACAO_DESPESA_EXERCICIO;
			case 2:
				return TipoLiquidacaoEnumeracao.LIQUIDACAO_DESPESAS_RESTO_PAGAR;			
		}
		return null;		
	}
	
	@Override
	public String toString(){
		switch(valor){
			case 1:
				return "Liquidação Despesa de Exercício";
			case 2:
				return "Liquidação Despesas Resto a Pagar";			
		}
		return "";
	}

}
