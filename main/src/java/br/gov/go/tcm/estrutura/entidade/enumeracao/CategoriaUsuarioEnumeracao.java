package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum CategoriaUsuarioEnumeracao implements NumberEnumeracaoPersistente{
	
	CHEFE_GOVERNO(1), GESTOR(2), AMBOS(3), PRESIDENTE_CAMARA(4);

	int valor;

	CategoriaUsuarioEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Chefe Governo";
		case 2:
			return "Gestor";
		case 3:
			return "Chefe de Governo e Gestor";
		default:
			return "";
		}
	}
	
	
	public boolean isChefeGoverno(){
		return valor == CHEFE_GOVERNO.valor;
	}
	
	public boolean isGestor(){
		return valor == GESTOR.valor;
	}
	
	public boolean isAmbos(){
		return valor == AMBOS.valor;
	}
	
	public static boolean isPresidenteCamara(int valor){
		return valor == PRESIDENTE_CAMARA.valor;
	}
}