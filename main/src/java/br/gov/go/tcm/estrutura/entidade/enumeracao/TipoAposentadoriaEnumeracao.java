package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum TipoAposentadoriaEnumeracao implements NumberEnumeracaoPersistente {

	INTEGRAL(1), PROPORCIONAL_GERAL(2), PROPORCIONAL_ART2(3),
	INTEGRAL_ART6(4),INTEGRAL_ART3(5); //, PROPORCIONAL_ART3(4);

	private final int valor;

	TipoAposentadoriaEnumeracao(int valor) {
		this.valor = valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoAposentadoriaEnumeracao.INTEGRAL;
		case 2:
			return TipoAposentadoriaEnumeracao.PROPORCIONAL_GERAL;
		case 3:
			return TipoAposentadoriaEnumeracao.PROPORCIONAL_ART2;
		case 4:
			return TipoAposentadoriaEnumeracao.INTEGRAL_ART6;
		case 5:
			return TipoAposentadoriaEnumeracao.INTEGRAL_ART3;
		}
		return null;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Integral";
		case 2:
			return "Proporcional Regra Geral";
		case 3:
			return "Proporcional Regra Art. 2� da EC 41/03";
		case 4:
			return "Integral Regra Art. 6�";
		case 5:
			return "Integral Regra Art. 3� da EC 47";
		default:
			return "";
		}
	}
}