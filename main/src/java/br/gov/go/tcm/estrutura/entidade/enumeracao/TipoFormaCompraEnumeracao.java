package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoFormaCompraEnumeracao implements NumberEnumeracaoPersistente {

	COMPRA_GLOBAL(1), COMPRA_ITEM_MENOR_VALOR(2), COMPRA_MANUAL(3);
	
	private int valor;
	
	TipoFormaCompraEnumeracao(int valor) {
		this.valor = valor;
	}
	
	public Number getValorOrdinal() {
		return this.valor;
	}
	
	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Compra Global";
		case 2:
			return "Compra Por Item de Menor Valor";
		case 3:
			return "Compra Manual";	
		default:
			return "";
		}
	}
}
