package br.gov.go.tcm.estrutura.entidade.enumeracao;


public interface StringEnumeracaoPersistente extends EnumeracaoPersistente{
	public String getValorOrdinal();
}
