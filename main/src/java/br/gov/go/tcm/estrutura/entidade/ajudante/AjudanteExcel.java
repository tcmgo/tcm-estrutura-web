package br.gov.go.tcm.estrutura.entidade.ajudante;

public class AjudanteExcel {

	private static final char[] colunas = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
			'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

	public final static String SEPARADOR_COLUNA_CELULA = ":";

	public static String obterCodigoLetraPorCodigoNumero(int coluna) {

		int numeroVoltas = coluna / colunas.length;
		int posicao = coluna % colunas.length;

		return (numeroVoltas > 0) ? Character.toString(colunas[numeroVoltas - 1])
				+ Character.toString(colunas[posicao]) : Character.toString(colunas[posicao]);

	}

	public static int obterCodigoNumeroPorCodigoLetra(char letra) {

		for (int i = 0; i < colunas.length; ++i) {
			if (colunas[i] == letra)
				return i;
		}

		return -1;
	}

}
