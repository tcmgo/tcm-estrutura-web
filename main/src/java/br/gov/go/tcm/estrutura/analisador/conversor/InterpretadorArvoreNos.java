package br.gov.go.tcm.estrutura.analisador.conversor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.gov.go.tcm.estrutura.analisador.arquivo.ArquivoAnalisador;
import br.gov.go.tcm.estrutura.analisador.conversor.regra.RegraAnalisador;
import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteExcecao;
import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteReflexao;
import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteTexto;
import br.gov.go.tcm.estrutura.entidade.ajudante.excecao.ReflexaoExcecao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoMensagemAnalisadorEnumeracao;

public class InterpretadorArvoreNos {

	private AjudanteTexto assistenteTexto;

	private AjudanteReflexao assistenteReflexao;

	private ConversorOuvinte conversorOuvinte;

	public InterpretadorArvoreNos(ConversorOuvinte conversorOuvinte) {
		this.conversorOuvinte = conversorOuvinte;
	}

	public List<ArquivoAnalisador> processarLinhasEGerarArquivos(
			List<String> linhas, NoRaiz noRaiz) {

		GerenciadorLinhasArquivo gerenciadorLinhasArquivo = new GerenciadorLinhasArquivo(
				linhas);

		NoRamoCorrente noRamoCorrente = null;

		List<ArquivoAnalisador> lstResultado = new ArrayList<ArquivoAnalisador>();

		leitorLinhas: while (!gerenciadorLinhasArquivo.getEhAUltimaLinha()) {

			String linha = gerenciadorLinhasArquivo.obterProximaLinha();
		
			NoRaiz noConversor = null;

			if (noRamoCorrente == null)
				noConversor = noRaiz;
			else
				noConversor = noRamoCorrente.getNoConversor();

			NoValido noValidoRetornado = retornarNoValidoParaALinha(linha,
					noConversor);

			try {
				ArquivoAnalisador arquivoAnalisador = null;

				if (noValidoRetornado.getTipoNo().equals(
						NoValido.TIPO_SEM_PARENTESCO)) {
					conversorOuvinte.notificarMensagem("Linha Inv�lida - "
							+ linha, TipoMensagemAnalisadorEnumeracao.ERRO);
					continue;
				}

				if (!noValidoRetornado.getTipoNo().equals(
						NoValido.TIPO_PARENTE_DISTANTE)) {
					arquivoAnalisador = gerarNo(linha, noValidoRetornado
							.getNoConversor());
					
					if (!arquivoComNumeroSequencialValido(arquivoAnalisador,
							gerenciadorLinhasArquivo.getLinhaCorrente())) {
						lstResultado.clear();
						return lstResultado;
					}
				}

				if ((noValidoRetornado.getTipoNo()
						.equals(NoValido.TIPO_PARENTE_DISTANTE))
						|| (noValidoRetornado.getNoConversor().getNoPai() instanceof NoConversor)) {

					NoRamoCorrente noRamo = new NoRamoCorrente();

					if (noValidoRetornado.getTipoNo().equals(NoValido.TIPO_PAI)) {

						while (noRamoCorrente.getNoRamoPai() != null
								&& (!noRamoCorrente.getNoConversor()
										.getMarcadorInicial().equals(
												noValidoRetornado
														.getNoConversor()
														.getMarcadorInicial()))
								&& noRamoCorrente.getNoRamoPai()
										.getNoConversor() instanceof NoConversor) {
							adicionarFilhoAoPai(noRamoCorrente
									.getArquivoAnalisadorCorrente(),
									noRamoCorrente.getNoRamoPai()
											.getArquivoAnalisadorCorrente(),
									noRamoCorrente.getNoConversor());

							NoRamoCorrente noRamoCorrenteTemp = new NoRamoCorrente();
							noRamoCorrenteTemp
									.setArquivoAnalisadorCorrente(noRamoCorrente
											.getNoRamoPai()
											.getArquivoAnalisadorCorrente());
							noRamoCorrenteTemp.setNoConversor(noRamoCorrente
									.getNoRamoPai().getNoConversor());

							noRamoCorrenteTemp.setNoRamoPai(noRamoCorrente
									.getNoRamoPai().getNoRamoPai());

							noRamoCorrente = noRamoCorrenteTemp;
						}

						adicionarFilhoAoPai(noRamoCorrente
								.getArquivoAnalisadorCorrente(), noRamoCorrente
								.getNoRamoPai().getArquivoAnalisadorCorrente(),
								noRamoCorrente.getNoConversor());

						noRamo.setNoRamoPai(noRamoCorrente.getNoRamoPai());

					} else if (noValidoRetornado.getTipoNo().equals(
							NoValido.TIPO_IRMAO)) {

						adicionarFilhoAoPai(noRamoCorrente
								.getArquivoAnalisadorCorrente(), noRamoCorrente
								.getNoRamoPai().getArquivoAnalisadorCorrente(),
								noRamoCorrente.getNoConversor());

						noRamo.setNoRamoPai(noRamoCorrente.getNoRamoPai());

					} else if (noValidoRetornado.getTipoNo().equals(
							NoValido.TIPO_PARENTE_DISTANTE)) {

						while (noRamoCorrente.getNoRamoPai() != null
								&& noRamoCorrente.getNoRamoPai()
										.getNoConversor() instanceof NoConversor) {

							adicionarFilhoAoPai(noRamoCorrente
									.getArquivoAnalisadorCorrente(),
									noRamoCorrente.getNoRamoPai()
											.getArquivoAnalisadorCorrente(),
									noRamoCorrente.getNoConversor());

							NoRamoCorrente noRamoCorrenteTemp = new NoRamoCorrente();
							noRamoCorrenteTemp
									.setArquivoAnalisadorCorrente(noRamoCorrente
											.getNoRamoPai()
											.getArquivoAnalisadorCorrente());
							noRamoCorrenteTemp.setNoConversor(noRamoCorrente
									.getNoRamoPai().getNoConversor());

							noRamoCorrenteTemp.setNoRamoPai(noRamoCorrente
									.getNoRamoPai().getNoRamoPai());

							noRamoCorrente = noRamoCorrenteTemp;

							NoValido noValidoRetornadoTemp = retornarNoValidoParaALinha(
									linha, noRamoCorrente.getNoConversor());

							if (noValidoRetornadoTemp.getTipoNo().equals(
									NoValido.TIPO_IRMAO)) {

								arquivoAnalisador = gerarNo(linha,
										noValidoRetornadoTemp.getNoConversor());
																							
								if (!arquivoComNumeroSequencialValido(
										arquivoAnalisador,
										gerenciadorLinhasArquivo
												.getLinhaCorrente())) {
									lstResultado.clear();
									return lstResultado;
								}

								if (noRamoCorrente.getNoConversor().getNoPai() instanceof NoConversor) {

									adicionarFilhoAoPai(
											noRamoCorrente
													.getArquivoAnalisadorCorrente(),
											noRamoCorrente
													.getNoRamoPai()
													.getArquivoAnalisadorCorrente(),
											noRamoCorrente.getNoConversor());

									noRamo.setNoRamoPai(noRamoCorrente
											.getNoRamoPai());
									noRamo
											.setArquivoAnalisadorCorrente(arquivoAnalisador);
									noRamo.setNoConversor(noValidoRetornadoTemp
											.getNoConversor());

									noRamoCorrente = noRamo;

								} else {

									adicionarFilhoAoPai(noRamoCorrente
											.getArquivoAnalisadorCorrente(),
											lstResultado, noRamoCorrente
													.getNoConversor());

									noRamo
											.setArquivoAnalisadorCorrente(arquivoAnalisador);
									noRamo.setNoConversor(noValidoRetornadoTemp
											.getNoConversor());

									noRamoCorrente = noRamo;

								}

								continue leitorLinhas;
							}

						}

					} else {
						noRamo.setNoRamoPai(noRamoCorrente);
					}

					noRamo.setArquivoAnalisadorCorrente(arquivoAnalisador);
					noRamo.setNoConversor(noValidoRetornado.getNoConversor());

					noRamoCorrente = noRamo;

				} else {

					if (noRamoCorrente != null) {

						while (noRamoCorrente.getNoRamoPai() != null
								&& noRamoCorrente.getNoRamoPai()
										.getNoConversor() instanceof NoConversor) {
							adicionarFilhoAoPai(noRamoCorrente
									.getArquivoAnalisadorCorrente(),
									noRamoCorrente.getNoRamoPai()
											.getArquivoAnalisadorCorrente(),
									noRamoCorrente.getNoConversor());

							NoRamoCorrente noRamoCorrenteTemp = new NoRamoCorrente();
							noRamoCorrenteTemp
									.setArquivoAnalisadorCorrente(noRamoCorrente
											.getNoRamoPai()
											.getArquivoAnalisadorCorrente());
							noRamoCorrenteTemp.setNoConversor(noRamoCorrente
									.getNoRamoPai().getNoConversor());

							noRamoCorrenteTemp.setNoRamoPai(noRamoCorrente
									.getNoRamoPai().getNoRamoPai());

							noRamoCorrente = noRamoCorrenteTemp;
						}

						adicionarFilhoAoPai(noRamoCorrente
								.getArquivoAnalisadorCorrente(), lstResultado,
								noRamoCorrente.getNoConversor());

						noRamoCorrente = null;
					}

					noRamoCorrente = new NoRamoCorrente();
					noRamoCorrente
							.setArquivoAnalisadorCorrente(arquivoAnalisador);
					noRamoCorrente.setNoConversor(noValidoRetornado
							.getNoConversor());
				}

			} catch (ConversorExcecao e) {
				conversorOuvinte.notificarMensagem("Linha Inv�lida - " + linha,
						TipoMensagemAnalisadorEnumeracao.ERRO);
			} catch (ReflexaoExcecao e) {
				conversorOuvinte.notificarMensagem("Linha Inv�lida - " + linha,
						TipoMensagemAnalisadorEnumeracao.ERRO);
			}

		}

		if (noRamoCorrente != null) {

			while (noRamoCorrente.getNoRamoPai() != null
					&& noRamoCorrente.getNoRamoPai().getNoConversor() instanceof NoConversor) {
				adicionarFilhoAoPai(noRamoCorrente
						.getArquivoAnalisadorCorrente(), noRamoCorrente
						.getNoRamoPai().getArquivoAnalisadorCorrente(),
						noRamoCorrente.getNoConversor());

				NoRamoCorrente noRamoCorrenteTemp = new NoRamoCorrente();
				noRamoCorrenteTemp.setArquivoAnalisadorCorrente(noRamoCorrente
						.getNoRamoPai().getArquivoAnalisadorCorrente());
				noRamoCorrenteTemp.setNoConversor(noRamoCorrente.getNoRamoPai()
						.getNoConversor());

				noRamoCorrenteTemp.setNoRamoPai(noRamoCorrente.getNoRamoPai()
						.getNoRamoPai());

				noRamoCorrente = noRamoCorrenteTemp;
			}

			try {
				adicionarFilhoAoPai(noRamoCorrente
						.getArquivoAnalisadorCorrente(), lstResultado,
						noRamoCorrente.getNoConversor());
			} catch (ReflexaoExcecao e) {
				conversorOuvinte.notificarMensagem("Registro Inv�lido - ",
						TipoMensagemAnalisadorEnumeracao.ERRO, noRamoCorrente
								.getArquivoAnalisadorCorrente());
			}
		}

		return lstResultado;

	}

	private boolean arquivoComNumeroSequencialValido(
			ArquivoAnalisador arquivoAnalisador, Integer linhaCorrente) {
		try {
			Long numeroSequencial = (Long) getAssistenteReflexao()
					.obterValorPropriedade(arquivoAnalisador,
							ArquivoAnalisador.PROPRIEDADE_NUMERO_SEQUENCIAL);

			if ((numeroSequencial == null)
					|| (numeroSequencial.intValue() != (linhaCorrente + 1))) {
				conversorOuvinte
						.notificarMensagem(
								"Registro com N�mero Sequencial Inv�lido - "
										+ "Todos os registros do arquivo ser�o ignorados ",
								TipoMensagemAnalisadorEnumeracao.ERRO,
								arquivoAnalisador);

				return false;
			}

			return true;

		} catch (ReflexaoExcecao e) {
			conversorOuvinte.notificarMensagem(
					"Registro com N�mero Sequencial Inv�lido - "
							+ "Todos os registros do arquivo ser�o ignorados ",
					TipoMensagemAnalisadorEnumeracao.ERRO, arquivoAnalisador);

			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean adicionarFilhoAoPai(ArquivoAnalisador arquivoAnalisador,
			ArquivoAnalisador arquivoAnalisadorPai, NoConversor noConversor) {

		String nomeClasse = arquivoAnalisador.getClass().getName().replaceAll(
				arquivoAnalisador.getClass().getPackage().getName() + ".", "");

		try {
			List<ArquivoAnalisador> listaTemp = (List<ArquivoAnalisador>) getAssistenteReflexao()
					.obterValorPropriedade(arquivoAnalisadorPai,
							"lst" + nomeClasse);

			List<ArquivoAnalisador> lista = new ArrayList<ArquivoAnalisador>();

			for (Iterator<ArquivoAnalisador> iter = listaTemp.iterator(); iter.hasNext();) {
				ArquivoAnalisador element = (ArquivoAnalisador) iter.next();

				if (element.getClass().equals(arquivoAnalisador.getClass())) {
					lista.add(element);
				}
			}

			try {
				if (!temChaveNula(arquivoAnalisador, noConversor)) {

					try {
						verificarNovoArquivoAnalisadorTemChaveExistenteNaListaIrmaos(
								arquivoAnalisador, lista, noConversor
										.getChaves());
					} catch (AjudanteExcecao e) {
						e.printStackTrace();
						throw new RuntimeException(e.getMessage());
					}

					listaTemp.add(arquivoAnalisador);

					return true;

				}
				conversorOuvinte.notificarMensagem(
						"Registro com Chave Nula - (Ignorado na an�lise) ",
						TipoMensagemAnalisadorEnumeracao.ERRO, arquivoAnalisador);

				return false;
			} catch (ConversorExcecao e) {
				conversorOuvinte.notificarMensagem(e.getMessage(),
						TipoMensagemAnalisadorEnumeracao.ERRO, arquivoAnalisador);
			}

		} catch (ReflexaoExcecao e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}

		return false;
	}

	public boolean adicionarFilhoAoPai(ArquivoAnalisador arquivoAnalisador,
			List<ArquivoAnalisador> listaTemp, NoConversor noConversor)
			throws ReflexaoExcecao {

		try {

			List<ArquivoAnalisador> lista = new ArrayList<ArquivoAnalisador>();

			for (Iterator<ArquivoAnalisador> iter = listaTemp.iterator(); iter.hasNext();) {
				ArquivoAnalisador element = (ArquivoAnalisador) iter.next();

				if (element.getClass().equals(arquivoAnalisador.getClass())) {
					lista.add(element);
				}
			}

			if (!temChaveNula(arquivoAnalisador, noConversor)) {

				try {
					verificarNovoArquivoAnalisadorTemChaveExistenteNaListaIrmaos(
							arquivoAnalisador, lista, noConversor.getChaves());
				} catch (AjudanteExcecao e) {
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}

				listaTemp.add(arquivoAnalisador);

				return true;

			}
			conversorOuvinte.notificarMensagem(
					"Registro com Chave Nula - (Ignorado na an�lise) ",
					TipoMensagemAnalisadorEnumeracao.ERRO, arquivoAnalisador);

			return false;
		} catch (ConversorExcecao e) {
			conversorOuvinte.notificarMensagem(e.getMessage(),
					TipoMensagemAnalisadorEnumeracao.ERRO, arquivoAnalisador);
		}

		return false;
	}

	public NoValido retornarNoValidoParaALinha(String linha, NoRaiz noCorrente) {

		NoValido noValido = new NoValido();

		for (NoConversor noFilho : noCorrente.obterFilhos()) {

			if (eDoTipoDoNo(linha, noFilho)) {

				noValido.setNoConversor(noFilho);
				noValido.setTipoNo(NoValido.TIPO_FILHO);

				return noValido;
			}

		}

		if ((noCorrente instanceof NoConversor)
				&& eDoTipoDoNo(linha, (NoConversor) noCorrente)) {
			noValido.setNoConversor((NoConversor) noCorrente);
			noValido.setTipoNo(NoValido.TIPO_IRMAO);

			return noValido;
		}

		if (noCorrente instanceof NoConversor) {
			NoConversor noConversor = (NoConversor) noCorrente;

			for (NoConversor noFilho : noConversor.getNoPai().obterFilhos()) {

				if (eDoTipoDoNo(linha, noFilho)) {

					noValido.setNoConversor(noFilho);
					noValido.setTipoNo(NoValido.TIPO_IRMAO);

					return noValido;
				}

			}
		}

		if ((noCorrente instanceof NoConversor && ((NoConversor) noCorrente)
				.getNoPai() instanceof NoConversor)
				&& eDoTipoDoNo(linha, (NoConversor) ((NoConversor) noCorrente)
						.getNoPai())) {
			noValido.setNoConversor((NoConversor) ((NoConversor) noCorrente)
					.getNoPai());
			noValido.setTipoNo(NoValido.TIPO_PAI);

			return noValido;
		}

		while (noCorrente instanceof NoConversor) {

			noCorrente = ((NoConversor) noCorrente).getNoPai();

			for (NoConversor filho : noCorrente.obterFilhos()) {
				if (eDoTipoDoNo(linha, filho)) {
					noValido.setTipoNo(NoValido.TIPO_PARENTE_DISTANTE);
					return noValido;
				}
			}

		}

		noValido.setTipoNo(NoValido.TIPO_SEM_PARENTESCO);
		return noValido;
	}

	private boolean eDoTipoDoNo(String linha, NoConversor noConversor) {
		return linha.indexOf(noConversor.getMarcadorInicial()) == 0;
	}

	public String gerarChaveIdentificacao(ArquivoAnalisador arquivoAnalisador,
			String[] chaves) throws AjudanteExcecao {
		String novoConteudo = new String();

		for (int j = 0; j < chaves.length; ++j) {
			try {
				novoConteudo += getAssistenteReflexao().obterValorPropriedade(
						arquivoAnalisador, chaves[j]).toString() + AjudanteTexto.TRACO;
			} catch (ReflexaoExcecao e) {
				throw new AjudanteExcecao(e.getMessage());
			}
		}

		return novoConteudo;
	}

	public void verificarNovoArquivoAnalisadorTemChaveExistenteNaListaIrmaos(
			ArquivoAnalisador novoArquivoAnalisador,
			List<? extends ArquivoAnalisador> irmaos, String[] chaves)
			throws AjudanteExcecao {

		String novoChaveIdentificacao = gerarChaveIdentificacao(
				novoArquivoAnalisador, chaves);
		novoArquivoAnalisador.setChaveIdentificacao(novoChaveIdentificacao);

		for (int i = 0; i < irmaos.size(); ++i) {
			ArquivoAnalisador arquivoAnalisador = irmaos.get(i);

			if (arquivoAnalisador.getChaveIdentificacao().equalsIgnoreCase(
					novoArquivoAnalisador.getChaveIdentificacao())) {
				conversorOuvinte.notificarMensagem("Registro duplicado",
						TipoMensagemAnalisadorEnumeracao.ERRO, novoArquivoAnalisador);
			}
		}

	}

	private boolean temChaveNula(ArquivoAnalisador arquivoAnalisador,
			NoConversor noConversor) throws ConversorExcecao {

		String[] chaves = noConversor.getChaves();

		for (int i = 0; i < chaves.length; ++i) {
			try {
				if (getAssistenteReflexao().obterValorPropriedade(
						arquivoAnalisador, chaves[i]) == null) {
					return true;
				}
			} catch (ReflexaoExcecao e) {
				throw new ConversorExcecao(e.getMessage());
			}
		}

		return false;
	}

	private ArquivoAnalisador gerarNo(String linha, NoConversor noConversor)
			throws ConversorExcecao {

		
		ArquivoAnalisador arquivoAnalisador = null;
		try {
			arquivoAnalisador = (ArquivoAnalisador) noConversor
					.getClasseDestino().newInstance();
		} catch (InstantiationException e) {
			conversorOuvinte.notificarMensagem(e.getMessage() + " - " + linha,
					TipoMensagemAnalisadorEnumeracao.ERRO);
			return null;
		} catch (IllegalAccessException e) {
			conversorOuvinte.notificarMensagem(e.getMessage() + " - " + linha,
					TipoMensagemAnalisadorEnumeracao.ERRO);
			return null;
		}

		/*if (linha.length() < noConversor.getTamanhoLinha()) {
			conversorOuvinte.notificarMensagem(
					"Linha com Tamanho Inconsistente com Layout - " + linha,
					TipoMensagemAnalisadorEnum.ERRO);
			return arquivoAnalisador;
		}*/
		
		List<String> errosConversao = new ArrayList<String>();

		for (RegraAnalisador regraAnalisador : noConversor.obterRegras()) {
			try {
				regraAnalisador.processarRegra(linha, arquivoAnalisador);
			} catch (ConversorExcecao e) {
				errosConversao.add(e.getMessage());
			}
		}

		for (String erro : errosConversao) {
			conversorOuvinte.notificarMensagem(erro,
					TipoMensagemAnalisadorEnumeracao.ERRO, arquivoAnalisador);
		}

		return arquivoAnalisador;
	}

	public AjudanteReflexao getAssistenteReflexao() {
		if (assistenteReflexao == null)
			assistenteReflexao = new AjudanteReflexao();
		return assistenteReflexao;
	}

	public AjudanteTexto getAssistenteTexto() {
		if (assistenteTexto == null)
			assistenteTexto = new AjudanteTexto();
		return assistenteTexto;
	}

	public void setAssistenteReflexao(AjudanteReflexao assistenteReflexao) {
		this.assistenteReflexao = assistenteReflexao;
	}

	public void setAssistenteTexto(AjudanteTexto assistenteTexto) {
		this.assistenteTexto = assistenteTexto;
	}

	private class NoValido {

		public final static int TIPO_PAI = 0;

		public final static int TIPO_IRMAO = 1;

		public final static int TIPO_FILHO = 2;

		public static final int TIPO_PARENTE_DISTANTE = 3;

		public static final int TIPO_SEM_PARENTESCO = 4;

		private NoConversor noConversor;

		private Integer tipoNo;

		public NoConversor getNoConversor() {
			return noConversor;
		}

		public Integer getTipoNo() {
			return tipoNo;
		}

		public void setNoConversor(NoConversor noConversor) {
			this.noConversor = noConversor;
		}

		public void setTipoNo(Integer tipoNo) {
			this.tipoNo = tipoNo;
		}
	}

	private class NoRamoCorrente {

		private ArquivoAnalisador arquivoAnalisadorCorrente;

		private NoConversor noConversor;

		private NoRamoCorrente noRamoPai;

		public ArquivoAnalisador getArquivoAnalisadorCorrente() {
			return arquivoAnalisadorCorrente;
		}

		public NoRamoCorrente getNoRamoPai() {
			return noRamoPai;
		}

		public void setArquivoAnalisadorCorrente(
				ArquivoAnalisador arquivoAnalisadorCorrente) {
			this.arquivoAnalisadorCorrente = arquivoAnalisadorCorrente;
		}

		public void setNoRamoPai(NoRamoCorrente noRamoPai) {
			this.noRamoPai = noRamoPai;
		}

		public NoConversor getNoConversor() {
			return noConversor;
		}

		public void setNoConversor(NoConversor noConversor) {
			this.noConversor = noConversor;
		}

	}

	@SuppressWarnings("unused")
	private class GerenciadorLinhasArquivo {

		private Boolean ehAUltimaLinha;

		private Integer linhaCorrente;

		private List<String> linhas;

		GerenciadorLinhasArquivo(List<String> linhas) {
			this.linhas = linhas;
		}

		public String obterProximaLinha() {
			if (!getEhAUltimaLinha())
				setLinhaCorrente(getLinhaCorrente() + 1);
			return getLinhas().get(getLinhaCorrente());
		}

		public Integer getLinhaCorrente() {
			if (linhaCorrente == null)
				linhaCorrente = new Integer(-1);
			return linhaCorrente;
		}

		public List<String> getLinhas() {
			return linhas;
		}

		public void setLinhaCorrente(Integer linhaCorrente) {
			this.linhaCorrente = linhaCorrente;
		}

		public void setLinhas(List<String> linhas) {
			this.linhas = linhas;
		}

		public Boolean getEhAUltimaLinha() {
			return getLinhaCorrente().equals(getLinhas().size() - 1);
		}

		public void setEhAUltimaLinha(Boolean ehAUltimaLinha) {
			this.ehAUltimaLinha = ehAUltimaLinha;
		}
	}

}
