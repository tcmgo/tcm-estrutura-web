package br.gov.go.tcm.estrutura.controlador.base;

import java.util.StringTokenizer;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoFaces;

@Controller("gerenciadorDePaginasSite")
@Scope("session")
public class GerenciadorDePaginasSite extends ControladorPadrao {

	private static final long serialVersionUID = -8363807326590005856L;

	private String pagina;

	public String getPagina() {

		String subDiretorio = AjudanteContextoFaces.getFacesContext().getExternalContext().getRequestServletPath();

		StringTokenizer st = new StringTokenizer(subDiretorio, "/");

		subDiretorio = st.nextToken();

		if (subDiretorio != null && subDiretorio.equals("index.jsf")) {
			return "../principal.xhtml";
		}

		// Vindo do menu principal
		String paginaRequest = AjudanteContextoFaces.getRequestParameterMap().get("pagina");

		// if (paginaRequest != null && !paginaRequest.equals(pagina)) {
		if (paginaRequest != null) {
			pagina = "../" + subDiretorio + "/" + paginaRequest;
		} else if (paginaRequest == null && subDiretorio != null) {
			pagina = "../" + subDiretorio + "/" + (pagina == null ? "principal.xhtml" : pagina);
		}

		return pagina;
	}

	public void setPagina(String pagina) {
		this.pagina = pagina;
	}
}