package br.gov.go.tcm.estrutura.entidade.pk.orcafi;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import br.gov.go.tcm.estrutura.entidade.pk.PK;

@Embeddable
public class OrgaoPK implements Serializable, PK {

	private static final long serialVersionUID = -8973585292482560765L;

	@Column(name = "codOrgao")
	@NotFound(action = NotFoundAction.IGNORE)
	private Long codigoOrgao;

	@Column(name = "codMunicipio")
	@NotFound(action = NotFoundAction.IGNORE)
	private Long codigoMunicipio;

	public Long getCodigoMunicipio() {
		return codigoMunicipio;
	}

	public Long getCodigoOrgao() {
		return codigoOrgao;
	}

	public void setCodigoMunicipio(Long municipio) {
		this.codigoMunicipio = municipio;
	}

	public void setCodigoOrgao(Long orgao) {
		this.codigoOrgao = orgao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		OrgaoPK orgaoPK = (OrgaoPK) obj;
		return orgaoPK.getCodigoOrgao().equals(this.getCodigoOrgao())
				&& orgaoPK.getCodigoMunicipio().equals(this.getCodigoMunicipio());
	}

	@Override
	public int hashCode() {
		assert false : "hashCode n�o definido";
		int hash = 4;
		hash = 34 * hash + (null == codigoOrgao ? 0 : codigoOrgao.hashCode());
		hash = 34 * hash + (null == codigoMunicipio ? 0 : codigoMunicipio.hashCode());
		return hash;
	}
}
