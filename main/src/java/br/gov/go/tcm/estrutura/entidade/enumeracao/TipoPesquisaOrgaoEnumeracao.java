package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoPesquisaOrgaoEnumeracao implements NumberEnumeracaoPersistente {

	EXECUTIVO(1), LEGISLATIVO_AUTARQUIA(2), AMBOS(3);

	int valor;

	TipoPesquisaOrgaoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Executivo";
		case 2:
			return "Legislativo / Autarquia";
		case 3:
			return "Ambos";
		default:
			return "";
		}

	}

}
