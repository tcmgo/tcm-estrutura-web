package br.gov.go.tcm.estrutura.persistencia.usuario.contrato;

import br.gov.go.tcm.estrutura.entidade.usuario.UsuarioChefeSecao;
import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;

public interface UsuarioChefeSecaoDAO extends GenericoDAO{

	UsuarioChefeSecao obterUsuarioChefeSecao(Long idUsuario, Long codigoSecao);
	
}
