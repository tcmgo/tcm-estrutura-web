package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoSituacaoProcessoEnumeracao implements StringEnumeracaoPersistente{
	NORMAL("0"),ARQUIVADO("1"),FORA_TRIBUNAL("2"),OUTROS(" ");
	
	String valor;
	
	 TipoSituacaoProcessoEnumeracao(String valor){
		this.valor = valor;
	}
	
	public String getValorOrdinal(){
		return this.valor;
	}
		
	public EnumeracaoPersistente valor(){
		if(valor.equals("0")){
			return TipoSituacaoProcessoEnumeracao.NORMAL;
		} else if(valor.equals("1")){
			return TipoSituacaoProcessoEnumeracao.ARQUIVADO;
		} else if(valor.equals("2")){
			return TipoSituacaoProcessoEnumeracao.FORA_TRIBUNAL;
		} else if(valor.equals(" ")){
			return TipoSituacaoProcessoEnumeracao.OUTROS;
		}  
		
		return null;
	}
	
	@Override
	public String toString(){
		if(valor.equals("0")){
			return "Normal";
		} else if(valor.equals("1")){
			return "Arquivado";
		} else if(valor.equals("2")){
			return "Fora do Tribunal";
		} else if(valor.equals(" ")){
			return "Outros";
		}   
		return "";
	}
}
