package br.gov.go.tcm.estrutura.entidade.controledeacesso;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoTipo;
import br.gov.go.tcm.estrutura.entidade.enumeracao.UsuarioEnumeracao;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;

import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "UsuarioPapelModulo")
@Proxy(lazy = true, proxyClass = UsuarioPerfilModulo.class)
@Banco(nome = Banco.TCMWEB)
@TypeDefs( { @TypeDef(name = "usuario", typeClass = EnumeracaoTipo.class, parameters = { @Parameter(name = "enumClassName", value = "br.gov.go.tcm.estrutura.entidade.enumeracao.UsuarioEnumeracao") }) })
@NamedQueries( {
		@NamedQuery(name = "obterUsuarioPerfilModuloPorUsuarioIdETipoUsuario", 
				query = "SELECT u FROM UsuarioPerfilModulo u WHERE u.usuarioId = :usuarioId AND u.tipoUsuario = :tipoUsuario "),
		@NamedQuery(name = "obterUsuarioPerfilModuloPorModuloUsuarioIdTipoUsuario", 
				query = "SELECT u FROM UsuarioPerfilModulo u WHERE u.modulo = :modulo AND u.usuarioId = :usuarioId AND u.tipoUsuario = :tipoUsuario "),
		@NamedQuery(name = "obterUsuarioPerfilModuloPorUsuarioIdTipoUsuario", 
				query = "SELECT u FROM UsuarioPerfilModulo u WHERE u.usuarioId = :usuarioId AND u.tipoUsuario = :tipoUsuario "),
		@NamedQuery(name = "obterUsuarioPerfilModuloPorPerfilTipoUsuario", 
				query = "SELECT u FROM UsuarioPerfilModulo u WHERE u.perfil = :perfil AND u.tipoUsuario = :tipoUsuario "),
		@NamedQuery( name = "obterUsuarioPerfilModuloPorId",
				query = "SELECT u FROM UsuarioPerfilModulo u LEFT JOIN fetch u.perfil LEFT JOIN fetch u.modulo WHERE u.id = :id"),
		@NamedQuery(name = "obterUsuarioIdPorPerfilTipoUsuario", 
				query = "SELECT u.usuarioId FROM UsuarioPerfilModulo u WHERE u.perfil = :perfil AND u.tipoUsuario = :tipoUsuario "),		
})
public class UsuarioPerfilModulo extends EntidadePadrao {

	private static final long serialVersionUID = -2565708671802618223L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	@NotNull
	private Long usuarioId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "papel_id")
	private Perfil perfil;

	@ManyToOne(fetch = FetchType.LAZY)
	private Modulo modulo;

	@Type(type = "usuario")
	@Column
	private UsuarioEnumeracao tipoUsuario;

	@Override
	public Long getId() {
		return id;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public UsuarioEnumeracao getTipoUsuario() {
		return tipoUsuario;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public void setUsuarioId(Long usuarioWebId) {
		this.usuarioId = usuarioWebId;
	}

	public void setTipoUsuario(UsuarioEnumeracao tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

}
