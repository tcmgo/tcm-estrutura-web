package br.gov.go.tcm.estrutura.container.spring;

import javax.faces.FactoryFinder;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.jsf.FacesContextUtils;

import br.gov.go.tcm.estrutura.modelo.FabricaModelo;
import br.gov.go.tcm.estrutura.relatorio.FabricaRelatorio;
import br.gov.go.tcm.estrutura.util.diretorio.AjudanteDiretorio;

public class ContextoSpring {

	private static String contextoRaiz;

	@SuppressWarnings("unused")
	private static String caminhoClasses;

	@SuppressWarnings("unused")
	private static String contextoVirtualRaiz;

	public final static String FABRICA_RELATORIO = "fabricaRelatorio";

	public static final String CABECALHO_RELATORIO_PADRAO = "cabecalhoRelatorioPadrao";

	public static final String PACOTE_CONTEM_ENUMERACAO_PERSISTENTE = "pacoteContemEnumeracaoPersistente";

	public static final String DIRETORIO_GED = "diretorioGed";
	
	public static final String DIRETORIO_GED_3 = "diretorioGed3";

	public static final String DIRETORIO_ANALISADOR = "diretorioAnalisador";

	public static final String LOCALIDADE_PADRAO = "localidadePadrao";

	public static final String ZONA_TEMPO_PADRAO = "zonaTempoPadrao";

	public static final String LINGUAGEM_PADRAO = "linguagemPadrao";

	public static final String ARQUIVO_CHAVE_CRIPTOGRAFIA = "arquivoChaveCriptografia";

	public static final String SENHA_CRIPTOGRAFIA = "senhaCriptografia";

	public final static String FABRICA_MODELO = "fabricaModelo";
	
	public static final String EMAIL_CADASTRO = "emailCadastro";
	
	public static final String EMAIL_PORTAL = "emailPortal";

	public static final String EMAIL_CADASTRO_USUARIO = "emailCadastroUsuario";

	public static final String EMAIL_CADASTRO_SENHA = "emailCadastroSenha";

	public static final String SERVIDOR_SMTP = "servidorSmtp";

	public static final String DIRETORIO_FOTOS_PAGINA_PRINCIPAL = "diretorioFotosPaginaPrincipal";

	public static final String EMAIL_DESTINO_FALE_CONOSCO = "emailDestinoFaleConosco";

	public static final String DIRETORIO_GED_DIGITAL = "diretorioGedDigital";
	
	public static final String CHAVE_PUBLICA_RECAPTCHA = "chavePublicaReCaptcha";
	
	public static final String CHAVE_PRIVADA_RECAPTCHA = "chavePrivadaReCaptcha";
	
	public static final String EMAIL_DESTINO_OUVIDORIA = "emailDestinoOuvidoria";
	
	public static final String EMAIL_ORIGEM_ESCOLA_DE_CONTAS = "emailOrigemEscolaDeContas";
	
	public static final String EMAIL_ORIGEM_RECURSOS_HUMANOS = "emailOrigemRH";
	
	public static final String EMAIL_ORIGEM_DIARIO_ELETRONICO = "emailOrigemDiarioEletronico";
	
	public static final String CAMINHO_CONTEXTO_FTL = "caminhoContextoFTL";
	
	public static final String CAMINHO_CONTEXTO = "caminhoContexto";
	
	public static final String NOME_BENEFICIARIO = "nomeBeneficiario";
	
	public static final String CNPJ_BENEFICIARIO = "cnpjBeneficiario";
	
	public static final String AGENCIA_BENEFICIARIO = "agenciaBeneficiario";
	
	public static final String CODIGO_BENEFICIARIO = "codigoBeneficiario";
	
	public static final String CARTEIRA_BOLETO = "carteira";
	
	public static final String 	ENDERECO_BENEFICIARIO = "enderecoBeneficiario";
	
	public static String getCabecalhoRelatorioPadrao() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.CABECALHO_RELATORIO_PADRAO);
	}

	public static String getDiretorioContexto() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
		String realpath = servletContext.getRealPath(".");

		ContextoSpring.contextoRaiz = realpath + AjudanteDiretorio.BARRA;

//		HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();

//		ContextoSpring.contextoVirtualRaiz = new String(request.getRequestURL().toString()).replaceAll(request
//				.getRequestURI(), "")
//				+ request.getContextPath() + AjudanteDiretorio.BARRA;

		ContextoSpring.caminhoClasses = ContextoSpring.contextoRaiz + "WEB-INF/classes" + AjudanteDiretorio.BARRA;

		return ContextoSpring.contextoRaiz;
	}

	public static String getPacoteContemEnumeracaoPersistente() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.PACOTE_CONTEM_ENUMERACAO_PERSISTENTE);
	}

	public static String getDiretorioGed() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.DIRETORIO_GED);
	}
	
	public static String getDiretorioGed3() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.DIRETORIO_GED_3);
	}

	public static String getDiretorioAnalisador() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.DIRETORIO_ANALISADOR);
	}

	public static String getLocalidadePadrao() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.LOCALIDADE_PADRAO);
	}

	public static String getZonaTempoPadrao() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.ZONA_TEMPO_PADRAO);
	}

	public static String getLinguagemPadrao() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.LINGUAGEM_PADRAO);
	}

	public static String getArquivoChaveCriptografia() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.ARQUIVO_CHAVE_CRIPTOGRAFIA);
	}

	public static String getSenhaCriptografia() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.SENHA_CRIPTOGRAFIA);
	}
	
	public static String getEmailCadastro() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.EMAIL_CADASTRO);
	}
	
	public static String getEmailPortal() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.EMAIL_PORTAL);
	}

	public static String getEmailCadastroUsuario() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.EMAIL_CADASTRO_USUARIO);
	}

	public static String getEmailCadastroSenha() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.EMAIL_CADASTRO_SENHA);
	}

	public static String getServidorSmtp() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.SERVIDOR_SMTP);
	}

	public static String getDiretorioFotosPaginaPrincipal() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.DIRETORIO_FOTOS_PAGINA_PRINCIPAL);
	}

	public static String getEmailDestinoFaleConosco() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.EMAIL_DESTINO_FALE_CONOSCO);
	}

	public static FabricaRelatorio getFabricaRelatorio() {
		return (FabricaRelatorio) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance())
				.getBean(ContextoSpring.FABRICA_RELATORIO);
	}

	public static FabricaRelatorio getFabricaRelatorioJasper() {
		return (FabricaRelatorio) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance())
				.getBean(ContextoSpring.FABRICA_RELATORIO);
	}

	public static FabricaModelo getFabricaModelo() {
		return (FabricaModelo) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.FABRICA_MODELO);
	}

	public static String getDiretorioGedDigital() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.DIRETORIO_GED_DIGITAL);
	}
	
	public static String getChavePublicaReCaptcha() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.CHAVE_PUBLICA_RECAPTCHA);
	}
	
	public static String getChavePrivadaReCaptcha() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.CHAVE_PRIVADA_RECAPTCHA);
	}
	
	public static String getEmailDestinoOuvidoria() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.EMAIL_DESTINO_OUVIDORIA);
	}
	
	public static String getEmailOrigemEscolaDeContas() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.EMAIL_ORIGEM_ESCOLA_DE_CONTAS); 
	}
	
	public static String getEmailOrigemRecursosHumanos() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.EMAIL_ORIGEM_RECURSOS_HUMANOS);
	}
	
	public static String getEmailOrigemDiarioEletronico() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.EMAIL_ORIGEM_DIARIO_ELETRONICO);
	}
	
	public static String getNomeBeneficiario() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.NOME_BENEFICIARIO);
	}
	
	public static String getCNPJBeneficiario() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.CNPJ_BENEFICIARIO);
	}
	
	public static String getAgenciaBeneficiario() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.AGENCIA_BENEFICIARIO);
	}
	
	public static String getCodigoBeneficiario() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.CODIGO_BENEFICIARIO);
	}
	
	public static String getCarteiraBoleto() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.CARTEIRA_BOLETO);
	}
	
	public static String getEnderecoBeneficiario() {
		return (String) FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance()).getBean(
				ContextoSpring.ENDERECO_BENEFICIARIO);
	}
	
	private static FacesContext facesContext;
	
	public static FacesContext obterContextoFaces() {
		return facesContext;
	}
	
	public static void setContextoFaces(ServletContext servletContext, HttpServletRequest request, HttpServletResponse response) {
		LifecycleFactory lifecycleFactory = (LifecycleFactory) FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
		FacesContextFactory contextFactory  = (FacesContextFactory) FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
		Lifecycle lifecycle  = lifecycleFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);
		
		facesContext = contextFactory.getFacesContext(servletContext, request, response, lifecycle);
	}
}