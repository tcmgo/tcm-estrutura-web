package br.gov.go.tcm.estrutura.entidade.ajudante;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class AjudanteOrdenacao {

	private static final Locale TEST_LOCALE = Locale.getDefault();
	
	class ComparadorGenerico implements Comparator<Object> {

		private String[] lstCampos;

		public ComparadorGenerico(String[] lstCampos) {
			this.lstCampos = lstCampos;
		}

		@SuppressWarnings("unchecked")
		public int compare(Object o1, Object o2) {

			AjudanteReflexao ajudanteReflexao = new AjudanteReflexao();
			
			/**
			 * Para entender:
			 * http://www.javapractices.com/topic/TopicAction.do?Id=207
			 * **/
			Collator collator = Collator.getInstance(TEST_LOCALE);
			collator.setStrength(Collator.PRIMARY);

			for (int i = 0 ; i < lstCampos.length; ++i) {

				try {
					
					Boolean ehString = Boolean.FALSE;
					Integer valorTemporario = new Integer(0);
					
					Object valor1 = ajudanteReflexao.obterValorPropriedade(o1,
							lstCampos[i]);
					Object valor2 = ajudanteReflexao.obterValorPropriedade(o2,
							lstCampos[i]);

					if (!(valor1 instanceof Comparable<?>)) {
						throw new AjudanteExcecao(
								"Propriedade n�o implementa a interface Comparable");
					}
					
					if(valor1 instanceof String){
						ehString = Boolean.TRUE;
					}
					
					
					if(ehString){
						valorTemporario = collator.compare(valor1, valor2);
					} else{
						valorTemporario = ((Comparable<Object>)valor1).compareTo(valor2);
					}
					
					
					if(i == lstCampos.length - 1)
						return valorTemporario;
					
					if(valorTemporario != 0)
						return valorTemporario;
					
				} catch (AjudanteExcecao e) {
					throw new AjudanteExcecao(e.getMessage());
				}
			}
			
			return 0;
		}

	}
	
	public void ordenarListaPor(List<?> lstBase, String[] lstCampos) {
		Collections.sort(lstBase,new ComparadorGenerico(lstCampos));
	}
	
	public void ordenarListaInversaPor(List<?> lstBase, String[] lstCampos) {
		Collections.sort(lstBase,new ComparadorGenerico(lstCampos));
		Collections.reverse(lstBase);
	}

}
