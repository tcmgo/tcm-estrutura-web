package br.gov.go.tcm.estrutura.persistencia.historico.contrato;

import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;

/**
 * Interface DAO que define opera��es para a entidade {@link br.gov.go.tcm.estrutura.entidade.web.HistoricoWeb}
 */
public interface HistoricoStpDAO extends GenericoDAO {

}
