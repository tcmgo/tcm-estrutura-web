package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoStatusPessoaImpedimentoEnumeracao implements NumberEnumeracaoPersistente {

	INATIVO(0), ATIVO(1);
	
	int valor;

	TipoStatusPessoaImpedimentoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 0:
			return TipoStatusPessoaImpedimentoEnumeracao.INATIVO;
		case 1:
			return TipoStatusPessoaImpedimentoEnumeracao.ATIVO;
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Inativo";
		case 1:
			return "Ativo";
		default:
			return "";
		}
	}
}
