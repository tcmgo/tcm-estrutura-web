package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum SituacaoRequisicaoDespesaEnumeracao implements
		NumberEnumeracaoPersistente {

	PENDENTE(1), PROTOCOLADA(2), LICITACAO_CONCLUIDA(3), ORDEM_FORNECIMENTO(4), ATENDIDA(5), NAO_ATENDIDA(6);
	
	private final int valor;
	
	SituacaoRequisicaoDespesaEnumeracao(int valor) {
		this.valor = valor;
	}
	
	
	public Boolean getEstaPendente() {
		return getValorOrdinal().equals(SituacaoRequisicaoDespesaEnumeracao.PENDENTE.getValorOrdinal());
	}
	
	public Boolean getEstaProtocolada() {
		return getValorOrdinal().equals(SituacaoRequisicaoDespesaEnumeracao.PROTOCOLADA.getValorOrdinal());
	}
	
	public Boolean getEstaLicitacaoConcluida() {
		return getValorOrdinal().equals(SituacaoRequisicaoDespesaEnumeracao.LICITACAO_CONCLUIDA.getValorOrdinal());
	}
	
	public Boolean getEstaOrdemFornecimento() {
		return getValorOrdinal().equals(SituacaoRequisicaoDespesaEnumeracao.ORDEM_FORNECIMENTO.getValorOrdinal());
	}
	
	public Boolean getEstaAtendida() {
		return getValorOrdinal().equals(SituacaoRequisicaoDespesaEnumeracao.ATENDIDA.getValorOrdinal());
	}
	
	public Boolean getEstaNaoAtendida() {
		return getValorOrdinal().equals(SituacaoRequisicaoDespesaEnumeracao.NAO_ATENDIDA.getValorOrdinal());
	}
	
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return SituacaoRequisicaoDespesaEnumeracao.PENDENTE;
		case 2:
			return SituacaoRequisicaoDespesaEnumeracao.PROTOCOLADA;			
		case 3:
			return SituacaoRequisicaoDespesaEnumeracao.LICITACAO_CONCLUIDA;
		case 4:
			return SituacaoRequisicaoDespesaEnumeracao.ORDEM_FORNECIMENTO;
		case 5:
			return SituacaoRequisicaoDespesaEnumeracao.ATENDIDA;	
		case 6:
			return SituacaoRequisicaoDespesaEnumeracao.NAO_ATENDIDA;	
		}
		return null;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Pendente";
		case 2:
			return "Protocolada";
		case 3:
			return "Licita��o Conclu�da";
		case 4:
			return "Ordem de Fornecimento";
		case 5:
			return "Atendida";	
		case 6:
			return "N�o Atendida";
		default:
			return "";
		}
	}
}
