package br.gov.go.tcm.estrutura.analisador.conversor.regra;

import br.gov.go.tcm.estrutura.analisador.arquivo.ArquivoAnalisador;
import br.gov.go.tcm.estrutura.analisador.conversor.ConversorExcecao;
import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteExcecao;
import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteReflexao;
import br.gov.go.tcm.estrutura.entidade.ajudante.excecao.ReflexaoExcecao;

public class RegraAnalisadorPadrao implements RegraAnalisador {

	protected Integer posicaoInicial;

	protected Integer posicaoFinal;

	protected String propriedadeDestino;

	public RegraAnalisadorPadrao(Integer posicaoInicial, Integer posicaoFinal, String propriedadeDestino) {
		this.posicaoInicial = posicaoInicial;
		this.posicaoFinal = posicaoFinal;
		this.propriedadeDestino = propriedadeDestino;
	}
	
	public void processarRegra(String linha, ArquivoAnalisador arquivoAnalisador) throws ConversorExcecao {
		
		String valorExtraido = null;
		
		try{
			valorExtraido = linha.substring(posicaoInicial - 1, posicaoFinal);
		} catch(StringIndexOutOfBoundsException e){
			throw new ConversorExcecao("Linha com tamanho inv�lido(Falha ao tentar ler " + propriedadeDestino + ")");
		}
		
		ParserPadrao parserPadrao = new ParserPadrao();
		AjudanteReflexao assistenteReflexao = new AjudanteReflexao();
				
		try {
			Class<?> classeDestino = assistenteReflexao.obterTipoPropriedade(arquivoAnalisador,getPropriedadeDestino());
			try{
				Object valorConvertido = parserPadrao.converter(classeDestino,valorExtraido);
				if(valorConvertido != null)
					assistenteReflexao.aplicarValorPropriedade(arquivoAnalisador,propriedadeDestino,valorConvertido);
			} catch(ConversorExcecao e){
				throw new ConversorExcecao(e.getMessage() + " - " + propriedadeDestino + "," + posicaoInicial + "," + posicaoFinal);
			}
		} catch (ReflexaoExcecao e) {
			throw new ConversorExcecao(e.getMessage());
		} catch (AjudanteExcecao e) {
			throw new ConversorExcecao(e.getMessage());
		}		
	}

	public Integer getPosicaoFinal() {
		return posicaoFinal;
	}

	public Integer getPosicaoInicial() {
		return posicaoInicial;
	}

	public String getPropriedadeDestino() {
		return propriedadeDestino;
	}

	public void setPosicaoFinal(Integer posicaoFinal) {
		this.posicaoFinal = posicaoFinal;
	}

	public void setPosicaoInicial(Integer posicaoInicial) {
		this.posicaoInicial = posicaoInicial;
	}

	public void setPropriedadeDestino(String propriedadeDestino) {
		this.propriedadeDestino = propriedadeDestino;
	}

}


