package br.gov.go.tcm.estrutura.entidade.sessao;

import br.gov.go.tcm.estrutura.entidade.web.UsuarioExterno;

public class UsuarioLogadoExterno extends UsuarioLogado {

	private UsuarioExterno usuarioExterno;

	public UsuarioExterno getUsuarioExterno() {
		return usuarioExterno;
	}

	public void setUsuarioExterno(UsuarioExterno usuarioExterno) {
		this.usuarioExterno = usuarioExterno;
	}

	@Override
	public String getNomeUsuario() {
		return getUsuarioExterno().getPessoa().getNome();
	}

	@Override
	public String getNomeCompletoUsuario() {
		return getUsuarioExterno().getPessoa().getNome();
	}
}
