package br.gov.go.tcm.estrutura.entidade.transiente;

import java.util.Date;

import org.springframework.util.StringUtils;

public class ProcessoEmTramite {

	private String codigo;

	private String teor;

	private String secaoAtual;

	private Date dataAutuacao;

	private Date dataJulgamento;

	private String resolucao;

	private String acordao;
	
	private String codParecerLeg;
	
	private String codParecerExec;

	public String getCodParecerLeg() {
		return codParecerLeg;
	}

	public void setCodParecerLeg(String codParecerLeg) {
		this.codParecerLeg = codParecerLeg;
	}

	public String getCodParecerExec() {
		return codParecerExec;
	}

	public void setCodParecerExec(String codParecerExec) {
		this.codParecerExec = codParecerExec;
	}

	public String getAcordao() {
		if(!StringUtils.isEmpty(codParecerLeg)){
			return acordao + "-" + codParecerLeg;
		}
		return acordao;
	}

	public String getCodigo() {
		return codigo;
	}

	public Date getDataAutuacao() {
		return dataAutuacao;
	}

	public Date getDataJulgamento() {
		return dataJulgamento;
	}

	public String getSecaoAtual() {
		return secaoAtual;
	}

	public String getResolucao() {
		if(!StringUtils.isEmpty(codParecerExec)){
			return resolucao + "-" + codParecerExec;
		}
		return resolucao;
	}

	public String getTeor() {
		return teor;
	}

	public void setAcordao(String acordao) {
		this.acordao = acordao;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setDataAutuacao(Date dataAutuacao) {
		this.dataAutuacao = dataAutuacao;
	}

	public void setDataJulgamento(Date dataJulgamento) {
		this.dataJulgamento = dataJulgamento;
	}

	public void setSecaoAtual(String posicao) {
		this.secaoAtual = posicao;
	}

	public void setResolucao(String resolucao) {
		this.resolucao = resolucao;
	}

	public void setTeor(String teor) {
		this.teor = teor;
	}
}
