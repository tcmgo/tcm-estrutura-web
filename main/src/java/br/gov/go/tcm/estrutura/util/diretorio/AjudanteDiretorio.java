package br.gov.go.tcm.estrutura.util.diretorio;

import java.io.File;

import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;

public class AjudanteDiretorio {

	public static final String BARRA = "/";

	public static final String BARRA_INVERTIDA = "\\";

	public static final String SEPARADOR_EXTENSAO = ".";
	
	public static String obterCaminhoAbsoluto(String caminhoRelativo){	
		/*ServletContext servletContext = ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext());
		return servletContext.getRealPath(caminhoRelativo);*/
		return ContextoSpring.getDiretorioContexto() + "/" + caminhoRelativo;
	}
	
	public String obterCaminhoContexto(){		
		/*ServletContext servletContext = ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext());
		return servletContext.getRealPath(".") + "\\";*/		
		return ContextoSpring.getDiretorioContexto();
	}

	public void excluir(String diretorio) {
		File f = new File(diretorio);
		
		if(f.exists() && f.isDirectory()){
			for(File arquivo : f.listFiles()){
				arquivo.delete();
			}
		}
		
		if(f.exists() && f.canWrite())
			f.delete();
	}
	
}
