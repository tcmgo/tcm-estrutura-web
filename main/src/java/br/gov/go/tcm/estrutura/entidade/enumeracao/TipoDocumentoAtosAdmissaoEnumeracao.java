package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoDocumentoAtosAdmissaoEnumeracao implements NumberEnumeracaoPersistente {

	EDITAL_CONCURSO_PUBLICO(1), LISTA_APROVADOS_EDITAL(2), EDITAL_LICITACAO(3);
	
	int valor;
	
	TipoDocumentoAtosAdmissaoEnumeracao(int valor) {
		this.valor = valor;
	}
	
	public Number getValorOrdinal(){
		return this.valor;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoDocumentoAtosAdmissaoEnumeracao.EDITAL_CONCURSO_PUBLICO;
		case 2:
			return TipoDocumentoAtosAdmissaoEnumeracao.LISTA_APROVADOS_EDITAL;
		case 3:
			return TipoDocumentoAtosAdmissaoEnumeracao.EDITAL_LICITACAO;	
		}
		return null;
	}
	
	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Edital de Concurso P�blico";
		case 2:
			return "Lista de Aprovados no Edital";
		case 3:
			return "Edital de Licita��o";	
		default:
			return "";
		}		
	}
}
