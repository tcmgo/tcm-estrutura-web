package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum CategoriaExtraOrcamentariaEnumeracao implements
		NumberEnumeracaoPersistente {

	RECEITA(0), DESPESA(1);
	int valor;

	CategoriaExtraOrcamentariaEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Receita";
		case 1:
			return "Despesa";
		default:
			return "";
		}
	}

}
