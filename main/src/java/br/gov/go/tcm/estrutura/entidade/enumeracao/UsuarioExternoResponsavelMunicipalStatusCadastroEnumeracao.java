package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum UsuarioExternoResponsavelMunicipalStatusCadastroEnumeracao implements
		NumberEnumeracaoPersistente {
	
	REALIZADO(1), CONFIRMADO(2), INATIVO(3);
	
	int valor;

	private UsuarioExternoResponsavelMunicipalStatusCadastroEnumeracao(int valor) {
		this.valor = valor;
	}

	
	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return UsuarioExternoResponsavelMunicipalStatusCadastroEnumeracao.REALIZADO;
		case 2:
			return UsuarioExternoResponsavelMunicipalStatusCadastroEnumeracao.CONFIRMADO;
		case 3:
			return UsuarioExternoResponsavelMunicipalStatusCadastroEnumeracao.INATIVO;	
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Realizado";
		case 2:
			return "Confirmado";
		case 3:
			return "Inativo";	
		default:
			return "";
		}
	}
}
