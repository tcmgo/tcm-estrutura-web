package br.gov.go.tcm.estrutura.analisador.conversor;

import java.io.File;
import java.util.List;

import br.gov.go.tcm.estrutura.analisador.arquivo.ArquivoAnalisador;

public interface Conversor {
	
	public List<ArquivoAnalisador> converter(File f,NoRaiz noRaiz) throws ConversorExcecao;
	
}
