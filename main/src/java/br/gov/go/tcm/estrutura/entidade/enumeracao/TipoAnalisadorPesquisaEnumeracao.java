package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum TipoAnalisadorPesquisaEnumeracao implements StringEnumeracaoPersistente{
	
	BALANCETES("BALANCETE"),PPA_LOA("PPA_LOA"),BALANCO("BALANCO");
	String valor;
		
	TipoAnalisadorPesquisaEnumeracao(String valor){
		this.valor = valor;
	}
	
	public String getValorOrdinal(){
		return this.valor;
	}

	public EnumeracaoPersistente valor(){
		if(valor.equals("BALANCETE")){
			return TipoAnalisadorPesquisaEnumeracao.BALANCETES;
		} else if(valor.equals("PPA_LOA")){
			return TipoAnalisadorPesquisaEnumeracao.PPA_LOA;
		} else if(valor.equals("BALANCO")){
			return TipoAnalisadorPesquisaEnumeracao.BALANCO;
		}
		return null;
	}
	
	@Override
	public String toString(){
		if(valor.equals("BALANCETE")){
			return "Balancete";
		} else if(valor.equals("PPA_LOA")){
			return "PPA_LOA";
		} else if(valor.equals("BALANCO")){
			return "Balan�o";
		} 
		return "";
	}
	
}

