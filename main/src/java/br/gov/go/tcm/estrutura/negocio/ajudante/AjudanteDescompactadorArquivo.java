package br.gov.go.tcm.estrutura.negocio.ajudante;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteExcecao;
import br.gov.go.tcm.estrutura.util.diretorio.AjudanteDiretorio;

public class AjudanteDescompactadorArquivo {

	private static final int TAMANHO_BUFFER = 512000;

	public static byte[] compactaArquivos(String[] origens)
			throws AjudanteExcecao {

		int length = origens == null ? 0 : origens.length;
		if (length == 0) {
			return new byte[0];
		}

		ByteArrayOutputStream fos = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(fos);
		zos.setMethod(ZipOutputStream.DEFLATED);
		File file = new File(origens[0]).getParentFile();

		try {
			for (int i = 0; i < length; i++) {
				file = new File(origens[i]);
				if (file.isFile()) {
					adicionaArquivo(new File(origens[0]).getParent(),
							origens[i], zos);
				}
			}

			zos.close();
		} catch (IOException e) {
			throw new AjudanteExcecao(e.getMessage());
		}

		return fos.toByteArray();
	}

	public static void compacta(String[] origens, String destino)
			throws IOException {
		int length = origens == null ? 0 : origens.length;
		if (length == 0 || destino == null) {
			return;
		}
		FileOutputStream fos = new FileOutputStream(destino);
		ZipOutputStream zos = new ZipOutputStream(fos);
		zos.setMethod(ZipOutputStream.DEFLATED);
		File file = new File(origens[0]).getParentFile();
		String root = file == null ? null : file.getCanonicalPath();
		for (int i = 0; i < length; i++) {
			file = new File(origens[i]);
			if (file.isDirectory()) {
				varreDiretorio(root, origens[i], zos);
			} else {
				adicionaArquivo(root, origens[i], zos);
			}
		}
		zos.close();
	}

	public static void descompacta(String origem, String destino)
			throws IOException {
		if (origem == null || destino == null) {
			return;
		}
		FileInputStream fis = new FileInputStream(origem);
		ZipInputStream zis = new ZipInputStream(fis);
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		ZipEntry ze = null;
		String name = null;
		while ((ze = zis.getNextEntry()) != null) {
			name = destino + AjudanteDiretorio.BARRA_INVERTIDA + ze.getName();
			try {
				fos = new FileOutputStream(name);
			} catch (FileNotFoundException exc) {
				montaDiretorio(name);
				fos = new FileOutputStream(name);
			}
			bos = new BufferedOutputStream(fos, TAMANHO_BUFFER);
			int bytes;
			byte buffer[] = new byte[TAMANHO_BUFFER];
			while ((bytes = zis.read(buffer, 0, TAMANHO_BUFFER)) != -1) {
				bos.write(buffer, 0, bytes);
			}
			bos.flush();
			bos.close();
		}
		zis.close();
	}

	public static void descompacta(byte[] origem, String destino)
			throws IOException {
		if (origem == null || destino == null) {
			return;
		}
		ByteArrayInputStream bais = new ByteArrayInputStream(origem);
		ZipInputStream zis = new ZipInputStream(bais);
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		ZipEntry ze = null;
		String name = null;
		while ((ze = zis.getNextEntry()) != null) {
			name = destino + AjudanteDiretorio.BARRA + ze.getName();
			try {
				File f = new File(name);
				if(ze.isDirectory()){
					f.mkdirs();
					continue;
				}
				if(!f.getParentFile().exists())
					montaDiretorio(name);
				
				if(f.exists() || f.createNewFile())
					fos = new FileOutputStream(f);
					
			} catch (FileNotFoundException exc) {
				exc.printStackTrace();
			}
			
			bos = new BufferedOutputStream(fos, TAMANHO_BUFFER);
			int bytes;
			byte buffer[] = new byte[TAMANHO_BUFFER];
			while ((bytes = zis.read(buffer, 0, TAMANHO_BUFFER)) != -1) {
				bos.write(buffer, 0, bytes);
			}
			bos.flush();
			bos.close();
		}
		zis.close();
	}

	private static void adicionaArquivo(final String raiz,
			final String arquivo, final ZipOutputStream saida)
			throws IOException {

		if (arquivo.length() > 0) {

			int length = raiz == null ? 0 : raiz.length();
			String nomeArquivo = length > 0 ? arquivo.substring(length)
					: arquivo;

			nomeArquivo = nomeArquivo.replace(AjudanteDiretorio.BARRA,
					AjudanteDiretorio.BARRA_INVERTIDA);

			while (nomeArquivo.startsWith(AjudanteDiretorio.BARRA_INVERTIDA)) {
				nomeArquivo = nomeArquivo.substring(1);
			}

			ZipEntry entry = new ZipEntry(nomeArquivo);
			saida.putNextEntry(entry);

			FileInputStream fis = new FileInputStream(arquivo);
			int offset = 0;
			byte[] buffer = new byte[TAMANHO_BUFFER];
			while ((offset = fis.read(buffer, 0, TAMANHO_BUFFER)) != -1) {
				saida.write(buffer, 0, offset);
			}
			fis.close();
		}
	}

	@SuppressWarnings("unused")
	private static String[] listaConteudo(File diretorio) throws IOException {
		File[] files = diretorio.listFiles();
		int length = files == null ? 0 : files.length;
		String[] children = new String[length];
		File f = null;
		for (int i = 0; i < length; i++) {
			children[i] = files[i].getCanonicalPath();
		}
		return children;
	}

	private static void montaDiretorio(String nome) throws IOException {
		File f;
		StringBuffer sb = new StringBuffer();
		StringTokenizer st = new StringTokenizer(nome,
				AjudanteDiretorio.BARRA);
		int tokens = st.countTokens() - 1;

		if (nome.indexOf(AjudanteDiretorio.BARRA) == 0)
			sb.append(AjudanteDiretorio.BARRA);

		for (int i = 0; i < tokens; i++) {
			sb.append(st.nextToken() + AjudanteDiretorio.BARRA);
		}

		f = new File(sb.toString());

		try {
			f.mkdirs();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unused")
	private static String[] separaArquivos(String texto) {
		StringTokenizer st = new StringTokenizer(texto, ";");
		int tokens = st.countTokens();
		String[] array = new String[tokens];
		for (int i = 0; i < tokens; i++) {
			array[i] = st.nextToken();
		}
		return array;
	}

	private static void varreDiretorio(final String raiz,
			final String diretorio, final ZipOutputStream saida)
			throws IOException {
		File file = new File(diretorio);
		String[] files = file.list();
		int length = files == null ? 0 : files.length;
		String caminho = null;
		for (int i = 0; i < length; i++) {
			file = new File(diretorio, files[i]);
			caminho = file.getCanonicalPath();
			if (file.isDirectory()) {
				varreDiretorio(raiz, caminho, saida);
			} else {
				adicionaArquivo(raiz, caminho, saida);
			}
		}
		saida.flush();
	}

}