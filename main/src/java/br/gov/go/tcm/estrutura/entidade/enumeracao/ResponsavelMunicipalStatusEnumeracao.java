package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum ResponsavelMunicipalStatusEnumeracao implements
		NumberEnumeracaoPersistente {
	
	NAO_VALIDADO(0), VALIDADO(1), INVALIDADO_MANUALMENTE(2);
	
	private final int valor;

	ResponsavelMunicipalStatusEnumeracao(int valor) {
		this.valor = valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 0:
			return ResponsavelMunicipalStatusEnumeracao.NAO_VALIDADO;
		case 1:
			return ResponsavelMunicipalStatusEnumeracao.VALIDADO;
		case 2:
			return ResponsavelMunicipalStatusEnumeracao.INVALIDADO_MANUALMENTE;
		}
		return null;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "N�o validado";
		case 1:
			return "Validado";
		case 2:
			return "Invalidado manualmente";
		default:
			return "";
		}
	}
	
	public boolean naoValidado() {
		return valor == 0;
	}
	
	public boolean validado() {
		return valor == 1;
	}
	
	public boolean invalidadoManualmente() {
		return valor == 2;
	}
	
}
