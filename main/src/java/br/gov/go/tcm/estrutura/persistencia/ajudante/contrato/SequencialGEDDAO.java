package br.gov.go.tcm.estrutura.persistencia.ajudante.contrato;

import br.gov.go.tcm.estrutura.entidade.transiente.SequencialGED;
import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;

public interface SequencialGEDDAO extends GenericoDAO {

	public SequencialGED obterUltimoSequencialGED();
}
