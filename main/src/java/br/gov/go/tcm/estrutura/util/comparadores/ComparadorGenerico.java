package br.gov.go.tcm.estrutura.util.comparadores;

import java.lang.reflect.Field;
import java.util.Comparator;

@SuppressWarnings("unchecked")
public class ComparadorGenerico implements Comparator<Object> {

	private String nomeCampo;

	public ComparadorGenerico(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}

	public int compare(Object objeto1, Object objeto2) {

		try {
			Object valor1 = null;
			Object valor2 = null;

			Field campo1 = getAccessibleField(objeto1);
			Field campo2 = getAccessibleField(objeto2);

			if (campo1 == null || campo2 == null)
				return 0;

			valor1 = campo1.get(objeto1);
			valor2 = campo2.get(objeto2);

			if (valor1 instanceof Comparable) {
				return ((Comparable<Object>) valor1).compareTo(valor2);
			}
			return valor1.toString().compareTo(valor2.toString());

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return 0;
	}

	private Field getAccessibleField(Object objeto) {

		Field[] atributos = objeto.getClass().getDeclaredFields();
		for (int i = 0; i < atributos.length; i++) {
			if (atributos[i].getName().equals(this.nomeCampo)) {
				atributos[i].setAccessible(true);
				return atributos[i];
			}
		}

		return null;
	}
}