package br.gov.go.tcm.estrutura.util.email;

import java.io.File;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;

import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;
import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteExcecao;
import br.gov.go.tcm.estrutura.util.spring.SpringUtils;

public class AjudanteMail {

	public enum ConteudoEmail {

		TEXTO("text/plain"), HTML("text/html"), COM_ANEXOS("multipart/mixed");
		String valor;

		ConteudoEmail(String valor) {
			this.valor = valor;
		}

		public String getValorOrdinal() {
			return this.valor;
		}

	}

	public void enviarMensagem(String destino[], String sujeito, String mensagem, OrigemMail origem,
			ConteudoEmail tipoConteudoEmail) throws AjudanteExcecao {

		MultiPartEmail email = gerarMensagemEmail(destino, sujeito, mensagem, origem, tipoConteudoEmail);

		if (origem instanceof OrigemMailAutenticacao) {
			OrigemMailAutenticacao oriAut = (OrigemMailAutenticacao) origem;
			email.setAuthentication(oriAut.getUsuario(), oriAut.getSenha());
		}

		try {
			email.setTLS(true);
			email.send();
		} catch (EmailException e) {
			e.printStackTrace();
			throw new AjudanteExcecao(e.getMessage());
		}
	}

	public void enviarMensagem(String destino[], String sujeito, String mensagem, OrigemMail origem, File[] anexos,
			ConteudoEmail tipoConteudoEmail) throws AjudanteExcecao {

		MultiPartEmail email = gerarMensagemEmail(destino, sujeito, mensagem, origem, tipoConteudoEmail);

		if (origem instanceof OrigemMailAutenticacao) {
			OrigemMailAutenticacao oriAut = (OrigemMailAutenticacao) origem;
			email.setAuthentication(oriAut.getUsuario(), oriAut.getSenha());
		}

		for (int i = 0; i < anexos.length; i++) {
			EmailAttachment attachment = new EmailAttachment();
			attachment.setPath(anexos[i].getAbsolutePath());
			attachment.setDisposition(EmailAttachment.ATTACHMENT);
			attachment.setDescription("Anexo " + i);
			attachment.setName(anexos[i].getName());

			try {
				email.attach(attachment);
			} catch (EmailException e) {
				e.printStackTrace();
				continue;
			}
		}

		try {
			email.send();
		} catch (EmailException e) {
			e.printStackTrace();
			throw new AjudanteExcecao(e.getMessage());
		}

	}

	private MultiPartEmail gerarMensagemEmail(String destino[], String sujeito, String mensagem, OrigemMail origem,
			ConteudoEmail tipoConteudoEmail) throws AjudanteExcecao {

		try {

			MultiPartEmail email = null;

			if (tipoConteudoEmail.equals(ConteudoEmail.HTML)) {
				HtmlEmail emailTemp = new HtmlEmail();
				emailTemp.setHtmlMsg(mensagem);
				email = emailTemp;
			} else if (tipoConteudoEmail.equals(ConteudoEmail.TEXTO)) {
				MultiPartEmail emailTemp = new MultiPartEmail();
				emailTemp.setMsg(mensagem);
				email = emailTemp;
			}

			for (int i = 0; i < destino.length; ++i) {
				email.addBcc(destino[i]);
			}

			if (email == null) {
				email = new MultiPartEmail();
			}

			email.setHostName((String) SpringUtils.getApplicationContext().getBean(ContextoSpring.SERVIDOR_SMTP));
			email.setCharset("UTF-8");
			email.setFrom(origem.getOrigem(), origem.getRotuloOrigem());
			email.addReplyTo(origem.getOrigem());
			email.setSubject(sujeito);

			/*
			 * CONFIGURACOES PARA ENVIO ATRAV�S DO GMAIL
			 */
			email.setSmtpPort(465);
			email.setSSL(true);
			email.setTLS(true);

			return email;

		} catch (EmailException e) {
			throw new AjudanteExcecao(e.getMessage());
		}
	}

}
