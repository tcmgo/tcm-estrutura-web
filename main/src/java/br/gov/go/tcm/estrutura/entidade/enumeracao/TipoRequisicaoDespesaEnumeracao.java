package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoRequisicaoDespesaEnumeracao implements NumberEnumeracaoPersistente {
	
	COMPRA_DIRETA(1), LICITACAO(2);
	
	private final int valor;
	
	TipoRequisicaoDespesaEnumeracao(int valor) {
		this.valor = valor;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoRequisicaoDespesaEnumeracao.COMPRA_DIRETA;
		case 2:
			return TipoRequisicaoDespesaEnumeracao.LICITACAO;			
		}
		return null;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Compra Direta";
		case 2:
			return "Licita��o";			
		default:
			return "";
		}
	}
}
