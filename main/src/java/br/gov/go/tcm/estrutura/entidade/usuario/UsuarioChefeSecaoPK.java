package br.gov.go.tcm.estrutura.entidade.usuario;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import br.gov.go.tcm.estrutura.entidade.pk.PK;

@Embeddable
public class UsuarioChefeSecaoPK implements Serializable, PK {

	private static final long serialVersionUID = 1L;

	@Column(name = "usuarioID")
	@NotFound(action = NotFoundAction.IGNORE)
	private Long usuarioId;

	@Column(name = "secaoID")
	@NotFound(action = NotFoundAction.IGNORE)
	private Long secaoId;

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Long getSecaoId() {
		return secaoId;
	}

	public void setSecaoId(Long secaoId) {
		this.secaoId = secaoId;
	}

}
