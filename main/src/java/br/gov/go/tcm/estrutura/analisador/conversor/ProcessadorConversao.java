package br.gov.go.tcm.estrutura.analisador.conversor;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;

import br.gov.go.tcm.estrutura.analisador.arquivo.ArquivoAnalisador;
import br.gov.go.tcm.estrutura.analisador.arquivo.ArquivoAnalisadorFinal;
import br.gov.go.tcm.estrutura.analisador.conversor.regra.RegraAnalisadorPadrao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.NumberEnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoAnalisadorEnumeracao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoMensagemAnalisadorEnumeracao;

public abstract class ProcessadorConversao implements Serializable {

	private static final long serialVersionUID = -8400848709500417914L;

	private boolean teveErroConversao;

	private ConversorPadrao conversorPadrao;
	
	public static final Logger log = Logger.getLogger(ProcessadorConversao.class);

	//private AnalisadorFaseListener analisadorListener;

	/*public ProcessadorConversao(AnalisadorFaseListener analisadorListener) {
		this.analisadorListener = analisadorListener;
	}*/

	public abstract TipoAnalisadorEnumeracao getTipoAnalisadorEnum();

	public abstract NumberEnumeracaoPersistente getTipoArquivoAnalisador();

	protected ConversorPadrao getConversorPadrao() {
		if (conversorPadrao == null) {
			conversorPadrao = new ConversorPadrao(new ConversorOuvinte() {

				public void notificarMensagem(String mensagem,
						TipoMensagemAnalisadorEnumeracao tipo) {
					if (tipo == TipoMensagemAnalisadorEnumeracao.ERRO)
						setTeveErroConversao(true);
					/*analisadorListener.adicionarMensagem(
							getTipoAnalisadorEnum(),
							getTipoArquivoAnalisador(), mensagem, tipo);*/
				}

				public void notificarMensagem(String mensagem,
						TipoMensagemAnalisadorEnumeracao tipo, Object objeto) {
					if (tipo == TipoMensagemAnalisadorEnumeracao.ERRO)
						setTeveErroConversao(true);
					/*analisadorListener.adicionarMensagem(
							getTipoAnalisadorEnum(),
							getTipoArquivoAnalisador(), mensagem, tipo, objeto);*/
				}

			});
		}
		return conversorPadrao;
	}

	/*public AnalisadorFaseListener getAnalisadorListener() {
		return analisadorListener;
	}*/

	protected NoConversor noFinalPadrao() {
		NoConversor noConversor = new NoConversor();
		noConversor.setMarcadorInicial(ArquivoAnalisador.MARCADOR_FIM_ARQUIVO);
		noConversor.setChaves(new String[] { "tipoRegistro" });
		noConversor.adicionarRegra(new RegraAnalisadorPadrao(1, 2,
				"tipoRegistro"));
		noConversor.setClasseDestino(ArquivoAnalisadorFinal.class);
		return noConversor;
	}

	public List<ArquivoAnalisador> processarConversao(File arquivo) {
		/*getAnalisadorListener().adicionarMensagemLog(getTipoAnalisadorEnum(),
				getTipoArquivoAnalisador(),
				"Analisando Estrutura " + getTipoArquivoAnalisador());*/
		List<ArquivoAnalisador> resultado = converter(arquivo);
		/*getAnalisadorListener().adicionarMensagemLog(
				getTipoAnalisadorEnum(),
				getTipoArquivoAnalisador(),
				"Estrutura " + getTipoArquivoAnalisador()
						+ " Analisada ");*/
		return resultado;
	}

	public abstract List<ArquivoAnalisador> converter(File arquivo);

	public boolean isTeveErroConversao() {
		return teveErroConversao;
	}

	public void setTeveErroConversao(boolean teveErroConversao) {
		this.teveErroConversao = teveErroConversao;
	}
}
