package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoPeriodoCursoEnumeracao implements NumberEnumeracaoPersistente {

	SEMESTRAL(0), ANUAL(1);

	int valor;

	TipoPeriodoCursoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 0:
			return TipoPeriodoCursoEnumeracao.SEMESTRAL;
		case 1:
			return TipoPeriodoCursoEnumeracao.ANUAL;
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Semestral";
		case 1:
			return "Anual";
		default:
			return "";
		}
	}
}