package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum SexoEnumeracao implements NumberEnumeracaoPersistente{

	MASCULINO(1), FEMININO(2);

	int valor;

	SexoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return SexoEnumeracao.MASCULINO;
		case 2:
			return SexoEnumeracao.FEMININO;		
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Masculino";
		case 2:
			return "Feminino";		
		default:
			return "";
		}
	}
}