package br.gov.go.tcm.estrutura.entidade.base;

public class EntidadeInvalidaException extends Exception{

	private static final long serialVersionUID = 1584271974125865709L;

	public EntidadeInvalidaException(String message) {
		super(message);
	}

	
}
