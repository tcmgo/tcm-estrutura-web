package br.gov.go.tcm.estrutura.entidade.enumeracao;


public interface BooleanEnumeracaoPersistente extends EnumeracaoPersistente {

	public Integer getValorOrdinal();
	
}
