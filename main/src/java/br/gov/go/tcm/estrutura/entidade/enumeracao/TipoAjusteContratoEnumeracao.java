package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoAjusteContratoEnumeracao implements NumberEnumeracaoPersistente {
	CONTRATO(1), CONVENIO(2);
	int valor;

	TipoAjusteContratoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public NumberEnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoAjusteContratoEnumeracao.CONTRATO;
		case 2:
			return TipoAjusteContratoEnumeracao.CONVENIO;
		}
		return null;
	}

	public String toString() {
		switch (valor) {
		case 1:
			return "Contrato";
		case 2:
			return "Conv�nio";
		default:
			return "";
		}
	}

}
