package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoOrdemPagamentoEnumeracao implements NumberEnumeracaoPersistente{

	DESPESA_TOTALMENTE_PAGA_MES(1), DESPESA_PAGAR(2), RESTO_PAGAR(3),
	SERVICO_DIVIDA_PAGAR(8),DESPESA_EXTRA_ORCAMENTARIA(9);
	int valor;

	TipoOrdemPagamentoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoOrdemPagamentoEnumeracao.DESPESA_TOTALMENTE_PAGA_MES;
		case 2:
			return TipoOrdemPagamentoEnumeracao.DESPESA_PAGAR;
		case 3:
			return TipoOrdemPagamentoEnumeracao.RESTO_PAGAR;
		case 8:
			return TipoOrdemPagamentoEnumeracao.SERVICO_DIVIDA_PAGAR;
		case 9:
			return TipoOrdemPagamentoEnumeracao.DESPESA_EXTRA_ORCAMENTARIA;			
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Despesa totalmente paga no m�s";
		case 2:
			return "Despesa a pagar";
		case 3:
			return "Resto a pagar";
		case 8:
			return "Servi�o da d�vida a pagar";
		case 9:
			return "Despesa Extra-Or�ament�ria";
		default:
			return "";
		}
	}


}
