package br.gov.go.tcm.estrutura.seguranca.ajudante;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.ServletRequest;

import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoSessao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.RegraSegurancaEnumeracao;
import br.gov.go.tcm.estrutura.entidade.web.Regra;


public class AjudanteSeguranca {

	public static Boolean temPermissaoAcesso(RegraSegurancaEnumeracao regraAcesso) {

		AjudanteContextoSessao contextoSessao = AjudanteContextoSessao
				.obterContextoSessao((ServletRequest) FacesContext
						.getCurrentInstance().getExternalContext().getRequest());

		if (contextoSessao.getUsuarioLogado() != null) {

			List<Regra> lstRegras = contextoSessao.getUsuarioLogado()
					.getUsuarioPerfilModulo().getPerfil().getLstRegras();

			for (Regra regra : lstRegras) {
				if (regra.getRotulo().equals(regraAcesso.getValorOrdinal())) {
					return true;
				}
			}
		}

		return false;
	}
	
	
	public static Boolean temPermissaoAcesso(String regraAcesso) {

		AjudanteContextoSessao contextoSessao = AjudanteContextoSessao
				.obterContextoSessao((ServletRequest) FacesContext
						.getCurrentInstance().getExternalContext().getRequest());

		if (contextoSessao.getUsuarioLogado() != null) {

			List<Regra> lstRegras = contextoSessao.getUsuarioLogado()
					.getUsuarioPerfilModulo().getPerfil().getLstRegras();

			for (Regra regra : lstRegras) {
				if (regra.getRotulo().equals(regraAcesso)) {
					return true;
				}
			}
		}

		return false;
	}
}
