package br.gov.go.tcm.estrutura.anotacao;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoAcaoHistoricoEnumeracao;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Auditoria {
	TipoAcaoHistoricoEnumeracao tipo();
}