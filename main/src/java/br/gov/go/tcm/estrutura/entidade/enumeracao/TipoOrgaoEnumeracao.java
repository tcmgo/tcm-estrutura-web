package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoOrgaoEnumeracao implements NumberEnumeracaoPersistente{
	
	ADMINISTRACAO_DIRETA_PREFEITURA(1),LEGISLATIVO(2),
	FUNDEF(3),ADMINISTRACAO_DIRETA_FUNDO_ESPECIAL(4),
	ADMINISTRACAO_INDIRETA_AUTARQUIA(5),
	ADMINISTRACAO_INDIRETA_FUNDACAO(6),
	EMPRESAS_PUBLICAS(7),SOCIEDADE_ECONOMIA_MISTA(8),
	PREVIDENCIA_MUNICIPAL(9),FUNDO_MUNICIPAL_SAUDE(10), 
	FMAS(11), FMCA_FMDCA_FMIJ_FMIA(12), FMH(13), FME(14), OUTROS(99);
	
	int valor;
	
	TipoOrgaoEnumeracao(int valor){
		this.valor = valor;
	}
	
	public Number getValorOrdinal(){
		return this.valor;
	}
	
	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Administra��o Direta (Prefeitura)";
		case 2:
			return "Legislativo (C�mara)";
		case 3:
			return "FUNDEF";
		case 4:
			return "Administra��o Direta - Fundo Especial";
		case 5:
			return "Administra��o Indireta Autarquia";
		case 6:
			return "Administra��o Indireta Funda��o";
		case 7:
			return "Empresas P�blicas";
		case 8:
			return "Sociedade de Economia Mista";
		case 9:
			return "Previd�ncia Municipal(Regimes Pr�prios)";
		case 10:
			return "Fundo Municipal de Sa�de - FMS";
		case 11:
			return "FMAS";
		case 12:
			return "FMCA/FMDCA/FMIJ/FMIA";
		case 13:
			return "FMH - Fundo Municipal de Habita��o";
		case 14:
			return "FME - Fundo Municipal de Educa��o";
		case 99:
			return "Outros";
		default:
			return "";
		}

	}
	
}