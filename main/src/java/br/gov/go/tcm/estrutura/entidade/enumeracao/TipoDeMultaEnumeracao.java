package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoDeMultaEnumeracao implements NumberEnumeracaoPersistente {

	MULTA_ATRASO_PRESTACAO_CONTAS(1), MULTA_OUTRAS_MULTAS(2), INDEFINIDO(3);

	int valor;

	TipoDeMultaEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}
	
	public boolean eMultaAtrasoPrestacaoContas(){
		return valor == MULTA_ATRASO_PRESTACAO_CONTAS.valor;
	}

	public boolean eOutrasMultas(){
		return valor == MULTA_OUTRAS_MULTAS.valor;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoDeMultaEnumeracao.MULTA_ATRASO_PRESTACAO_CONTAS;
		case 2:
			return TipoDeMultaEnumeracao.MULTA_OUTRAS_MULTAS;
		}
		return null;
	}
	
	
	public String tipoReceita(){
		switch( valor ){
		
		case 1:
			return "1919.99.02";
		case 2:
			return "1919.99.03";
		}
		
		return "";
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Atraso Presta��o de Contas";
		case 2:
			return "Outras Multas";
		default:
			return "";
		}
	}

}