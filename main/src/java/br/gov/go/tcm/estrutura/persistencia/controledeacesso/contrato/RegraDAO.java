package br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato;

import java.util.List;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.entidade.web.Regra;
import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;

public interface RegraDAO extends GenericoDAO {

	public List<Regra> obterRegraPorRotulo(String rotulo);
	
	public List<Regra> obterLstRegraOrdenadaPorRotulo();
	
	public List<Regra> obterLstRegraPorModulo(Modulo modulo);
	
	public List<Integer> obterLstPerfilVinculadosRegra(Regra regra);
}
