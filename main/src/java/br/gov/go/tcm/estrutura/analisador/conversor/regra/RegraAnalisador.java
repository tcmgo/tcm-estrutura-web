package br.gov.go.tcm.estrutura.analisador.conversor.regra;

import br.gov.go.tcm.estrutura.analisador.arquivo.ArquivoAnalisador;
import br.gov.go.tcm.estrutura.analisador.conversor.ConversorExcecao;

public interface RegraAnalisador {

	public void processarRegra(String linha,ArquivoAnalisador arquivoAnalisador) throws ConversorExcecao;
	
	
}
