package br.gov.go.tcm.estrutura.util.email;


public class OrigemMailAutenticacao extends OrigemMail {
	
	private String usuario;
	
	private String senha;

	public String getSenha() {
		return senha;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
}
