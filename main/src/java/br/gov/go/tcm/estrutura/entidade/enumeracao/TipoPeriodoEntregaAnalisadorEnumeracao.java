package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoPeriodoEntregaAnalisadorEnumeracao implements NumberEnumeracaoPersistente {
	
	MENSAL(0),QUADRIMESTRAL(1),ANUAL(2), LIVRE(9);
	
	int valor;
	
	TipoPeriodoEntregaAnalisadorEnumeracao(int valor){
		this.valor = valor;
	}
	
	public Number getValorOrdinal(){
		return this.valor;
	}
	
	@Override
	public String toString(){
		switch(valor){
			case 0:
				return "Mensal";
			case 1:
				return "Quadrimestral";
			case 2:
				return "Anual";
			case 9:
				return "Livre";
			default:
				return "";				
		}
	}
	
}
