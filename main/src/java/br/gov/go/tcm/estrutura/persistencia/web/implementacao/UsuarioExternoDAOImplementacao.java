package br.gov.go.tcm.estrutura.persistencia.web.implementacao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.web.contrato.UsuarioExternoDAO;

@Repository
public class UsuarioExternoDAOImplementacao extends GenericoDAOImplementacao implements UsuarioExternoDAO {
	
	private static final long serialVersionUID = 1L;

	@Autowired
	public UsuarioExternoDAOImplementacao(@Qualifier("sessionFactory") SessionFactory factory, @Qualifier("hibernateTemplate") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}


}
