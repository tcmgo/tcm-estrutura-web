package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoTurnoEnumeracao implements NumberEnumeracaoPersistente {

	MATUTINO(0), VESPERTINO(1), MATUTINO_VESPERTINO(2);

	int valor;

	TipoTurnoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 0:
			return TipoTurnoEnumeracao.MATUTINO;
		case 1:
			return TipoTurnoEnumeracao.VESPERTINO;
		case 2:
			return TipoTurnoEnumeracao.MATUTINO_VESPERTINO;
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Matutino";
		case 1:
			return "Vespertino";
		case 2:
			return "Matutino/Vespertino";
		default:
			return "";
		}
	}
}