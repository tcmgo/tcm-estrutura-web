package br.gov.go.tcm.estrutura.util.conversor;

import java.util.EnumSet;
import java.util.Iterator;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.springframework.stereotype.Service;

import br.gov.go.tcm.estrutura.entidade.enumeracao.BooleanEnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.NumberEnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.StringEnumeracaoPersistente;

@Service("conversorEnumeracao")
public class ConversorEnumeracao implements Converter {

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object getAsObject(FacesContext faces, UIComponent componente, String value) throws ConverterException {

		Class classEnumPersistente = componente.getValueExpression("value").getType(faces.getELContext());
		EnumSet<?> enums = EnumSet.allOf(classEnumPersistente);
		Iterator<?> iter = enums.iterator();

		if (value == null || value.trim().equals(""))
			return null;
		

		while (iter.hasNext()) {
			EnumeracaoPersistente enumeracaoPersistente = (EnumeracaoPersistente) iter.next();

			if (enumeracaoPersistente instanceof NumberEnumeracaoPersistente) {
				NumberEnumeracaoPersistente numberEnum = (NumberEnumeracaoPersistente) enumeracaoPersistente;
				if (numberEnum.getValorOrdinal().intValue() == new Integer(value).intValue()) {
					return numberEnum;
				}
			} else if (enumeracaoPersistente instanceof StringEnumeracaoPersistente) {
				StringEnumeracaoPersistente stringEnum = (StringEnumeracaoPersistente) enumeracaoPersistente;
				if (stringEnum.getValorOrdinal().equals(value)) {
					return stringEnum;
				}
			} else if (enumeracaoPersistente instanceof BooleanEnumeracaoPersistente) {
				BooleanEnumeracaoPersistente booleanEnum = (BooleanEnumeracaoPersistente) enumeracaoPersistente;
				if (booleanEnum.getValorOrdinal().intValue() == Integer.valueOf(value).intValue()) {
					return booleanEnum;
				}
			}
		}
		throw new ConverterException("Valor incompat�vel por Enum.");
	}

	public String getAsString(FacesContext faces, UIComponent componente, Object valor) throws ConverterException {
		
		if (valor == null)
			return "";
		else if (valor instanceof NumberEnumeracaoPersistente) {
			return ((NumberEnumeracaoPersistente) valor).getValorOrdinal().toString();
		} else if (valor instanceof StringEnumeracaoPersistente) {
			return ((StringEnumeracaoPersistente) valor).getValorOrdinal();
		} else if (valor instanceof BooleanEnumeracaoPersistente) {
			return ((BooleanEnumeracaoPersistente) valor).getValorOrdinal().toString();
		} else
			throw new ConverterException("Enum n�o implementa interface passivel de convers�o");
	}

}
