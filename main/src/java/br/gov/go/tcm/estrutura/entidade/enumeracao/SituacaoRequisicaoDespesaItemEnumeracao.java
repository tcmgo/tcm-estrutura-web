package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum SituacaoRequisicaoDespesaItemEnumeracao implements NumberEnumeracaoPersistente {

	PENDENTE(1), NORMAL(2), INDEFERIDO(3);
	
	private final int valor;
	
	SituacaoRequisicaoDespesaItemEnumeracao(int valor) {
		this.valor = valor;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return SituacaoRequisicaoDespesaItemEnumeracao.PENDENTE;
		case 2:
			return SituacaoRequisicaoDespesaItemEnumeracao.NORMAL;			
		case 3:
			return SituacaoRequisicaoDespesaItemEnumeracao.INDEFERIDO;	
		}
		return null;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Pendente";
		case 2:
			return "Normal";
		case 3:
			return "Indeferido";
		default:
			return "";
		}
	}
	
}
