package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum DestinoNoticiaEnumeracao implements NumberEnumeracaoPersistente {
	SITE_TCM(1), SITE_ESCOLA_DE_CONTAS(2), AMBOS(10);

	int valor;
	
	DestinoNoticiaEnumeracao(int valor) {
		this.valor = valor;
	}
	
	@Override
	public Number getValorOrdinal() {		
		return this.valor;
	}
	
	@Override
	public String toString() {
		
		switch (valor) {
		case 1:
			return "Site TCM";			
		case 2: 
			return "Site Escola de Contas";
		case 10: 
			return "Ambos";
		default:
			return "";
		}
	}

}
