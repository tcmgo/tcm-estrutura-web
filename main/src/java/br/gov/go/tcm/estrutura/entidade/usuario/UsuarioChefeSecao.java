package br.gov.go.tcm.estrutura.entidade.usuario;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import br.gov.go.tcm.estrutura.entidade.base.EntidadeComposta;
import br.gov.go.tcm.estrutura.entidade.pk.PK;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;

@Entity
@Table(name = "UsuarioChefeSecao")
@Banco(nome = Banco.USUARIO)
public class UsuarioChefeSecao extends EntidadeComposta {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	@NotFound(action = NotFoundAction.IGNORE)
	private UsuarioChefeSecaoPK usuarioChefeSecaoPK;

	public UsuarioChefeSecaoPK getUsuarioChefeSecaoPK() {
		if (usuarioChefeSecaoPK == null){
			usuarioChefeSecaoPK = new UsuarioChefeSecaoPK();
		}
		return usuarioChefeSecaoPK;
	}

	public void setUsuarioChefeSecaoPK(UsuarioChefeSecaoPK usuarioChefeSecaoPK) {
		this.usuarioChefeSecaoPK = usuarioChefeSecaoPK;
	}

	@Override
	public PK getPK() {
		return this.getUsuarioChefeSecaoPK();
	}

}
