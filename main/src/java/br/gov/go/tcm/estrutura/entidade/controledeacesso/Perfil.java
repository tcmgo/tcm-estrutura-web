package br.gov.go.tcm.estrutura.entidade.controledeacesso;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Proxy;

import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;
import br.gov.go.tcm.estrutura.entidade.web.Regra;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;

import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "Papel")
@Proxy(lazy = true, proxyClass = Perfil.class)
@Banco(nome = Banco.TCMWEB)
@NamedQueries( {
		@NamedQuery(name = "selecionarPerfilPorModuloEDescricaoExata", 
				query = "SELECT p FROM Perfil p WHERE p.modulo.id = :moduloId AND p.descricao = :descricao"),
		@NamedQuery(name = "selecionarPerfilPorDescricaoExata", 
				query = "SELECT p FROM Perfil p WHERE p.descricao = :descricao"),
		@NamedQuery(name = "selecionarLstPerfilPorModuloEDescricaoQualquerLugar", 
				query = "SELECT p FROM Perfil p WHERE p.modulo = :modulo AND lower(p.descricao) like lower('%' || :descricao || '%') ORDER BY p.descricao ASC"),
		@NamedQuery(name = "selecionarLstPerfilPorModulo", 
				query = "SELECT p FROM Perfil p LEFT JOIN fetch p.modulo LEFT JOIN fetch p.lstRegras WHERE p.modulo = :modulo"),
		@NamedQuery(name = "selecionarLstPerfilOrdenadoPorDescricao", 
				query = "SELECT p FROM Perfil p LEFT JOIN fetch p.modulo LEFT JOIN fetch p.lstRegras ORDER BY p.descricao"),
		@NamedQuery(name = "selecionarPerfilPorId", 
				query = "SELECT p FROM Perfil p LEFT JOIN fetch p.modulo LEFT JOIN fetch p.lstRegras WHERE p.id = :id") })
public class Perfil extends EntidadePadrao {

	private static final long serialVersionUID = -4440690310378212978L;

	public final static String AUTORIDADE_CADASTRO_ANALISADOR = "AUTORIDADE_CADASTRO_ANALISADOR";
	
	public final static String USUARIO_RESPONSAVEL_MUNICIPAL = "USUARIO_RESPONSAVEL_MUNICIPAL";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	@NotNull
	private String descricao;

	@ManyToOne(fetch = FetchType.EAGER)
	private Modulo modulo;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "PapelRegra", joinColumns = @JoinColumn(name = "papel_id"), inverseJoinColumns = @JoinColumn(name = "regra_id"))
	@Cascade( { CascadeType.ALL, CascadeType.DELETE })
	private List<Regra> lstRegras;

	/**
	 * Retorna uma inst�ncia de Long
	 * 
	 * @return id - Long
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * Instancia um objeto do tipo Long
	 * 
	 * @param id
	 *            id
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Retorna uma inst�ncia de String
	 * 
	 * @return descricao - String
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Instancia um objeto do tipo String
	 * 
	 * @param descricao
	 *            descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Retorna uma inst�ncia de Modulo
	 * 
	 * @return modulo - Modulo
	 */
	public Modulo getModulo() {
		return modulo;
	}

	/**
	 * Instancia um objeto do tipo Modulo
	 * 
	 * @param modulo
	 *            modulo
	 */
	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	/**
	 * Retorna uma inst�ncia de List<Regra>
	 * 
	 * @return lstRegras - List<Regra>
	 */
	public List<Regra> getLstRegras() {
		if (lstRegras == null)
			lstRegras = new ArrayList<Regra>();
		return lstRegras;
	}

	/**
	 * Instancia um objeto do tipo List<Regra>
	 * 
	 * @param lstRegras
	 *            lstRegras
	 */
	public void setLstRegras(List<Regra> lstRegras) {
		this.lstRegras = lstRegras;
	}
}
