package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoArquivoAnalisadorBalancoEnumeracao implements NumberEnumeracaoPersistente {

	IDE(1), ORGAO(2), REC(3),DES(4),AFD(5),AFR(6),APB(7),ROP(8),APC(9),
	PFR(10),PFD(11),PPD(12),REP(13),COM(14),BLP(15);

	int valor;

	TipoArquivoAnalisadorBalancoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "IDE";
		case 2:
			return "ORGAO";
		case 3:
			return "REC";
		case 4:
			return "DES";
		case 5:
			return "AFD";
		case 6:
			return "AFR";
		case 7:
			return "APB";
		case 8:
			return "ROP";	
		case 9:
			return "APC";
		case 10:
			return "PFR";
		case 11:
			return "PFD";
		case 12:
			return "PPD";	
		case 13:
			return "REP";
		case 14:
			return "COM";
		case 15:
			return "BLP";			
		default:
			return "";
		}
	}
}