package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum UsuarioExternoAnalisadorStatusEnumeracao implements NumberEnumeracaoPersistente {

	PRE_CADASTRO(1), SENHA_DEFINIDA(2), FICHA_CADASTRO_IMPRESSA(3), EMAIL_CHAVE_ENVIADA(
			4), CHAVE_OBTIDA(5),PRIMEIRO_ACESSO_CONCLUIDO(6);

	int valor;

	UsuarioExternoAnalisadorStatusEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Pr�-Cadastro";
		case 2:
			return "Senha Definida";
		case 3:
			return "Ficha Cadastro Impressa";
		case 4:
			return "E-mail Chave Enviada";
		case 5:
			return "Chave Obtida";
		case 6:
			return "Primeiro Acesso Conclu�do";	
		default:
			return "";
		}
	}

}
