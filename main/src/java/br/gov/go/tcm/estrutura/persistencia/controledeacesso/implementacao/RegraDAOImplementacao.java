package br.gov.go.tcm.estrutura.persistencia.controledeacesso.implementacao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.entidade.web.Regra;
import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.RegraDAO;

@Repository("RegraDAO")
public class RegraDAOImplementacao extends GenericoDAOImplementacao implements
		RegraDAO {

	private static final long serialVersionUID = -7073505679025316376L;

	@Autowired
	public RegraDAOImplementacao(
			@Qualifier("sessionFactory") SessionFactory factory,
			@Qualifier("hibernateTemplate") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}

	@SuppressWarnings("unchecked")
	public List<Regra> obterRegraPorRotulo(String rotulo) {
		String parametro = "rotulo";
		Object objeto = rotulo;

		return getHibernateTemplate(getSessionFactoryWeb()).findByNamedQueryAndNamedParam(
				"selecionarRegraPorRotulo", parametro, objeto);
	}

	@SuppressWarnings("unchecked")
	public List<Regra> obterLstRegraOrdenadaPorRotulo() {
		return getHibernateTemplate(getSessionFactoryWeb()).findByNamedQuery(
				"selecionarLstRegraOrdenadaPorRotulo");
	}

	@SuppressWarnings("unchecked")
	public List<Regra> obterLstRegraPorModulo(Modulo modulo) {
		String parametro = "modulo";
		Object objeto = modulo;

		return getHibernateTemplate(getSessionFactoryWeb()).findByNamedQueryAndNamedParam(
				"selecionarLstRegraPorModulo", parametro, objeto);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Integer> obterLstPerfilVinculadosRegra(Regra regra) {
		final String sql = "Select pr.papel_id from PapelRegra pr where pr.regra_id=" + regra.getId();

		List<Integer> lstPerfil = getHibernateTemplate(getSessionFactoryWeb()).executeFind(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {

				Query query = session.createSQLQuery(sql);
				List<Integer> resultadoRetorno = new ArrayList<Integer>();
				List<?> resultado = query.list();

				Iterator<?> i = resultado.iterator();
				while (i.hasNext()) {
					resultadoRetorno.add((Integer)i.next());
				}

				return resultadoRetorno;
			}
		});

		return lstPerfil;
	}
}
