package br.gov.go.tcm.estrutura.analisador.conversor;

public class ConversorExcecao extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4652504130778621375L;

	public ConversorExcecao(String message) {
		super(message);
	}	
}
