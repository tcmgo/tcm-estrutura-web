package br.gov.go.tcm.estrutura.persistencia.historico.implementacao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.historico.contrato.HistoricoAdministracaoDAO;

/**
 * Classe DAO que define opera��es para a entidade {@link br.gov.go.tcm.estrutura.entidade.web.HistoricoWeb}
 */
@Repository
public class HistoricoAdministracaoDAOImplementacao extends GenericoDAOImplementacao implements HistoricoAdministracaoDAO {

	private static final long serialVersionUID = -2964091367947734916L;

	@Autowired
	public HistoricoAdministracaoDAOImplementacao(@Qualifier("sessionFactoryAdministracao") SessionFactory factory,
			@Qualifier("hibernateTemplateAdministracao") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}
}
