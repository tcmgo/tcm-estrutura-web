package br.gov.go.tcm.estrutura.container.faces;

import java.io.Serializable;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import br.gov.go.tcm.estrutura.entidade.sessao.UsuarioLogado;

public class AjudanteContextoSessao implements Serializable {

	private static final long serialVersionUID = 2403293947831470195L;

	private UsuarioLogado usuarioLogado;

	public static AjudanteContextoSessao obterContextoSessao(
			ServletRequest request) {
		AjudanteContextoSessao contextoSessao = (AjudanteContextoSessao) ((HttpServletRequest) request)
				.getSession().getAttribute(
						DadosContexto.CONTEXTO_SESSAO.toString());
		if (contextoSessao == null) {
			contextoSessao = new AjudanteContextoSessao();
			((HttpServletRequest) request).getSession().setAttribute(
					DadosContexto.CONTEXTO_SESSAO.toString(), contextoSessao);
		}
		return contextoSessao;
	}

	public UsuarioLogado getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(UsuarioLogado usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	enum DadosContexto implements Serializable {

		CONTEXTO_SESSAO("contextoSessao"), USUARIO_LOGADO("usuarioLogado");

		private String valor = "";

		private DadosContexto(String valor) {
			this.valor = valor;
		}

		public String getValor() {
			return valor;
		}
	};
}