package br.gov.go.tcm.estrutura.entidade.ajudante;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import br.gov.go.tcm.estrutura.entidade.enumeracao.MesEnumeracao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.QuadrimestreEnumeracao;

public class AjudantePeriodoPrestacao {

	private Map<QuadrimestreEnumeracao, MesEnumeracao[]> mesesQuadrimestre;

	public Map<QuadrimestreEnumeracao, MesEnumeracao[]> getMesesQuadrimestre() {
		if (mesesQuadrimestre == null) {
			mesesQuadrimestre = new LinkedHashMap<QuadrimestreEnumeracao, MesEnumeracao[]>();
			mesesQuadrimestre.put(QuadrimestreEnumeracao.QUADRIMESTRE_01, new MesEnumeracao[] { MesEnumeracao.JANEIRO,
					MesEnumeracao.FEVEREIRO, MesEnumeracao.MARCO, MesEnumeracao.ABRIL });
			mesesQuadrimestre.put(QuadrimestreEnumeracao.QUADRIMESTRE_02, new MesEnumeracao[] { MesEnumeracao.MAIO,
					MesEnumeracao.JUNHO, MesEnumeracao.JULHO, MesEnumeracao.AGOSTO });
			mesesQuadrimestre.put(QuadrimestreEnumeracao.QUADRIMESTRE_03, new MesEnumeracao[] { MesEnumeracao.SETEMBRO,
					MesEnumeracao.OUTUBRO, MesEnumeracao.NOVEMBRO, MesEnumeracao.DEZEMBRO });
		}
		return mesesQuadrimestre;
	}

	public QuadrimestreEnumeracao quadrimestreAQueSeRefereOMes(MesEnumeracao mesReferencia) {

		Iterator<Map.Entry<QuadrimestreEnumeracao, MesEnumeracao[]>> iter = getMesesQuadrimestre().entrySet()
				.iterator();

		while (iter.hasNext()) {
			Map.Entry<QuadrimestreEnumeracao, MesEnumeracao[]> entry = iter.next();

			MesEnumeracao[] mesesQuadrimestre = entry.getValue();

			for (int i = 0; i < mesesQuadrimestre.length; ++i) {
				if (mesesQuadrimestre[i].equals(mesReferencia))
					return entry.getKey();
			}
		}

		return null;

	}

}
