package br.gov.go.tcm.estrutura.persistencia.controledeacesso.implementacao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteConversao;
import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteConversao.TipoConversao;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.Perfil;
import br.gov.go.tcm.estrutura.entidade.web.Regra;
import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.PerfilDAO;

@Repository
public class PerfilDAOImplementacao extends GenericoDAOImplementacao implements PerfilDAO {

	private static final long serialVersionUID = 4033382480449155959L;
	
	@Autowired
	public PerfilDAOImplementacao(@Qualifier("sessionFactory") SessionFactory factory,
			@Qualifier("hibernateTemplate") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}

	@SuppressWarnings("unchecked")
	public List<Perfil> selecionarPerfilPorModuloEDescricaoExata(Long moduloId, String descricao) {
		String[] parametro = { "moduloId", "descricao" };
		Object[] objeto = { moduloId, descricao };

		return super.getHibernateTemplate(super.getSessionFactoryWeb()).findByNamedQueryAndNamedParam("selecionarPerfilPorModuloEDescricaoExata",
				parametro, objeto);
	}
	
	@SuppressWarnings("unchecked")
	public List<Perfil> selecionarListaPerfilPorDescricaoExata(String descricao) {
		String[] parametro = { "descricao" };
		Object[] objeto = { descricao };

		return super.getHibernateTemplate(super.getSessionFactoryWeb()).
				findByNamedQueryAndNamedParam("selecionarPerfilPorDescricaoExata", parametro, objeto);
	}
	
	@SuppressWarnings("unchecked")
	public List<Perfil> obterLstPerfilPorModuloEDescricaoQualquerLugar(Modulo modulo, String descricao) {
		String[] parametro = { "modulo", "descricao" };
		Object[] objeto = { modulo, descricao };

		return super.getHibernateTemplate(super.getSessionFactoryWeb()).findByNamedQueryAndNamedParam(
				"selecionarLstPerfilPorModuloEDescricaoQualquerLugar", parametro, objeto);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Perfil> obterLstPerfilOrdenadoPorDescricao() {
		return super.getHibernateTemplate(super.getSessionFactoryWeb()).executeFind(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				Query query = session.createQuery("FROM Perfil p ORDER BY p.modulo.descricao, p.descricao");

				List<Perfil> lstPerfil = query.list();
				for (Perfil item : lstPerfil) {
					Hibernate.initialize(item.getModulo());
					Hibernate.initialize(item.getLstRegras());
				}

				return lstPerfil;
			}
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Perfil> obterLstPerfilPorModulo(final Modulo modulo) {

		return super.getHibernateTemplate(super.getSessionFactoryWeb()).executeFind(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				Query query = session.createQuery("FROM Perfil p WHERE p.modulo = ? ORDER BY p.descricao");
				query.setEntity(0, modulo);

				List<Perfil> lstPerfil = query.list();
				for (Perfil item : lstPerfil) {
					Hibernate.initialize(item.getModulo());
					Hibernate.initialize(item.getLstRegras());
				}

				return lstPerfil;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public Perfil obterPerfilPorId(Long perfilId) {
		String parametro = "id";
		Object objeto = perfilId;

		List<Perfil> lstPerfil = super.getHibernateTemplate(super.getSessionFactoryWeb()).findByNamedQueryAndNamedParam("selecionarPerfilPorId",
				parametro, objeto);
		return lstPerfil != null && lstPerfil.size() > 0 ? lstPerfil.get(0) : null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Perfil> obterLstPerfilPorRegra(Regra regra) {
		final String sql = "SELECT p.id, p.descricao FROM Papel p inner join PapelRegra pr on p.id = pr.papel_id "
				+ "WHERE pr.regra_id = " + regra.getId();
		List<Perfil> lstPerfil = super.getHibernateTemplate(super.getSessionFactoryWeb()).executeFind(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				Query query = session.createSQLQuery(sql);
				List<Perfil> resultadoRetorno = new ArrayList<Perfil>();
				List<Perfil> resultado = query.list();

				Iterator i = resultado.iterator();
				while (i.hasNext()) {
					Object[] objetos = (Object[]) i.next();

					Perfil perfil = new Perfil();
					perfil.setId((Long) AjudanteConversao.converter(objetos[0], TipoConversao.Long));
					perfil.setDescricao((String) objetos[1]);

					resultadoRetorno.add(perfil);
				}

				return resultadoRetorno;
			}
		});

		return lstPerfil;
	}

}
