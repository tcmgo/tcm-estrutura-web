package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoContatoCursoParticipanteEnumecacao implements NumberEnumeracaoPersistente {
	TELEFONE_PESSOAL(1), TELEFONE_PROFISSIONAL(2), TELEFONE_CHEFE(3),
	EMAIL_PESSOAL(4), EMAIL_PROFISSIONAL(5), EMAIL_CHEFE(6);
	
	private int valor;

	TipoContatoCursoParticipanteEnumecacao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Telefone Pessoal";
		case 2:
			return "Telefone Profissional";
		case 3:
			return "Telefone do Chefe";
		case 4:
			return "Email Pessoal";
		case 5:
			return "Email Profissional";
		case 6: 
			return "Email do Chefe";
		default:
			return "";
		}
	}
}
