package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoCamaraSessaoEnumeracao implements NumberEnumeracaoPersistente {

	PLENO(1), PLENO_EXTRAORDINARIO(2), PRIMEIRACAMARA(3), PRIMEIRACAMARA_EXTRAORDINARIA(4), SEGUNDACAMARA(5), SEGUNDACAMARA_EXTRAORDINARIA(
			6), TECNICO_ADMINISTRATIVA(8), TECNICO_ADMINISTRATIVA_EXTRAORDINARIA(9);

	private final int valor;

	TipoCamaraSessaoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}
	
	public boolean isPleno(){
		return valor == PLENO.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Pleno";
		case 2:
			return "Pleno Extraordin�rio";
		case 3:
			return "Primeira C�mara";
		case 4:
			return "Primeira C�mara Extraordin�ria";
		case 5:
			return "Segunda C�mara";
		case 6:
			return "Segunda C�mara Extraordin�ria";
		case 8:
			return "T�cnico Administrativa";
		case 9:
			return "T�cnico Administrativa Extraordin�ria";
		default:
			return "";
		}
	}
}
