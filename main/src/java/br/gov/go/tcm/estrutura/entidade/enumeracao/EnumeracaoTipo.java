package br.gov.go.tcm.estrutura.entidade.enumeracao;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.usertype.EnhancedUserType;
import org.hibernate.usertype.ParameterizedType;

public class EnumeracaoTipo implements EnhancedUserType, ParameterizedType {

	private Class<EnumeracaoPersistente> enumClasse;

	@SuppressWarnings("unchecked")
	public void setParameterValues(Properties parameters) {
		String enumClassName = parameters.getProperty("enumClassName");
		
		try {
			enumClasse = (Class<EnumeracaoPersistente>) Class.forName(enumClassName);
		} catch (ClassNotFoundException cnfe) {
			throw new HibernateException("Enum class n�o existe", cnfe);
		}
	}

	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		return cached;
	}

	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}

	public Serializable disassemble(Object value) throws HibernateException {
		return (Enum<?>) value;
	}

	public boolean equals(Object x, Object y) throws HibernateException {
		return x == y;
	}

	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}

	public boolean isMutable() {
		return false;
	}

	public Object nullSafeGet(ResultSet rs, String[] names, Object owner) throws HibernateException, SQLException {
		String value = rs.getString(names[0]);
		EnumeracaoPersistente enums[] = enumClasse.getEnumConstants();

		if (rs.wasNull())
			return null;

		for (int i = 0; i < enums.length; ++i) {
			if (enums[i] instanceof StringEnumeracaoPersistente) {
				StringEnumeracaoPersistente str = (StringEnumeracaoPersistente) enums[i];
				if (str.getValorOrdinal().equals(value))
					return str;
			} else if (enums[i] instanceof NumberEnumeracaoPersistente) {
				NumberEnumeracaoPersistente num = (NumberEnumeracaoPersistente) enums[i];
				if (num.getValorOrdinal().toString().equals(value.trim()))
					return num;
			} else if (enums[i] instanceof BooleanEnumeracaoPersistente) {
				BooleanEnumeracaoPersistente bool = (BooleanEnumeracaoPersistente) enums[i];
				if (bool.getValorOrdinal().intValue() == Integer.valueOf(value.trim()).intValue())
					return bool;
			}
		}

		throw new HibernateException("Enumeration Invalido");
	}

	public void nullSafeSet(PreparedStatement st, Object value, int index) throws HibernateException, SQLException {
		if (value == null) {
			st.setNull(index, Types.VARCHAR);
		} else {
			EnumeracaoPersistente enumPersistente = (EnumeracaoPersistente) value;
			if (enumPersistente instanceof StringEnumeracaoPersistente) {
				st.setString(index, ((StringEnumeracaoPersistente) value).getValorOrdinal());
			} else if (enumPersistente instanceof NumberEnumeracaoPersistente) {
				st.setString(index, ((NumberEnumeracaoPersistente) value).getValorOrdinal().toString());
			} else if (enumPersistente instanceof BooleanEnumeracaoPersistente) {
				st.setString(index, ((BooleanEnumeracaoPersistente) value).getValorOrdinal().toString());
			}
		}
	}

	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}

	public Class<?> returnedClass() {
		return enumClasse;
	}

	public int[] sqlTypes() {
		return new int[] { Types.VARCHAR };
	}

	public Object fromXMLString(String xmlValue) {
		return null;
	}

	public String objectToSQLString(Object value) {
		return '\'' + ((Enum<?>) value).name() + '\'';
	}

	public String toXMLString(Object value) {
		return ((Enum<?>) value).name();
	}

}