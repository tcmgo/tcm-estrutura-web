package br.gov.go.tcm.estrutura.container.faces;

import org.springframework.stereotype.Service;

import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;

@Service("localidadeTempoAjudante")
public class LocalidadeTempoAjudante {

	private String zonaTempoPadrao;

	private String localidadePadrao;

	public String getLocalidadePadrao() {
		if (localidadePadrao == null) {
			localidadePadrao = ContextoSpring.getLocalidadePadrao();
		}
		return localidadePadrao;
	}

	public String getZonaTempoPadrao() {
		if (zonaTempoPadrao == null) {
			zonaTempoPadrao = ContextoSpring.getZonaTempoPadrao();
		}
		return zonaTempoPadrao;
	}

	public void setLocalidadePadrao(String localidadePadrao) {
		this.localidadePadrao = localidadePadrao;
	}

	public void setZonaTempoPadrao(String zonaPadrao) {
		this.zonaTempoPadrao = zonaPadrao;
	}

}
