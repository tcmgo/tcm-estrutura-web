package br.gov.go.tcm.estrutura.entidade.enumeracao;



public enum TipoLancamentoExtraOrcamentariaEnumeracao2006 implements
	TipoLancamentoExtraOrcamentaria {

	DEPOSITOS_CONSIGNACOES("01"), DEBITOS_TESOURARIA("02"), 
	ATIVO_REALIZAVEL("03"), TRANSFERENCIAS_FINANCEIRAS("04"), OUTROS("99");

	String valor;

	TipoLancamentoExtraOrcamentariaEnumeracao2006(String valor) {
		this.valor = valor;
	}

	public String getValorOrdinal() {
		return this.valor;
	}


	@Override
	public String toString() {
		if (valor.equals("01")) {
			return "Dep�sitos / Consigna��es";
		} else if (valor.equals("02")) {
			return "D�bitos / Tesouraria";
		} else if (valor.equals("03")) {
			return "Ativo / Realiz�vel";
		} else if (valor.equals("04")) {
			return "Transfer�ncias Financeiras";
		} else if (valor.equals("99")) {
			return "Outros";
		}
		return "";
	}

	@Override
	public Boolean eAtivoRealizavel() {
		if (getValorOrdinal().equals(
				ATIVO_REALIZAVEL.getValorOrdinal())) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

	@Override
	public Boolean eDebitoTesouraria() {
		if (getValorOrdinal().equals(
				DEBITOS_TESOURARIA.getValorOrdinal())) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

	@Override
	public Boolean eDepositoEConsignacao() {
		if (getValorOrdinal().equals(
				DEPOSITOS_CONSIGNACOES.getValorOrdinal())) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

	@Override
	public Boolean eDiversos() {
		return Boolean.FALSE;
	}

	@Override
	public Boolean eOutros() {
		if (getValorOrdinal().equals(
				OUTROS.getValorOrdinal())) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

	@Override
	public Boolean eTransferenciaFinanceira() {
		if (getValorOrdinal().equals(
				TRANSFERENCIAS_FINANCEIRAS.getValorOrdinal())) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}
	
}