package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoSecaoEnumeracao implements StringEnumeracaoPersistente{
	EXTERNO("E"),INTERNO("I"),SUBSECAO("S");
	
	String valor;
	
	TipoSecaoEnumeracao(String valor){
		this.valor = valor;
	}
	
	public String getValorOrdinal(){
		return this.valor;
	}
	
	public EnumeracaoPersistente valor(){
		if(valor.equals("E")){
			return TipoSecaoEnumeracao.EXTERNO;
		} else if(valor.equals("I")){
			return TipoSecaoEnumeracao.INTERNO;
		} else if(valor.equals("S")){
			return TipoSecaoEnumeracao.SUBSECAO;
		} 
		
		return null;
	}
	
	@Override
	public String toString(){
		if(valor.equals("E")){
			return "Externo";
		} else if(valor.equals("I")){
			return "Interno";
		} else if(valor.equals("S")){
			return "Sub-Se��o";
		}    
		return "";
	}
}
