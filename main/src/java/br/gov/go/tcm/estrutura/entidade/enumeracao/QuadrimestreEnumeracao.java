package br.gov.go.tcm.estrutura.entidade.enumeracao;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteEnumeracao;


public enum QuadrimestreEnumeracao implements NumberEnumeracaoPersistente {

	QUADRIMESTRE_01(1), QUADRIMESTRE_02(2), QUADRIMESTRE_03(3);

	private final int valor;

	QuadrimestreEnumeracao(int valor) {
		this.valor = valor;
	}

	public String getNomeConstante() {
		switch (valor) {
		case 1:
			return "QUADRIMESTRE_01";
		case 2:
			return "QUADRIMESTRE_02";
		case 3:
			return "QUADRIMESTRE_03";
		default:
			return "";
		}
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return QuadrimestreEnumeracao.QUADRIMESTRE_01;
		case 2:
			return QuadrimestreEnumeracao.QUADRIMESTRE_02;
		case 3:
			return QuadrimestreEnumeracao.QUADRIMESTRE_03;
		}
		return null;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "1� Quadrimestre";
		case 2:
			return "2� Quadrimestre";
		case 3:
			return "3� Quadrimestre";
		default:
			return "";
		}
	}

	public static String toString(int quadrimestreTemp) {
		switch (quadrimestreTemp) {
		case 1:
			return "1� Quadrimestre";
		case 2:
			return "2� Quadrimestre";
		case 3:
			return "3� Quadrimestre";
		default:
			return "";
		}
	}

	public static QuadrimestreEnumeracao obterQuadrimestreAnterior(
			QuadrimestreEnumeracao quadrimestreEnum) {
		int quadrimestreAnterior = quadrimestreEnum.getValorOrdinal()
				.intValue() - 1;
		return (quadrimestreAnterior <= 0) ? QuadrimestreEnumeracao.QUADRIMESTRE_03
						: (QuadrimestreEnumeracao) AjudanteEnumeracao
								.obterEnumeracaoPorValorOrdinal(
										QuadrimestreEnumeracao.class,
										quadrimestreAnterior);
	}
}