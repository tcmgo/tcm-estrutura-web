package br.gov.go.tcm.estrutura.entidade.ajudante;

public class MoedaExtenso {
	
	private double num; // N�mero que ser� convertido
	
	private String s; // String que ser� retornada
	
	private int maxlen; // Tamanho limite da string
	
	@SuppressWarnings("unused")
	private boolean centavo = false;

	public MoedaExtenso() {
	}
	
	public MoedaExtenso(double num_, int maxlen_) {
		setNumber(num_, maxlen_);
	}
	
	public void setNumber(double num_, int maxlen_) {
		num = num_;
		s = new String();
		maxlen = maxlen_;
		Extenso();
	}

	/** Fun��o que faz a convers�o */
	private void Extenso() {

		String nome[] = { "um bilh�o", " bilh�es", "um milh�o", " milh�es" };
		long n = (long) num;
		long mil_milhoes;
		long milhoes;
		long milhares;
		long unidades;
		long centavos;
		
		double frac = num - n;
		@SuppressWarnings("unused")
		int nl;
		int rp;
		int last;
		int p;
		int len;
		if (num == 0) {
			s += "zero";
			return;
		}
		mil_milhoes = (n - n % 1000000000) / 1000000000;
		n -= mil_milhoes * 1000000000;
		milhoes = (n - n % 1000000) / 1000000;
		n -= milhoes * 1000000;
		milhares = (n - n % 1000) / 1000;
		n -= milhares * 1000;
		unidades = n;
		centavos = (long) (frac * 100);
		if ((long) (frac * 1000 % 10) > 5) {
			centavos++;
		}
		if (mil_milhoes > 0) {
			if (mil_milhoes == 1) {
				s += nome[0];
			} else {
				s += numero(mil_milhoes);
				s += nome[1];
			}
			if ((unidades == 0) && ((milhares == 0) && (milhoes > 0))) {
				s += " e ";
			} else if ((unidades != 0) || ((milhares != 0) || (milhoes != 0))) {
				s += ", ";
			}
		}
		if (milhoes > 0) {
			if (milhoes == 1) {
				s += nome[2];
			} else {
				s += numero(milhoes);
				s += nome[3];
			}
			if ((unidades == 0) && (milhares > 0)) {
				s += " e ";
			} else if ((unidades != 0) || (milhares != 0)) {
				s += ", ";
			}
		}
		if (milhares > 0) {
			if (milhares != 1) {
				s += numero(milhares);
			}
			s += " mil";
			if (unidades > 0) {
				if ((milhares >= 10) && (unidades > 100)) {
					s += ", ";
				} else if (((unidades % 100) != 0) || ((unidades % 100 == 0) && (milhares < 10))) {
					s += " e ";
				} else {
					s += " ";
				}
			}
		}
		s += numero(unidades);
		if (num > 0) {
			s += ((long) num == 1L) ? " real" : " reais";
		}
		if (centavos != 0) {
			if (n != 0) {
				centavo = true;
			}
			s += " e ";
			s += numero(centavos);
			s += (centavos == 1) ? " centavo" : " centavos";
		}

		len = s.length();
		StringBuffer sar = new StringBuffer(s);
		StringBuffer l = new StringBuffer();
		last = 0;
		rp = 0;
		nl = 1;

		for (p = 0; p < len; ++p) {
			if (sar.charAt(p) != '-') {
				rp++;
			}
			if (rp > maxlen) {
				if (sar.charAt(last) == ' ') {
					sar.replace(last, last + 1, " ");
				} else {
					sar.insert(last + 1, ' ');
				}
				rp -= maxlen;
				nl++;
			}
			if ((sar.charAt(p) == ' ') || (sar.charAt(p) == '-')) {
				last = p;
			}
		} 
		rp = 0;
		len = sar.length();

		for (p = 0; p < len; ++p) {
			if (!((sar.charAt(p) == '-') && (sar.charAt(p + 1) != ' '))) {
				l.insert(rp++, sar.charAt(p));
			}
		} 

		s = l.toString();
	}

	/**
	 * Retorna o extenso do n�mero
	 * 
	 * @return ...
	 */
	public String getResult() {
		String temp;
		if (s == null) {
			return "N�mero n�o est� setado!";
		}
		temp = s;
		s = null;
		return temp;
	}

	/**
	 * Retorna n�meros entre 0-999 por extenso
	 * 
	 * @param n
	 *            Description of the Parameter
	 * @return ...
	 */
	private String numero(long n) {
		
		String u[] = { "", "um", "dois", "tr�s", "quatro", "cinco", "seis",
				"sete", "oito", "nove", "dez", "onze", "doze", "treze",
				"catorze", "quinze", "dezesseis", "dezesete",
				"dezoito", "dezenove" };
		String d[] = { "", "", "vinte", "trinta", "quarenta",
				"cinquenta", "sessenta", "setenta", "oitenta",
				"noventa" };
		String c[] = { "", "cento", "duzentos", "trezentos",
				"quatrocentos", "quinhentos", "seiscentos",
				"setecentos", "oitocentos", "novecentos" };
		String extensoDoNumero = "";
		if ((n < 1000) && (n != 0)) {
			if (n == 100) {
				extensoDoNumero = "cem";
			} else {
				if (n > 99) {
					extensoDoNumero += c[(int) (n / 100)];
					if (n % 100 > 0) {
						extensoDoNumero += " e ";
					}
				}
				if (n % 100 < 20) {
					extensoDoNumero += u[(int) n % 100];
				} else {
					extensoDoNumero += d[((int) n % 100) / 10];
					if ((n % 10 > 0) && (n > 10)) {
						extensoDoNumero += " e ";
						extensoDoNumero += u[(int) n % 10];
					}
				}
			}
		} else if (n > 999) {
			extensoDoNumero = "<<ERRO: NUMERO > 999>>";
		}
		return extensoDoNumero;
	}
}