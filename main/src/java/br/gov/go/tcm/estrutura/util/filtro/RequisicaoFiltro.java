package br.gov.go.tcm.estrutura.util.filtro;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoRequisicao;

public class RequisicaoFiltro implements Filter {

	private FilterConfig filterConfig;

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.filterConfig = config;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException,
			ServletException {

		if (filterConfig == null)
			return;

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;

		// SpringWebContext.inicializarContexto(filterConfig.getServletContext()
		// .getRealPath("."), new String(request.getRequestURL()
		// .toString()).replaceAll(request.getRequestURI(), "")
		// + request.getContextPath() + "/");

		// GerenteSessaoHibernate gerenteSessao = SpringWebContext
		// .getGerenteSessao();

		AjudanteContextoRequisicao contextoRequisicao = new AjudanteContextoRequisicao();
		// contextoRequisicao.setSessaoHibernateRequisicao(gerenteSessao
		// .obterNovaSessaoRequisicao());
		contextoRequisicao.setCaminhoContexto(new String(request.getRequestURL().toString()).replaceAll(request
				.getRequestURI(), "")
				+ request.getContextPath() + "/");
		contextoRequisicao.setIpOrigem(req.getRemoteAddr());
		request.setAttribute(AjudanteContextoRequisicao.CONTEXTO_REQUISICAO, contextoRequisicao);

		try {
			filterChain.doFilter(request, response);
		} catch (Throwable e) {
			e.printStackTrace();
			// contextoRequisicao.getSessaoHibernateRequisicao()
			// .finalizarSessoesCancelando();
			return;
		}
	}

	@Override
	public void destroy() {
	}

}
