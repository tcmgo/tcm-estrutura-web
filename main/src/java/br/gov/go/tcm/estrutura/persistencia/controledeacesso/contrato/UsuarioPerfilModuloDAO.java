package br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato;

import java.util.List;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.Perfil;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioPerfilModulo;
import br.gov.go.tcm.estrutura.entidade.enumeracao.UsuarioEnumeracao;
import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;

public interface UsuarioPerfilModuloDAO extends GenericoDAO {

	List<UsuarioPerfilModulo> obterLstUsuarioPerfilModuloPorUsuarioIdETipoUsuario(Long usuarioId,
			UsuarioEnumeracao tipoUsuario);

	List<UsuarioPerfilModulo> selecionarLstUsuarioPerfilModuloPorModuloUsuarioIdTipoUsuario(Modulo modulo,
			Long usuarioId, UsuarioEnumeracao usuarioEnumeracao);
	
	List<UsuarioPerfilModulo> selecionarLstUsuarioPerfilModuloPorUsuarioIdTipoUsuario(Long usuarioId, 
			UsuarioEnumeracao usuarioEnumeracao);

	List<UsuarioPerfilModulo> obterLstUsuarioPerfilModuloPorPerfilTipoUsuario(Perfil perfil,
			UsuarioEnumeracao tipoUsuario);

	UsuarioPerfilModulo obterUsuarioPerfilModuloPorId(Long id);

	UsuarioPerfilModulo selecionarUsuarioNoModulo(Long idUsuario, Modulo modulo, UsuarioEnumeracao usuarioEnumeracao);

	List<UsuarioPerfilModulo> obterUsuarioPapelModuloPorUsuarioIdETipoUsuario(Long usuarioId,
			UsuarioEnumeracao tipoUsuario);
	
	void commitTransacao();

	List<Long> obterLstUsuarioIdPorPerfilTipoUsuario(Perfil perfil,
			UsuarioEnumeracao tipoUsuario);
}
