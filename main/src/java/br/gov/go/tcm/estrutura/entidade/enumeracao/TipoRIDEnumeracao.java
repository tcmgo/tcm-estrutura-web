package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoRIDEnumeracao implements NumberEnumeracaoPersistente {

	IMPUTACAO_DEBITO(1), IMPUTACAO_MULTA(2);

	int valor;

	TipoRIDEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoRIDEnumeracao.IMPUTACAO_DEBITO;
		case 2:
			return TipoRIDEnumeracao.IMPUTACAO_MULTA;
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Impulta��o D�bito";
		case 2:
			return "Impulta��o Multa";
		default:
			return "";
		}
	}

}