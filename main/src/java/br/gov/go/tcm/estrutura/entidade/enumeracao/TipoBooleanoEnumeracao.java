package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoBooleanoEnumeracao implements NumberEnumeracaoPersistente {

	NAO(0), SIM(1);

	private final int valor;

	TipoBooleanoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}
	
	public Boolean getValorBooleano() {
		return this.equals(SIM) ? Boolean.TRUE : Boolean.FALSE;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "N�o";
		case 1:
			return "Sim";		
		default:
			return "";
		}
	}

}
