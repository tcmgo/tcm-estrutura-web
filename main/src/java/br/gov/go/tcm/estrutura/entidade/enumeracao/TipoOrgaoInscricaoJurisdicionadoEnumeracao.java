package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoOrgaoInscricaoJurisdicionadoEnumeracao implements
		NumberEnumeracaoPersistente {
	PODER_EXECUTIVO(1), PODER_LEGISLATIVO(2), OUTROS(3);

	private int valor;

	TipoOrgaoInscricaoJurisdicionadoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Poder Executivo";
		case 2:
			return "Poder Legislativo";
		case 3:
			return "Outros";
		default:
			return "";
		}
	}

	public Boolean eOutros() {
		if (getValorOrdinal().equals(OUTROS.getValorOrdinal())) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

}
