package br.gov.go.tcm.estrutura.entidade.transiente;

import java.io.Serializable;
import java.util.Date;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteEnumeracao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoArquivoProcessoEnumeracao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoTransacionadorEnumeracao;

public class InfoBasica implements Serializable {

	private static final long serialVersionUID = 8255337705064332962L;

	private Integer numeroProcesso;
	private Number anoProcesso;
	private String interessado;
	private String teor;
	private Number regiao;

	private String assunto;
	private Date dataAutuacao;
	private String respAutuacao;
	private Number numeroProcessoOrigem;
	private Number anoOrigem;

	private Character tpInteressado;
	private TipoTransacionadorEnumeracao tipoInteressado;

	private Number julgadoId;
	private Date dataJulgado;
	private Number possuiDocDigital;

	// Não alimentado pela SQL, apenas para facilitar o DocDigital
	private Boolean docDigital;

	private Number anoResolucao;
	private Number nrResolucao;
	private String codigoParecerResolucao;

	private Number anoAcordao;
	private Number nrAcordao;
	private String codigoParecerAcordao;

	private Number anoProcessoPai;
	private Number nrProcessoPai;

	private String ultimaSecaoDestino;
	private Number volumes;

	private Character tpArquivo;
	private TipoArquivoProcessoEnumeracao tipoArquivamento;
	private Number numeroArquivo;

	private Number fasePrincipal;

	private Date dataEntrada;
	private Date dataRemessa;
	private String secaoOrigem;
	private Number nrRemessaTramite;
	private Number anoRemessaTramite;
	private String tipoServicoOrigem;
	private Number numeroParecerExecutivo;
	private Number anoParecerExecutivo;
	private String codigoParecerResolucaoOrigem;
	private Number numeroParecerLegislativo;
	private Number anoParecerLegislativo;
	private String codigoParecerAcordaoOrigem;
	private String responsavelOrigemAnalise;
	private String responsavelOrigem;

	private Date dataEntradaDestino;
	private String analistaResponsavelDestino;

	private String responsavelDestino;

	private String nomeConselheiro;
	private String telefoneConselheiro;
	private String emailConselheiro;

	private String nomeConselheiroRegiao;
	private String telefoneConselheiroRegiao;
	private String emailConselheiroRegiao;

	private String conselheiro;
	private String telefone;
	private String email;

	// TODO Remover add apenas para resolver rapido o relatorio
	private String juntadoApensadoFormatado;
	private String resolucaoFormatado;
	private String acordaoFormatado;

	private Integer sigiloso;
	private String descSigiloso;

	public String getJuntadoApensadoFormatado() {
		if ((nrProcessoPai != null && nrProcessoPai.intValue() > 0)
				&& (anoProcessoPai != null && anoProcessoPai.intValue() > 0)) {
			juntadoApensadoFormatado = nrProcessoPai.intValue() + "/" + anoProcessoPai.intValue();
		}

		return juntadoApensadoFormatado;
	}

	public String getResolucaoFormatado() {
		if ((nrResolucao != null && nrResolucao.intValue() > 0) && (anoResolucao != null && anoResolucao.intValue() > 0)
				&& (codigoParecerResolucao != null)) {
			resolucaoFormatado = nrResolucao.intValue() + "/" + anoResolucao.intValue() + "-" + codigoParecerResolucao;
		}

		return resolucaoFormatado;
	}

	public String getAcordaoFormatado() {
		if ((nrAcordao != null && nrAcordao.intValue() > 0) && (anoAcordao != null && anoAcordao.intValue() > 0)
				&& (codigoParecerAcordao != null)) {
			acordaoFormatado = nrAcordao.intValue() + "/" + anoAcordao.intValue() + "-" + codigoParecerAcordao;
		}

		return acordaoFormatado;
	}

	// GET and SET
	public Integer getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(Integer numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public Number getAnoProcesso() {
		return anoProcesso;
	}

	public void setAnoProcesso(Number anoProcesso) {
		this.anoProcesso = anoProcesso;
	}

	public String getInteressado() {
		return interessado;
	}

	public void setInteressado(String interessado) {
		this.interessado = interessado;
	}

	public String getTeor() {
		return teor;
	}

	public void setTeor(String teor) {
		this.teor = teor;
	}

	public Number getRegiao() {
		return regiao;
	}

	public void setRegiao(Number regiao) {
		this.regiao = regiao;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public Date getDataAutuacao() {
		return dataAutuacao;
	}

	public void setDataAutuacao(Date dataAutuacao) {
		this.dataAutuacao = dataAutuacao;
	}

	public String getRespAutuacao() {
		return respAutuacao;
	}

	public void setRespAutuacao(String respAutuacao) {
		this.respAutuacao = respAutuacao;
	}

	public Number getNumeroProcessoOrigem() {
		return numeroProcessoOrigem;
	}

	public void setNumeroProcessoOrigem(Number numeroProcessoOrigem) {
		this.numeroProcessoOrigem = numeroProcessoOrigem;
	}

	public Number getAnoOrigem() {
		return anoOrigem;
	}

	public void setAnoOrigem(Number anoOrigem) {
		this.anoOrigem = anoOrigem;
	}

	public Character getTpInteressado() {
		return tpInteressado;
	}

	public void setTpInteressado(Character tpInteressado) {
		this.tpInteressado = tpInteressado;
	}

	public Number getJulgadoId() {
		return julgadoId;
	}

	public void setJulgadoId(Number julgadoId) {
		this.julgadoId = julgadoId;
	}

	public Date getDataJulgado() {
		return dataJulgado;
	}

	public void setDataJulgado(Date dataJulgado) {
		this.dataJulgado = dataJulgado;
	}

	public Number getPossuiDocDigital() {
		return possuiDocDigital;
	}

	public void setPossuiDocDigital(Number possuiDocDigital) {
		this.possuiDocDigital = possuiDocDigital;
	}

	public Number getAnoResolucao() {
		return anoResolucao;
	}

	public void setAnoResolucao(Number anoResolucao) {
		this.anoResolucao = anoResolucao;
	}

	public Number getNrResolucao() {
		return nrResolucao;
	}

	public void setNrResolucao(Number nrResolucao) {
		this.nrResolucao = nrResolucao;
	}

	public String getCodigoParecerResolucao() {
		return codigoParecerResolucao;
	}

	public void setCodigoParecerResolucao(String codigoParecerResolucao) {
		this.codigoParecerResolucao = codigoParecerResolucao;
	}

	public Number getAnoAcordao() {
		return anoAcordao;
	}

	public void setAnoAcordao(Number anoAcordao) {
		this.anoAcordao = anoAcordao;
	}

	public Number getNrAcordao() {
		return nrAcordao;
	}

	public void setNrAcordao(Number nrAcordao) {
		this.nrAcordao = nrAcordao;
	}

	public String getCodigoParecerAcordao() {
		return codigoParecerAcordao;
	}

	public void setCodigoParecerAcordao(String codigoParecerAcordao) {
		this.codigoParecerAcordao = codigoParecerAcordao;
	}

	public Number getAnoProcessoPai() {
		return anoProcessoPai;
	}

	public void setAnoProcessoPai(Number anoProcessoPai) {
		this.anoProcessoPai = anoProcessoPai;
	}

	public Number getNrProcessoPai() {
		return nrProcessoPai;
	}

	public void setNrProcessoPai(Number nrProcessoPai) {
		this.nrProcessoPai = nrProcessoPai;
	}

	public String getUltimaSecaoDestino() {
		return ultimaSecaoDestino;
	}

	public void setUltimaSecaoDestino(String ultimaSecaoDestino) {
		this.ultimaSecaoDestino = ultimaSecaoDestino;
	}

	public Number getVolumes() {
		return volumes;
	}

	public void setVolumes(Number volumes) {
		this.volumes = volumes;
	}

	public Character getTpArquivo() {
		return tpArquivo;
	}

	public void setTpArquivo(Character tpArquivo) {
		this.tpArquivo = tpArquivo;
	}

	public Number getNumeroArquivo() {
		return numeroArquivo;
	}

	public void setNumeroArquivo(Number numeroArquivo) {
		this.numeroArquivo = numeroArquivo;
	}

	public Number getFasePrincipal() {
		return fasePrincipal;
	}

	public void setFasePrincipal(Number fasePrincipal) {
		this.fasePrincipal = fasePrincipal;
	}

	// Apenas para facilitar o enum
	public TipoTransacionadorEnumeracao getTipoInteressado() {
		tipoInteressado = (TipoTransacionadorEnumeracao) AjudanteEnumeracao.obterEnumeracaoPorValorOrdinal(
				TipoTransacionadorEnumeracao.class, Integer.parseInt(tpInteressado.toString()));

		return tipoInteressado;
	}

	public TipoArquivoProcessoEnumeracao getTipoArquivamento() {
		tipoArquivamento = (TipoArquivoProcessoEnumeracao) AjudanteEnumeracao
				.obterEnumeracaoPorValorOrdinal(TipoArquivoProcessoEnumeracao.class, tpArquivo);
		return tipoArquivamento;
	}

	public Boolean getDocDigital() {

		if (possuiDocDigital.intValue() == 1)
			docDigital = true;

		return docDigital;
	}

	public Date getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public Date getDataRemessa() {
		return dataRemessa;
	}

	public void setDataRemessa(Date dataRemessa) {
		this.dataRemessa = dataRemessa;
	}

	public String getSecaoOrigem() {
		return secaoOrigem;
	}

	public void setSecaoOrigem(String secaoOrigem) {
		this.secaoOrigem = secaoOrigem;
	}

	public Number getNrRemessaTramite() {
		return nrRemessaTramite;
	}

	public void setNrRemessaTramite(Number nrRemessaTramite) {
		this.nrRemessaTramite = nrRemessaTramite;
	}

	public Number getAnoRemessaTramite() {
		return anoRemessaTramite;
	}

	public void setAnoRemessaTramite(Number anoRemessaTramite) {
		this.anoRemessaTramite = anoRemessaTramite;
	}

	public String getTipoServicoOrigem() {
		return tipoServicoOrigem;
	}

	public void setTipoServicoOrigem(String tipoServicoOrigem) {
		this.tipoServicoOrigem = tipoServicoOrigem;
	}

	public Number getNumeroParecerExecutivo() {
		return numeroParecerExecutivo;
	}

	public void setNumeroParecerExecutivo(Number numeroParecerExecutivo) {
		this.numeroParecerExecutivo = numeroParecerExecutivo;
	}

	public Number getAnoParecerExecutivo() {
		return anoParecerExecutivo;
	}

	public void setAnoParecerExecutivo(Number anoParecerExecutivo) {
		this.anoParecerExecutivo = anoParecerExecutivo;
	}

	public String getCodigoParecerResolucaoOrigem() {
		return codigoParecerResolucaoOrigem;
	}

	public void setCodigoParecerResolucaoOrigem(String codigoParecerResolucaoOrigem) {
		this.codigoParecerResolucaoOrigem = codigoParecerResolucaoOrigem;
	}

	public Number getNumeroParecerLegislativo() {
		return numeroParecerLegislativo;
	}

	public void setNumeroParecerLegislativo(Number numeroParecerLegislativo) {
		this.numeroParecerLegislativo = numeroParecerLegislativo;
	}

	public Number getAnoParecerLegislativo() {
		return anoParecerLegislativo;
	}

	public void setAnoParecerLegislativo(Number anoParecerLegislativo) {
		this.anoParecerLegislativo = anoParecerLegislativo;
	}

	public String getCodigoParecerAcordaoOrigem() {
		return codigoParecerAcordaoOrigem;
	}

	public void setCodigoParecerAcordaoOrigem(String codigoParecerAcordaoOrigem) {
		this.codigoParecerAcordaoOrigem = codigoParecerAcordaoOrigem;
	}

	public String getResponsavelOrigemAnalise() {
		return responsavelOrigemAnalise;
	}

	public void setResponsavelOrigemAnalise(String responsavelOrigemAnalise) {
		this.responsavelOrigemAnalise = responsavelOrigemAnalise;
	}

	public String getResponsavelOrigem() {
		return responsavelOrigem;
	}

	public void setResponsavelOrigem(String responsavelOrigem) {
		this.responsavelOrigem = responsavelOrigem;
	}

	public void setDocDigital(Boolean docDigital) {
		this.docDigital = docDigital;
	}

	public String getResponsavelDestino() {
		return responsavelDestino;
	}

	public void setResponsavelDestino(String responsavelDestino) {
		this.responsavelDestino = responsavelDestino;
	}

	public String getNomeConselheiro() {
		return nomeConselheiro;
	}

	public void setNomeConselheiro(String nomeConselheiro) {
		this.nomeConselheiro = nomeConselheiro;
	}

	public Object getTelefoneConselheiro() {
		return telefoneConselheiro;
	}

	public void setTelefoneConselheiro(String telefoneConselheiro) {
		this.telefoneConselheiro = telefoneConselheiro;
	}

	public String getEmailConselheiro() {
		return emailConselheiro;
	}

	public void setEmailConselheiro(String emailConselheiro) {
		this.emailConselheiro = emailConselheiro;
	}

	public String getNomeConselheiroRegiao() {
		return nomeConselheiroRegiao;
	}

	public void setNomeConselheiroRegiao(String nomeConselheiroRegiao) {
		this.nomeConselheiroRegiao = nomeConselheiroRegiao;
	}

	public Object getTelefoneConselheiroRegiao() {
		return telefoneConselheiroRegiao;
	}

	public void setTelefoneConselheiroRegiao(String telefoneConselheiroRegiao) {
		this.telefoneConselheiroRegiao = telefoneConselheiroRegiao;
	}

	public String getEmailConselheiroRegiao() {
		return emailConselheiroRegiao;
	}

	public void setEmailConselheiroRegiao(String emailConselheiroRegiao) {
		this.emailConselheiroRegiao = emailConselheiroRegiao;
	}

	public void setTipoInteressado(TipoTransacionadorEnumeracao tipoInteressado) {
		this.tipoInteressado = tipoInteressado;
	}

	public void setTipoArquivamento(TipoArquivoProcessoEnumeracao tipoArquivamento) {
		this.tipoArquivamento = tipoArquivamento;
	}

	public Date getDataEntradaDestino() {
		return dataEntradaDestino;
	}

	public void setDataEntradaDestino(Date dataEntradaDestino) {
		this.dataEntradaDestino = dataEntradaDestino;
	}

	public String getAnalistaResponsavelDestino() {
		return analistaResponsavelDestino;
	}

	public void setAnalistaResponsavelDestino(String analistaResponsavelDestino) {
		this.analistaResponsavelDestino = analistaResponsavelDestino;
	}

	public String getConselheiro() {
		conselheiro = nomeConselheiro == null ? nomeConselheiroRegiao : nomeConselheiro;
		return conselheiro;
	}

	public String getTelefone() {
		if (telefoneConselheiro != null) {
			telefone = telefoneConselheiro;

		} else if (telefoneConselheiroRegiao != null) {
			telefone = telefoneConselheiroRegiao;
		}

		return telefone;
	}

	public String getEmail() {
		email = emailConselheiro == null ? emailConselheiroRegiao : emailConselheiro;
		return email;
	}

	public Integer getSigiloso() {
		return sigiloso;
	}

	public void setSigiloso(Integer sigiloso) {
		this.sigiloso = sigiloso;
	}

	public String getDescSigiloso() {
		return descSigiloso;
	}

	public void setDescSigiloso(String descSigiloso) {
		this.descSigiloso = descSigiloso;
	}

	public boolean isProcessoSigiloso() {
		if (getSigiloso() != null && getSigiloso() == 1) {
			return true;
		} else {
			return false;
		}
	}
}