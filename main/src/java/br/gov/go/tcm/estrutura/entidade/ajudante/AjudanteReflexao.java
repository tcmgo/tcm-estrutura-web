package br.gov.go.tcm.estrutura.entidade.ajudante;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;
import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.NumberEnumeracaoPersistente;
import br.gov.go.tcm.estrutura.entidade.enumeracao.StringEnumeracaoPersistente;
import br.gov.go.tcm.estrutura.util.diretorio.AjudanteDiretorio;

public class AjudanteReflexao {

	public Object obterValorPropriedade(Object fonte, String propriedade) throws AjudanteExcecao {

		if (fonte == null)
			throw new AjudanteExcecao("Fonte nula");

		Class<?> fonteClasse = fonte.getClass();
		String[] propriedades = propriedade.split("[.]");

		Object valor = null;
		valor = fonte;
		for (int i = 0; i < propriedades.length; i++) {
			Method getter;
			try {
				getter = fonteClasse.getMethod("get" + AjudanteTexto.primeiraLetraMaiuscula(propriedades[i]));
				valor = getter.invoke(valor, new Object[] {});
			} catch (NoSuchMethodException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (IllegalArgumentException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (IllegalAccessException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (InvocationTargetException e) {
				throw new AjudanteExcecao(e.getMessage());
			}
			if (valor == null)
				break;
			fonteClasse = valor.getClass();
		}

		return valor;
	}

	public boolean temPropriedade(Object fonte, String propriedade) throws AjudanteExcecao {

		if (fonte == null)
			throw new AjudanteExcecao("Fonte nula");

		Class<?> fonteClasse = fonte.getClass();
		String[] propriedades = propriedade.split("[.]");

		boolean encontrado = false;

		for (int i = 0; i < propriedades.length; i++) {

			encontrado = false;

			Method[] methods = fonteClasse.getMethods();

			for (int j = 0; j < methods.length; j++) {
				Method method = methods[j];
				if (method.getName().equals("get" + AjudanteTexto.primeiraLetraMaiuscula(propriedades[i]))) {
					fonteClasse = method.getReturnType();
					encontrado = true;
					break;
				}
			}
		}

		return encontrado;
	}

	public void aplicarValorPropriedade(Object fonte, String propriedade, String valorPropriedade)
			throws AjudanteExcecao {
		if (fonte == null || propriedade == null)
			throw new AjudanteExcecao("Parametro nulo");

		Class<?> fonteClasse = fonte.getClass();
		String[] propriedades = propriedade.split("[.]");

		Object valor = null;
		valor = fonte;

		for (int i = 0; i < propriedades.length; i++) {
			Method getter, setter;
			try {
				getter = fonteClasse.getMethod("get" + AjudanteTexto.primeiraLetraMaiuscula(propriedades[i]));
				setter = fonteClasse.getMethod("set" + AjudanteTexto.primeiraLetraMaiuscula(propriedades[i]),
						new Class[] { getter.getReturnType() });

				Object valorTemp = getter.invoke(valor, new Object[] {});

				if (EntidadePadrao.class.isAssignableFrom(getter.getReturnType())) {
					if (i == propriedades.length - 1) {
						setter.invoke(valor, new Object[] { valorPropriedade });
						continue;
					}

					if (valorTemp == null) {
						setter.invoke(valor, new Object[] { valor = getter.getReturnType().newInstance() });
					} else
						valor = valorTemp;

				} else if (EnumeracaoPersistente.class.isAssignableFrom(getter.getReturnType())) {

					@SuppressWarnings("rawtypes")
					Class enumTemp = getter.getReturnType();
					@SuppressWarnings({ "rawtypes", "unchecked" })
					EnumSet valoresEnum = EnumSet.allOf(enumTemp);

					if (NumberEnumeracaoPersistente.class.isAssignableFrom(getter.getReturnType())) {

						Integer valorPropriedadeInteiro = Integer.valueOf(valorPropriedade);

						for (Object object : valoresEnum) {
							NumberEnumeracaoPersistente numberEnumeracaoPersistente = (NumberEnumeracaoPersistente) object;
							if (numberEnumeracaoPersistente.getValorOrdinal().equals(valorPropriedadeInteiro)) {
								setter.invoke(valor, new Object[] { numberEnumeracaoPersistente });
								break;
							}
						}

					} else if (StringEnumeracaoPersistente.class.isAssignableFrom(getter.getReturnType())) {

						for (Object object : valoresEnum) {
							StringEnumeracaoPersistente stringEnumeracaoPersistente = (StringEnumeracaoPersistente) object;
							if (stringEnumeracaoPersistente.getValorOrdinal().equals(valorPropriedade)) {
								setter.invoke(valor, new Object[] { stringEnumeracaoPersistente });
								break;
							}
						}
					}

				} else if (List.class.isAssignableFrom(getter.getReturnType())) {
					if (valorPropriedade == null) {
						setter.invoke(valor, new Object[] { null });
					}

				} else if (String.class.isAssignableFrom(getter.getReturnType())) {
					setter.invoke(valor, new Object[] { new String(valorPropriedade) });
				} else if (Date.class.isAssignableFrom(getter.getReturnType())) {
					setter.invoke(valor, new Object[] { AjudanteData.converterModeloPadraoParaData(valorPropriedade) });
				} else if (Number.class.isAssignableFrom(getter.getReturnType())) {

					if (Long.class.isAssignableFrom(getter.getReturnType())) {
						setter.invoke(valor, new Object[] { (valorPropriedade.equals("") ? null : new Long(
								valorPropriedade)) });
					} else if (Integer.class.isAssignableFrom(getter.getReturnType())) {
						setter.invoke(valor, new Object[] { (valorPropriedade.equals("") ? null : new Integer(
								valorPropriedade)) });
					} else if (Short.class.isAssignableFrom(getter.getReturnType())) {
						setter.invoke(valor, new Object[] { (valorPropriedade.equals("") ? null : new Short(
								valorPropriedade)) });
					} else if (Double.class.isAssignableFrom(getter.getReturnType())) {
						setter.invoke(valor, new Object[] { (valorPropriedade.equals("") ? null : new Double(
								valorPropriedade)) });
					}

				} else {
					if (valorTemp == null) {
						setter.invoke(valor, new Object[] { valor = getter.getReturnType().newInstance() });
					} else
						valor = valorTemp;
				}

			} catch (SecurityException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (NoSuchMethodException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (IllegalArgumentException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (IllegalAccessException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (InvocationTargetException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (InstantiationException e) {
				throw new AjudanteExcecao(e.getMessage());
			}
			if (valor == null)
				break;
			fonteClasse = valor.getClass();
		}
	}

	public void aplicarValorPropriedade(Object fonte, String propriedade, Object valorPropriedade)
			throws AjudanteExcecao {
		if (fonte == null || propriedade == null)
			throw new AjudanteExcecao("Parametro nulo");

		Class<?> fonteClasse = fonte.getClass();
		String[] propriedades = propriedade.split("[.]");

		Object valor = null;
		valor = fonte;

		for (int i = 0; i < propriedades.length; i++) {
			Method getter, setter;
			try {
				getter = fonteClasse.getMethod("get" + AjudanteTexto.primeiraLetraMaiuscula(propriedades[i]));
				setter = fonteClasse.getMethod("set" + AjudanteTexto.primeiraLetraMaiuscula(propriedades[i]),
						new Class[] { getter.getReturnType() });

				Object valorTemp = getter.invoke(valor, new Object[] {});

				if (String.class.isAssignableFrom(getter.getReturnType())) {
					setter.invoke(valor, valorPropriedade);
				} else if (Number.class.isAssignableFrom(getter.getReturnType())) {
					setter.invoke(valor, valorPropriedade);
				} else if (EnumeracaoPersistente.class.isAssignableFrom(getter.getReturnType())) {
					setter.invoke(valor, valorPropriedade);
				} else if (Date.class.isAssignableFrom(getter.getReturnType())) {
					setter.invoke(valor, valorPropriedade);
				} else if (Boolean.class.isAssignableFrom(getter.getReturnType())) {
					setter.invoke(valor, valorPropriedade);
				} else {
					if (valorTemp == null) {
						setter.invoke(valor, new Object[] { valor = getter.getReturnType().newInstance() });
					} else
						valor = valorTemp;
				}

			} catch (SecurityException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (NoSuchMethodException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (IllegalArgumentException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (IllegalAccessException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (InvocationTargetException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (InstantiationException e) {
				throw new AjudanteExcecao(e.getMessage());
			}
			if (valor == null)
				break;
			fonteClasse = valor.getClass();
		}
	}

	public Class<?> obterTipoPropriedade(Object fonte, String propriedade) throws AjudanteExcecao {
		if (fonte == null || propriedade == null)
			throw new AjudanteExcecao("Parametro nulo");

		Class<?> fonteClasse = fonte.getClass();
		String[] propriedades = propriedade.split("[.]");

		Object valor = null;
		valor = fonte;

		for (int i = 0; i < propriedades.length; i++) {
			Method getter;
			try {
				getter = fonteClasse.getMethod("get" + AjudanteTexto.primeiraLetraMaiuscula(propriedades[i]));

				if (i == propriedades.length - 1)
					return getter.getReturnType();

			} catch (SecurityException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (NoSuchMethodException e) {
				throw new AjudanteExcecao(e.getMessage());
			} catch (IllegalArgumentException e) {
				throw new AjudanteExcecao(e.getMessage());
			}

			if (valor == null)
				break;
			fonteClasse = valor.getClass();
		}

		return fonteClasse;
	}
	/*
	public static Class<?>[] obterClassesDoPacote(String pckgname) throws AjudanteExcecao {
		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
		// Get a File object for the package
		File directory = null;
		try {
			String path = pckgname.replace('.', '/');
			path = ContextoSpring.getDiretorioContexto() + "WEB-INF/classes" + AjudanteDiretorio.BARRA + path;
			path = path.replace('\\', '/');
			directory = new File(path);
		} catch (NullPointerException x) {
			throw new AjudanteExcecao(pckgname + " (" + directory + ") n�o parece ser um pacote v�lido.");
		}
		if (directory.exists()) {
			// Get the list of the files contained in the package
			String[] files = directory.list();
			for (int i = 0; i < files.length; i++) {
				// we are only interested in .class files
				if (files[i].endsWith(".class")) {
					// removes the .class extension
					try {
						classes.add(Class.forName(pckgname + '.' + files[i].substring(0, files[i].length() - 6)));
					} catch (ClassNotFoundException e) {
						throw new AjudanteExcecao(e.getMessage());
					}
				}
			}
		} else {
			throw new AjudanteExcecao(pckgname + " n�o parece ser um pacote v�lido.");
		}
		Class<?>[] classesA = new Class[classes.size()];
		classes.toArray(classesA);
		return classesA;
	}
	*/
	
	@SuppressWarnings("resource")
	public static Class<?>[] obterClassesDoPacote(String pacote) {

		String packageName = pacote.replace(".", "/");

		ArrayList<Class<?>> classes = new ArrayList<Class<?>>();

		try {
			Enumeration<URL> enumUrl = Thread.currentThread().getContextClassLoader().getResources(packageName);

			while (enumUrl.hasMoreElements()) {

				URL packageURL = (URL) enumUrl.nextElement();

				if (packageURL.getProtocol().equals("jar")) {

					// build jar file name, then loop through zipped entries
					String jarFileName = URLDecoder.decode(packageURL.getFile(), "UTF-8");
					jarFileName = jarFileName.substring(5, jarFileName.indexOf("!"));

					JarFile jf = new JarFile(jarFileName);
					Enumeration<JarEntry> jarEntries = jf.entries();

					while (jarEntries.hasMoreElements()) {
						String entryName = jarEntries.nextElement().getName();

						if (entryName.startsWith(packageName) && entryName.length() > packageName.length() + 5) {

							if (entryName.indexOf(".class") > -1) {
								entryName = entryName.substring(packageName.length() + 1, entryName.lastIndexOf('.'))
										.replace("/", ".");

								classes.add(Class.forName(pacote + "." + entryName));
							}
						}
					}

				// loop through files in classpath
				} else {
					File folder = new File(packageURL.getFile());

					if (folder.exists()) {
						File[] lstArquivos = folder.listFiles();
						String entryName;

						for (File atual : lstArquivos) {
							entryName = atual.getName();
							entryName = entryName.substring(0, entryName.lastIndexOf('.'));

							String classe = pacote + "." + entryName;
							classes.add(Class.forName(classe));
						}
					}
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		Class<?>[] classesA = new Class[classes.size()];
		classes.toArray(classesA);
		return classesA;
	}

	public List<String> obterPropriedades(Class<?> classe) {
		List<String> resultado = new ArrayList<String>();
		Field[] campos = classe.getDeclaredFields();

		while (!classe.equals(java.lang.Object.class)) {
			for (int i = 0; i < campos.length; ++i) {
				if (!Modifier.isFinal(campos[i].getModifiers()))
					resultado.add(campos[i].getName());
			}
			classe = classe.getSuperclass();
			campos = classe.getDeclaredFields();
		}
		return resultado;
	}

	public static Class<?> obterClasseDoObjeto(Object o) {
		return o.getClass();
	}

	public static Object novaInstanciaDaClasse(Class<?> tipoConteudo) throws AjudanteExcecao {
		try {
			return tipoConteudo.newInstance();
		} catch (InstantiationException e) {
			throw new AjudanteExcecao(e.getMessage());
		} catch (IllegalAccessException e) {
			throw new AjudanteExcecao(e.getMessage());
		}
	}
	
	public static Class<?> obterClasseParaNome(String nome) {
		try {
			return Class.forName(nome);
		} catch (ClassNotFoundException e) {
			throw new AjudanteExcecao(e.getMessage());
		}
	}

}