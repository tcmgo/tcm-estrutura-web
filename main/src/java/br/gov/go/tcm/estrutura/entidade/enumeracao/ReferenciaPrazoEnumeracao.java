package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum ReferenciaPrazoEnumeracao implements NumberEnumeracaoPersistente {

	ANO(1), MES(2), DIA(3);
	
	int valor;

	ReferenciaPrazoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}
	
	public boolean isAno(){
		return this.valor == 1;
	}
	public boolean isMes(){
		return this.valor == 2;
	}
	public boolean isDia(){
		return this.valor == 3;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return ReferenciaPrazoEnumeracao.ANO;
		case 2:
			return ReferenciaPrazoEnumeracao.MES;
		case 3:
			return ReferenciaPrazoEnumeracao.DIA;
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Ano";
		case 2:
			return "M�s";
		case 3:
			return "Dias";
		default:
			return "";
		}
	}
}
