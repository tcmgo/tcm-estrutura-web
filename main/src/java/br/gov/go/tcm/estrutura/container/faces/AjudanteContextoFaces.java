package br.gov.go.tcm.estrutura.container.faces;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AjudanteContextoFaces {

	public static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}
	
	public static Map<String, Object> getRequestMap() {
		return getFacesContext().getExternalContext().getRequestMap();
	}
	
	public static Map<String, Object> getSessionMap() {
		return getFacesContext().getExternalContext().getSessionMap();
	}
	
	public static Map<String, Object> getApplicationMap() {
		return getFacesContext().getExternalContext().getApplicationMap();
	}
	
	public static Map<String, String> getRequestParameterMap() {
		return getFacesContext().getExternalContext().getRequestParameterMap();
	}
	
	public static HttpSession getSessao() {
		HttpServletRequest request = (HttpServletRequest) getFacesContext()
				.getExternalContext().getRequest();
		return request.getSession();
	}

	public static void imprimirConsoleRequestMap() {
		imprimirMap(getRequestMap());
	}

	public static void imprimirConsoleSessionMap() {
		imprimirMap(getSessionMap());
	}

	public static void imprimirConsoleRequestParameterMap() {
		imprimirMap(getRequestParameterMap());
	}

	public static String getIdentificacaoUsuario() {

		String userId = new String();

		if (getFacesContext() != null) {
			HttpSession session = (HttpSession) getFacesContext().getExternalContext().getSession(true);
			if (session == null) {
				userId = userId.concat(new Long((long) (System.currentTimeMillis() * (Math.random() * 50000) * (Math
						.random() * 1000000))).toString());
			} else {
				userId = userId.concat(session.getId() + new Long(System.currentTimeMillis()).toString());
			}

		} else {
			userId = userId.concat(new Long(System.currentTimeMillis()).toString());
		}

		return userId;
	}

	private static void imprimirMap(Map<?, ?> map) {
		Iterator<?> iter = map.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<?, ?> entry = (Entry<?, ?>) iter.next();
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
	}

	public static String getContextoWeb() {
		if (getFacesContext() != null) {
			HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();

			return new String(request.getRequestURL().toString()).replaceAll(request.getServletPath(), "/");
		}
		return "";
	}

	public static String getContextoSiteTcm() {
		if (getFacesContext() != null) {
			HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();

			return new String(request.getRequestURL().toString()).replaceAll(request.getContextPath()
					+ request.getServletPath(), "/site/");
		}
		return "";
	}
	
	private static HttpServletRequest getRequest() {
		HttpServletRequest rs = (HttpServletRequest) getFacesContext()
				.getExternalContext().getRequest();
		return rs;
	}
	
	public static String getHost() {
		if (getFacesContext() != null) {
			HttpServletRequest request = getRequest();

			return new String(request.getHeader("Host") + "/");
		}
		return "";
	}
	
	public static void addObjetoSessao(String rotulo, Object objeto){
		getRequest().getSession().setAttribute(rotulo, objeto);
	}
	
	public static Object getObjetoSessao(String rotulo){
		return getRequest().getSession().getAttribute(rotulo);
	}
	
	
	
}
