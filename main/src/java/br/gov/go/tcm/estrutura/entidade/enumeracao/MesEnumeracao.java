package br.gov.go.tcm.estrutura.entidade.enumeracao;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteEnumeracao;




public enum MesEnumeracao implements NumberEnumeracaoPersistente {

	JANEIRO(1), FEVEREIRO(2), MARCO(3), ABRIL(4), MAIO(5), JUNHO(6), JULHO(7), AGOSTO(
			8), SETEMBRO(9), OUTUBRO(10), NOVEMBRO(11), DEZEMBRO(12);

	private final int valor;

	MesEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}
	
	public String getNome(){
		return toString();
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Janeiro";
		case 2:
			return "Fevereiro";
		case 3:
			return "Mar�o";
		case 4:
			return "Abril";
		case 5:
			return "Maio";
		case 6:
			return "Junho";
		case 7:
			return "Julho";
		case 8:
			return "Agosto";
		case 9:
			return "Setembro";
		case 10:
			return "Outubro";
		case 11:
			return "Novembro";
		case 12:
			return "Dezembro";
		default:
			return "";
		}
	}

	public static MesEnumeracao getMesEnumeracao(int mes){
		switch (mes) {
		case 1:
			return JANEIRO;
		case 2:
			return FEVEREIRO;
		case 3:
			return MARCO;
		case 4:
			return ABRIL;
		case 5:
			return MAIO;
		case 6:
			return JUNHO;
		case 7:
			return JULHO;
		case 8:
			return AGOSTO;
		case 9:
			return SETEMBRO;
		case 10:
			return OUTUBRO;
		case 11:
			return NOVEMBRO;
		case 12:
			return DEZEMBRO;
		default:
			throw new IllegalArgumentException();
		}
	}
	
	public static String toString(int mesTemp) {
		switch (mesTemp) {
		case 1:
			return "Janeiro";
		case 2:
			return "Fevereiro";
		case 3:
			return "Mar�o";
		case 4:
			return "Abril";
		case 5:
			return "Maio";
		case 6:
			return "Junho";
		case 7:
			return "Julho";
		case 8:
			return "Agosto";
		case 9:
			return "Setembro";
		case 10:
			return "Outubro";
		case 11:
			return "Novembro";
		case 12:
			return "Dezembro";
		default:
			return "";
		}
	}

	public static MesEnumeracao obterMesAnterior(MesEnumeracao mesEnumeracao) {
		int mesAnterior = mesEnumeracao.getValorOrdinal().intValue() - 1;
		return (mesAnterior < 0) ? null : mesAnterior == 0 ? MesEnumeracao.DEZEMBRO
				: (MesEnumeracao) AjudanteEnumeracao.obterEnumeracaoPorValorOrdinal(
						MesEnumeracao.class, mesAnterior);
	}
	
	public static MesEnumeracao obterMesPosterior(MesEnumeracao mesEnumeracao) {
		int mesPosterior = mesEnumeracao.getValorOrdinal().intValue() + 1;
		return (mesPosterior > 12) ? MesEnumeracao.JANEIRO
				: (MesEnumeracao) AjudanteEnumeracao.obterEnumeracaoPorValorOrdinal(
						MesEnumeracao.class, mesPosterior);
	}

}
