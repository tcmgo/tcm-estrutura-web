package br.gov.go.tcm.estrutura.util.excecao;

public class InterpretadorChaveAcessoOrgaoExcecao extends Exception{

	private static final long serialVersionUID = 576326503375235670L;

	public InterpretadorChaveAcessoOrgaoExcecao(String message) {
		super(message);
	}
	
}
