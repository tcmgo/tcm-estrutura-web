package br.gov.go.tcm.estrutura.relatorio.excecao;

public class GeradorRelatorioExcecao extends RuntimeException {

	private static final long serialVersionUID = 2662771498896452917L;

	public GeradorRelatorioExcecao(String message) {
		super(message);
	}

}
