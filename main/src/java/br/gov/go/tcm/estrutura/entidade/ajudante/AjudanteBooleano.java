package br.gov.go.tcm.estrutura.entidade.ajudante;

public class AjudanteBooleano {
	

	public static String converterBooleanString(Boolean bol){
		if(bol == null){
			return "";
		}
		if (bol.booleanValue()){
			return "Sim";
		}else if(!bol.booleanValue()){
			return "N�o";
		}
		return "";
	}
	
}
