package br.gov.go.tcm.estrutura.entidade.enumeracao;



public enum TipoArquivoPromoexEnumeracao implements NumberEnumeracaoPersistente {

	DOCUMENTO(1), PROJETO(2), POA(3);
	
	int valor;
	
	TipoArquivoPromoexEnumeracao(int valor){
		this.valor = valor;
	}
	
	public Number getValorOrdinal(){
		return this.valor;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoArquivoPromoexEnumeracao.DOCUMENTO;
		case 2:
			return TipoArquivoPromoexEnumeracao.PROJETO;
		case 3:
			return TipoArquivoPromoexEnumeracao.POA;
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Documento";
		case 2:
			return "Projeto";
		case 3:
			return "POA";
		default:
			return "";
		}
	}
	
}