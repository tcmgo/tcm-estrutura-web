package br.gov.go.tcm.estrutura.relatorio.fonteDeDados;

public class GeradorRecursoVisaoExcecao extends RuntimeException {

	private static final long serialVersionUID = 3376671722568372096L;

	public GeradorRecursoVisaoExcecao(String message) {
		super(message);
	}

}
