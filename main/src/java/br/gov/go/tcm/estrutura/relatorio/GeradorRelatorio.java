package br.gov.go.tcm.estrutura.relatorio;

import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoFaces;
import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;
import br.gov.go.tcm.estrutura.relatorio.excecao.GeradorRelatorioExcecao;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.FonteDadosRelatorio;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.ParametroVisao;
import br.gov.go.tcm.estrutura.util.diretorio.AjudanteDiretorio;


public class GeradorRelatorio {

	public final static int SAIDA_HTML = 0;

	public final static int SAIDA_PDF = 1;

	public final static int SAIDA_PDF_LANDSCAPE = 2;

	public static final int SAIDA_XLS = 3;

	public FabricaRelatorio getFabricaRelatorio() {
		return ContextoSpring.getFabricaRelatorio();
	}

	public String gerarRelatorio(String identificador, List<ParametroVisao> parametros,
			FonteDadosRelatorio fonteDadosRelatorio, int tipoSaida) throws GeradorRelatorioExcecao {
		try {
			File reportFile = getFabricaRelatorio().obterRelatorio(identificador);
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile.getPath());
			Map<String, Object> parametrosJasper = new HashMap<String, Object>();

			for (ParametroVisao parametro : parametros) {
				parametrosJasper.put(parametro.getId(), parametro.getValor());
			}

			JasperPrint jasperPrint = null;

			if (fonteDadosRelatorio.getFonteDados() instanceof Connection) {
				jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosJasper,
						(Connection) fonteDadosRelatorio.getFonteDados());
			}

			if (fonteDadosRelatorio.getFonteDados() instanceof JRDataSource) {
				jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosJasper,
						(JRDataSource) fonteDadosRelatorio.getFonteDados());
			}

			AjudanteDiretorio assistenteDiretorio = new AjudanteDiretorio();

			String nomeArquivo = "";
			if (tipoSaida == GeradorRelatorio.SAIDA_HTML) {
				String userId = AjudanteContextoFaces.getIdentificacaoUsuario();

				nomeArquivo = assistenteDiretorio.obterCaminhoContexto()
						+ getFabricaRelatorio().getDiretorioRelativoArquivosSaidaRelatorio() + identificador + userId
						+ ".html";
				JasperExportManager.exportReportToHtmlFile(jasperPrint, nomeArquivo);
				nomeArquivo = identificador + userId + ".html";
				;
			} else if (tipoSaida == GeradorRelatorio.SAIDA_PDF || tipoSaida == GeradorRelatorio.SAIDA_PDF_LANDSCAPE) {

				String userId = AjudanteContextoFaces.getIdentificacaoUsuario();

				nomeArquivo = assistenteDiretorio.obterCaminhoContexto()
						+ getFabricaRelatorio().getDiretorioRelativoArquivosSaidaRelatorio() + identificador + userId
						+ ".pdf";
				JasperExportManager.exportReportToPdfFile(jasperPrint, nomeArquivo);
				nomeArquivo = identificador + userId + ".pdf";
			} else if (tipoSaida == GeradorRelatorio.SAIDA_XLS) {
				String userId = AjudanteContextoFaces.getIdentificacaoUsuario();

				nomeArquivo = assistenteDiretorio.obterCaminhoContexto()
						+ getFabricaRelatorio().getDiretorioRelativoArquivosSaidaRelatorio() + identificador + userId
						+ ".xls";

				JRXlsExporter exporterXLS = new JRXlsExporter();
				exporterXLS.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporterXLS.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				exporterXLS.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
				exporterXLS.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, nomeArquivo);
				//exporterXLS.setParameter(JRXlsAbstractExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);

				exporterXLS.exportReport();
				nomeArquivo = identificador + userId + ".xls";
			}

			return getFabricaRelatorio().getDiretorioRelativoArquivosSaidaRelatorio() + nomeArquivo;
		} catch (JRException e) {
			e.printStackTrace();
			throw new GeradorRelatorioExcecao(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			throw new GeradorRelatorioExcecao(e.getMessage());
		}

	}

}
