package br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato;

import java.util.List;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioMonitor;
import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;

public interface UsuarioMonitorDAO extends GenericoDAO{
	
	List<UsuarioMonitor> obterLstUsuarioMonitorPorNome(String nome);
	
	UsuarioMonitor obterUsuarioMonitorPorNomeSenha(UsuarioMonitor usuarioMonitor);

	UsuarioMonitor obterUsuarioMonitorAtivoPorNomeSenha(
			UsuarioMonitor usuarioMonitor);
	
	UsuarioMonitor obterUsuarioMonitorPorNome(String nome);

	List<UsuarioMonitor> obterTodosUsuariosAtivos();

	List<UsuarioMonitor> obterTodosUsuariosAtivosPorSecao(String codigoSecao);
}
