package br.gov.go.tcm.estrutura.container.faces;

import org.springframework.stereotype.Service;

import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;

@Service("linguagemAjudante")
public class LinguagemAjudante {

	private String linguagemPadrao;

	public String getLinguagemPadrao() {
		if(linguagemPadrao == null){
			linguagemPadrao = ContextoSpring.getLinguagemPadrao();
		}
		return linguagemPadrao;
	}

	public void setLinguagemPadrao(String linguagemPadrao) {
		this.linguagemPadrao = linguagemPadrao;
	}
}
