package br.gov.go.tcm.estrutura.entidade.transiente;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;


public class ArquivoSelecaoEstagiario implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static final String CURRICULO = "Curr�culo";
	public static final String HISTORICO = "Hist�rico";
	public static final String DECLARACAO_FREQUENCIA = "Declara��o de Frequ�ncia";
	public static final String LAUDO_MEDICO = "Laudo M�dico";
	public static final String FOTO_3X4 = "Foto 3x4";
	
	private String nomeArquivo;
	private String sufixo;
	private String tipoArquivo;
	private String caminhoArquivo;
	
	
	public ArquivoSelecaoEstagiario() {}
	
	public ArquivoSelecaoEstagiario(String nomeArquivo, String sufixo, String tipoArquivo) {
		super();
		this.nomeArquivo = nomeArquivo;
		this.sufixo = sufixo;
		this.tipoArquivo = tipoArquivo;
	}
	
	
	public String getNomeArquivo() {
		return nomeArquivo;
	}
	public void setNomeArquivo(String nomeArquivo) {
		try {
			this.nomeArquivo = (new String(nomeArquivo.getBytes(), "UTF-8")); // file system
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
	}
	public String getSufixo() {
		return sufixo;
	}
	public void setSufixo(String sufixo) {
		this.sufixo = sufixo;
	}
	public String getTipoArquivo() {
		return tipoArquivo;
	}
	public void setTipoArquivo(String tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}
	public String getCaminhoArquivo() {
		return caminhoArquivo;
	}
	public void setCaminhoArquivo(String caminhoArquivo) {
		this.caminhoArquivo = caminhoArquivo;
	}
	
	
	public boolean ehCurriculo() {
		return tipoArquivo.equals(CURRICULO);
	}
	public boolean ehHistorico() {
		return tipoArquivo.equals(HISTORICO);
	}
	public boolean ehDeclaracaoFrequencia() {
		return tipoArquivo.equals(DECLARACAO_FREQUENCIA);
	}
	public boolean ehFoto3x4() {
		return tipoArquivo.equals(FOTO_3X4);
	}
	public boolean ehLaudoMedico() {
		return tipoArquivo.equals(LAUDO_MEDICO);
	}
	
}
