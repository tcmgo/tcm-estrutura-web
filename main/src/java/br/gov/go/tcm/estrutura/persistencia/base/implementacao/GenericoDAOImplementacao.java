package br.gov.go.tcm.estrutura.persistencia.base.implementacao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.transform.ResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteReflexao;
import br.gov.go.tcm.estrutura.entidade.base.Entidade;
import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;
import br.gov.go.tcm.estrutura.persistencia.base.ExemploBusca;
import br.gov.go.tcm.estrutura.persistencia.base.contrato.GenericoDAO;

public abstract class GenericoDAOImplementacao extends HibernateDaoSupport implements GenericoDAO, Serializable {
	
	private static final long serialVersionUID = 1L;

	public static final Logger log = Logger.getLogger(GenericoDAOImplementacao.class);
	
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactoryWeb;
	
	@Autowired
	@Qualifier("sessionFactoryOrcafi")
	private SessionFactory sessionFactoryOrcafi;
	
	@Autowired
	@Qualifier("sessionFactoryStp")
	private SessionFactory sessionFactoryStp;
	
	@Autowired
	@Qualifier("sessionFactoryUsuario")
	private SessionFactory sessionFactoryUsuario;
	
//	@Autowired
//	@Qualifier("sessionFactoryAdministracao")
//	private SessionFactory sessionFactoryAdministracao;
	

	public Entidade obterEntidadePorId(Class<? extends Entidade> classe, Serializable id) {
		Banco banco = (Banco) classe.getAnnotation(Banco.class);
		Entidade e = (Entidade) getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome())).get(classe, id);
		if(e != null){
			e.toString(); // s� para inicializar os atributos.
		}
		return e;
	}

	public void salvar(Object objeto) {
		Banco banco = (Banco) objeto.getClass().getAnnotation(Banco.class);
		HibernateTemplate hibernateTemplate = getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome()));
		Session session = getSession(hibernateTemplate);
		
		try {
			session.beginTransaction();
			session.setFlushMode(FlushMode.AUTO);
			hibernateTemplate.save(objeto);
			session.getTransaction().commit();
		} catch (Exception ex) {
			session.getTransaction().rollback();
			ex.printStackTrace();
		} finally {
			hibernateTemplate.clear();
			session.close();
		}
	}

	public void salvarOuAtualizar(Object objeto) {
		Banco banco = (Banco) objeto.getClass().getAnnotation(Banco.class);
		HibernateTemplate hibernateTemplate = getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome()));
		Session session = getSession(hibernateTemplate);
		
		try {
			session.beginTransaction();
			session.setFlushMode(FlushMode.AUTO);
			hibernateTemplate.merge(objeto);
			session.getTransaction().commit();
		} catch (Exception ex) {
			session.getTransaction().rollback();
			ex.printStackTrace();
		} finally {
			hibernateTemplate.clear();
			session.close();
		}
	}

	public void atualizar(Object objeto) {
		Banco banco = (Banco) objeto.getClass().getAnnotation(Banco.class);
		HibernateTemplate hibernateTemplate = getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome()));
		Session session = getSession(hibernateTemplate);
		
		try {
			session.beginTransaction();
			session.setFlushMode(FlushMode.AUTO);
			hibernateTemplate.merge(objeto);
			session.getTransaction().commit();
		} catch (Exception ex) {
			session.getTransaction().rollback();
			ex.printStackTrace();
		} finally {
			hibernateTemplate.clear();
			session.close();
		}
	}

	public void excluir(Object objeto) {
		Banco banco = (Banco) objeto.getClass().getAnnotation(Banco.class);
		HibernateTemplate hibernateTemplate = getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome()));
		Session session = getSession(hibernateTemplate);
		
		try {
			session.beginTransaction();
			session.setFlushMode(FlushMode.AUTO);
			hibernateTemplate.delete(objeto);
			session.getTransaction().commit();
		} catch (Exception ex) {
			session.getTransaction().rollback();
			ex.printStackTrace();
		} finally {
			hibernateTemplate.clear();
			session.close();
		}
	}

	public List<?> obterTodos(Class<?> classe) {
		return obterPorCriterio(classe);
	}

	public List<?> obterPorExemplo(Object objeto) {
		return obterPorExemplo(objeto, ExemploBusca.ALGUM_LUGAR);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<?> obterPorExemplo(final Object objeto, final int exemploBusca) {
		Banco banco = (Banco) objeto.getClass().getAnnotation(Banco.class);
		return (List<?>) getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome())).execute(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				MatchMode modeDeBusca = null;

				switch (exemploBusca) {
				case 0:
					modeDeBusca = MatchMode.EXACT;
					break;
				case 1:
					modeDeBusca = MatchMode.START;
					break;
				case 2:
					modeDeBusca = MatchMode.END;
					break;
				case 3:
					modeDeBusca = MatchMode.ANYWHERE;
					break;
				default:
					return null;
				}

				AjudanteReflexao ajudante = new AjudanteReflexao();
				List<String> lstPropriedades = ajudante.obterPropriedades(objeto.getClass());

				Example exemplo = Example.create(objeto).ignoreCase().excludeZeroes().enableLike(modeDeBusca);

				for (String propriedade : lstPropriedades) {
					if (Boolean.class.isAssignableFrom(ajudante.obterTipoPropriedade(objeto, propriedade))) {
						exemplo.excludeProperty(propriedade);
					}
				}

				Criteria criterio = session.createCriteria(objeto.getClass()).add(exemplo);

				try {
					return criterio.list();
				} catch (Exception e) {
					return new ArrayList();
				} finally {
					session.clear();
					session.close();
				}
			}
		});
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<?> obterPorCriterio(final Class<? extends EntidadePadrao> classe, final ResultTransformer transformador, final Projection projecoes, final Order order, final Criterion ... criterio) {
		Banco banco = (Banco) classe.getAnnotation(Banco.class);
		
		return (List<?>) getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome())).execute(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				Criteria crit = session.createCriteria(classe);
				for (Criterion c : criterio) {
					crit.add(c);
				}
				
				crit.setProjection(projecoes);
				
				crit.setResultTransformer(transformador);
				
				if (order != null) {
					
					crit.addOrder(order);
				}
				
				try {
					return crit.list();
				} catch (Exception e) {
					e.printStackTrace();
					return new ArrayList();
				} finally {
					session.clear();
					session.close();
				}
			}
		});
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected List<?> obterPorCriterio(final Class<?> classe, final Criterion... criterio) {
		Banco banco = (Banco) classe.getAnnotation(Banco.class);
		return (List<?>) getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome())).execute(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				Criteria crit = session.createCriteria(classe);
				for (Criterion c : criterio) {
					crit.add(c);
				}
				
				crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				
				try {
					return crit.list();
				} catch (Exception e) {
					return new ArrayList();
				} finally {
					session.clear();
					session.close();
				}
			}
		});
	}

	public void atachar(Object objeto) {
		Banco banco = (Banco) objeto.getClass().getAnnotation(Banco.class);
		getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome())).lock(objeto, LockMode.NONE);
	}

	public void desatachar(Object objeto) {
		Banco banco = (Banco) objeto.getClass().getAnnotation(Banco.class);
		Hibernate.initialize(objeto);
		getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome())).evict(objeto);
	}

	@Override
	public boolean contemObjetoNaSessao(Object objeto) {
		Banco banco = (Banco) objeto.getClass().getAnnotation(Banco.class);
		return getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome())).contains(objeto);
	}

	@Override
	public Entidade carregarObjetoNaSessao(Class<? extends Entidade> classe, Serializable id) {		
		Banco banco = (Banco) classe.getAnnotation(Banco.class);
		return (Entidade) getHibernateTemplate(getSessionFactoryNomeBanco(banco.nome())).load(classe, id);
	}
	
	public Session getSession(HibernateTemplate hibernateTemplate){
		if(getSession() == null){
			return getSession(hibernateTemplate);
		}
		return getSession();
	}
	
	public HibernateTemplate getHibernateTemplate(SessionFactory sessionFactory) {
		if(getHibernateTemplate() == null){
			setHibernateTemplate(new HibernateTemplate());
			getHibernateTemplate().setSessionFactory(sessionFactory);
		}
		return super.getHibernateTemplate();
	}
	
	public SessionFactory getSessionFactoryNomeBanco(String nomeBanco) {
		String sessionFactory = "sessionFactory";
		
		if(nomeBanco != null && !nomeBanco.isEmpty()){
			nomeBanco = nomeBanco.substring(0,1).toUpperCase() + nomeBanco.substring(1).toLowerCase();
			sessionFactory = sessionFactory.concat(nomeBanco);
		}
		
		if(sessionFactory.equals("sessionFactoryTcmweb"))
			return sessionFactoryWeb;
		else if(sessionFactory.equals("sessionFactoryOrcafi"))
			return sessionFactoryOrcafi;
		else if(sessionFactory.equals("sessionFactoryStp"))
			return sessionFactoryStp;
		else if(sessionFactory.equals("sessionFactoryUsuario"))
			return sessionFactoryUsuario;
//		else if(sessionFactory.equals("sessionFactoryAdministracao"))
//			return sessionFactoryAdministracao;
		
		return sessionFactoryWeb;
	}
	
	public SessionFactory getSessionFactoryWeb() {
		return sessionFactoryWeb;
	}
	
	public SessionFactory getSessionFactoryOrcafi() {
		return sessionFactoryOrcafi;
	}
	
	public SessionFactory getSessionFactoryStp() {
		return sessionFactoryStp;
	}
	
	public SessionFactory getSessionFactoryUsuario() {
		return sessionFactoryUsuario;
	}
	
//	public SessionFactory getSessionFactoryAdministracao() {
//		return sessionFactoryAdministracao;
//	}
	
}