package br.gov.go.tcm.estrutura.relatorio.fonteDeDados;

import net.sf.jasperreports.engine.JREmptyDataSource;

public class FonteDadosVazia implements FonteDadosRelatorio{
	
	public JREmptyDataSource getFonteDados() {
		return new JREmptyDataSource();
	}

}
