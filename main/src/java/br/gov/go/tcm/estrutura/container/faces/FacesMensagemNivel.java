package br.gov.go.tcm.estrutura.container.faces;

import javax.faces.application.FacesMessage;

public class FacesMensagemNivel {

	public static final FacesMensagemNivel INFORMACAO = new FacesMensagemNivel(
			FacesMessage.SEVERITY_INFO);

	public static final FacesMensagemNivel AVISO = new FacesMensagemNivel(
			FacesMessage.SEVERITY_WARN);

	public static final FacesMensagemNivel ERRO = new FacesMensagemNivel(
			FacesMessage.SEVERITY_ERROR);

	public static final FacesMensagemNivel FATAL = new FacesMensagemNivel(
			FacesMessage.SEVERITY_FATAL);

	private FacesMessage.Severity nivel;

	public FacesMessage.Severity getNivel() {
		return nivel;
	}

	public void setNivel(final FacesMessage.Severity nivel) {
		this.nivel = nivel;
	}

	private FacesMensagemNivel(final FacesMessage.Severity nivel) {
		setNivel(nivel);
	}

}
