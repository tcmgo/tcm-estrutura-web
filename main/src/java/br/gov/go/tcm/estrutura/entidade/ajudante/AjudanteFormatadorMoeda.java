package br.gov.go.tcm.estrutura.entidade.ajudante;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Currency;

import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

public class AjudanteFormatadorMoeda {

	public static final String MASCARA_CPF = "###.###.###-##";

	private static JFormattedTextField jFormattedTextField = null;

	private static MaskFormatter maskFormatter = null;

	private static Currency currency = Currency.getInstance("BRL");

	private static DecimalFormat formato = new DecimalFormat("#,##0.00");

	public static String aplicarMascara(final String numero, final String mascara) {
		try {
			maskFormatter = new MaskFormatter(mascara);
			jFormattedTextField = new JFormattedTextField(maskFormatter);
		} catch (ParseException e) {
			e.printStackTrace();
			return numero;
		}
		jFormattedTextField.setText(numero);
		return jFormattedTextField.getText();
	}

	public static String aplicarMascara(Double moeda) {
		String stringMoeda = new String();
		stringMoeda = currency.getSymbol() + " " + formato.format(moeda);
		return stringMoeda;

	}
	
	public static String aplicarMascaraSemSimbolo(Double moeda) {
		String stringMoeda = new String();
		stringMoeda = formato.format(moeda);
		return stringMoeda;

	}

	public static Double retirarMascaraMoeda(String moeda) {
		return Double.parseDouble(moeda.replace(currency.getSymbol(), "").replaceAll("\\.", "").replaceAll(",", "."));
	}
}