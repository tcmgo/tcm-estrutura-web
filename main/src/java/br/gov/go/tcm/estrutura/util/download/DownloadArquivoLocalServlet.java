package br.gov.go.tcm.estrutura.util.download;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;
import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoFaces;
import br.gov.go.tcm.estrutura.entidade.transiente.Arquivo;

public class DownloadArquivoLocalServlet extends HttpServlet {

	private static final long serialVersionUID = -2016673965503264557L;

	public final static String ARQUIVO_REQ = "arquivo";

	@Override
	@SuppressWarnings( { "static-access" })
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		Arquivo arquivo = (Arquivo) request.getAttribute(DownloadArquivoLocalServlet.ARQUIVO_REQ);

		OutputStream out = response.getOutputStream();

		byte[] lidos = new byte[arquivo.getStream().available()];
		arquivo.getStream().read(lidos);

		Magic parser = new Magic();
		MagicMatch match = null;
		try {
			match = parser.getMagicMatch(lidos);
		} catch (MagicParseException e) {
			out.flush();
		} catch (MagicMatchNotFoundException e) {
			out.flush();
		} catch (MagicException e) {
			out.flush();
		}

		FileOutputMetaData fileOutputMetaData = new FileOutputMetaData();
		fileOutputMetaData.setExtensao(arquivo.getSufixo());

		if (arquivo.getNome() != null && (!arquivo.getNome().trim().equals("")))
			fileOutputMetaData.setNome(arquivo.getNome());
		else
			fileOutputMetaData.setNome(AjudanteContextoFaces.getIdentificacaoUsuario());

		if (match == null || match.getMimeType().equals("???"))
			fileOutputMetaData.setContentType("application/octet-stream");
		else
			fileOutputMetaData.setContentType(match.getMimeType());

		response.setContentType(fileOutputMetaData.getContentType());
		response.addHeader("Content-Disposition", "attachment;filename=\"" + fileOutputMetaData.getNome() + "."
				+ fileOutputMetaData.getExtensao() + "\"");
		response.setContentLength(lidos.length);

		AjudanteContextoFaces.getRequestMap().put(ConfigurarSaidaArquivoFiltro.FILE_OUTPUT_META_DATA,
				fileOutputMetaData);

		out.write(lidos, 0, lidos.length);
		out.flush();
		out.close();

		AjudanteContextoFaces.getFacesContext().getCurrentInstance().responseComplete();
	}

}
