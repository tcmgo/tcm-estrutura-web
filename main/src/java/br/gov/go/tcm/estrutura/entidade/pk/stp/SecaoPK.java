package br.gov.go.tcm.estrutura.entidade.pk.stp;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.gov.go.tcm.estrutura.entidade.pk.PK;

@Embeddable
public class SecaoPK implements PK {

	private static final long serialVersionUID = -1671067231595284839L;

	@Column(name = "CodSecao")
	private String codigo;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigoSecao) {
		this.codigo = codigoSecao;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		SecaoPK secaoPK = (SecaoPK) obj;
		return secaoPK.getCodigo().equals(this.getCodigo());
	}

	@Override
	public int hashCode() {
		assert false : "hashCode n�o definido";
		int hash = 5;
		hash = 35 * hash + (null == codigo ? 0 : codigo.hashCode());
		return hash;
	}

	@Override
	public String toString() {
		//N�o alterar formato : pacote.classe[atributo=valor]
		return this.getClass().getName() + "[codigo=" + this.getCodigo() + "]";
	}
}
