package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum GrupoAssuntosEnumeracao {
	
	LEI_ORCAMENTARIA_ANUAL("LEI ORCAMENTARIA ANUAL", "5A"),
	BALANCETE("BALANCETES MENSAIS", "5B"),
	BALANCO("BALANCO GERAL", "5C"),
	BALANCETE_QUADRIMESTRAL("BALANCETES QUADRIMESTRAIS", "BQ");
	
	private String descricao, codAssunto;

	GrupoAssuntosEnumeracao(String descricao, String codAssunto) {
		this.descricao = descricao;
		this.codAssunto = codAssunto;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public String getCodAssunto() {
		return codAssunto;
	}
	
	@Override
	public String toString() {
		return this.getCodAssunto();
	}
	
}
