package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum UnidadeFederacaoEnumeracao implements StringEnumeracaoPersistente {

	AC("AC"), AL("AL"), AM("AM"), AP("AP"), BA("BA"), CE("CE"), DF("DF"), ES("ES"), GO("GO"), MA("MA"), MG("MG"), MS("MS"), MT("MT"), 
	PA("PA"), PB("PB"), PE("PE"), PI("PI"), PR("PR"), RJ("RJ"), RN("RN"), RO("RO"), RR("RR"), RS("RS"), SC("SC"), SE("SE"), SP("SP"), TO("TO");
	
	String valor;

	UnidadeFederacaoEnumeracao(String valor) {
		this.valor = valor;
	}

	public String getValorOrdinal() {
		return this.valor;
	}

	public EnumeracaoPersistente valor() {
		if (valor.equals("AC")) {
			return UnidadeFederacaoEnumeracao.AC;
		} else if (valor.equals("AL")) {
			return UnidadeFederacaoEnumeracao.AL;
		} else if (valor.equals("AM")) {
			return UnidadeFederacaoEnumeracao.AM;
		} else if (valor.equals("AP")) {
			return UnidadeFederacaoEnumeracao.AP;
		} else if (valor.equals("BA")) {
			return UnidadeFederacaoEnumeracao.BA;
		} else if (valor.equals("CE")) {
			return UnidadeFederacaoEnumeracao.CE;
		} else if (valor.equals("DF")) {
			return UnidadeFederacaoEnumeracao.DF;
		} else if (valor.equals("ES")) {
			return UnidadeFederacaoEnumeracao.ES;
		} else if (valor.equals("GO")) {
			return UnidadeFederacaoEnumeracao.GO;
		} else if (valor.equals("MA")) {
			return UnidadeFederacaoEnumeracao.MA;
		} else if (valor.equals("MG")) {
			return UnidadeFederacaoEnumeracao.MG;
		} else if (valor.equals("MS")) {
			return UnidadeFederacaoEnumeracao.MS;
		} else if (valor.equals("MT")) {
			return UnidadeFederacaoEnumeracao.MT;
		} else if (valor.equals("PA")) {
			return UnidadeFederacaoEnumeracao.PA;
		} else if (valor.equals("PB")) {
			return UnidadeFederacaoEnumeracao.PB;
		} else if (valor.equals("PE")) {
			return UnidadeFederacaoEnumeracao.PE;
		} else if (valor.equals("PI")) {
			return UnidadeFederacaoEnumeracao.PI;
		} else if (valor.equals("PR")) {
			return UnidadeFederacaoEnumeracao.PR;
		} else if (valor.equals("RJ")) {
			return UnidadeFederacaoEnumeracao.RJ;
		} else if (valor.equals("RN")) {
			return UnidadeFederacaoEnumeracao.RN;
		} else if (valor.equals("RO")) {
			return UnidadeFederacaoEnumeracao.RO;
		} else if (valor.equals("RR")) {
			return UnidadeFederacaoEnumeracao.RR;
		} else if (valor.equals("RS")) {
			return UnidadeFederacaoEnumeracao.RS;
		} else if (valor.equals("SC")) {
			return UnidadeFederacaoEnumeracao.SC;
		} else if (valor.equals("SE")) {
			return UnidadeFederacaoEnumeracao.SE;
		} else if (valor.equals("SP")) {
			return UnidadeFederacaoEnumeracao.SP;
		} else if (valor.equals("TO")) {
			return UnidadeFederacaoEnumeracao.TO;
		}
		return null;
	}

	public Integer getCodigo() {
		if (valor.equals("AC")) {
			return 12;
		} else if (valor.equals("AL")) {
			return 27;
		} else if (valor.equals("AM")) {
			return 13;
		} else if (valor.equals("AP")) {
			return 16;
		} else if (valor.equals("BA")) {
			return 29;
		} else if (valor.equals("CE")) {
			return 23;
		} else if (valor.equals("DF")) {
			return 53;
		} else if (valor.equals("ES")) {
			return 32;
		} else if (valor.equals("GO")) {
			return 52;
		} else if (valor.equals("MA")) {
			return 21;
		} else if (valor.equals("MG")) {
			return 31;
		} else if (valor.equals("MS")) {
			return 50;
		} else if (valor.equals("MT")) {
			return 51;
		} else if (valor.equals("PA")) {
			return 15;
		} else if (valor.equals("PB")) {
			return 25;
		} else if (valor.equals("PE")) {
			return 26;
		} else if (valor.equals("PI")) {
			return 22;
		} else if (valor.equals("PR")) {
			return 41;
		} else if (valor.equals("RJ")) {
			return 33;
		} else if (valor.equals("RN")) {
			return 24;
		} else if (valor.equals("RO")) {
			return 11;
		} else if (valor.equals("RR")) {
			return 14;
		} else if (valor.equals("RS")) {
			return 43;
		} else if (valor.equals("SC")) {
			return 42;
		} else if (valor.equals("SE")) {
			return 28;
		} else if (valor.equals("SP")) {
			return 35;
		} else if (valor.equals("TO")) {
			return 17;
		}
		return null;
	}

	public String toString() {
		if (valor.equals("AC")) {
			return "AC";
		} else if (valor.equals("AL")) {
			return "AL";
		} else if (valor.equals("AM")) {
			return "AM";
		} else if (valor.equals("AP")) {
			return "AP";
		} else if (valor.equals("BA")) {
			return "BA";
		} else if (valor.equals("CE")) {
			return "CE";
		} else if (valor.equals("DF")) {
			return "DF";
		} else if (valor.equals("ES")) {
			return "ES";
		} else if (valor.equals("GO")) {
			return "GO";
		} else if (valor.equals("MA")) {
			return "MA";
		} else if (valor.equals("MG")) {
			return "MG";
		} else if (valor.equals("MS")) {
			return "MS";
		} else if (valor.equals("MT")) {
			return "MT";
		} else if (valor.equals("PA")) {
			return "PA";
		} else if (valor.equals("PB")) {
			return "PB";
		} else if (valor.equals("PE")) {
			return "PE";
		} else if (valor.equals("PI")) {
			return "PI";
		} else if (valor.equals("PR")) {
			return "PR";
		} else if (valor.equals("RJ")) {
			return "RJ";
		} else if (valor.equals("RN")) {
			return "RN";
		} else if (valor.equals("RO")) {
			return "RO";
		} else if (valor.equals("RR")) {
			return "RR";
		} else if (valor.equals("RS")) {
			return "RS";
		} else if (valor.equals("SC")) {
			return "SC";
		} else if (valor.equals("SE")) {
			return "SE";
		} else if (valor.equals("SP")) {
			return "SP";
		} else if (valor.equals("TO")) {
			return "TO";
		}
		return "";
	}

}
