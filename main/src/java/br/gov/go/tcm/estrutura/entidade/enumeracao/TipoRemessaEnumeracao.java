package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoRemessaEnumeracao implements NumberEnumeracaoPersistente {

	PPA_LOA(1), BALANCETE(2),PESSOAL(3),BALANCO(4),PPA(5),LOA(6),BALANCETE_EMPRESA_ESTATAL(7),HOMOLOGACAO_CONCURSO_PUBLICO(8);
	
	private final int valor;

	TipoRemessaEnumeracao(int valor) {
		this.valor = valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoRemessaEnumeracao.PPA_LOA;
		case 2:
			return TipoRemessaEnumeracao.BALANCETE;
		case 3:
			return TipoRemessaEnumeracao.PESSOAL;
		case 4:
			return TipoRemessaEnumeracao.BALANCO;	
		case 5:
			return TipoRemessaEnumeracao.PPA;
		case 6:
			return TipoRemessaEnumeracao.LOA;
		case 7:
			return TipoRemessaEnumeracao.BALANCETE_EMPRESA_ESTATAL;
		case 8:
			return TipoRemessaEnumeracao.HOMOLOGACAO_CONCURSO_PUBLICO;
		}
		return null;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "PPA/LDO/LOA";
		case 2:
			return "Cont�bil";
		case 3:
			return "Pessoal";
		case 4:
			return "Balan�o";	
		case 5:
			return "PPA";
		case 6:
			return "LOA";
		case 7:
			return "Cont�bil Empresa Estatal";
		case 8:
			return "Homologa��o em Concurso P�blico";
		default:
			return "";
		}
	}
}