package br.gov.go.tcm.estrutura.persistencia.historico.implementacao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.historico.contrato.HistoricoStpDAO;

/**
 * Classe DAO que define opera��es para a entidade {@link br.gov.go.tcm.estrutura.entidade.web.HistoricoWeb}
 */
@Repository
public class HistoricoStpDAOImplementacao extends GenericoDAOImplementacao implements HistoricoStpDAO {

	private static final long serialVersionUID = 3369619654975838133L;

	@Autowired
	public HistoricoStpDAOImplementacao(@Qualifier("sessionFactoryStp") SessionFactory factory,
			@Qualifier("hibernateTemplateStp") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}
}
