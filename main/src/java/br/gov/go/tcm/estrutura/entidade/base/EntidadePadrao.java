package br.gov.go.tcm.estrutura.entidade.base;

import java.util.List;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteReflexao;

public abstract class EntidadePadrao implements Entidade {

	private static final long serialVersionUID = 1L;

	public abstract Long getId();

	public abstract void setId(Long id);

	@Override
	public String toString() {
		String descObjeto = "[";
		AjudanteReflexao assistenteReflexao = new AjudanteReflexao();
		List<String> propriedades = assistenteReflexao.obterPropriedades(this.getClass());

		for (String propriedade : propriedades) {
			Object valor;
			// FIXME: implementar exce��o;
//			try {
				valor = assistenteReflexao.obterValorPropriedade(this, propriedade);
//			} catch (ReflexaoExcecao e) {
//				e.printStackTrace();
//				return "";
//			}

			if (valor != null && List.class.isAssignableFrom(valor.getClass())) {
				continue;
			}

			descObjeto += propriedade + "=" + ((valor != null) ? valor.toString().trim() : "") + " ,";
		}
		if (descObjeto.length() > 1)
			descObjeto = descObjeto.substring(0, descObjeto.length() - 1);
		descObjeto += "]";
		return descObjeto;
	}
}
