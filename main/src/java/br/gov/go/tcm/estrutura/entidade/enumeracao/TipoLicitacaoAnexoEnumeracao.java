package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoLicitacaoAnexoEnumeracao implements NumberEnumeracaoPersistente {

	COMPLEMENTO(1), RETIFICACAO(2);

	private final int valor;

	TipoLicitacaoAnexoEnumeracao(int valor) {
		this.valor = valor;
	}

	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoLicitacaoAnexoEnumeracao.COMPLEMENTO;
		case 2:
			return TipoLicitacaoAnexoEnumeracao.RETIFICACAO;
		}
		return null;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Complemento";
		case 2:
			return "Retificação";
		default:
			return "";
		}
	}
}