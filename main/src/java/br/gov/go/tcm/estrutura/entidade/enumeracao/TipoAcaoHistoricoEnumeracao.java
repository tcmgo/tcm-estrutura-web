package br.gov.go.tcm.estrutura.entidade.enumeracao;


/**
 * Enum referente �s a��es registradas na tabela HistoricoWeb
 * @author paulo_hc
 */
public enum TipoAcaoHistoricoEnumeracao implements NumberEnumeracaoPersistente {
	INCLUSAO(1), ALTERACAO(2), EXCLUSAO(3);

	private final int valor;

	TipoAcaoHistoricoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "INCLUSAO";
		case 2:
			return "ALTERACAO";
		case 3:
			return "EXCLUSAO";
		default:
			return "";
		}
	}
}