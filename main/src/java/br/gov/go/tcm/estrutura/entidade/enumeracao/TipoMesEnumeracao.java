package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoMesEnumeracao implements NumberEnumeracaoPersistente {

	NENHUM(0), JANEIRO(1), FEVEREIRO(2), MARCO(3), ABRIL(4), MAIO(5), JUNHO(6), JULHO(
			7), AGOSTO(8), SETEMBRO(9), OUTUBRO(10), NOVEMBRO(11), DEZEMBRO(12), BALANCO(
			13), ORCAMENTO(14), BALANCENTE_COMPLEMENTAR(15);

	int valor;

	private TipoMesEnumeracao(int valor) {
		this.valor = valor;
	}

	@Override
	public Number getValorOrdinal() {
		return valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "";
		case 1:
			return "Janeiro";
		case 2:
			return "Fevereiro";
		case 3:
			return "Mar�o";
		case 4:
			return "Abril";
		case 5:
			return "Maio";
		case 6:
			return "Junho";
		case 7:
			return "Julho";
		case 8:
			return "Agosto";
		case 9:
			return "Setembro";
		case 10:
			return "Outubro";
		case 11:
			return "Novembro";
		case 12:
			return "Dezembro";
		case 13:
			return "Dezembro";
		case 14:
			return "";
		case 15:
			return "";
		default:
			return "";
		}
	}

}
