package br.gov.go.tcm.estrutura.modelo.fonteDeDados;

import java.util.ArrayList;
import java.util.List;

import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoFaces;
import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.ParametroVisao;


public class AjudanteParametrosModelo {

	public final static String LOGO_ID = "logotipoTCM";
	public final static String DESCRICAO_ESTADO_GOIAS = "descricaoEstadoGoias";
	public final static String DESCRICAO_TRIBUNAL_CONTAS_MUNICIPIO = "descricaoTribunalContasMunicipio";
	public final static String ZONA_TEMPO_PADRAO = "zonaTempoPadrao";

	private List<ParametroVisao> parametrosModelo = new ArrayList<ParametroVisao>();

	public List<ParametroVisao> getParametrosModelo() {
		return parametrosModelo;
	}

	public void adicionarParametroRelatorio(String id, Object valor) {
		parametrosModelo.add(new ParametroVisao(id, valor));
	}

	public List<ParametroVisao> obterListaParametrosModelo() {

		String logomarca = AjudanteContextoFaces.getContextoWeb()+ "/imagens/tcm_logomarca_titulo.jpg";
		adicionarParametroRelatorio(LOGO_ID, logomarca);
		adicionarParametroRelatorio(DESCRICAO_ESTADO_GOIAS, "ESTADO DE GOI�S");
		adicionarParametroRelatorio(DESCRICAO_TRIBUNAL_CONTAS_MUNICIPIO,
				"TRIBUNAL DE CONTAS DOS MUNIC�PIOS");
		adicionarParametroRelatorio(ZONA_TEMPO_PADRAO, ContextoSpring
				.getZonaTempoPadrao());
		return parametrosModelo;
	}
}
