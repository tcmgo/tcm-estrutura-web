package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TelefoneFaxEnumeracao implements NumberEnumeracaoPersistente {

	TELEFONE(1),FAX(2);

	private final int valor;

	TelefoneFaxEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {
		switch (valor) {
		
		case 1:
			return "Telefone";
		case 2:
			return "Fax";
		default:
			return "";
		}
	}

	
}
