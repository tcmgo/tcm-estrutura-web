package br.gov.go.tcm.estrutura.entidade.enumeracao;



public enum ElementoDespesaEnumeracao implements StringEnumeracaoPersistente {

	NENHUM("00"),APOSENTADORIAS_E_REFORMAS("01"), PENSOES("03"),
	CONTRATACAO_POR_TEMPO_DETERMINADO("04"),
	OUTROS_BENEFICIOS_PREVIDENCIARIOS("05"),
	BENEFICIO_MENSAL_AO_DEFICIENTE_E_AO_IDOSO("06"),
	CONTRIBUICAO_A_ENTIDADES_FECHADAS_DE_PREVIDENCIA("07"),
	OUTROS_BENEFICIOS_ASSISTENCIAIS("08"),
	SALARIO_FAMILIA("09"),
	OUTROS_BENEFICIOS_DE_NATUREZA_SOCIAL("10"),
	VENCIMENTOS_E_VANTAGENS_FIXAS_CIVIL("11"),
	VENCIMENTOS_E_VANTAGENS_FIXAS_MILITAR("12"),
	OBRIGACOES_PATRONAIS("13"),DIARIAS_CIVIL("14"),DIARIAS_MILITAR("15"),DESPESAS_VARIAVEIS_CIVIL("16"),
	DESPESAS_VARIAVEIS_MILITAR("17"),AUXILIO_FINANCEIRO_ESTUDANTES("18"),AUXILIO_FARDAMENTO("19"),
	AUXILIO_FINANCEIRO_PESQUISADORES("20"),JUROS_SOBRE_A_DIVIDA_POR_CONTRATO("21"),
	OUTROS_ENCARGOS_SOBRE_A_DIVIDA_POR_CONTRATO("22"),JUROS_DESAGIOS_E_DESCONTOS_DIVIDA_MOBILIARIA("23"),
	OUTROS_ENCARGOS_SOBRE_DIVIDA_MOBILIARIA("24"),ENCARGOS_OPERACOES_CREDITO_POR_ANTECIPACAO_RECEITA("25"),
	OBRIGACOES_DECORRENTES_POLITICA_MONETARIA("26"),ENCARGOS_PELA_HONRA_DE_AVAIS_GARANTIAS_SEGUROS("27"),
	REMUNERACAO_COTAS_FUNDOS_AUTARQUICOS("28"),MATERIAL_CONSUMO("30"),PREMIACOES_CULTURAIS_ARISTICAS("31"),
	MATERIAL_DISTRIBUICAO_GRATUITA("32"),
	PASSAGENS_DESPESAS_LOCOMOCAO("33"),
	OUTRAS_DESPESAS_PESSOAL_CONTRATOS_TERCEIRIZACAO("34"),
	SERVICOS_CONSULTORIA("35"),
	OUTROS_SERVICOS_TERCEIROS_PESSOA_FISICA("36"),
	LOCACAO_MAO_DE_OBRA("37"),ARRENDAMENTO_MERCANTIL("38"),
	OUTROS_SERVICOS_TERCEIROS_PESSOA_JURIDICA("39"),CONTRUIBUICOES("41"),
	AUXILIOS("42"),SUBVENCOES_SOCIAIS("43"),EQUALIZACAO_PRECOS_TAXAS("45"),
	AUXILIO_ALIMENTACAO("46"),OBRIGACOES_TRIBUTARIAS_CONTRIBUTIVAS("47"),
	OUTROS_AUXILIOS_FINANCEIROS_PESSOA_FISICAS("48"),AUXILIO_TRANSPORTE("49"),
	OBRAS_E_INSTALACOES("51"),EQUIPAMENTOS_E_MATERIAL_PERMANENTE("52"),
	APOSENTADORIAS_RGPS_AREA_RURAL("53"), 
	APOSENTADORIAS_RGPS_AREA_URBANA("54"),
	PENSOES_RGPS_AREA_RURAL("55"), 
	PENSOES_RGPS_AREA_URBANA("56"),
	OUTROS_BENEFICIOS_RGPS_AREA_RURAL("57"), 
	OUTROS_BENEFICIOS_RGPS_AREA_URBANA("58"),
	AQUISICAO_DE_IMOVEIS("61"),AQUISICAO_DE_PRODUTOS_PARA_REVENDA("62"),
	AQUISICAO_DE_TITULOS_DE_CREDITO("63"),AQUISICAO_DE_TITULOS_REPRESENTATIVOS_DE_CAPITAL("64"),
	CONSTITUICAO_OU_AUMENTO_DE_CAPITAL_CAPITAL_EMPRESAS("65"),
	CONCESSAO_DE_EMPRESTIMOS_E_FINANCIMENTOS("66"),
	DEPOSITOS_COMPULSORIOS("67"),PRINCIPAL_DIVIDA_CONTRATUAL_RESGATADO("71"),
	PRINCIPAL_DIVIDA_MOBILIARIA_RESGATADO("72"),
	CORRECAO_MONETARIA_CAMBIAL_DIVIDA_CONTRATUAL_RESGATADA("73"),
	CORRECAO_MONETARIA_CAMBIAL_DIVIDA_MOBILIARIA_RESGATADA("74"),
	CORRECAO_MONETARIA_DIVIDA_OPERACOES_CREDITO("75"),
	PRINCIPAL_CORRIGIDO_DIVIDA_MOBILIARIA_REFINANCIADO("76"),
	PRINCIPAL_CORRIGIDO_DIVIDA_CONTRATUAL_REFINANCIADO("77"),
	DISTRIBUICAO_RECEITAS("81"),
	SENTENCAS_JUDICIAIS("91"),
	DESPESAS_DE_EXERCICIOS_ANTERIORES("92"),
	INDENIZACAO_E_RESTITUICOES("93"),
	INDENIZACAO_E_RESTITUICOES_TRABALHISTAS("94"),
	INDENIZACAO_PELA_EXECUCAO_TRABALHO_CAMPO("95"),
	RESSARCIMENTO_DESPESAS_PESSOAL("96"),
	RESERVA_CONTINGENCIA("99");

	String valor;
	
	ElementoDespesaEnumeracao(String valor){
		this.valor = valor;
	}
	
	public String getValorOrdinal(){
		return this.valor;
	}
	
	@Override
	public String toString(){
		int id = Integer.parseInt( valor );
		
		switch( id ){
			case 0:
				return "Selecione um item";
			case 1:
				return "1 - Aposentadorias e Reformas";
			case 3:
				return "3 - Pens�es";
			case 4:
				return "4 - Contrata��o por Tempo Determinado";
			case 5:
				return "5 - Outros Benef�cios Previdenci�rios";
			case 6:
				return "6 - Benef�cio Mensal ao Deficiente e ao Idoso";
			case 7:
				return "7 - Contribui��o a Entidades Fechadas de Previd�ncia";
			case 8:
				return "8 - Outros Benef�cios Assistencias";
			case 9:
				return "9 - Sal�rio Fam�lia";
			case 10:
				return "10 - Outros Benef�cios de Natureza Social";
			case 11:
				return "11 - Vencimentos e Vantagens Fixas C�vil";
			case 12:
				return "12 - Vencimentos e Vantagens Fixas Militar";
			case 13:
				return "13 - Obriga��es Patronais";
			case 14:
				return "14 - Di�rias Civis";
			case 15:
				return "15 - Di�rias Militares";
			case 16:
				return "16 - Despesas Vari�veis Civis";
			case 17:
				return "17 - Despesas Vari�veis Militares";
			case 18:
				return "18 - Auxilio Financeiro a Estudantes";
			case 19:
				return "19 - Auxilio Fardamento";
			case 20:
				return "20 - Auxilio Financeiro Pesquisadores";
			case 21:
				return "21 - Juros Sobre a Divida por Contrato";
			case 22:
				return "22 - Outros Encargos Sobre a Divida por Contrato"; 
			case 23:
				return "23 - Juros Des�gios e Descontos Divida Mobiliaria";
			case 24:
				return "24 - Outros Encargos Sobre a Divida Mobiliaria";
			case 25:
				return "25 - Encargos Opera��es Credito por Antecipa��o de Receita"; 
			case 26:
				return "26 - Obriga��es Decorrentes Pol�tica Monet�ria";
			case 27:
				return "27 - Encargos pela Honra de Avais e Garantias de Seguros";
			case 28:
				return "28 - Remunera��o Cotas Fundos Autarquicos";
			case 30:
				return "30 - Material de Consumo";
			case 31:
				return "31 - Premia��es Culturais Aristicas";
			case 32:
				return "32 - Material Distribuia��o Gratuita";
			case 33:
				return "33 - Passagens Despesas Locomo��o";
			case 34:
				return "34 - Outras Despesas Pessoal para Contratos Terceiriza��o";
			case 35:
				return "35 - Servi�os Consultoria";
			case 36:
				return "36 - Outros Servi�os Terceiros Pessoa Fis�ca";
			case 37:
				return "37 - Loca��o de M�o de Obra";
			case 38:
				return "38 - Arrendamento Mercantil";
			case 39:
				return "39 - Outros Servi�os Terceiros Pessoa Jur�dica";
			case 41:
				return "41 - Contribui��es";
			case 42:
				return "42 - Aux�lios";
			case 43:
				return "43 - Subven��es Sociais";
			case 45:
				return "45 - Equaliza��o Pre�os e Taxas";
			case 46:
				return "46 - Aux�lio Alimenta��o";
			case 47:
				return "47 - Obriga��es Tribut�rias";
			case 48:
				return "48 - Outros Auxilios Financeiros e Pessoas F�sicas";
			case 49:
				return "49 - Aux�lio Transporte";
			case 51:
				return "51 - Obras e Instala��es";
			case 52:
				return "52 - Equipamentos e Material Permanente";
			case 61:
				return "61 - Aquisi��o de Im�veis";
			case 62:
				return "62 - Aquisi��o de Produtos para Revenda";
			case 63:
				return "63 - Aquisi��o de T�tulos de Cr�dito";
			case 64:
				return "64 - Aquisi��o de T�tulos Representativos de Capital";
			case 65:
				return "65 - Constitui��o ou Aumento de Capital de Empresas";
			case 66:
				return "66 - Concess�o de Emprestimos e Financiamentos";
			case 67:
				return "67 - Depositos Compuls�rios";
			case 71:
				return "71 - Principal Divida";
			case 72:
				return "72 - Principal Divida Mobil�aria Resgatado";
			case 73:
				return "73 - Corre��o Monet�ria Cambial D�vida Contratual Resgatada";
			case 74:
				return "74 - Corre��o Monet�ria Cambial D�vida Mobiliaria Resgatada";
			case 75:
				return "75 - Corre��o Monet�ria D�vida Opera��es de Cr�dito";
			case 76:
				return "76 - Principal Corrigido D�vida Mobiliaria Refinanciado";
			case 77:
				return "77 - Principal Corrigido D�vida Contratual Refinanciado";
			case 81:
				return "81 - Distribui��o Receitas";
			case 91:
				return "91 - Senten�as Judiciais";
			case 92:
				return "92 - Despesas de Exerc�cios Anteriores";
			case 93:
				return "93 - Indeniza��o e Restitui��es";
			case 94:
				return "94 - Indeniza��o e Restitui��es Trabalhistas";
			case 95:
				return "95 - Indeniza��o pela Execu��o De Trabalho no Campo";
			case 96:
				return "96 - Ressarciemnto Despesas Pessoal";
			case 99:
				return "99 - Reserva Conting�ncia";
			default:
				return "";
		}
	}
	
}
