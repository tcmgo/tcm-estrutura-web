package br.gov.go.tcm.estrutura.entidade.ajudante;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.MaskFormatter;

public class AjudanteTexto {
	
	public static final String ESPACO = " ";
	public static final String PONTO = ".";
	public static final String VIRGULA = ",";
	public static final String TEXTO_VAZIO = "";
	public static final String SUBLINHA = "_";
	public static final String TRACO = "-";
	public static String[] LETRAS_SEM_CARACTER_ESPECIAL = { "a", "e", "i", "o", "u", "c", "e" };
	public static Pattern[] LETRAS_COM_CARACTER_ESPECIAL = null;
	

	public static String formatarTextoTamanhoMaximo(String palavra,Integer tamanho){		
		if (palavra.length()>tamanho){
			palavra = palavra.substring(0,tamanho-1);
			palavra = palavra + "...";
		}
		return palavra;
	}
	
	public static String preencherTextoComZeros(String palavra,Integer tamanho){		
		int delta = tamanho - palavra.length();
		
		if(delta > 0){
			for(int i = 0 ; i < delta ; ++i)
				palavra = "0" + palavra;
		}
		return palavra;
	}
	
	public static String preencherTextoComZeros(Number numero,Integer tamanho){		
		String palavra = numero.toString();
		
		int delta = tamanho - palavra.length();
		
		if(delta > 0){
			for(int i = 0 ; i < delta ; ++i)
				palavra = "0" + palavra;
		}
		return palavra;
	}
	
	public static String primeiraLetraMaiuscula(String palavra){
		String primeiraLetra = palavra.substring(0,1);
        String restante   = palavra.substring(1);
        return primeiraLetra.toUpperCase() + restante;
	}
	
	public static String primeiraLetraMinuscula(String palavra){
		String primeiraLetra = palavra.substring(0,1);
        String restante   = palavra.substring(1);
        return primeiraLetra.toLowerCase() + restante;
	}
	
	public static String primeiraLetraMaiusculaRestoMinusculo(String palavra){
		String primeiraLetra = palavra.substring(0,1);
        String restante   = palavra.substring(1);
        return primeiraLetra.toUpperCase() + restante.toLowerCase();
	}

	public static String primeiraLetraMinisculo(String palavra) {
		String primeiraLetra = palavra.substring(0,1);
        String restante   = palavra.substring(1);
        return primeiraLetra.toLowerCase() + restante;
	}
	
	public static String obterPrimeiroEUltimoNome(String nome){
		String partes[] = nome.split(" ");
		return primeiraLetraMaiusculaRestoMinusculo(partes[0]) + " " + primeiraLetraMaiusculaRestoMinusculo(partes[partes.length - 1]);
	}
	
	public static String substituiCaracterEspecial(String texto) {
		if (LETRAS_COM_CARACTER_ESPECIAL == null) {
			LETRAS_COM_CARACTER_ESPECIAL = new Pattern[LETRAS_SEM_CARACTER_ESPECIAL.length];
			LETRAS_COM_CARACTER_ESPECIAL[0] = Pattern.compile("[����������]", Pattern.CASE_INSENSITIVE);
			LETRAS_COM_CARACTER_ESPECIAL[1] = Pattern.compile("[����]", Pattern.CASE_INSENSITIVE);
			LETRAS_COM_CARACTER_ESPECIAL[2] = Pattern.compile("[��������]", Pattern.CASE_INSENSITIVE);
			LETRAS_COM_CARACTER_ESPECIAL[3] = Pattern.compile("[�����]", Pattern.CASE_INSENSITIVE);
			LETRAS_COM_CARACTER_ESPECIAL[4] = Pattern.compile("[����]", Pattern.CASE_INSENSITIVE);
			LETRAS_COM_CARACTER_ESPECIAL[5] = Pattern.compile("[�]", Pattern.CASE_INSENSITIVE);
			LETRAS_COM_CARACTER_ESPECIAL[6] = Pattern.compile("[����]", Pattern.CASE_INSENSITIVE);
		}

		String resultado = texto;
		for (int i = 0; i < LETRAS_COM_CARACTER_ESPECIAL.length; i++) {
			Matcher matcher = LETRAS_COM_CARACTER_ESPECIAL[i].matcher(resultado);
			resultado = matcher.replaceAll(LETRAS_SEM_CARACTER_ESPECIAL[i]);
		}
		return resultado;
	}
	
	public static String formataTextoParaUrlAmigavel(String texto) {
		  
		texto = texto.replaceAll("[-+=*&%$#@!���;:._]", "");
		texto = texto.replaceAll("['\"]", "");
		texto = texto.replaceAll("[<>()\\{\\}]", "");
		texto = texto.replaceAll("['\\\\.,()|/]", "");
		texto = texto.replaceAll("[^!-�]{1}[^ -�]{0,}[^!-�]{1}|[^!-�]{1}", " ");
        
        /** Troca os espa�os no in�cio por "" **/  
        texto = texto.replaceAll("^\\s+", "");  
        /** Troca os espa�os no in�cio por "" **/  
        texto = texto.replaceAll("\\s+$", "");  
        /** Troca os espa�os duplicados, tabula��es e etc por  " " **/  
        texto = texto.replaceAll("\\s+", " ");  
        
		return AjudanteTexto.substituiCaracterEspecial(texto).toLowerCase().replaceAll(" ", "-");		
	}
	
	public static String retirarAspasSimlesEDuplas(String texto){
		if( texto != null ){
			texto = texto.replace("\'", "").replace("\"", ""); 
		}
		return texto;
	}
	
	public static String formatarPorExtensoOrdinal(Integer numero) {
    	
    	String numeroOrdinalExtenso = "";
    	
    	if (numero >= 30) {
    		numeroOrdinalExtenso = "trig�simo ";
			numero -= 30;
    	}else if (numero >= 20) {
    		numeroOrdinalExtenso = "vig�simo ";
			numero -= 20;
    	} else if (numero >= 10) {
    		numeroOrdinalExtenso = "d�cimo ";
			numero -= 10;
    	}
		
    	switch (numero){
	    	case 1: numeroOrdinalExtenso += "primeiro"; break;
	    	case 2: numeroOrdinalExtenso += "segundo"; break;
	    	case 3: numeroOrdinalExtenso += "terceiro"; break;
	    	case 4: numeroOrdinalExtenso += "quarto"; break;
	    	case 5: numeroOrdinalExtenso += "quinto"; break;
	    	case 6: numeroOrdinalExtenso += "sexto"; break;
	    	case 7: numeroOrdinalExtenso += "s�timo"; break;
	    	case 8: numeroOrdinalExtenso += "oitavo"; break;
	    	case 9: numeroOrdinalExtenso += "nono"; break;	    	
    	}
    	
    	return numeroOrdinalExtenso;
    }
    
public static String formatarPorExtensoCardinal(Integer numero) {
    	
    	String numeroCardinalExtenso = "";
    	
    	if (numero == 100) {
    		numeroCardinalExtenso = "cem";
			numero -= 100;
    	} else if (numero >= 90) {
    		numeroCardinalExtenso = "noventa";
			numero -= 90;
    	} else if (numero >= 80) {
    		numeroCardinalExtenso = "oitenta";
			numero -= 80;
    	} else if (numero >= 70) {
    		numeroCardinalExtenso = "setenta";
			numero -= 70;
    	} else if (numero >= 60) {
    		numeroCardinalExtenso = "sessenta";
			numero -= 60;
    	} else if (numero >= 50) {
    		numeroCardinalExtenso = "cinquenta";
			numero -= 50;
    	} else if (numero >= 40) {
    		numeroCardinalExtenso = "quarenta";
			numero -= 40;
    	} else if (numero >= 30) {
    		numeroCardinalExtenso = "trinta";
			numero -= 30;
    	}else if (numero >= 20) {
    		numeroCardinalExtenso = "vinte";
			numero -= 20;
    	} 
    	
    	if (numero > 0 && numero < 10 && !numeroCardinalExtenso.equals("")) {		
    		numeroCardinalExtenso += " e ";
    	}
    		
    	switch (numero){
	    	case 1: numeroCardinalExtenso += "um"; break;
	    	case 2: numeroCardinalExtenso += "dois"; break;
	    	case 3: numeroCardinalExtenso += "tr�s"; break;
	    	case 4: numeroCardinalExtenso += "quatro"; break;
	    	case 5: numeroCardinalExtenso += "cinco"; break;
	    	case 6: numeroCardinalExtenso += "seis"; break;
	    	case 7: numeroCardinalExtenso += "sete"; break;
	    	case 8: numeroCardinalExtenso += "oito"; break;
	    	case 9: numeroCardinalExtenso += "nove"; break;
	    	case 10: numeroCardinalExtenso += "dez"; break;
	    	case 11: numeroCardinalExtenso += "onze"; break;
	    	case 12: numeroCardinalExtenso += "doze"; break;
	    	case 13: numeroCardinalExtenso += "treze"; break;
	    	case 14: numeroCardinalExtenso += "catorze"; break;
	    	case 15: numeroCardinalExtenso += "quinze"; break;
	    	case 16: numeroCardinalExtenso += "dezesseis"; break;
	    	case 17: numeroCardinalExtenso += "dezessete"; break;
	    	case 18: numeroCardinalExtenso += "dezoito"; break;
	    	case 19: numeroCardinalExtenso += "dezenove"; break;	    	
    	}
    	
    	
    	return numeroCardinalExtenso;
    }
    
    public static String formatarFracaoParaCardinalExtenso(Double numero) {

    	if (numero == 0) 
    		return "zero";
    	
    	String numeroFormatado;
    	
    	String[] arrNumero = numero.toString().split("[.]");
    	
    	numeroFormatado = formatarPorExtensoCardinal(Integer.parseInt(arrNumero[0]));
    	
    	if (arrNumero.length > 1 && !arrNumero[1].equals("0")) {
    		
    		if (Integer.parseInt(arrNumero[1]) > 0)
    			numeroFormatado += " v�rgula ";
    		
    		if (Integer.parseInt(arrNumero[1]) < 10 && arrNumero[1].length() > 1) 
    			numeroFormatado += " zero ";
    		
    		numeroFormatado += formatarPorExtensoCardinal(Integer.parseInt(arrNumero[1]));    		
    	}
    	
    	return numeroFormatado;
    }
    
    public static String formatarMoedaPorExtenso(Double valor) {
    	
    	MoedaExtenso ex = new MoedaExtenso(valor, 30);
    	
    	return ex.getResult();
    }
    
    /**Character   Description
     *  #           Qualquer numero valido.
     *
     *  '           Usado para n�o usar nenhum caracter especial na formatacao ("\n", "\t"....)
     *
     *  U           Qualquer caracter
     *  Todas as letras minusculas sao passadas para maiuscula.
     *
     *  L           Qualquer caracter
     * Todas as letras maiusculas sao passadas para minusculas
     *
     *  A          Qualquer caracter ou numero
     *  ( Character.isLetter or Character.isDigit )
     *
     *  ?           Qualquer caracter ( Character.isLetter ).
     *
     *  *           Qualquer Coisa.
     *
     *  H           Qualquer caracter hexa (0-9, a-f ou A-F).
     *
     * ====================================
     * ex:
     * valor = "A1234B567Z"
     * mascara = "A-AAAA-AAAA-A"
     * retorno : A-1234-B567-Z
     *
     * ===================================
     * @param string
     * @param mascara
     * @return
     * @throws java.text.ParseException
     */
	public static String mascararString(String string, String mascara)
			throws java.text.ParseException {
		MaskFormatter mf = new MaskFormatter(mascara);
		mf.setValueContainsLiteralCharacters(false);
		return mf.valueToString(string);
	}
	
	public static String formatarCampoPesquisa(String palavra)
    {
        palavra = palavra.trim();
        palavra = palavra.replace("'", "");
        palavra = palavra.replace("AND", "");
        palavra = palavra.replace("OR", "");
        palavra = palavra.trim();
        palavra = palavra.replaceAll("[ ]+", " ");
        if(palavra.startsWith("\"") && palavra.endsWith("\""))
        {
            palavra = palavra.trim();
        } else
        {
            palavra = palavra.trim();
            palavra = palavra.replaceAll("\"", "");
            palavra = palavra.trim();
            palavra = palavra.replaceAll("[ ]+", " OR ");
            palavra = palavra.trim();
        }
        return palavra;
    }
	
	public static String formatarProcessoAno( Integer processo, Integer ano ){
		if( (processo == null || processo.intValue() == 0) || ( ano == null || ano.intValue() == 0) ){
			return "-";
		}
		
		return processo + "/" + ano;
	}
	
	public static String[] split(String input, String delimitador) {
		String[] array;
		
		if (input == null)
			input = "";
		
		if (input.length() == 0) {
			array = new String[1];
			array[0] = "";
			return array;
		}

		if (delimitador == null)
			delimitador = "";

		StringTokenizer tok = new StringTokenizer(input, delimitador);
		int count = tok.countTokens();
		array = new String[count];
		int i = 0;
		
		while (tok.hasMoreTokens()) {
			array[i++] = tok.nextToken();
		}
		
		return array;
	}
	
}
