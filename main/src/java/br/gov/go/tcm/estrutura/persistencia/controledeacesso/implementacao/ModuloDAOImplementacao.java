package br.gov.go.tcm.estrutura.persistencia.controledeacesso.implementacao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.ModuloDAO;

@Repository("ModuloDAO")
public class ModuloDAOImplementacao extends GenericoDAOImplementacao implements ModuloDAO {

	private static final long serialVersionUID = -5961823245019997581L;

	@Autowired
	public ModuloDAOImplementacao(@Qualifier("sessionFactory") SessionFactory factory,
			@Qualifier("hibernateTemplate") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Modulo> selecionarTodosModulosOrdenadosPorDescricao() {
		return getHibernateTemplate(getSessionFactoryWeb()).findByNamedQuery("selecionarTodosModulosOrdenadosPorDescricao");
	}

	@SuppressWarnings("unchecked")
	public List<Modulo> obterModuloPorDescricao(String descricao) {
		String parametro = "descricao";
		Object objeto = descricao;
		return getHibernateTemplate(getSessionFactoryWeb()).findByNamedQueryAndNamedParam("selecionarModuloPorDescricao", parametro, objeto);
	}
	
	public Modulo selecionarModuloDoRecursoSolicitado(Long id, String recursoSolicitado) {
		/*
		 * final String sql = "select id,loginUrl from Modulo where PATINDEX(padraoUrl + '%','" + recursoSolicitado +
		 * "') <> 0 and loginUrl <> '" + recursoSolicitado + "'";
		 */
		final String sql = "select id,loginUrl from Modulo where id = " + id + " and loginUrl <> '" + recursoSolicitado
				+ "'";

		@SuppressWarnings({ "unchecked", "rawtypes" })
		Modulo modulo = (Modulo) getHibernateTemplate(getSessionFactoryWeb()).execute(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				Query query = session.createSQLQuery(sql);
				Object[] objeto = (Object[]) query.uniqueResult();

				Modulo modulo = null;
				if (objeto != null) {
					modulo = new Modulo();
					modulo.setId(new Long(String.valueOf(objeto[0])));
					// (Long) AjudanteConversao
					// .conveter(, TipoConversao.Long));
					modulo.setLoginUrl((String) objeto[1]);
				}
				return modulo;
			}
		});
		return modulo;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Modulo selecionarModuloDoRecursoSolicitado(String padraoUrl, String recursoSolicitado) {
		/*
		 * String sql = "select id,descricao,padraoUrl,loginUrl,interno from Modulo where PATINDEX(padraoUrl + '%','"
					+ recursoSolicitado + "') <> 0 and loginUrl <> '" + recursoSolicitado + "'";
		*/
		final String sql = "select id,loginUrl from Modulo where PATINDEX(padraoUrl ,'" + padraoUrl +
		 "') <> 0 and loginUrl <> '" + recursoSolicitado + "'";
		
		Modulo modulo = (Modulo) getHibernateTemplate(getSessionFactoryWeb()).execute(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				Query query = session.createSQLQuery(sql);
				Object[] objeto = (Object[]) query.uniqueResult();

				Modulo modulo = null;
				if (objeto != null) {
					modulo = new Modulo();
					modulo.setId(new Long(String.valueOf(objeto[0])));
					// (Long) AjudanteConversao
					// .conveter(, TipoConversao.Long));
					modulo.setLoginUrl((String) objeto[1]);
				}
				return modulo;
			}
		});
		return modulo;
	}

	@SuppressWarnings("unchecked")
	public Modulo obterModuloPorDescricaoExata(String descricao) {
		String parametro = "descricao";
		Object objeto = descricao;
		List<Modulo> lstModulo = getHibernateTemplate(getSessionFactoryWeb()).findByNamedQueryAndNamedParam(
				"selecionarModuloPorDescricaoExata", parametro, objeto);
		return lstModulo != null && lstModulo.size() > 0 ? lstModulo.get(0) : null;
	}
}
