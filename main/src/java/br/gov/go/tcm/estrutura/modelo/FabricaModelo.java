package br.gov.go.tcm.estrutura.modelo;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import br.gov.go.tcm.estrutura.modelo.identificador.IdentificadoresModelo;
import br.gov.go.tcm.estrutura.util.diretorio.AjudanteDiretorio;

public class FabricaModelo implements IdentificadoresModelo {

	private boolean identificadoresCarregados = false;

	private Map<String, String> modelos;

	private String diretorioArquivosEntradaModelo;

	private String diretorioRelativoArquivosSaidaModelo;

	public String getDiretorioArquivosEntradaModelo() {
		return diretorioArquivosEntradaModelo;
	}

	public String getDiretorioRelativoArquivosSaidaModelo() {
		return diretorioRelativoArquivosSaidaModelo;
	}

	public boolean isIdentificadoresCarregados() {
		return identificadoresCarregados;
	}

	public Map<String, String> getModelos() {
		if (modelos == null)
			modelos = new HashMap<String, String>();
		return modelos;
	}

	public void setDiretorioArquivosEntradaModelo(String diretorioArquivosEntradaModelo) {
		this.diretorioArquivosEntradaModelo = diretorioArquivosEntradaModelo;
	}

	public void setDiretorioRelativoArquivosSaidaModelo(String diretorioRelativoArquivosSaidaModelo) {
		this.diretorioRelativoArquivosSaidaModelo = diretorioRelativoArquivosSaidaModelo;
	}

	public void setIdentificadoresCarregados(boolean identificadoresCarregados) {
		this.identificadoresCarregados = identificadoresCarregados;
	}

	public void setModelos(Map<String, String> modelos) {
		this.modelos = modelos;
	}

	public File obterModelo(String identificador) {
		if (!identificadoresCarregados) {

			File diretorio = new File(AjudanteDiretorio.obterCaminhoAbsoluto(diretorioArquivosEntradaModelo));
			if (diretorio.isDirectory()) {
				String[] file = diretorio.list();
				for (int i = 0; i < file.length; ++i) {
					getModelos().put(file[i].replaceFirst(".ftl", ""), file[i]);
				}
			}

			identificadoresCarregados = true;
		}

		return new File(AjudanteDiretorio.obterCaminhoAbsoluto(diretorioArquivosEntradaModelo) + "/"
				+ getModelos().get(identificador));
	}
}
