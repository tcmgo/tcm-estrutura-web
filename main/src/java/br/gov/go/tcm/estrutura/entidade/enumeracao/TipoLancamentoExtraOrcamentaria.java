package br.gov.go.tcm.estrutura.entidade.enumeracao;

public interface TipoLancamentoExtraOrcamentaria extends TextoEnumeracao {

	public static final String NOME_BASE_DA_CLASSE = "TipoLancamentoExtraOrcamentariaEnumeracao";

	public abstract Boolean eDepositoEConsignacao();

	public abstract Boolean eDebitoTesouraria();

	public abstract Boolean eAtivoRealizavel();

	public abstract Boolean eTransferenciaFinanceira();

	public abstract Boolean eDiversos();

	public abstract Boolean eOutros();
}
