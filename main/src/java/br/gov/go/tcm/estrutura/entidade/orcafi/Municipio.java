package br.gov.go.tcm.estrutura.entidade.orcafi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.gov.go.tcm.estrutura.entidade.base.EntidadePadrao;
import br.gov.go.tcm.estrutura.entidade.enumeracao.EnumeracaoTipo;
import br.gov.go.tcm.estrutura.entidade.enumeracao.TipoRegiaoEnumeracao;
import br.gov.go.tcm.estrutura.persistencia.base.Banco;

@Entity
@Table(name = "Municipio")
@Banco(nome = Banco.ORCAFI)
@TypeDefs( { @TypeDef(name = "tipoRegiao", typeClass = EnumeracaoTipo.class, parameters = { @Parameter(name = "enumClassName", value = "br.gov.go.tcm.estrutura.entidade.enumeracao.TipoRegiaoEnumeracao") }) })
@NamedQueries( {
		@NamedQuery(name = "selecionarMunicipiosPorDescricao", 
				query = "SELECT m FROM Municipio m WHERE lower(m.descricao) like lower('%' || :descricao || '%') order by m.descricao asc "),
		@NamedQuery(name = "selecionarMunicipioPorDescricaoExata", query = "SELECT m FROM Municipio m WHERE m.descricao = :descricao"),
		@NamedQuery(name = "selecionarTodosMunicipios", 
				query = "SELECT m FROM Municipio m order by m.descricao asc ")})
public class Municipio extends EntidadePadrao implements Comparable<Municipio> {

	private static final long serialVersionUID = -1995810154989489068L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codMunicipio")
	private Long id;

	@Column(name = "cnpjMunicipio")
	private String cnpj;

	@Column(name = "nomeMunicipio")
	private String descricao;

	@Type(type = "tipoRegiao")
	@Column(name = "regiao")
	// Anteriormente era CodAuditoria - 18/03/2009
	private TipoRegiaoEnumeracao regiao;

	@Override
	public Long getId() {
		return id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public String getDescricao() {
		return descricao;
	}

	public TipoRegiaoEnumeracao getRegiao() {
		return regiao;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setRegiao(TipoRegiaoEnumeracao regiao) {
		this.regiao = regiao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Municipio other = (Municipio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@SuppressWarnings("unused")
	@Override
	public int compareTo(Municipio municipio) {
		
		if (municipio.getDescricao() != null && this.getDescricao() != null) {
			return this.descricao.compareTo(municipio.getDescricao());
		} else {
			
			if (municipio.getDescricao() == null && this.getDescricao() == null) {
				return 0;
			} else {
				return -1;
			}
		}
	}


	
}
