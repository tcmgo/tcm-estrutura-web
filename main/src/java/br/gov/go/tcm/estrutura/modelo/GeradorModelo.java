package br.gov.go.tcm.estrutura.modelo;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import br.gov.go.tcm.estrutura.container.faces.AjudanteContextoFaces;
import br.gov.go.tcm.estrutura.container.spring.ContextoSpring;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.GeradorRecursoVisaoExcecao;
import br.gov.go.tcm.estrutura.relatorio.fonteDeDados.ParametroVisao;
import br.gov.go.tcm.estrutura.util.diretorio.AjudanteDiretorio;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.html.SAXmyHtmlHandler;
import com.lowagie.text.pdf.PdfWriter;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

public class GeradorModelo {
	public final static int SAIDA_HTML = 0;

	public final static int SAIDA_PDF = 1;
	
	public final static int SAIDA_PDF_LANSCAPE = 2;

	public FabricaModelo getFabricaModelo() {
		return ContextoSpring.getFabricaModelo();
	}

	public String gerarRelatorio(String identificador,
			List<ParametroVisao> parametros, int tipoSaida)
			throws GeradorRecursoVisaoExcecao {
		try {
			AjudanteDiretorio ajudanteDiretorio = new AjudanteDiretorio();
			String nomeArquivo = new String();

			nomeArquivo = identificador + AjudanteContextoFaces.getIdentificacaoUsuario()
					+ ".html";

			String caminhoArquivo = new String();
			caminhoArquivo = ajudanteDiretorio.obterCaminhoContexto()
					+ getFabricaModelo()
							.getDiretorioRelativoArquivosSaidaModelo()
					+ nomeArquivo;
			
			Configuration cfg = new Configuration();
			cfg.setEncoding(new Locale("BR"),"ISO-8859-1");
			cfg.setDefaultEncoding("ISO-8859-1");
			cfg.setLocale(new Locale("pt","BR"));
			cfg.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
						
			cfg.setDirectoryForTemplateLoading(new File(ajudanteDiretorio
					.obterCaminhoContexto()
					+ getFabricaModelo().getDiretorioArquivosEntradaModelo()));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			Template temp = cfg.getTemplate(getFabricaModelo().obterModelo(
					identificador).getName());

			Map<String, Object> parametrosModelo = new HashMap<String, Object>();

			for (ParametroVisao parametro : parametros) {
				parametrosModelo.put(parametro.getId(), parametro.getValor());
			}

			Writer out = new OutputStreamWriter(new FileOutputStream(
					caminhoArquivo));
			temp.process(parametrosModelo, out);
			out.flush();

			if (tipoSaida == GeradorModelo.SAIDA_PDF) {
				Document document = new Document(PageSize.A4);
				nomeArquivo = identificador +  AjudanteContextoFaces.getIdentificacaoUsuario()
						+ ".pdf";

				File arquivoCopia = new File(caminhoArquivo);

				caminhoArquivo = ajudanteDiretorio.obterCaminhoContexto()
						+ getFabricaModelo()
								.getDiretorioRelativoArquivosSaidaModelo()
						+ nomeArquivo;

				PdfWriter.getInstance(document, new FileOutputStream(
						caminhoArquivo));
				SAXParser parser = SAXParserFactory.newInstance()
						.newSAXParser();
//				parser.parse(arquivoCopia.getAbsolutePath(),
//						new SAXmyHtmlHandler(document));
				
				parser.parse(arquivoCopia.toURI().toString(),
						new SAXmyHtmlHandler(document));
				
				document.close();
			}
			if (tipoSaida == GeradorModelo.SAIDA_PDF_LANSCAPE) {
				Document document = new Document(PageSize.A4.rotate());
				
				nomeArquivo = identificador + AjudanteContextoFaces.getIdentificacaoUsuario()
						+ ".pdf";
							
				File arquivoCopia = new File(caminhoArquivo);

				caminhoArquivo = ajudanteDiretorio.obterCaminhoContexto()
						+ getFabricaModelo()
								.getDiretorioRelativoArquivosSaidaModelo()
						+ nomeArquivo;

				PdfWriter.getInstance(document, new FileOutputStream(
						caminhoArquivo));
				SAXParser parser = SAXParserFactory.newInstance()
						.newSAXParser();
				parser.parse(arquivoCopia.getAbsolutePath(),
						new SAXmyHtmlHandler(document));
				
				document.close();
			}
			
			out.close();

			return getFabricaModelo().getDiretorioRelativoArquivosSaidaModelo()
					+ nomeArquivo;
		} catch (Exception e) {
			e.printStackTrace();
			throw new GeradorRecursoVisaoExcecao(e.getMessage());
		}

	}
	
	public String gerarConteudo(String identificador,
			List<ParametroVisao> parametros)
			throws GeradorRecursoVisaoExcecao {
		try {
			AjudanteDiretorio ajudanteDiretorio = new AjudanteDiretorio();

			Configuration cfg = new Configuration();
			cfg.setEncoding(new Locale("BR"),"ISO-8859-1");
			cfg.setDirectoryForTemplateLoading(new File(ajudanteDiretorio
					.obterCaminhoContexto()
					+ getFabricaModelo().getDiretorioArquivosEntradaModelo()));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			Template temp = cfg.getTemplate(getFabricaModelo().obterModelo(
					identificador).getName());

			Map<String, Object> parametrosModelo = new HashMap<String, Object>();

			for (ParametroVisao parametro : parametros) {
				parametrosModelo.put(parametro.getId(), parametro.getValor());
			}

			CharArrayWriter out = new CharArrayWriter();
			temp.process(parametrosModelo, out);
			out.flush();
			
			return new String(out.toCharArray());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new GeradorRecursoVisaoExcecao(e.getMessage());
		}

	}
}
