package br.gov.go.tcm.estrutura.persistencia.controledeacesso.implementacao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import br.gov.go.tcm.estrutura.entidade.controledeacesso.Modulo;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.Perfil;
import br.gov.go.tcm.estrutura.entidade.controledeacesso.UsuarioPerfilModulo;
import br.gov.go.tcm.estrutura.entidade.enumeracao.UsuarioEnumeracao;
import br.gov.go.tcm.estrutura.persistencia.base.implementacao.GenericoDAOImplementacao;
import br.gov.go.tcm.estrutura.persistencia.controledeacesso.contrato.UsuarioPerfilModuloDAO;

@Repository("UsuarioPerfilModuloDAO")
public class UsuarioPerfilModuloDAOImplementacao extends GenericoDAOImplementacao implements UsuarioPerfilModuloDAO {

	private static final long serialVersionUID = 4671377003008054202L;

	@Autowired
	public UsuarioPerfilModuloDAOImplementacao(@Qualifier("sessionFactory") SessionFactory factory,
			@Qualifier("hibernateTemplate") HibernateTemplate hibernateTemplate) {
		setHibernateTemplate(hibernateTemplate);
		setSessionFactory(factory);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<UsuarioPerfilModulo> obterLstUsuarioPerfilModuloPorUsuarioIdETipoUsuario(final Long usuarioId,
			final UsuarioEnumeracao tipoUsuario) {

		return getHibernateTemplate(getSessionFactoryWeb()).executeFind(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				Query query = session.getNamedQuery("obterUsuarioPerfilModuloPorUsuarioIdETipoUsuario");
				query.setParameter("usuarioId", usuarioId);
				query.setParameter("tipoUsuario", tipoUsuario);

				List<UsuarioPerfilModulo> lstUsuarioPerfilModulo = query.list();

				for (UsuarioPerfilModulo usuarioPerfilModulo : lstUsuarioPerfilModulo) {
					Hibernate.initialize(usuarioPerfilModulo.getModulo());
					Hibernate.initialize(usuarioPerfilModulo.getPerfil());
				}

				return lstUsuarioPerfilModulo;
			}
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<UsuarioPerfilModulo> selecionarLstUsuarioPerfilModuloPorModuloUsuarioIdTipoUsuario(final Modulo modulo,
			final Long usuarioId, final UsuarioEnumeracao usuarioEnumeracao) {

		return getHibernateTemplate(getSessionFactoryWeb()).executeFind(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				Query query = session.getNamedQuery("obterUsuarioPerfilModuloPorModuloUsuarioIdTipoUsuario");
				query.setParameter("modulo", modulo);
				query.setParameter("usuarioId", usuarioId);
				query.setParameter("tipoUsuario", usuarioEnumeracao);

				List<UsuarioPerfilModulo> lstUsuarioPerfilModulo = query.list();

				for (UsuarioPerfilModulo usuarioPerfilModulo : lstUsuarioPerfilModulo) {
					Hibernate.initialize(usuarioPerfilModulo.getModulo());
					Hibernate.initialize(usuarioPerfilModulo.getPerfil());
					Hibernate.initialize(usuarioPerfilModulo.getPerfil().getLstRegras());
				}

				return lstUsuarioPerfilModulo;
			}
		});
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<UsuarioPerfilModulo> selecionarLstUsuarioPerfilModuloPorUsuarioIdTipoUsuario(
			final Long usuarioId, final UsuarioEnumeracao usuarioEnumeracao) {

		return getHibernateTemplate(getSessionFactoryWeb()).executeFind(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {

				Query query = session.getNamedQuery("obterUsuarioPerfilModuloPorUsuarioIdTipoUsuario");
				query.setParameter("usuarioId", usuarioId);
				query.setParameter("tipoUsuario", usuarioEnumeracao);

				List<UsuarioPerfilModulo> lstUsuarioPerfilModulo = query.list();

				for (UsuarioPerfilModulo usuarioPerfilModulo : lstUsuarioPerfilModulo) {
					Hibernate.initialize(usuarioPerfilModulo.getModulo());
					Hibernate.initialize(usuarioPerfilModulo.getPerfil());
					Hibernate.initialize(usuarioPerfilModulo.getPerfil().getLstRegras());
				}

				return lstUsuarioPerfilModulo;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public List<UsuarioPerfilModulo> obterLstUsuarioPerfilModuloPorPerfilTipoUsuario(Perfil perfil,
			UsuarioEnumeracao tipoUsuario) {
		String[] parametro = { "perfil", "tipoUsuario" };
		Object[] objeto = { perfil, tipoUsuario };

		return getHibernateTemplate(getSessionFactoryWeb()).findByNamedQueryAndNamedParam("obterUsuarioPerfilModuloPorPerfilTipoUsuario",
				parametro, objeto);
	}
	
	@SuppressWarnings("unchecked")
	public UsuarioPerfilModulo obterUsuarioPerfilModuloPorId(Long id) {
		String[] parametro = { "id" };
		Object[] objeto = { id };

		List<UsuarioPerfilModulo> lstUsuarioPerfilModulo = getHibernateTemplate(getSessionFactoryWeb()).findByNamedQueryAndNamedParam(
				"obterUsuarioPerfilModuloPorId", parametro, objeto);
		return lstUsuarioPerfilModulo != null && lstUsuarioPerfilModulo.size() > 0 ? lstUsuarioPerfilModulo.get(0)
				: null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public UsuarioPerfilModulo selecionarUsuarioNoModulo(final Long idUsuario, final Modulo modulo,
			final UsuarioEnumeracao usuarioEnumeracao) {

		return (UsuarioPerfilModulo) getHibernateTemplate(getSessionFactoryWeb()).execute(new HibernateCallback() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = session.createCriteria(UsuarioPerfilModulo.class).add(
						Restrictions.eq("usuarioId", idUsuario)).add(Restrictions.eq("tipoUsuario", usuarioEnumeracao))
						.add(Restrictions.eq("modulo", modulo));

				return criteria.uniqueResult();
			}
		});

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<UsuarioPerfilModulo> obterUsuarioPapelModuloPorUsuarioIdETipoUsuario(final Long usuarioId,
			final UsuarioEnumeracao tipoUsuario) {
		return (List<UsuarioPerfilModulo>) getHibernateTemplate(getSessionFactoryWeb()).execute(new HibernateCallback() {
			public Object doInHibernate(Session sessao) throws HibernateException, SQLException {
				Criteria c = sessao.createCriteria(UsuarioPerfilModulo.class);
				c.add(Restrictions.eq("usuarioId", usuarioId));
				c.add(Restrictions.eq("tipoUsuario", tipoUsuario));

				List<?> l = c.list();

				return l;
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> obterLstUsuarioIdPorPerfilTipoUsuario(Perfil perfil,
			UsuarioEnumeracao tipoUsuario) {
		String[] parametro = { "perfil", "tipoUsuario" };
		Object[] objeto = { perfil, tipoUsuario };

		return getHibernateTemplate(getSessionFactoryWeb()).findByNamedQueryAndNamedParam("obterUsuarioIdPorPerfilTipoUsuario",
				parametro, objeto);
	}

	@Override
	public void commitTransacao() {
		getHibernateTemplate(getSessionFactoryWeb()).flush();
	}
}