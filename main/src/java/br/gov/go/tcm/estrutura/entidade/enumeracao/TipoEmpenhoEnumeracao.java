package br.gov.go.tcm.estrutura.entidade.enumeracao;


public enum TipoEmpenhoEnumeracao implements NumberEnumeracaoPersistente{
	
	EMPENHO_VALIDO(0),ANULACAO_EMPENHO_VALIDO(1);
	
	int valor;
	
	TipoEmpenhoEnumeracao(int valor){
		this.valor = valor;
	}
	
	public Number getValorOrdinal(){
		return this.valor;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 0:
			return TipoEmpenhoEnumeracao.EMPENHO_VALIDO;
		case 1:
			return TipoEmpenhoEnumeracao.ANULACAO_EMPENHO_VALIDO;
		
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 0:
			return "Empenho V�lido";
		case 1:
			return "Anula��o de Empenho V�lido";
		default:
			return "";
		}

	}
	
}