package br.gov.go.tcm.estrutura.util.excecao;

public class ArquivoNaoEncontradoExcecao extends Exception{

	private static final long serialVersionUID = 2666004593939897672L;

	public ArquivoNaoEncontradoExcecao(String message) {
		super(message);
	}

}
