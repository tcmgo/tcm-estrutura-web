package br.gov.go.tcm.estrutura.entidade.enumeracao;

public enum TipoTransacionadorEnumeracao implements NumberEnumeracaoPersistente {
	
	PREFEITURA(1), CAMARA(2), AUTARQUIA(3), TRIBUNAL_CONTAS(4), CONSORCIO(5), PROCESSO_ADMINISTRATIVO(9);
	int valor;

	TipoTransacionadorEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}
	
	public EnumeracaoPersistente valor() {
		switch (valor) {
		case 1:
			return TipoTransacionadorEnumeracao.PREFEITURA;
		case 2:
			return TipoTransacionadorEnumeracao.CAMARA;	
		case 3:
			return TipoTransacionadorEnumeracao.AUTARQUIA;
		case 4:
			return TipoTransacionadorEnumeracao.TRIBUNAL_CONTAS;
		case 5:
			return TipoTransacionadorEnumeracao.CONSORCIO;
		case 9:
			return TipoTransacionadorEnumeracao.PROCESSO_ADMINISTRATIVO;
		}
		return null;
	}

	@Override
	public String toString() {
		switch (valor) {
		case 1:
			return "Prefeitura";
		case 2:
			return "C�mara";
		case 3:
			return "Autarquia";
		case 4:
			return "Tribunal de Contas";
		case 5:
			return "Cons�rcio";
		case 9:
			return "Proceso Administrativo";
		default:
			return "";
		}
	}
}

