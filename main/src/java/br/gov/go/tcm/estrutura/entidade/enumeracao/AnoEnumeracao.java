package br.gov.go.tcm.estrutura.entidade.enumeracao;

import java.util.Calendar;
import java.util.Date;

import br.gov.go.tcm.estrutura.entidade.ajudante.AjudanteEnumeracao;

public enum AnoEnumeracao implements NumberEnumeracaoPersistente {
	
	
	A2030(2030), A2029(2029), A2028(2028), A2027(2027),A2026(2026), A2025(2025), 
	A2024(2024), A2023(2023), A2022(2022), A2021(2021),A2020(2020), A2019(2019), 
	A2018(2018), A2017(2017), A2016(2016), A2015(2015),A2014(2014), A2013(2013), 
	A2012(2012), A2011(2011), A2010(2010), A2009(2009),A2008(2008), A2007(2007), 
	A2006(2006), A2005(2005), A2004(2004), A2003(2003),A2002(2002), A2001(2001), 
	A2000(2000), A1999(1999), A1998(1998), A1997(1997),A1996(1996), A1995(1995), 
	A1994(1994), A1993(1993), A1992(1992), A1991(1991),A1990(1990), A1989(1989), 
	A1988(1988), A1987(1987), A1986(1986), A1985(1985),A1984(1984), A1983(1983), 
	A1982(1982), A1981(1981), A1980(1980), A1979(1979),A1978(1978), NENHUM(0);

	private final int valor;

	AnoEnumeracao(int valor) {
		this.valor = valor;
	}

	public Number getValorOrdinal() {
		return this.valor;
	}

	@Override
	public String toString() {

		if (valor == 0)
			return null;

		Integer v1 = valor;
		return v1.toString();
	}

	public static AnoEnumeracao obterAnoAnterior(AnoEnumeracao anoEnumeracao) {
		int anoAnterior = anoEnumeracao.getValorOrdinal().intValue() - 1;
		return (AnoEnumeracao) AjudanteEnumeracao
				.obterEnumeracaoPorValorOrdinal(AnoEnumeracao.class,
						anoAnterior);
	}

	public static AnoEnumeracao obterAnoCorrente() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());

		return (AnoEnumeracao) AjudanteEnumeracao.obterEnumeracaoPorValorOrdinal(AnoEnumeracao.class, calendar
				.get(Calendar.YEAR));
	}

	public static AnoEnumeracao obterAnoPosterior(AnoEnumeracao anoEnumeracao) {
		int anoPosterior = anoEnumeracao.getValorOrdinal().intValue() + 1;
		return (AnoEnumeracao) AjudanteEnumeracao
				.obterEnumeracaoPorValorOrdinal(AnoEnumeracao.class,
						anoPosterior);
		
	}
	
	public boolean isAnoCorrente(){
		Calendar calendar = Calendar.getInstance();
		return valor == calendar.get( Calendar.YEAR );
	}
}
